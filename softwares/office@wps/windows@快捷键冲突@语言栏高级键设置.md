[toc]



## 文本服务和输入语言对话框

部分输入法可以直接跳转到**文本服务和输入语言**对话框,比如微信输入法

| win11                                                        | win10                                                        | win10 (old) ,win8 and before                                 |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/fe9952a0328347439a1105278a70799e.png) | ![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/65066f835be448debee6b07b7a1a7db1.png) | ![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/47f850c46ca24901b66d4b44c2fe3e87.png)<br />[使用语言栏在语言之间进行切换 - Microsoft 支持](https://support.microsoft.com/zh-cn/office/使用语言栏在语言之间进行切换-1c2242c0-fe15-4bc3-99bc-535de6f4f258#:~:text=Press the Windows logo key and type Control,it’s available check box%2C and then click Options.) |
| 也可以通过命令行执行`Start ms-settings:typing`打开设置页面,再点击高级键盘设置 | 与win11类似                                                  | 这里的文档(第二节)过于老旧,部分系统不适用(控制面板中的功能逐渐被移动到设置里面) |

## Note

- 上述界面中修改完热键后不一定能生效,建议重启后查看效果
