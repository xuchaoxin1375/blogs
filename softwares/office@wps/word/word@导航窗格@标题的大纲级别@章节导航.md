[toc]



# 项目符号@编号@多级列表

- [定义新的项目符号、编号和多级列表 - Microsoft 支持](https://support.microsoft.com/zh-cn/office/定义新的项目符号-编号和多级列表-6c06ef65-27ad-4893-80c9-0b944cb81f5f#bkmk_turnoffautonumber)
- 在Word中，<u>项目符号(Bullets)、编号(Numbering)和多级列表(Multilevel List)</u>是常用的排版工具，用于对文档中的条目、章节、目录等进行编号或符号标记，可以使文档更易于阅读和理解。
- Bullets
  - Create a bulleted list

- Numbering
  - Create a numbered list.(<u>single-level</u>)

- Multilevel List:
  - Create a <u>multi-level</u> list to **organize items** or **create an outline**
  - you can also **change the look** of individual levels in your list or add **numbering** to headings in your document


### 项目符号

项目符号是一种用于标记文档中条目的符号，如圆点、方框、箭头等。

在Word中，可以通过以下步骤添加项目符号：

- 选中需要添加符号的段落或文本
- 在“开始”选项卡的“段落”组中，点击“项目符号”按钮
- 选择需要的符号样式即可

### 编号

编号是一种用于对文档中章节、标题、图表等进行编号的工具。在Word中，可以通过以下步骤添加编号：

- 选中需要添加编号的段落或文本
- 在“开始”选项卡的“段落”组中，点击“编号”按钮
- 选择需要的编号样式即可

Word提供了多种编号样式，如数字、字母、罗马数字等，还可以自定义编号格式。

### 多级列表🎈

顾名思义,多级列表就是每一级的条目有各自的编号顺序
- 例如,论文标题中的第1章,第2章作为一级列表
- 而每个章节内部又可以分为子章节,例如1.1节,1.2节...
- 每个节内部又可以再细分目1.1.1小节,1.1.2小节

多级列表是一种用于对文档中多个级别的条目进行编号或符号标记的工具。在Word中，可以通过以下步骤添加多级列表：

- 选中需要添加列表的段落或文本
- 在“开始”选项卡的“段落”组中，点击“多级列表”按钮
- 选择需要的列表样式
- 在列表中添加或删除条目，可以使用Tab键或Shift+Tab键进行级别切换

Word中的多级列表可以实现不同级别的编号或符号，还可以自定义样式、缩进等。多级列表通常用于制作目录、大纲等结构化文档。

#### 多级列表的定义

- 使用多级列表的第一步就是完整定义一套规则,通常3~4层就够用了
- 如果您中途想要加层或者修改,那么建议对所有之前定义过的层级检测一下看是否被重置

####  多级列表(multilevel list)绑定标题样式(heading style)🎈

![在这里插入图片描述](https://img-blog.csdnimg.cn/767f596860404b37916545fe875cec5a.png)

#### 重新自定义多级列表

- 本节讨论在定义多级列表的时候,不小心将域给删除了怎么办
  - 在文档中的一个空白地方单击鼠标
  - 回到功能区,在多级列表窗口中选中一个比较近接近预期的word自带的样式
  - 然后开始重新自定义一个多级列表(注意`将级别链接到样式`各个级别的编号要设置好)
  - 如果只是想要对现有的自定义列表稍加改进,那么直接点击自定义多级列表,可以继续上一次的定制(或者用鼠标点中已有的代编号标题,然后再自定义多级列表)

### 编号相关缩进设置

- 单级列表（编号）和多级列表的设置方法类似，通常多级列表可以设置的属性更多，如果文档内容中正文部分有较多的带序号列表，建议使用多级列表

- [Change bullet indents - Microsoft Support](https://support.microsoft.com/en-us/office/change-bullet-indents-5ed8b9a0-d44c-4e9a-81b3-47c234e980d3)

- [更改项目符号缩进 - Microsoft 支持](https://support.microsoft.com/zh-cn/office/更改项目符号缩进-5ed8b9a0-d44c-4e9a-81b3-47c234e980d3)，包括以下内容：

  - 编号和内容间的间隙
  - 编号在文档中的缩进

- 例

  - | 对齐方式1（默认)                                             | 对齐方式2(指定为空格)                                        |      |
    | ------------------------------------------------------------ | ------------------------------------------------------------ | ---- |
    | ![在这里插入图片描述](https://img-blog.csdnimg.cn/d69d7eceec0342c29c08e3cb3344e091.png) | ![在这里插入图片描述](https://img-blog.csdnimg.cn/3ed07782f41c4338902aa6faabd7af4a.png) |      |

- | 简单单级列表编号的缩进设置                                   | 多级列表上中某个级别的缩进调整                               |      |
  | ------------------------------------------------------------ | ------------------------------------------------------------ | ---- |
  | ![在这里插入图片描述](https://img-blog.csdnimg.cn/0f18d892fd604204a0723176bd909239.png) | ![在这里插入图片描述](https://img-blog.csdnimg.cn/0fdb61cd185840a8a96c246911e95733.png) |      |
  |                                                              | 和单级列表（编号）类似的设置路径，<br />还可以设置每个小结重新编号 |      |

  

## 补充:论文的章节结构

一篇论文的章节结构通常分为三个级别：章、节、小节。这些级别的命名方式通常是由编辑或出版社决定的，可能会因不同的出版物或杂志而异。

以下是各级别的可能称呼方式：

- 章（chapter）：该术语用于将文章按内容组织分为独立的一篇文章。例如，在某本期刊上发表的一篇文章通常被分为一到多个章。章的命名可能采用下列方式： A 章、B 章等。
- 节（section）：该术语用于将文章按内容组织分为一个单独的部分，每个节通常涉及到某个主题或领域。例如，某篇论文可能会分为多个节，每个节都是针对一个特定的主题或问题。节的命名可能采用下列方式： I 节、II 节等。
- 小节（subsection）：该术语用于将文章按内容组织分为较小的单元，通常是为了进一步说明某个特定的概念或问题。例如，在某篇期刊上发表的一篇文章可能会被分为多个小节，每个小节都是针对某个特定的主题或问题。小节的命名可能采用下列方式： Section A、Section B等。

需要注意的是，这些术语只是大致分类，并不是唯一和固定的称呼方式，也不代表每本杂志或编辑的规定。同时，命名也可能根据实际情况和编辑规定进行调整。

### 论文写作中编号和多级列表的常见用途

在编写论文时，项目符号、编号和多级列表通常用于以下用途：

#### 章节标题和子标题

- 在论文中，通常需要使用标题和子标题来组织内容，以便读者更好地理解文章结构和主要内容。
- 可以使用编号或多级列表对章节标题进行编号，以便读者更好地理解论文的章节结构和层次关系。

#### 列表和条目

- 在论文中，可以使用项目符号和编号来标记列表和条目，以便读者更好地理解文章中的概念或步骤。
- 例如，可以使用项目符号标记一组相关的事实或观点，或使用编号标记一个多步骤的过程。

#### 参考文献	

- 在论文中，参考文献通常需要按特定的格式进行编号或标记。
- 可以使用编号对参考文献进行编号，以便读者更好地理解参考文献的序号和引用位置。

#### 图表和表格

- 在论文中，图表和表格通常需要按特定的格式进行编号和标记。可以使用编号或多级列表对图表和表格进行编号，以便读者更好地理解它们在文章中的位置和关系。

## 多级列表常见问题@FAQ

### 多级列表编号错乱问题🎈

- 正如上一节所讲的,如果您最初定义多级列表指定义到了第3级,此时想要再加一级,可能会导致之前的设置丢失,您可能需要重新检查并设置1~3级的定义,顺利的话那么编号错乱就会得到修复

### 多级列表标题中的不当缩进

- 有时设置好多级列表对各级标题的关联后,可能会遇到标题不顶格的情况(即便我们在多级列表和标题样式中没有设置缩进)
- 在这种情况下,可以选中某个异常缩进的标题,然后鼠标选择菜单栏`开始`->`选择`->`选择对象:选择格式相似的文本`
- 这样就可以选中所有同级别的标题
- 然后点击多级列表中的自定义多级列表,重新将缩进设置为0的按钮再按一次,检测效果

### 显示被隐藏的样式以及排序

1. 管理样式->选中需要配置的样式->设置显示/隐藏/使用前隐藏

2. | ![在这里插入图片描述](https://img-blog.csdnimg.cn/32bb9e5ad0ab4eba9c6c4117cd29d7b9.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAeHVjaGFveGluMTM3NQ==,size_11,color_FFFFFF,t_70,g_se,x_16) | ![在这里插入图片描述](https://img-blog.csdnimg.cn/4815f2ccdd6248f494d167ea7a80419f.png) |
   | ------------------------------------------------------------ | ------------------------------------------------------------ |
   | 配置                                                         | 效果                                                         |

### 无编号标题的设置🎈

- 这是关于带有大纲级别的样式设置说明

- ![在这里插入图片描述](https://img-blog.csdnimg.cn/99a902b5fd43408296ab36c4f34887b1.png)

- 通常我们在论文中会使用多级列表关联对应级别的标题(样式)
- 多级列表可以这些被关联的样式自动进行编号,例如`第一章`,`第二章`
- 而我们只需要专注于标题的名字而不需要自己数当前当章节是第几章
- 但是某些标题我们不希望他带上编号,例如摘要,或者目录,我们可能只想要他们加入到`导航窗格`中的标题列表中放便我们跳转
- 这种情况下,不要再用已有的标题样式,而应该新建样式,这个样式可以和某个级别的标题的样式一样,但是不作为标题带编号的标题样式
- 可以通过设置新样式的`段落`->`常规`->`大纲级别`进行调整
- 总之,某个标题样式和其再大纲中是如何显示的是相对独立的

## 目录生成

- [Insert a table of contents - Microsoft Support](https://support.microsoft.com/en-us/office/insert-a-table-of-contents-882e8564-0edb-435e-84b5-1d8552ccf0c0)
- [插入(引用)目录 - Microsoft 支持](https://support.microsoft.com/zh-cn/office/插入目录-882e8564-0edb-435e-84b5-1d8552ccf0c0)
- [插入图表目录 - Microsoft 支持](https://support.microsoft.com/zh-cn/office/插入图表目录-c5ea59c5-487c-4fb2-bd48-e34dd57f0ec1)
- 特别要注意`正文目录`插入后会产生蓝体字的`目录`二字
- 推荐的做法是使用(创建)一个一级大纲标题样式(不带编号),将生成的目录表上的`目录`二字刷称大标题

### TOC样式

- ![在这里插入图片描述](https://img-blog.csdnimg.cn/0be97fc050d243d5a8ca186d2787b9be.png)



### 多级列表@使用文档🎈

- [Number your headings - Microsoft Support](https://support.microsoft.com/en-us/office/number-your-headings-ce24e028-4cb4-4d4a-bf25-fb2c61fc6585)
- 自定义多级列表
  - [Define new bullets, numbers, and multilevel lists - Microsoft Support](https://support.microsoft.com/en-us/office/define-new-bullets-numbers-and-multilevel-lists-6c06ef65-27ad-4893-80c9-0b944cb81f5f?ns=winword&version=19&syslcid=1033&uilcid=1033&appver=zwd190&helpid=506&ui=en-us&rs=en-us&ad=us#bkmk_turnoffautonumber)

### 参考word自带的列表编号

- | ![](https://img-blog.csdnimg.cn/fa5ad0aaa3c04e17be2f214319386357.png) | 中文版 |
  | ------------------------------------------------------------ | ------ |
  | ![](https://img-blog.csdnimg.cn/4f81a1f6d9304ef1871523de92a70033.png) | 英文版 |

- note:

  - 使用英文界面和中文创作语言的同学需要注意,`heading` 和`标题`两个可能是不一样的,所以要看清楚预览窗口和`link level to style`,英文版的一`heading`为准

- 要点如下:

  - 单击要修改的级别这里,设置指定级别标题的编号时,关注第一纵栏中的$1\sim{9}$的级别

    - $$
      \begin{pmatrix}
      1\\2\\3\\4\\\vdots\\9
      \end{pmatrix}
      $$

      

  - 第二纵览中的内容主要提供预览,再不太复杂的文档中,设置三个级别的编号基本足够

    - 对于论文这种的,一般设置3到5个级别

  - 第三个纵栏中,主要关注`将级别引用到样式`,通常我们可以从word提供的这个自带模板来稍加修改编号样式就可以用了

### 编号格式的设置

- 关注编号格式中的内容,在没有动手修改之前,里面有些区域带有灰色带,这里的内容是可以自动编号的
- 假设你正在设置第k级别标题的编号,该编号包括$n_k$个字段,那么就保留相应个数的灰色带,其余删除
- 然后修改灰色带中的内容,通常,里面的内容是有序符号,比如1,2,3或中文一,二,三
  - 修改方式是,先单击某个灰色带,然后从下一行中的`此级别样式`中选取需要的编号样式

![在这里插入图片描述](https://img-blog.csdnimg.cn/20e3cf65c24b478b9e7daf48dcd5c855.png)



### 编号之后的空白符设置@文本缩进

- 编号之后的空格可以设置为`空格`,`制表符`,等,这还会影响到目录生成

- 文本缩进,根据需要设置,可以点击`设置所有级别`,将各级编号的缩进同时设置为同一个值,通常设置为0即可



### 例

- 假设我需要前三个级别标题的编号,那么从4级别标题以及更深的级别就不用去修改
- 现在假设我的需求如下:
  - 一级标题用中文`一,二,三,...`编号
  - 二级标题用带括号的中文`(一),(二),(三),...`
  - 三级标题用`1,2,3,...`编号
  - 同时,对于同级别的标题,它们管辖的子标题的编号相对独立编号
- 用多级列表绑定标题样式,可以容易做到



# 导航窗格

## 借助章节标题导航

- To go to a page or a heading in a Word document without scrolling, use the Navigation pane.
  - 和pdf中的书签一样,都能够使用户不需要执行鼠标滚动操作就可以跳转到文档的指定章节位置
- If you’ve [applied heading styles](https://support.microsoft.com/en-us/office/add-a-heading-3eb8b917-56dc-4a17-891a-a026b2c790f2) to the headings in the body of your document, those headings appear in the Navigation pane. 
  - 对于运用了`标题样式`的文档内容,会被显示在导航窗格中的对应级别中
- The Navigation pane doesn’t display headings that are in tables, text boxes, or headers or footers.
  - 导航窗格不会显示被表格/文本框/页眉/页脚中的内容
  - 因此,如果您希望在这些被特别控制的文本运用标题来作导航标记将是徒劳,顶多起到样式化作用而不能提供跳转锚点的作用
- If you scroll through your document, Word highlights the heading in the Navigation pane to show you where you are. 
  - 高亮当前正在阅读的章节
- To go back to the top, select **Jump to the beginning**.
  - 如果的文档内容很多,导航窗格会在合适的时候提供一个向上箭头,方面用户回到文档正文顶部
  - 这个设计不如网站(css)中的sticky效果(按钮,它不会随着文档的滚动而滚动,而是粘滞在一个地方),但是word提供的`回到顶部`没有这么优化

## 借助页面缩略图来导航文档位置

- 有时候,您可能希望看到文档中的内容轮廓而不是通过章节标题来跳转文章,可以考虑使用(Navigation窗格提供的pages来做到这一点)
- 不过,我更推荐的是缩小主文档来达到相似的效果,但是更具高效,而且可以更灵活控制页面缩略图的大小

  - ```mermaid
    flowchart LR
    	view ---> zoom --->multiplePage["multiple page"]
    ```

    

  - ![在这里插入图片描述](https://img-blog.csdnimg.cn/32004615b8b040288edbd23a875de836.png)


### 检查文档中大片空白的页面

- 借助上述介绍的缩略图，可以快速查找文档中存在的大片空白的页面，如果可能的话，进行空白缩减。

### 图标的交叉引用

- 可以通过设置总是显示`域阴影`（shade),来检查那些图表没有正文被引用（提及）



##  office word

- [Use the Navigation pane in Word - Microsoft Support](https://support.microsoft.com/en-us/office/use-the-navigation-pane-in-word-394787be-bca7-459b-894e-3f8511515e55)

- [将 Word 大纲导入到 PowerPoint - Microsoft 支持](https://support.microsoft.com/zh-cn/office/将-word-大纲导入到-powerpoint-a25f6e01-9a19-4c0d-a108-7f533e42dfe9)

### 导航窗格@章节跳转和导航

- [Use the Navigation pane in Word - Microsoft Support](https://support.microsoft.com/en-us/office/use-the-navigation-pane-in-word-394787be-bca7-459b-894e-3f8511515e55)

- 视图选项卡(View)->显示(show)->导航窗格(navigation Pane)



###  建立大纲标题的方法

- 鼠标光标点在某个想要添加到导航的标题所在行，按住ctrl+alt+n(n为标题级别）

- ![在这里插入图片描述](https://img-blog.csdnimg.cn/20210414094445694.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3h1Y2hhb3hpbjEzNzU=,size_16,color_FFFFFF,t_70)

###  大纲创建失败

- 成功添加到导航中的列表视图，需要**选中一整行**才有效，如果只选择**半行**标题（或者说从中间截取一些文字作为标题），不会奏效：

- 比如失败的案例：

  - 只选择了标题的部分文字：

  - ![在这里插入图片描述](https://img-blog.csdnimg.cn/20210408184154350.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3h1Y2hhb3hpbjEzNzU=,size_16,color_FFFFFF,t_70)

## wps word

- [Word文字技巧：如何使用章节导航功能？-WPS+博客](https://plus.wps.cn/news/p99272.html)

### 章节和书签

- 章节->章节导航

- ![在这里插入图片描述](https://img-blog.csdnimg.cn/7fb71a54fa024efe92b78bbd735a4333.png)

# 其他内容

### 样式重置

- [reset Word Style Heading 2 back to default Style - Microsoft Community](https://answers.microsoft.com/en-us/msoffice/forum/all/reset-word-style-heading-2-back-to-default-style/47988cd8-7087-407d-9527-512c6c855e33)



