[toc]



- word@tips官方文档和教程@软件界面介绍@功能区自定义@拼写检查@AI润色改进@ 图片顶部上方插入文字

#  word 文档和教程

- 官方教程:[Word help & learning (microsoft.com)](https://support.microsoft.com/en-us/word)
- 搜索引擎检索官方文档:
  - `microsoft office word support+<查询内容关键字>`
  - 例如:microsoft office word support language change

## word软件界面元素

- Microsoft Word是一款功能强大的文字处理软件，它的界面包含许多元素，下面是一些常见的元素：
- [在 Office 中自定义功能区|界面元素介绍 - Microsoft 支持](https://support.microsoft.com/zh-cn/office/在-office-中自定义功能区-00f24ca7-6021-48d3-9514-a31a460ecb31)

- ![在这里插入图片描述](https://img-blog.csdnimg.cn/3144a60c469b45cd8c7625fdbf4f96f2.png)

1. 标题栏：位于Word窗口的顶部，显示文档的名称和Word的版本号。

2. 主功能区(主工具栏)(Toolbar）：位于标题栏下方，包含一系列常用的工具和命令按钮，如字体、段落、插入、页面布局等。如上图中黄色和红色框出的部分

   1. 选项卡(tab):功能区选项卡(有些地方成为**标签**)是指应用程序窗口上方的一组选项卡，包括“文件”、“主页”、“插入”、“布局”、“公式”、“数据”、“审阅”和“视图”等。用户可以单击这些选项卡来访问不同的功能和工具。

      - 标签页：位于工具栏下方，包含多个选项卡，如“开始”、“插入”、“页面布局”等，每个选项卡都包含一组相关的命令和工具。
      - 如上图中的黄色区域所示

   2. 功能区菜单（Ribbon）:

      - [在 Office 中显示或隐藏功能区 - Microsoft 支持](https://support.microsoft.com/zh-cn/office/在-office-中显示或隐藏功能区-d946b26e-0c8c-402d-a0f7-c6efa296b527)

      - "Ribbon" 在中文中通常被称为“功能区”，也有一些人称之为“功能区菜单”。

        - 不过，由于“Ribbon”是一个较为通用的术语，因此在某些场合下，也可以直接使用“Ribbon”这个词来表示。

      - 在Microsoft Word中，Ribbon指的是一个包含一系列**选项卡和工具栏**的用户界面元素，用于访问和使用各种功能和工具。更具体的是指可以被折叠的部分

      - Ribbon于Microsoft Office 2007中首次引入，取代了早期版本中的传统菜单和工具栏。

        在Microsoft Word中，Ribbon通常出现在应用程序窗口的顶部，可以通过单击选项卡来访问不同的功能和工具。例如，“开始”选项卡包括文本格式化、段落格式化、样式、插入表格和图片等工具；“插入”选项卡包括插入表格、插入图片、插入链接等工具，以此类推。

        Ribbon中的工具栏通常由按钮、下拉列表、对话框框等元素组成，可以通过单击这些元素来执行特定的操作或访问更多选项。

        Ribbon的优点是它可以更快速地访问各种功能和工具，使用户可以更高效地完成任务。此外，Ribbon的设计使得工具和选项更易于发现和理解，从而提高了工作效率。

   3. 组(Group):

      - 每个tab下对应若干功能组(这些组是功能选项卡下功能的划分,例如`开始`(home)选项卡下的`剪切板(Clipboard)`组和`字体(Font)`组分别是关于剪切板的和字体调整的功能)

3. 快速访问工具栏（Quick Access Toolbar)：是Microsoft Word软件界面上的一个常用元素，包含一组常用的命令和工具按钮，如保存、撤销、重做等。

   - 用户可以自定义快速访问工具栏，将常用的命令和工具添加到其中，以便快速访问。该工具栏用于显示最常用的功能按钮，可以放置在**标题栏**的同一行上显示，也可以放置在功能区下方.
   - [自定义快速访问工具栏 - Microsoft 支持](https://support.microsoft.com/zh-cn/office/自定义快速访问工具栏-43fff1c9-ebc4-4963-bdbd-c2b6b0739e52)

4. 弹出的工具栏(pop up toolbar):

   - 即，用户鼠标划选一段文字后，弹出的工具栏

5. 状态栏(Stauts Bar)：

   1. | 截图                                                         | 说明                                                         |
      | ------------------------------------------------------------ | ------------------------------------------------------------ |
      | ![在这里插入图片描述](https://img-blog.csdnimg.cn/adc3e2250b3941c09af6bc8d8a084847.png) | 位于Word窗口的底部，显示有关文档的信息，如页码、字数、缩放比例等。 |


6. 标尺：位于Word窗口的顶部和左侧，用于测量和调整文本和图像的位置和大小。
7. 文本框：用于在文档中添加文本或图像的矩形区域。
8. 插入符号：用于指示文本输入的位置，通常是一个闪烁的竖线。

## 字符和标记

### 格式标记

- 在Microsoft Word中，格式标记是一种特殊的可见字符，用于显示文档中的格式信息
  - 例如段落标记、换行符、制表符、空格等。
- 这些标记对于编辑和调整文档格式非常有用，可以帮助用户更好地控制文本外观和布局。

下面是一些常见的格式标记：

1. 段落标记：显示为一个向下的箭头，表示段落的结束和新段落的开始。
2. 换行符：显示为一个箭头向左的符号，表示文本的换行，但不会产生新的段落。
3. 制表符：显示为一个箭头向右的符号，用于在文本中创建制表位。
4. 空格：显示为一个小点，用于表示空格的位置。
5. 对齐标记：显示为竖线、箭头或是叉号，用于指示文本的对齐方式，例如左对齐、居中或右对齐。
6. 分页符：显示为一个符号，用于表示页面的分隔符。

- 要显示或隐藏格式标记，可以在Word中点击“开始”选项卡上的“段落标记”按钮，或使用键盘快捷键“Ctrl + shift+8”来切换显示格式标记。
- ![在这里插入图片描述](https://img-blog.csdnimg.cn/39aaeea263404dcdb92b26833efe75f9.png)

### 段落标记(paragraph marks)

- [Turn formatting marks on or off - Microsoft Support](https://support.microsoft.com/en-us/office/turn-formatting-marks-on-or-off-b166e811-d762-4f24-8328-d897cdace459)

- 在Microsoft Word中，段落标记（¶）是一个特殊的符号，用于表示文本中的段落结束。它有助于用户更清晰地看到文档中的段落结构。段落标记通常在文档中是隐藏的，但可以通过以下步骤显示或隐藏：

  1. 打开Word软件。
  2. 在“开始”选项卡的工具栏中，找到“显示/隐藏”按钮，它的图标是一个¶符号。
  3. 单击“显示/隐藏”按钮，可以切换显示或隐藏段落标记和其他格式符号。

  显示段落标记有助于更好地理解文档的格式和布局，尤其是在调整文档格式时。请注意，虽然段落标记在屏幕上可见，但在打印文档时，它们不会出现在打印的页面上。

- 不同版本的word显示的段落标记图表可能不同

# 自定义功能区(Ribbon)

- [在 Office 中自定义功能区 - Microsoft 支持](https://support.microsoft.com/zh-cn/office/在-office-中自定义功能区-00f24ca7-6021-48d3-9514-a31a460ecb31)

- [Customize the ribbon in Word - Microsoft Support](https://support.microsoft.com/en-us/office/customize-the-ribbon-in-word-077e5f5d-06b8-4498-84b6-c87c11cd3434?ns=winword&version=19&syslcid=1033&uilcid=1033&appver=zwd190&helpid=105579&ui=en-us&rs=en-us&ad=us)

  - [Customizing the source list of commands](https://support.microsoft.com/en-us/office/customize-the-ribbon-in-word-077e5f5d-06b8-4498-84b6-c87c11cd3434?ns=winword&version=19&syslcid=1033&uilcid=1033&appver=zwd190&helpid=105579&ui=en-us&rs=en-us&ad=us#ID0EFLBD)
  - [The ribbon list](https://support.microsoft.com/en-us/office/customize-the-ribbon-in-word-077e5f5d-06b8-4498-84b6-c87c11cd3434?ns=winword&version=19&syslcid=1033&uilcid=1033&appver=zwd190&helpid=105579&ui=en-us&rs=en-us&ad=us#ID0EDJBD)
  - [Add or remove commands](https://support.microsoft.com/en-us/office/customize-the-ribbon-in-word-077e5f5d-06b8-4498-84b6-c87c11cd3434?ns=winword&version=19&syslcid=1033&uilcid=1033&appver=zwd190&helpid=105579&ui=en-us&rs=en-us&ad=us#ID0EDHBD)
  - [Reorder commands, groups, and tabs](https://support.microsoft.com/en-us/office/customize-the-ribbon-in-word-077e5f5d-06b8-4498-84b6-c87c11cd3434?ns=winword&version=19&syslcid=1033&uilcid=1033&appver=zwd190&helpid=105579&ui=en-us&rs=en-us&ad=us#ID0EDFBD)
  - [Customizing Ribbon items](https://support.microsoft.com/en-us/office/customize-the-ribbon-in-word-077e5f5d-06b8-4498-84b6-c87c11cd3434?ns=winword&version=19&syslcid=1033&uilcid=1033&appver=zwd190&helpid=105579&ui=en-us&rs=en-us&ad=us#ID0EDDBD)
  - [Finishing up](https://support.microsoft.com/en-us/office/customize-the-ribbon-in-word-077e5f5d-06b8-4498-84b6-c87c11cd3434?ns=winword&version=19&syslcid=1033&uilcid=1033&appver=zwd190&helpid=105579&ui=en-us&rs=en-us&ad=us#ID0EDBBD)

- 点击右上角?查询文档

- | ![在这里插入图片描述](https://img-blog.csdnimg.cn/ba7071aa18dd45179217c9c5b1a489df.png) | 查看在线文档             |                                  |
  | ------------------------------------------------------------ | ------------------------ | -------------------------------- |
  | ![在这里插入图片描述](https://img-blog.csdnimg.cn/384c9589a7414031b1c713a11b496ae3.png) | 在功能区设置自己的选项卡 | 通过右侧的箭头可以调整选项卡顺序 |

  - 设置自己的选项卡时,需要先点击`新建选项卡`
    - 对新建选项卡重命名
    - 然后从左侧选择命令组或者单个命令,点击添加导右侧自定义的选项卡中

## 自定义功能区要点@层次关系

- 功能区->选项卡->组->命令

  - 注意区分**选项卡(tab)**和**组(group)**的自定义

  - 选项卡是功能区中<u>最大的单位</u>,而命令(诸如按钮和下拉框)是最小功能单位

- | 示意图                                                       | 注释                         |
  | ------------------------------------------------------------ | ---------------------------- |
  | ![在这里插入图片描述](https://img-blog.csdnimg.cn/d1bef609d14b40f99607479a5d0893ef.png) | 在已有的选项卡中创建自己的组 |
  | ![在这里插入图片描述](https://img-blog.csdnimg.cn/b7a110de4e3c4df2be998b4c4462b0cc.png) | 效果                         |

  

### 添加自定义选项卡(tab)

单击“**新建**”选项卡，可添加自定义选项卡和自定义组。 只能向自定义组中添加命令。

1. 在“**自定义功能区**”窗口的“**自定义功能区**”列表中，单击“**新建**”选项卡。
2. 若要查看并保存所做的更改，请单击“**确定**”。

### 向已有的选项卡(tab)中添加自定义组(group)

#### 默认选项卡

- 默认选项卡就是word刚安装完成时,功能区上的若干个选项卡(tab)

  - 包括:File,home,insert,draw,design等

  - ![功能区上的选项卡](https://support.content.office.net/zh-cn/media/a4209139-a2b0-4753-a31a-8bd417941a07.png)

#### 自定义选项卡

你可以向自定义选项卡或**默认选项卡**中添加自定义组。

1. 在“**自定义功能区**”窗口中的“**自定义功能区**”列表下，单击要向其中添加组的选项卡。

2. 单击“**新建组**”。

3. 若要重命名“**新建组(自定义)**”组，请右键单击该组，单击“**重命名**”，然后输入新名称。

   **注意:** 还可以添加表示自定义组的图标，方法是单击自定义组，然后单击“**重命名**”。 当“**符号**”对话框打开时，选择用于表示组的图标。

4. 若要隐藏你添加到此自定义组中的命令的标签，请右键单击该组，然后单击“**隐藏命令标签**”。 重复操作可取消隐藏它们。

5. 若要查看并保存所做的更改，请单击“**确定**”。

## 自定义快速访问工具栏

- 和自定义功能取共享一份文档
- 但是他们修改的地方不同
- ![在这里插入图片描述](https://img-blog.csdnimg.cn/d00ff238e31a40f9a8a7661dccfdcb5b.png)



# word最后的空白页

在Microsoft Word中，最后一页的空白页可能是由于多余的：

- 分页符
- 段落标记
- 其他格式（样式）设置导致的。

要删除这个空白页，请尝试以下方法：

1. 显示隐藏字符：首先，点击Word工具栏中的“显示/隐藏”按钮（或按Ctrl+Shift+8），以显示文档中的隐藏字符，如分页符、段落标记等。

2. 删除多余的分页符：在显示隐藏字符后，检查空白页上是否有多余的分页符。
   1. 如果有，请将光标放在分页符前，然后按“baskspace删除”键删除它

   2. 或者光标放在分页后，按下delete键删除它

   3. 补充：
      1. 键盘上的backspace键在中文中通常被称为“退格键”或“向前删除键”。其中，“退格”一词源于打字机时代，表示将光标向左移动一格，而“删除”则表示删除光标前面的字符。

      2. 键盘上的delete键在中文中通常被称为“向后删除键”，它的作用是删除光标后面的字符或文本。在一些场合中，delete键也被称为“Del键”，这是它的英文名称的缩写。

3. 检查段落设置：如果空白页上没有多余的分页符，可能是段落设置导致的。
   - 右键点击**空白页**上的段落标记，选择“段落”设置。

   - 在弹出的对话框中，检查“间距”选项卡中的“段前”和“段后”设置，确保它们的值不会导致空白页。

   - 另外，检查“行距”设置，确保它没有设置得过大。

4. 检查表格设置：如果空白页是由表格导致的，尝试将表格的最后一行移到前一页。
   - 将光标放在表格的最后一行，然后按“回退键”（Backspace）删除表格的最后一个单元格，直到表格移到前一页。

5. 调整页面设置：如果以上方法都无法解决问题，检查页面设置。
   - 点击“布局”选项卡，然后点击“页面设置”。在弹出的对话框中，检查“纸张”和“边距”选项卡，确保设置合适。



## 鼠标/快捷键方式

鼠标选中倒二页的最后一个字符,然后选用下面的方法之一:

###  鼠标拖动法

- 点住此时的鼠标,直接拖动鼠标,使文档滚动到末尾处
- 键入`backspace`

### 快捷键法

- 笔记本(简化键盘)
  - `ctrl+shift+Fn+right`+`Delete/backspace`

- 独立完整键盘:
  - `ctrl+shift+end`+Delete




#  拼写/语法检查

## word对内容错误的检查

- [在 Word 中检查语法、拼写等 - Microsoft 支持](https://support.microsoft.com/zh-cn/office/在-word-中检查语法-拼写等-0f43bf32-ccde-40c5-b16a-c6a282c0d251#ID0EBBF=Office_2016_-_2021)
- [在 Office 中检查拼写和语法 - Microsoft 支持](https://support.microsoft.com/zh-cn/office/在-office-中检查拼写和语法-5cdeced7-d81d-47de-9096-efd0ee909227)
  - [打开或关闭拼写检查 - Microsoft 支持](https://support.microsoft.com/zh-cn/office/打开或关闭拼写检查-e2805461-77d4-4832-b006-061163c8d01a)
- 记得运行拼写检查。（不一定是word自带的相关功能，可以是更加专业和先进的方案）
- 拼写和语法错误可能会严重地影响你想要表达的内容，尤其是当老板、老师或人力资源部门的人看到这些错误时。

## 拼写检查和语法简介（校对功能）

- Microsoft Word拼写检查是一个非常有用的功能，它可以自动检查文档中的拼写错误，并提供纠正建议。以下是一些与拼写检查相关的功能介绍：
  1. 自动拼写检查：Microsoft Word自动检查文档中的拼写错误，并在单词下方用红色波浪线标出错误单词。当用户将鼠标指向错误单词时，会显示一个小弹出窗口，显示纠正建议。
  2. 自定义字典：用户可以向Word中添加自定义单词，将其加入到自定义字典中。这些单词将被视为正确拼写，并不会被自动检查为拼写错误。
  3. 自动更正：Microsoft Word内置了一些自动更正功能，当用户输入常见的拼写错误时，它会自动更正为正确的单词。例如，将“teh”更正为“the”。
  4. 语言设置：用户可以根据需要设置文档的语言，以确保拼写检查器可以正确识别和纠正单词拼写错误。可以通过“文件”选项卡中的“选项”菜单进入“语言”设置。
  5. 拼写和语法检查：除了拼写检查外，Microsoft Word还提供了语法检查功能。它可以检查文档中的语法错误，例如主谓不一致、动词时态错误等。如果发现语法错误，它会在单词下方用绿色波浪线标出，并提供纠正建议。

### 基于AI的纠错建议

- word传统的拼写检查（特别是中文)显得很鸡肋，可以尝试用AI检查，例如：

- [秘塔写作猫 (xiezuocat.com)](https://xiezuocat.com/)
  - 简单易用，但是免费版有限制
  - 提供了word插件：[Find the right app | Microsoft AppSource](https://appsource.microsoft.com/en-us/product/office/wa200001825?tab=overview&exp=kyyw)
- [binary-husky/gpt_academic:](https://github.com/binary-husky/gpt_academic)
  - 需要有一定的代码经验，有一定门槛，目前需要openai 的api
- [My Grammarly - Grammarly](https://app.grammarly.com/)
  - 目前仅支持英文，高级功能要付费
- [在线语法检查器 | Microsoft 编辑器](https://www.microsoft.com/zh-cn/microsoft-365/microsoft-editor/grammar-checker)

### 实操

- 文件->选项

- | ![](https://img-blog.csdnimg.cn/c8ea7e8866da4df3b7868176b95ad028.png) | ![在这里插入图片描述](https://img-blog.csdnimg.cn/05fa3eb431b2450ca38a109f080e6d92.png) |      |
  | ------------------------------------------------------------ | ------------------------------------------------------------ | ---- |
  | 中文版                                                       | 英文版                                                       |      |




- Note：关闭**语法检查**和**拼写检查**是相独立的操作

## 功能使用

- [Check grammar, spelling, and more in Word - Microsoft Support](https://support.microsoft.com/en-us/office/check-grammar-spelling-and-more-in-word-0f43bf32-ccde-40c5-b16a-c6a282c0d251)
- [在 Word 中检查语法、拼写等 - Microsoft 支持](https://support.microsoft.com/zh-cn/office/在-word-中检查语法-拼写等-0f43bf32-ccde-40c5-b16a-c6a282c0d251)
  - 注意office365和office其他版本的文档区别
  - [Check grammar, spelling, and more in Word - Microsoft Support](https://support.microsoft.com/en-us/office/check-grammar-spelling-and-more-in-word-0f43bf32-ccde-40c5-b16a-c6a282c0d251#ID0EBBF=Office_2016_-_2021)



# 图片顶部插入文字

## case1:图片顶部已经有文本

- 这种情况比较好办,只需将光标移动到图片上方文本末尾即可

## case2:图片已经处于页面的顶部

- 有如下做法可以选择

### 将鼠标光标移动到到上一页末尾

- 适用于当前页面不是文档的第一页的情况

### 光标移动

- 方式1：将鼠标移动图片左下角以外的附近位置，然后鼠标左键
  - 顺利的话图片左下角会有一个待输入光标
  - 注意,如果用鼠标点击图片左侧可能会选中图片而不是出现一个可插入提示符(光标)
  - ![](https://img-blog.csdnimg.cn/img_convert/018ee26621044241d60750f660d9f4b7.png)
  - 如果不巧选中了图片，请重新尝试上述方法

- 方式2：鼠标点击图片右下角附近(但不接触图片），再通过键盘上的向左箭头，移动光标
  - 光标定好后直接输入文字，这时文字就会出现在图片顶部
    - 但是注意，这种操作相当于将word中的文字和图片排在同一行，文字和图片之间没有换行符
      - ![在这里插入图片描述](https://img-blog.csdnimg.cn/ed94fba656124ce19e5cb19a5c194207.png)

    - 当您缩小图片之后，就会发现，它们其实同处于一行
      - ![在这里插入图片描述](https://img-blog.csdnimg.cn/db90a6a47d4b48ada0bfd79626dcf844.png)

  - 按下回车，文本和图片就会被划分到不同行
    - ![在这里插入图片描述](https://img-blog.csdnimg.cn/f0c76352ac5f434e850dfafb4f0a7866.png)
    - 这种情况下，即使缩小图片，依然可以确保它们被显示在不同的行上

  - 如果您当前处理的页面上一页还有空白区域（为满）可以存放内容,那么您按下回车后，内容可能会从当前页面被移动到上一页的尾部空间中，而图片若没有更上去则是因为上一页空白区域不足以容纳图片，只能容纳些许文字。
  - 下一节介绍另起一页的方法


### 剪切粘贴或直接拖动图片

- 现在图片后方插入文字,然后使用鼠标将图片拖到文字后面
- 关于挪动后文本在本页“丢失”（被移动到上一页），您依然可以通过上一节介绍的方式进行处理

# 分页符@分节符@另起一页

- [在 Word 中插入分页符 - Microsoft 支持](https://support.microsoft.com/zh-cn/office/在-word-中插入分页符-eb0e569c-8e3f-4db4-9b13-7bfe1cabdbd7)
- [插入分节符 - Microsoft 支持](https://support.microsoft.com/zh-cn/office/插入分节符-eef20fd8-e38c-4ba6-a027-e503bdf8375c)
- 如果无论如何都希望插入的文字作为页面的第一部分内容而**不希望它会跑到上一页的空白空间中**，可以插入**分节符**实现
- 另一种方式是通过在上一页末尾不断追加换行符来换**页**，但这种方式不稳定，而且不优雅
- 下面分别介绍分页符和分节符以及各自的几个类型

## 分页符（page break)

### 基础分页符（page）

- word中，分页符中又细分为多个功能不同的分页符

- 在Microsoft Word中，分页符是一种特殊的字符，用于指示文档中的分页位置。当你在文档中插入分页符后，它会将当前位置之前的所有内容移到新页面的开头。这对于需要在文档中添加新章节或分节的情况非常有用。

  要插入分页符，请按照以下步骤操作：

  1. 将光标放置在您想要分页的位置。
  2. 在“插入”选项卡上，单击“分页符”命令，或按下“Ctrl + Enter”键。
  3. 分页符将被插入文档中，将当前位置之前的所有内容移到新页面的开头。

  你也可以在Word中查看和编辑分页符，以及在需要时删除它们。要显示分页符，请在Word中使用“显示/隐藏”命令（通常是在“主页”选项卡的“段落”部分中）。然后，你可以在文档中看到所有的分页符，并可以对它们进行编辑或删除。



### 分栏符（Column)

- [“分栏”对话框 - Microsoft 支持](https://support.microsoft.com/zh-cn/office/-分栏-对话框-86744130-b87d-4ee0-a930-4163e732b84a)
  - **注意:** 
    - [插入分栏符](https://support.microsoft.com/zh-cn/office/插入分栏符-fa34916a-d6ce-4c99-8646-0461a6030451)以控制文本在分栏之间的排列。 例如，插入分栏符结束一栏中的段落，并在下一栏顶部重起新段落。
    - 如果文档具有多个节，则列布局将仅应用于当前节。
- [插入分栏符 - Microsoft 支持](https://support.microsoft.com/zh-cn/office/插入分栏符-fa34916a-d6ce-4c99-8646-0461a6030451)
- 分栏符应该在`分栏布局`（columns layout)的基础上使用
  - 否则相比于基础的分页符，在效果上什么不同，但是语义上不够纯粹。
- 通常分列布局在期刊/报纸类的文档上用的比较多。而且以2列布局为主。
- 假设通过布局->页面设置组->分列布局后,想要调整每一列的内容划分,可以使用**分栏符**来讲某个列的某个地方之后内容移动到下一列中(如果页面布局是单列,那么会被移动到下一页中)

- ![在这里插入图片描述](https://img-blog.csdnimg.cn/bf57199edda24883ba5f24e0fa30475b.png)

### 自动换行符（text wrapping)

- [How to use Text Wrapping Breaks in Microsoft Word - Office Watch (office-watch.com)](https://office-watch.com/2022/text-wrapping-break-microsoft-word/)

|                                                              |                                                              |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![在这里插入图片描述](https://img-blog.csdnimg.cn/d8f92051a7aa40a4a7d80f0e1873aa7b.png) | 假设您的图片是以文本环绕的方式嵌入的                         |
| ![在这里插入图片描述](https://img-blog.csdnimg.cn/719c1cc17a5143548f5f39f898e851ff.png) | 假设您希望从环绕的中途另起一段，并且新的段要完全位于图片下方，如果采用回车的方式，很不方便 |
| ![在这里插入图片描述](https://img-blog.csdnimg.cn/c7b69c6ef2c94d3881a858a71f27def7.png) | 考虑通过插入自动换行符来实现，更加优雅                       |

## 分节符（section break）

- [使用分节符更改文档的一个节的布局或格式 - Microsoft 支持](https://support.microsoft.com/zh-cn/office/使用分节符更改文档的一个节的布局或格式-4cdfa638-3ea9-434a-8034-bf1e4274c450)
- [Use section breaks to change the layout or formatting in one section of your document - Microsoft Support](https://support.microsoft.com/en-us/office/use-section-breaks-to-change-the-layout-or-formatting-in-one-section-of-your-document-4cdfa638-3ea9-434a-8034-bf1e4274c450)

### 下一页分节符(next page)

- 这类分节符后的内容将被(强制)移动到下一页中

### 连续分节符(Continuous)

- 这类分节符后的内容不会被(强制)移动到下一页中

### 偶数页(奇数页)分页分节符

- “**偶数页**”或“**奇数页**”命令会插入分节符并从下一个偶数页或奇数页开始新的一节。
- 若要始终在奇数页或偶数页上开始文档章节， **请使用"奇** 数页"或"偶数 **页"** 分节符选项。

- 这个分节符后的内容(新节)将在下一个偶数页出现,我们用`2i`表示偶数,`2i+1`表示奇数
  - 如果当且页面为第`x=2i+1`页,则该分节符后的内容会被放置到第`x+1`页
  - 如果当前页面为第`x=2i`页,则分节符后的内容会被放置在第`x+2`页(在word中会跳过对x+1页的显示,但是在使用打印(或预览打印)的时候,您可以发现x+1页是空白页)





