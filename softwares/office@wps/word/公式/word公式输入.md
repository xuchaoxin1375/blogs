

# 菜单栏@快捷键设置

## word 设置(选项)

- [Word options (General) - Microsoft Support](https://support.microsoft.com/en-us/office/word-options-general-7bfe9d54-1821-4fc7-b661-c1caaa2e8c95)
  - 点击文件->选项

# 公式输入

## ref



- [Linear format equations using UnicodeMath and LaTeX in Word - Microsoft Support](https://support.microsoft.com/en-us/office/linear-format-equations-using-unicodemath-and-latex-in-word-2e00618d-b1fd-49d8-8cb4-8d17f25754f8)
  - Linear format equations using UnicodeMath and LaTeX in Word
    - [Write like a pro](https://support.microsoft.com/en-us/office/linear-format-equations-using-unicodemath-and-latex-in-word-2e00618d-b1fd-49d8-8cb4-8d17f25754f8#TopPageHeader)
    - [Type equations in linear format](https://support.microsoft.com/en-us/office/linear-format-equations-using-unicodemath-and-latex-in-word-2e00618d-b1fd-49d8-8cb4-8d17f25754f8#ID0EDN)
    - [Create fractions in linear formats](https://support.microsoft.com/en-us/office/linear-format-equations-using-unicodemath-and-latex-in-word-2e00618d-b1fd-49d8-8cb4-8d17f25754f8#ID0EDL)🎈
    - [UnicodeMath editing examples](https://support.microsoft.com/en-us/office/linear-format-equations-using-unicodemath-and-latex-in-word-2e00618d-b1fd-49d8-8cb4-8d17f25754f8#ID0EDJ)
    - [LaTeX equation editing examples](https://support.microsoft.com/en-us/office/linear-format-equations-using-unicodemath-and-latex-in-word-2e00618d-b1fd-49d8-8cb4-8d17f25754f8#ID0EDH)🎈
    - [Automatically convert expressions to professional format](https://support.microsoft.com/en-us/office/linear-format-equations-using-unicodemath-and-latex-in-word-2e00618d-b1fd-49d8-8cb4-8d17f25754f8#ID0EDF)
    - [Use Math AutoCorrect rules outside of an equation](https://support.microsoft.com/en-us/office/linear-format-equations-using-unicodemath-and-latex-in-word-2e00618d-b1fd-49d8-8cb4-8d17f25754f8#ID0EDD)

### office-word公式快捷键

- 使用快捷键可以快速切换当前编辑的公式
  - Enter your equation using `Alt + =` on the keyboard.
  - Choose Convert and select professional to build your typed fractions to their Professional form into subscripts, or use `Ctrl + =`. 
  - You can similarly <u>convert an equation back down（to latex（sorce code) ）code</u> to a linear format with `Ctrl + Shift + =`. 

## 输入公式

- 在键盘上输入默认公式模式热键`alt+=`也就是按住`alt`不放,在按下等号`=`所在的键
  - 注意不要按成`ctrl+=`
- 也可以从菜单->插入->公式
  - Note:公式按钮不总是可用的(假设可用)
    - 点击公式会弹出一堆公式模板
    - 下面的新建一个公式,如果模板不满足需求,可以直接新建空白公式开始编辑
    - ![在这里插入图片描述](https://img-blog.csdnimg.cn/e6840f1dfc7d463d992f06a164769d42.png)
  - 此外,您还可以将`插入新公式`右键,将其添加导快速访问工具栏中
  - 下面还有一个按钮提供手写识别公式字符,根据需要使用

## 专业输入公式软件

- Axmath(国内)

- MathType(老牌工具)

- ...

  这些类软件大多数收费,虽然有特殊版,但是体验不是很好(比如容易出现乱码等情况)

### MathType再word中的使用

- [MathType使用入门之快速在word中插入公式的方法-MathType中文网](https://www.mathtype.cn/jiqiao/mathtype-nbyo.html)

### mathType 公式转换🎈

- [converting-equations|MathType 7 with Microsoft Office 2016 or later (wiris.com)](https://docs.wiris.com/mathtype/en/mathtype-office-tools/mathtype-7-for-windows-and-mac/mathtype-7-with-microsoft-office-2016-or-later.html#converting-equations)
- [convert-equations-to-mathtype-equations|MathType 7 with Microsoft Office 2016 or later (wiris.com)](https://docs.wiris.com/mathtype/en/mathtype-office-tools/mathtype-7-for-windows-and-mac/mathtype-7-with-microsoft-office-2016-or-later.html#convert-equations-to-mathtype-equations)

#### 查阅mathtype在word中的快捷键

![在这里插入图片描述](https://img-blog.csdnimg.cn/861c2384b6ac476aa39119aab1e602c2.png)

#### 将latex代码切换为mathtype公式:

- 上一节中显示`alt+\`可以将合法的latex公式(方程)包裹为形如`$...$`或者`\[...\]`的内容切换显示为mathtype公式



#### 将mathType公式(方程)转化为word可直接编辑的公式🎈

- 您可以选中需要转换的公式（方程）
- 通过工具栏上的转换按钮将公式转为`latex`
  - ![在这里插入图片描述](https://img-blog.csdnimg.cn/251e85f0311545eea8466c639f7b0daf.png)
  - 不出意外的话，您将看到word中的latex源码
  - 鼠标选中这些`latex`源码(不要连`$$ $$`号一起选中),将他转化为word equation(通过快捷键`alt+=`或者用鼠标点击`插入->公式`)
    - 再输入`ctrl+=`将公式渲染出来(对于不太复杂的公式,通常会成功)
    - 注意`alt+shift+=`,使用来检查latex源码的!
    - ![在这里插入图片描述](https://img-blog.csdnimg.cn/84f42c7b14964aa1a244f17352c51151.png)

#### mathtype公式编号🎈

- [怎样用MathType为word中的公式自动编号-MathType中文网](https://www.mathtype.cn/jiqiao/mathtype-iiiz.html)

- [MathType公式编号怎么随章节变化 MathType公式编号不在最右边-MathType中文网](https://www.mathtype.cn/jiqiao/mathtype-jdfcf.html)该教程包含以下内容

  - 一、MathType公式编号怎么随章节变化

    1、插入公式编号

    2、公式编号随章节自动变化

  - 二、MathType公式编号不在最右边

    **1、内联公式编号**

    2、复制公式与编号进来后右侧不对齐

    1、在新文档中直接插入编号公式

    2、通过制表位调整右侧位置

  - 三、修改编号格式

- 尽管某些论文要求使用特定的方式插入公式(比如要求用word自带的公式工具编辑)

- 但是通常用mathtype编辑公式更加高效,尤其是在公式自动编号这一块以及插入对公式号的引用

- 可以先用mathtype编辑打草稿,然后再转换为其他格式

#### 其他方案

- [microsoft office - Convert MathType equations to native Word 2007/2010 equations - Super User](https://superuser.com/questions/403933/convert-mathtype-equations-to-native-word-2007-2010-equations)

## 小结🎈

- 如果是大量输入公式,可以考虑使用专业工具

- 如果熟悉latex公式,那么手写latex公式源码回车即可渲染,但是对于较长公式不建议在word的环境下直接编写,公式缩在一行不方便看
  - 可以在typora等markdown笔记下编写公式或者word草稿,然后粘贴word格式进行格式的规范化处理
  - 要么用专业公式编辑软件(提供的word/office插件)

# 附：常见数学标记语言

## 数学标记语言MathML

- [**数学标记语言**](https://zh.wikipedia.org/zh-cn/%E6%95%B0%E5%AD%A6%E7%BD%AE%E6%A0%87%E8%AF%AD%E8%A8%80#%E4%B8%8E%E5%85%B6%E5%AE%83%E6%A0%BC%E5%BC%8F%E7%9A%84%E5%AF%B9%E6%AF%94)（Mathematical Markup Language，MathML），是一种基于[XML](https://zh.wikipedia.org/wiki/XML)的标准，用来描述数学符号和公式。它的目标是把数学公式集成到[万维网](https://zh.wikipedia.org/wiki/万维网)和其他文档中。从2015年开始，MathML成为了[HTML5](https://zh.wikipedia.org/wiki/HTML5)的一部分和[ISO](https://zh.wikipedia.org/wiki/ISO)标准。

- 由于数学符号和公式的结构复杂且符号与符号之间存在多种逻辑关系，MathML的格式十分繁琐。因此，大多数人都不会去手写MathML，而是利用其它的工具来编写，其中包括[TEX](https://zh.wikipedia.org/wiki/TeX)到MathML的转换器。

### Office MathML (OMML)

- [Office Math Markup Language](https://en.wikipedia.org/wiki/Office_Open_XML_file_formats#Office_MathML_(OMML)) is a mathematical markup language which can be embedded in WordprocessingML, with intrinsic support for including word processing markup like revision markings,[[16\]](https://en.wikipedia.org/wiki/Office_Open_XML_file_formats#cite_note-16) footnotes, comments, images and elaborate formatting and styles.[[17\]](https://en.wikipedia.org/wiki/Office_Open_XML_file_formats#cite_note-17) The OMML format is different from the [World Wide Web Consortium](https://en.wikipedia.org/wiki/World_Wide_Web_Consortium) (W3C) [MathML](https://en.wikipedia.org/wiki/MathML) recommendation that does not support those office features, but is partially compatible[[18\]](https://en.wikipedia.org/wiki/Office_Open_XML_file_formats#cite_note-18) through [XSL Transformations](https://en.wikipedia.org/wiki/XSL_Transformations); tools are provided with office suite and are automatically used via clipboard transformations.[[19\]](https://en.wikipedia.org/wiki/Office_Open_XML_file_formats#cite_note-19)

- The following Office MathML example defines the [fraction](https://en.wikipedia.org/wiki/Fraction_(mathematics)):${\frac {\pi }{2}}$

  - ```
    <m:oMathPara><!-- mathematical block container used as a paragraph -->
      <m:oMath><!-- mathematical inline formula -->
        <m:f><!-- a fraction -->
          <m:num><m:r><m:t>π</m:t></m:r></m:num><!-- numerator containing a single run of text -->
          <m:den><m:r><m:t>2</m:t></m:r></m:den><!-- denominator containing a single run of text -->
        </m:f>
      </m:oMath>
    </m:oMathPara>
    ```

- Some have queried the need for Office MathML (OMML) instead advocating the use of [MathML](https://en.wikipedia.org/wiki/MathML), a [W3C](https://en.wikipedia.org/wiki/World_Wide_Web_Consortium) recommendation for the "inclusion of mathematical expressions in Web pages" and "machine to machine communication".[[20\]](https://en.wikipedia.org/wiki/Office_Open_XML_file_formats#cite_note-20) Murray Sargent has answered some of these issues in a blog post, which details some of the philosophical differences between the two formats.[[21\]](https://en.wikipedia.org/wiki/Office_Open_XML_file_formats#cite_note-21)

## mathjax

- [MathJax | Beautiful math in all browsers.](https://www.mathjax.org/)

- **MathJax**是一个[跨浏览器](https://zh.wikipedia.org/wiki/浏览器兼容性)的[JavaScript库](https://zh.wikipedia.org/wiki/JavaScript库)，它使用[MathML](https://zh.wikipedia.org/wiki/MathML)、[LATEX](https://zh.wikipedia.org/wiki/LaTeX)和ASCIIMathML标记在[Web浏览器](https://zh.wikipedia.org/wiki/Web浏览器)中显示[数学符号](https://zh.wikipedia.org/wiki/數學符號)。[[2\]](https://zh.wikipedia.org/zh-cn/MathJax#cite_note-2)[[3\]](https://zh.wikipedia.org/zh-cn/MathJax#cite_note-3)[[4\]](https://zh.wikipedia.org/zh-cn/MathJax#cite_note-4)MathJax是在[Apache许可证](https://zh.wikipedia.org/wiki/Apache许可证)下作为[开源软件](https://zh.wikipedia.org/wiki/开源软件)发布的。

  MathJax项目始于2009年，是早期JavaScript数学格式化库jsMath的继承者，[[5\]](https://zh.wikipedia.org/zh-cn/MathJax#cite_note-5)由[美国数学学会](https://zh.wikipedia.org/wiki/美国数学学会)管理。

  - [[6\]](https://zh.wikipedia.org/zh-cn/MathJax#cite_note-6)该项目由美国数学学会、设计科学学会、工业和应用数学学会共同发起，并得到[美国物理联合会](https://zh.wikipedia.org/wiki/美国物理联合会)和[Stack Exchange](https://zh.wikipedia.org/wiki/Stack_Exchange)等众多资助机构的支持。[[7\]](https://zh.wikipedia.org/zh-cn/MathJax#cite_note-7)MathJax被[arXiv](https://zh.wikipedia.org/wiki/ArXiv)[[8\]](https://zh.wikipedia.org/zh-cn/MathJax#cite_note-8)、[爱思唯尔](https://en.wikipedia.org/wiki/Elsevier)的[ScienceDirect](https://zh.wikipedia.org/wiki/ScienceDirect)[[9\]](https://zh.wikipedia.org/zh-cn/MathJax#cite_note-9)、MathSciNet[[10\]](https://zh.wikipedia.org/zh-cn/MathJax#cite_note-10)、n-category cafe、[MathOverflow](https://zh.wikipedia.org/wiki/MathOverflow)、[维基百科](https://zh.wikipedia.org/wiki/维基百科)（在后台）[[11\]](https://zh.wikipedia.org/zh-cn/MathJax#cite_note-11)[[12\]](https://zh.wikipedia.org/zh-cn/MathJax#cite_note-12)、[Scholarpedia](https://zh.wikipedia.org/wiki/Scholarpedia)、Project Euclid期刊[[13\]](https://zh.wikipedia.org/zh-cn/MathJax#cite_note-13)、[IEEE Xplore](https://zh.wikipedia.org/wiki/IEEE_Xplore)[[14\]](https://zh.wikipedia.org/zh-cn/MathJax#cite_note-14)、Publons、[Coursera](https://zh.wikipedia.org/wiki/Coursera)和全俄数学门户网站等网站使用。[[15\]](https://zh.wikipedia.org/zh-cn/MathJax#cite_note-15)

- **MathJax**是一个[跨浏览器](https://zh.wikipedia.org/wiki/浏览器兼容性)的[JavaScript库](https://zh.wikipedia.org/wiki/JavaScript库)，它使用[MathML](https://zh.wikipedia.org/wiki/MathML)、[LATEX](https://zh.wikipedia.org/wiki/LaTeX)和ASCIIMathML标记在[Web浏览器](https://zh.wikipedia.org/wiki/Web浏览器)中显示[数学符号](https://zh.wikipedia.org/wiki/數學符號)。[[2\]](https://zh.wikipedia.org/zh-cn/MathJax#cite_note-2)[[3\]](https://zh.wikipedia.org/zh-cn/MathJax#cite_note-3)[[4\]](https://zh.wikipedia.org/zh-cn/MathJax#cite_note-4)MathJax是在[Apache许可证](https://zh.wikipedia.org/wiki/Apache许可证)下作为[开源软件](https://zh.wikipedia.org/wiki/开源软件)发布的。

  MathJax项目始于2009年，是早期JavaScript数学格式化库jsMath的继承者，[[5\]](https://zh.wikipedia.org/zh-cn/MathJax#cite_note-5)由[美国数学学会](https://zh.wikipedia.org/wiki/美国数学学会)管理。

  - [[6\]](https://zh.wikipedia.org/zh-cn/MathJax#cite_note-6)该项目由美国数学学会、设计科学学会、工业和应用数学学会共同发起，并得到[美国物理联合会](https://zh.wikipedia.org/wiki/美国物理联合会)和[Stack Exchange](https://zh.wikipedia.org/wiki/Stack_Exchange)等众多资助机构的支持。[[7\]](https://zh.wikipedia.org/zh-cn/MathJax#cite_note-7)MathJax被[arXiv](https://zh.wikipedia.org/wiki/ArXiv)[[8\]](https://zh.wikipedia.org/zh-cn/MathJax#cite_note-8)、[爱思唯尔](https://en.wikipedia.org/wiki/Elsevier)的[ScienceDirect](https://zh.wikipedia.org/wiki/ScienceDirect)[[9\]](https://zh.wikipedia.org/zh-cn/MathJax#cite_note-9)、MathSciNet[[10\]](https://zh.wikipedia.org/zh-cn/MathJax#cite_note-10)、n-category cafe、[MathOverflow](https://zh.wikipedia.org/wiki/MathOverflow)、[维基百科](https://zh.wikipedia.org/wiki/维基百科)（在后台）[[11\]](https://zh.wikipedia.org/zh-cn/MathJax#cite_note-11)[[12\]](https://zh.wikipedia.org/zh-cn/MathJax#cite_note-12)、[Scholarpedia](https://zh.wikipedia.org/wiki/Scholarpedia)、Project Euclid期刊[[13\]](https://zh.wikipedia.org/zh-cn/MathJax#cite_note-13)、[IEEE Xplore](https://zh.wikipedia.org/wiki/IEEE_Xplore)[[14\]](https://zh.wikipedia.org/zh-cn/MathJax#cite_note-14)、Publons、[Coursera](https://zh.wikipedia.org/wiki/Coursera)和全俄数学门户网站等网站使用。[[15\]](https://zh.wikipedia.org/zh-cn/MathJax#cite_note-15)

MathJax下载网页内容，扫描页面内容寻找等式标记，并对数学进行排版。因此，MathJax不需要在阅读器系统上安装软件或额外的[字体](https://zh.wikipedia.org/wiki/字体)。这允许MathJax在任何支持JavaScript的浏览器中运行，包括移动设备。[[16\]](https://zh.wikipedia.org/zh-cn/MathJax#cite_note-cervone-16)

- MathJax可以使用[HTML](https://zh.wikipedia.org/wiki/HTML)和[CSS](https://zh.wikipedia.org/wiki/CSS)的组合显示数学，或者在可用时使用浏览器的原生MathML支持。MathJax用于排版数学的确切方法取决于用户浏览器的功能、用户系统上可用的字体和配置设置。MathJax 自 v2.0-beta版引入了[SVG](https://zh.wikipedia.org/wiki/SVG)渲染功能。[[17\]](https://zh.wikipedia.org/zh-cn/MathJax#cite_note-17)
  - 在HTML和CSS排版的情况下，MathJax通过使用数学字体（如果有的话）和使用旧浏览器的图像来最大化数学显示质量。对于支持Web字体的新浏览器，MathJax提供了一组全面的Web字体，可以根据需要下载。如果浏览器不支持Web字体，MathJax将检查用户系统上是否有有效的字体。如果这不起作用，MathJax将提供所需符号的图像。可以配置MathJax来启用或禁用Web字体、本地字体和图像字体。
  - MathJax使用STIX字体在Web页面中包含数学。在本地计算机上安装字体可以提高MathJax的排版速度。[[18\]](https://zh.wikipedia.org/zh-cn/MathJax#cite_note-18)
- MathJax可以显示用[LaTeX](https://zh.wikipedia.org/wiki/LaTeX)或MathML标记编写的数学符号。<u>因为MathJax只用于数学显示，而LaTeX是一种文档布局语言，所以MathJax只支持用于描述数学表示法的LaTeX子集。[[16\]](https://zh.wikipedia.org/zh-cn/MathJax#cite_note-cervone-16)</u>
- MathJax复制LaTeX的数学环境命令。通过扩展支持AMS-LaTeX数学命令。MathJax还支持TeX宏和其他格式，比如`\color`和`\underline`。[[24\]](https://zh.wikipedia.org/zh-cn/MathJax#cite_note-24)
- MathJax在beta 2版本中增加了对<u>MathML 2.0和一些MathML 3.0构造的部分支持</u>。

## Tex

- **TEX**（[/tɛx/](https://zh.wikipedia.org/wiki/Help:英語國際音標)，常被读作[/tɛk/](https://zh.wikipedia.org/wiki/Help:英語國際音標)，音译“泰赫”，“泰克”，写作“TEX”），是一个由美国计算机教授[高德纳](https://zh.wikipedia.org/wiki/高德纳)（Donald Ervin Knuth）编写的排版**软件**。
- TeX的[MIME类型](https://zh.wikipedia.org/wiki/互联网媒体类型)为`application/x-tex`，是一款[自由软件](https://zh.wikipedia.org/wiki/自由软件)。
- 它在学术界特别是[数学](https://zh.wikipedia.org/wiki/数学)、[物理学](https://zh.wikipedia.org/wiki/物理学)和[计算机科学](https://zh.wikipedia.org/wiki/计算机科学)界十分流行。TEX被普遍认为是一个优秀的排版工具，尤其是对于复杂数学公式的处理。利用[LATEX](https://zh.wikipedia.org/wiki/LaTeX)等终端软件，TeX就能够排版出精美的文本以帮助人们辨认和查找。

## Latex

- **LaTeX**（[/ˈlɑːtɛx/](https://zh.wikipedia.org/wiki/Help:英語國際音標)，常被读作[/ˈlɑːtɛk/](https://zh.wikipedia.org/wiki/Help:英語國際音標)或[/ˈleɪtɛk/](https://zh.wikipedia.org/wiki/Help:英語國際音標)，[风格化](https://zh.wikipedia.org/wiki/風格化)后写作“LATEX”），是一种基于[TEX](https://zh.wikipedia.org/wiki/TeX)的[排版](https://zh.wikipedia.org/wiki/排版)系统，由[美国](https://zh.wikipedia.org/wiki/美国)[计算机科学](https://zh.wikipedia.org/wiki/计算机科学)家[莱斯利·兰伯特](https://zh.wikipedia.org/wiki/莱斯利·兰伯特)在20世纪80年代初期开发

- 利用这种格式系统的处理，即使用户没有排版和程序设计的知识也可以充分发挥由TEX所提供的强大功能，不必一一亲自去设计或校对，能在几天，甚至几小时内生成很多具有书籍质量的印刷品。
- 对于生成复杂表格和[数学](https://zh.wikipedia.org/wiki/数学)公式，这一点表现得尤为突出。因此它非常适用于生成高印刷质量的[科技](https://zh.wikipedia.org/wiki/科技)和[数学](https://zh.wikipedia.org/wiki/数学)、[物理](https://zh.wikipedia.org/wiki/物理)文档。这个系统同样适用于生成从简单的信件到完整书籍的所有其他种类的文档。
- LaTeX使用TeX作为它的格式化引擎，当前的版本是LaTeX2e

### AMS-LaTex

- AMS:美国数学协会
- [AMS-LaTeX - American Mathematical Society](https://www.bing.com/ck/a?!&&p=ccbe1749c3b5b9cfJmltdHM9MTY3NjMzMjgwMCZpZ3VpZD0yNTFhYjVkNS03ZDg3LTZkZTMtMGYxMC1hNzU0Nzk4NzZiMGQmaW5zaWQ9NTE4Mg&ptn=3&hsh=3&fclid=251ab5d5-7d87-6de3-0f10-a75479876b0d&psq=AMS-LaTeX&u=a1aHR0cHM6Ly93d3cuYW1zLm9yZy90ZXgvYW1zbGF0ZXguaHRtbA&ntb=1)

- **AMS-LaTeX**是一个包含数学公式所需<u>符号、排版、字体</u>的[LaTeX](https://zh.wikipedia.org/wiki/LaTeX)文档类和宏包集合。
- 其开发主要参考了[美国数学协会](https://zh.wikipedia.org/wiki/美国数学协会)在数学期刊和书籍排版工作中的经验，以AMS-LaTeX撰写的文章可被该协会旗下期刊接受。[[1\]](https://zh.wikipedia.org/zh-cn/AMS-LaTeX#cite_note-1)

## 对象链接与嵌入OLE@部件对象模型COM

- **对象链接与嵌入**（英语：[Object Linking and Embedding](https://en.wikipedia.org/wiki/Object_Linking_and_Embedding)，**OLE**）是能让应用程序创建包含不同来源的[复合文档](https://zh.wikipedia.org/wiki/复合二进制文档)的技术[[3\]](https://zh.wikipedia.org/zh-cn/对象链接与嵌入#cite_note-3)。
- OLE不仅是[桌面应用程序](https://zh.wikipedia.org/w/index.php?title=桌面应用程序&action=edit&redlink=1)集成，而且还<u>定义和实现了</u>允许应用程序作为软件“[对象](https://zh.wikipedia.org/wiki/对象)”（数据集合和操作数据的函数）彼此进行“链接”的机制，这种<u>链接机制和协议</u>称为**部件对象模型**（Component Object Model），简称[COM](https://zh.wikipedia.org/wiki/COM)。
- OLE可以用来创建复合文档，复合文档包含了创建于不同源应用程序，有着不同类型的数据，因此可以把[文字](https://zh.wikipedia.org/wiki/文字)、[声音](https://zh.wikipedia.org/wiki/声音)、[图像](https://zh.wikipedia.org/wiki/图像)、[表格](https://zh.wikipedia.org/wiki/表格)、[应用程序](https://zh.wikipedia.org/wiki/应用程序)等组合在一起。
