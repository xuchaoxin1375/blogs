[toc]

## abstract

- Microsoft365 office 3件套的变化

## 复制粘贴格式设置

- [Control the formatting when you paste text - Microsoft Support](https://support.microsoft.com/en-us/office/control-the-formatting-when-you-paste-text-20156a41-520e-48a6-8680-fb9ce15bf3d6)

- 在Microsoft365中,推荐粘贴的时候用鼠标右键,这样可以选择粘贴格式;直接粘贴的效果往往不是我们想要的

- 原来的高级粘贴设置选项被阉割了,将来不知道会有新的改变

- Microsoft 365 改变

  - [PowerPoint Options (Advanced) - Microsoft Support](https://support.microsoft.com/en-us/office/powerpoint-options-advanced-bdbee278-4984-4c7d-88ad-6f493bd18343?ns=powerpnt&version=90&syslcid=1033&uilcid=1033&appver=zpp900&helpid=198297&ui=en-us&rs=en-us&ad=us)

  - 这里可以从有选项的右上角问号点击到帮助文档

  - Cut, copy, and paste剪切、复制和粘贴

    ***\*Use smart cut and paste\****   Select this check box if you want PowerPoint to adjust the spacing of words and objects that you paste into your presentation. Smart cut and paste ensures that pasted content does not run up against other words or objects that appear before or after the content that you paste. Clear this check box if you do not want PowerPoint to automatically adjust the spacing of words or objects.使用智能剪切和粘贴 如果希望 PowerPoint 调整粘贴到演示文稿中的文字和对象的间距，请选择此复选框。智能剪切和粘贴可确保粘贴的内容不会与粘贴内容之前或之后出现的其他单词或对象相冲突。如果不想让 PowerPoint 自动调整文字或对象的间距，请清除此复选框。

    ***\*Show Paste Options buttons\****   Select this check box to show the **Paste Options** buttons, or clear this check box to hide the **Paste Options** buttons. The **Paste Options** buttons appear alongside text that you paste. By using these buttons, you can quickly choose between keeping the source formatting or pasting text only.显示 "粘贴选项 "按钮 选择该复选框可显示 "粘贴选项 "按钮，清除该复选框可隐藏 "粘贴选项 "按钮。粘贴选项按钮会出现在粘贴文本的旁边。通过使用这些按钮，您可以快速选择保留源格式或仅粘贴文本。

    **Note:** When you clear the **Show Paste Options buttons** check box, you turn off this feature in all Office programs in which it is an option.注意：清除显示 "粘贴选项 "按钮复选框后，所有 Office 程序中的该功能都将关闭。

### 粘贴只有一种格式选项的问题

- 实际操作中可能得拼手速,不同时间右键可以选择的粘贴选项可能会变,从3种变为1种,可能是bug
- 考虑到我使用了第三方剪切版管理工具Ditto,有可能对粘贴造成了一定的影响;如果发现确实是第三方剪切板的问题,那么可以考虑在使用office(比如ppt)时暂时禁用第三方剪切板
- ppt的开始选项卡的第一个功能区里的粘贴选项可以打开剪切板,记录了ppt内的剪切板的历史记录

## AI功能

- 粘贴图片后,Microsoft 365可以分析出图片中的内容,用文本显示出来,如果您接受这个识别结果,可以将其Approve(接受),当图片无法显示出来时,或者更有意义的是给视力障碍人士使用ppt朗读功能朗读时,能够让读者知道图片的大致内容