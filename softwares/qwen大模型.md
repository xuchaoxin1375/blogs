[toc]

## qwen

- [QwenLM/Qwen1.5: Qwen1.5 is the improved version of Qwen, the large language model series developed by Qwen team, Alibaba Cloud. (github.com)](https://github.com/QwenLM/Qwen1.5)
  - 这是1.5版本的仓库,不久后会推出新一代模型,readme中会提示
- [Qwen1.5 介绍 | Qwen (qwenlm.github.io)](https://qwenlm.github.io/zh/blog/qwen1.5/)
  - 安装和使用方法有多种
  - 其中ollama这类方法安装最简单,但是模型参数细节不容看出来
  - 可以考虑其他安装方式[Installation - Qwen](https://qwen.readthedocs.io/en/latest/getting_started/installation.html)

- [Text Generation Web UI - Qwen](https://qwen.readthedocs.io/en/latest/web_ui/text_generation_webui.html)