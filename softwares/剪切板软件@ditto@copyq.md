[toc]



## abstract

- 介绍剪切板软件
- 主要从以下角度选择软件:
  - 体积小巧
  - 高性能
  - 跨平台
  - 开源
  - 功能丰富性
  - 自定义程度
  - 配置导出或同步

### 安装方法说明

在windows平台上建议使用国内加速版的scoop来安装,也可以去软件官网或者联想应用商店这类软件商店下载

## ditto

体积小,速度快,开源,自定义丰富,支持多语言

但是从头配置会花点功夫,可以将配置过程记录下来,或者拷贝备份软件版本,以便在其他设备快速部署

软件不一定要更新,也不推荐频繁更新

- [Ditto clipboard manager](https://ditto-cp.sourceforge.io/)
- [Home · sabrogden/Ditto Wiki](https://github.com/sabrogden/Ditto/wiki)

### 配置建议

- 默认保存的剪切版历史条目为100条,建议调大点,比如500条甚至1000条
- 默认的快捷键可以改为自己喜欢的快捷键
- 默认的剪切板窗口位置和大小可以调整一下

### 效果预览👺

- ![在这里插入图片描述](https://i-blog.csdnimg.cn/direct/c1cdc85ca1b2446485c5ffe7f74ebfb8.gif#pic_center)

###  配置快捷键

- 分为**键盘快捷键**和**快速粘贴快捷键**,主要讨论后者

### 常用粘贴快捷键(keyboard)

可以为同一个行为配置多个快捷键.

#### View Full Description(查看完整说明)

![](https://i-blog.csdnimg.cn/blog_migrate/012c2521d27c346cc98f549cbdd0d9fd.png)

- 选择添加(add),可以分配第二组(或者更多)的快捷键
- 注意,空格似乎别能被分配为快捷键
- 分配完毕后点击(Assign)分配按钮,正式分配

![](https://i-blog.csdnimg.cn/blog_migrate/31cd8b12947558b4c497a8932fb7711b.png)

### 管理已分配的快捷键

![在这里插入图片描述](https://i-blog.csdnimg.cn/direct/5bd9d01c278d4d94906eecfc89982329.png)

### 快速选项和总选项(总设置)

| 快速选项                                                     | 总选项                                                       |      |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ---- |
| ![](https://i-blog.csdnimg.cn/blog_migrate/af0aaf2373b7cd28dbddcd585c2f2f8c.png) | ![image-20241030101622328](C:\Users\cxxu\AppData\Roaming\Typora\typora-user-images\image-20241030101622328.png) |      |

快速选项上方第一行就是**总选项**面板,也就是总设置页面

###  配置弹出位置positioning👺

有两个地方可以设置

从总设置(选项)的常规选项卡里设置弹出位置

![image-20241030101622328](C:\Users\cxxu\AppData\Roaming\Typora\typora-user-images\image-20241030101622328.png)

在剪切板窗口右下角(三个点)进入**快速选项(Quick Optins)**选择**窗口弹出位置(Positioning)**

![](https://i-blog.csdnimg.cn/blog_migrate/bca864bc9924e666209ff5418c4896d1.png)

### 查看/预览剪切板条目详情和窗口设置👺

您可以这样做,将历史剪切记录管理面板分配为右半屏幕,左半屏幕作为显示详情的空间(这样不容易挡住其他缩略选项

>相关快捷键可以为其增加一个mouse click,比较方便
>
>![](https://i-blog.csdnimg.cn/blog_migrate/bc49cd5125230d20260042d0568b086f.png)
>预览窗口的配置
>![在这里插入图片描述](https://i-blog.csdnimg.cn/blog_migrate/1c99c52f3c5df7e3976a19aeb041e110.png)
>根据您的喜好,可以将预览窗口设定为

- 固定大小以及固定位置`(Remenber window postion)`

#### 详情窗口大小内容自适应开关

- 建议关闭(取消勾选)`size window  to content`,把窗口拉到左半屏幕

- 缩放图片来适应窗口大小或者不固定预览窗口的大小,而由被预览的内容来决定预览窗口的大小

  

###  窗口置顶与自动收起

点击ditto面板之外的地方自动关闭面板

您可以双击右侧的ditto标记,切换之后即可(应该是由bug)
![](https://i-blog.csdnimg.cn/blog_migrate/6a17396e38a1be39ea3c2c139ff10f33.png)
窗口置顶的设置(自动收起)(似乎不设置也可以达到目的)
自动收起（需要设置后需要重新启动ditto）
![](https://i-blog.csdnimg.cn/blog_migrate/2f3a9b29feab99dc598632a233ac5e03.png)

###  每行行数设置(预览行高大小设置)

![](https://i-blog.csdnimg.cn/blog_migrate/8ea007ee1f6996c9caac735486f31c81.png)



## copyq

- 开源跨平台,体积略大
- [CopyQ](https://hluk.github.io/CopyQ/)

### ecopaste

- 跨平台,体积适中

[EcoPasteHub/EcoPaste: 🎉跨平台的剪贴板管理工具 | Cross-platform clipboard management tool](https://github.com/EcoPasteHub/EcoPaste)