[toc]

## abstract

honeyView 是一款高性能的图片查看器,体积小响应快

为了更舒服的使用，可能需要一些设置,比如鼠标滚轮行为设置:

## 快捷键或鼠标设置

默认情况下,滚轮会切换图片,但是许多软件则是将滚轮绑定为放大或缩小图片

右键honeyView软件任意位置,选择`Configuration`,`mouse`,然后选择`mousewheelup`设置为`zoomIn`(放大),选择`mousewheeldown`,选择`zoomOut`(缩小)