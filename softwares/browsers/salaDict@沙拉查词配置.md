[toc]

## abstract

- 沙拉查词配置@独立窗口全局快捷键@独立窗口不刷新问题

## document

- [沙拉查词使用方式 | Saladict 沙拉查词 (crimx.com)](https://saladict.crimx.com/manual.html)

##  后台驻留

- 浏览器端的设置
  - [edge://settings/system](edge://settings/system):`System and Performance`
  - 选择`continue running background extensions and apps when edge is closed`
- 沙拉插件端的设置:
  - `General->Keep in Background`
- 注意保存设置
  - 如果不生效,请彻底重启浏览器(或者注销(log off)或重启(restart)windows后查看)

- 如果设置成功，windows通知中心将给出一条通知


## 快捷键
- [edge://extensions/shortcuts](edge://extensions/shortcuts)
- 主要配置以下两个快捷键
  1. 打开独立词典窗口(`Open or highlight standalone dict panel`)
  2. 查询剪切板内容(`Search Clipboard content in standalone Panel`)

##  权限permission

- [权限(permission)|直达设置](extension://idghocbbahafpfhjnfhpbfbmpegphmmp/options.html?menuselected=Permissions)

## bug:独立窗口全局快捷键页面卡住问题

-  某些时候,独立查词窗口弹不出来/内容还是旧的内容,

### 方案1:

- 可以考虑将独立窗口移动到虚拟桌面中

###  方案2:

- 按下win (windows标徽菜单键)即刷新查词窗口的会话窗口

### 快捷键失效🎈bug

这种情况是偶尔突然就出现了

- 导出当前配置
- 卸载saladict
- 彻底退出浏览器(包括后台运行的插件,也可以用任务管理器/注销/重启等方式进行)
- 重新安装saladict扩展,导入配置
- 设置快捷键
- 再次彻底退出浏览器
- 重新进入浏览器检查快捷键是否修复

##  词典窗口/面板大小调整

###  网页内部的查词窗口面板
- 配置完注意保存修改(save)

###  独立窗口的查词面板@位置和大小设置

- `quick Search->Standalone Panel Options->Remenber size and positon`

- 启用记住窗口位置和大小,这样下次调整窗口位置和大小后就可以记住设置(这是一种使用鼠标设置的方式而不是键盘设定参数的方式)

### 浏览器内部的查词窗口

- 进入`popup panel`进行设置

