## 常用播放器

### potplayer👺

- 中文网[Global Potplayer](https://potplayer.tv/?lang=zh_CN)
- 国际网[Global Potplayer (daum.net)](https://potplayer.daum.net/)

- 使用手册[PotPlayer中文网 - 万能格式影音视频播放器 PotPlayer播放器 PotPlayer官网 (potplayercn.com)](http://www.potplayercn.com/)

### 其他视频播放器👺

- [mpv播放器](https://mpv.io/)
- 下载:[mpv.io | Installation](https://mpv.io/installation/)

### 对比

**PotPlayer**

- 开发者：由韩国 Daum 公司开发，原作者姜奉求曾是 KMPlayer 的开发者。
- 用户界面：PotPlayer 提供丰富的定制选项和美观易用的图形界面，适合普通用户和有一定技术要求的用户，支持自定义皮肤和徽标。
- 功能特性：PotPlayer 支持大量编解码器，内置强大的 DXVA 硬件加速解码功能，多线程解码能力强，提供高清流畅播放体验，兼容多种视频格式，同时也支持在线流媒体播放和直播源播放。
- 扩展性：PotPlayer 支持外部滤镜插件，比如 MadVR 进行高质量渲染，但相比 mpv，其开源性和扩展性稍逊一筹。
- 平台支持：PotPlayer 主要是针对 Windows 平台开发，原生支持Windows系统。

**mpv**
- 开源性：mpv 是基于 MPlayer 和 mplayer2 的分支项目，完全开源免费，并跨平台支持 Windows、Linux、macOS 等多个操作系统。
- 用户界面：mpv 的界面极简，可通过命令行参数或配置文件进行深度定制，基本界面不含任何图形控制元素，更适合高级用户和技术爱好者，可通过脚本和插件增强功能和界面。
- 功能特性：mpv 的核心优势在于其极高的播放效率、优秀的视频渲染质量和对新技术的快速支持，特别适合处理高清和超高清视频。mpv 默认支持 libavcodec (ffmpeg) 解码库，硬件解码能力同样出色。
- 扩展性：mpv 因其开源性质，拥有庞大的社区支持和丰富的第三方插件，用户可以根据个人需求深度定制播放器功能，包括音视频滤镜、脚本接口等。
- 使用体验：mpv 的操作依赖键盘快捷键或者外部控制器，对于喜欢简洁高效界面、注重纯粹播放体验和可编程性的用户来说是理想选择。

总结：

- 如果你更偏好直观的图形界面、易于使用的操作和丰富的内置功能，同时主要使用的是 Windows 系统，PotPlayer 可能是更好的选择；
- 如果你是技术型用户，需要高度定制能力和跨平台支持，或者更欣赏简洁高效的纯播放体验，mpv 则可能更适合你。

### FAQ

- potplayer可能有广告问题和启动性能问题
- mpv可能不支持webdav,因此potplayer在某些场合下更受欢迎,能够直接播放网盘中的视频,例如阿里云盘的视频

## potplayer进入设置

- 播放任意一个视频,或者打开播放器主界面
- 右键,选择选项(偏好)

## 播放窗口@屏幕

### 窗口尺寸和位置设置

#### 设置启动参数

![1644930996825](https://img-blog.csdnimg.cn/img_convert/99b828ea0b2be9495efe839426863d33.png)

#### 设置播放设置

- ![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/139d040b39b74685be6032f4c8d47b36.png)

## 播放列表(专辑)

### 打开/关闭播放列表菜单

![1644930893378](https://img-blog.csdnimg.cn/img_convert/9d198adbc902d2515281a8e3a32fc9cd.png)

或者

![1644930791367](https://img-blog.csdnimg.cn/img_convert/fe1d2d754354f8a7443b334378e29dcc.png)

也可以通过右键,点击列表

### 新建专辑(播放列表)

![1644679491531](https://img-blog.csdnimg.cn/img_convert/2e48c7a5672ae20418499f0aa691da30.png)

### 为专辑添加音视频文件(比如文件夹)

- ![1644679582933](https://img-blog.csdnimg.cn/img_convert/fd92850e0bcd548105308945ce9d6e28.png)

## 快捷键

> 欲达到`修改`默认快捷键包括两个步骤:
>
> - 屏蔽默认快捷键配置
> - `添加`对应快捷键(绑定指令)

### 屏蔽(废弃)默认快捷键

![](https://img-blog.csdnimg.cn/img_convert/834cf1b5360ca48d4af9596a239c39c8.png)

### 添加快捷键

![1644930565761](https://img-blog.csdnimg.cn/img_convert/9389cdca503c62f23b119cceb7249907.png)

### 修改自定自定义的快捷键

注意,无法直接修改potplayer默认的快捷键!(按照上面两个步骤来实现修改效果)

![1644930656900](https://img-blog.csdnimg.cn/img_convert/265c62ee23dd98851c43f6eec66dfb04.png)
