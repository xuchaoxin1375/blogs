[toc]



## 使用相关插件

- [Markdown Viewer - Microsoft Edge Addons](https://microsoftedge.microsoft.com/addons/detail/markdown-viewer/cgfmehpekedojlmjepoimbfcafopimdg)
  - 关键在于设置插件权限(右键扩展图标,管理扩展插件,设置权限),允许其访问文件url
  - 然后找到markdown文件,选择打开方式,用浏览器打开
  - 您可以在扩展中选择显示模式,可以显示源码或者渲染后的结果

### 插件访问本地文件

- 浏览器为了安全考虑,一般会禁止插件访问本地文件,所以向markdown阅读器插件没有授权访问本地文件url就无法渲染其中的内容
- 此外,翻译类插件,比如沙拉查词,也需要授予其访问文件url的权限才可以使用诸如pdf中划词翻译
- 沙拉查词可以右键其图标,选择在pdf阅读器中打开,这样可以方便划词翻译pdf文件中的内容

### 其他权限

比如读取剪切版的权限,也可以设置

