[toc]



## abstract

部分没有扬声器和麦克风的显示器接入系统后无法播放声音,但是音频输入输出列表中却出现了显示器音频输出输出选项,如果默认选项被调整为显示器优先可能就无法播放声音了或者检测到计算机原本的麦克风声音

这种情况下打开声音混合声音设置`sndvol.exe`(命令行内输入该命令),win10以上的系统可以开始菜单中输入sndvol来找到这个程序

