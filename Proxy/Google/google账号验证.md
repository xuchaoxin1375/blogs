[toc]

## 移动设备上的安全码

- 以支持google服务的某些miui版本为例
- 当某次和google账号验证/授权/登录需要使用设备安全码的时候,需要做以下准备:
  - 移动设备系统支持google服务,并且能够(通过某些方式)访问google,这样才能接受到**安全码**
  - 移动设备根据安全码执行相应的操作
    - 例如授权时要求打开手机点击收到的验证通知窗口内的数组进行验证
  - 这类操作会将你的操作结果发送回google,完成完整的验证过程

### 方法2

- 下载google app
- 进入安全
- 打开安全码,手动输入到需要码的网址