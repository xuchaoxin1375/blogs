[toc]
## 通过任务管理器可以获得相关信息

![1645849212170](https://img-blog.csdnimg.cn/img_convert/1aecd709a783fa3eff7f563de26c8e87.png)

某些软件所在目录因为权限问题,无法直接`open file location`

不过可以通过properties获取目录值

![1645849321305](https://img-blog.csdnimg.cn/img_convert/45b294e22caceecdc9915b7e7d0e17a2.png)

使用管理员模式的powershell打开目录:

![1645849358727](https://img-blog.csdnimg.cn/img_convert/0daac26d1511b1a41cad0e2d974af74e.png)

## 使用powershell`ps`命令查询相关信息

![1645850748964](https://img-blog.csdnimg.cn/img_convert/c5df2b216e1e54a90c587b909c257805.png)
