[toc]

## win11之后

- win11之后的系统可以在开始菜单搜索磁盘清理工具(cleanup)
- 或者用命令行启动,即直接输入`cleanmgr`即可启动`disk cleanup`软件界面
- 工具路径位于`C:\Windows\System32\cleanmgr.exe`

## win11之前

- 除了上述方法,还可以打开资源管理,随便右键点集一个分区,选择属性,有清理磁盘功能(新系统被改成了详情(details))

## windows old清理

- 推荐使用系统自带的`cleanmgr`扫描C盘进行清理,可以避免直接删除的权限不足问题