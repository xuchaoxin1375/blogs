[toc]

## abstract

- 较新系统的镜像可以通过Microsoft官方网站下载
  - 比如最新一代和上一代的未结束支持的windows,可以官网下载
- 对于结束支持的系统镜像,一般要自己找资源
- 如果是非原版镜像,那么资源会非常多

## windows系统镜像站

- [MSDN, 我告诉你 - 做一个安静的工具站 (itellyou.cn)](https://msdn.itellyou.cn/)
- [NEXT, ITELLYOU](https://next.itellyou.cn/)
- 新版系统则是建议到Microsoft官网下载

### 第三方下载源👺

#### Iot@LTSC版本

包含了企业版或LTSC版下载,IoT Enterprise LTSC版本等(Windows 10/11 Enterprise LTSC (Long-Term Servicing Channel))

体验下来LTSC版本尤其是Iot enterprise LTSC是不错的,老机器可以较好运行.Iot可以转为非Iot,所以是不是iot问题不大,后期可以自己转换

注意下载镜像时选择合适的语言(默认可能是en-us(美国英语))

不是所有版本都提供多语言,例如IOT Enterprise LTSC可能只有英语版(不过后期安装中文包也可以,虽然还是有一定差异)

此外功能相近的版本之间可以使用版本转换工具或密钥进行转换,如果是低版本转高版本也可以,比如从家庭版转到专业版以上,可能需要下载额外的东西

#### LTSC下载地址

填写表单获取评估副本下载(评估版和完整版是有区别的,评估版体积比完整版要小,激活需要征升级到完整版)

1. Enterprise LTSC[Windows 11 Enterprise | Microsoft Evaluation Center](https://www.microsoft.com/en-us/evalcenter/evaluate-windows-11-enterprise)
  - [Download the ISO – Windows 11 Enterprise LTSC](https://go.microsoft.com/fwlink/p/?linkid=2195682&clcid=0x409&culture=en-us&country=us)
2. IOT Enterprise LTSC[Please select your Windows 11 IoT Enterprise LTSC download](https://www.microsoft.com/en-us/evalcenter/download-windows-11-iot-enterprise-ltsc-eval)

3. [Windows Evaluation Editions | MAS (massgrave.dev)](https://massgrave.dev/evaluation_editions)

4. 第三方整理[Download Windows / Office | MAS (massgrave.dev)](https://massgrave.dev/genuine-installation-media)


#### win10 ltsc 2021 原版初版cpu占用高问题

```cmd
 WSReset.exe -i
```

执行上述语句尝试在线修复(需要联网)

或者直接到系统更新中更新补丁(速度会慢一些)

### 预览版/开发版进行下载

- 稳定性和可靠性一般不如正式版
  - [使用 ISOs - Windows Insider Program | Microsoft Learn](https://learn.microsoft.com/zh-cn/windows-insider/isos)
  - [Download Windows Insider Preview ISO (microsoft.com)](https://www.microsoft.com/en-us/software-download/windowsinsiderpreviewiso)

- 可以在系统更新中**申请内测**(称为预览版成员,可以选择预览Canary,Dev,Beta,Preview 等通道),然后重启系统后检查更新(顺利的话可以收到更新推送，但是有时候收不到更新,特别是大版本更新,比如从23H2更新到24H)
- 也可以从**官方网站下载预览版镜像**体验最新功能

### win11

- [Download Windows 11 (microsoft.com)](https://www.microsoft.com/software-download/windows11)

### win10

- [下载 Windows 10 (microsoft.com)](https://www.microsoft.com/zh-cn/software-download/windows10)
  - 使用下载工具下载镜像（用于下载最新版windows）
  - 可以在工具里选择下载的目的
  - 缺点无法直接点击下载来获取镜像

### win7

- 下载链接参考本节头部列出的网站(非官方镜像通常以ed2k链接和bt磁力链接提供,普通的下载工具很难下载下来,即便是支持磁力下载,也不一定下载得动,建议直接用迅雷省事)

- Windows 7 是微软公司推出的个人电脑操作系统之一，共有多个版本以适应不同用户的需求。以下是Windows 7各主要版本的主要功能区别对比：

  1. **Windows 7 Starter（初级版）**
     - 最基本的版本，功能相对较少。
     - 不支持Aero特效和其他高级视觉效果。
     - 限制可同时运行的应用程序数量（早期版本有限制，后来有所放宽）。
     - 没有64位版本。
     - 不包含Windows Media Center和Windows Mobility Center等功能。

  2. **Windows 7 Home Basic（家庭普通版）**
     - 目标市场主要是新兴市场，功能比Starter版稍多。
     - 包含基础的Windows Aero界面。
     - 缺少一些多媒体和高级娱乐功能，比如Media Center。

  3. **Windows 7 Home Premium（家庭高级版）**
     - 提供完整的Aero体验，包括Flip 3D、触控功能等。
     - 支持Windows Media Center，方便多媒体播放和管理。
     - 支持多显示器输出和DVD回放功能。

  4. **Windows 7 Professional（专业版）**
     - 在Home Premium的基础上增加了对企业环境的支持特性。
     - 包括域加入、远程桌面连接、EFS加密文件系统和Windows XP模式等功能。
     - 更适合商务办公和小型企业用户。

  5. **Windows 7 Enterprise（企业版）**
     - 为企业级用户提供更多的安全性和管理功能，如DirectAccess、BranchCache、AppLocker等。
     - 只通过批量许可计划提供给企业和组织。

  6. **Windows 7 Ultimate（旗舰版）**
     - 结合了所有其他版本的所有功能，是最全面的一个版本。
     - 包括企业版的所有功能以及多语言包支持。
     - 适用于对功能要求最为全面的高端用户和开发者。

- 以上各版本均支持32位和64位（前提是硬件支持64位运算），其中64位版本能够更好地利用现代计算机的大内存和处理能力。
- 随着后续Service Pack（SP1）的发布，各版本均获得了性能提升和额外的兼容性改进。

## 其他下载方式

- [UUP dump](https://uupdump.net/) 这个网站可能被墙无法打开\
- 如果可以打开,我们可以选择版本(比如家庭版,专业版等),选择完毕后会生成一个下载按钮,下载下来的是一个脚本集合,需要解压脚本,然后选择当前自己的系统平台,比如windows,那么选择运行其中的windows脚本(bat或cmd脚本)
- 运行后脚本会开始下载需要的文件,根据之前的配置,可能会下载一堆文件最后合并他们(因此这里用脚本下载和合并,而不是直接下载一个单一文件)

## 镜像版本查看

- 要查看Windows ISO镜像中的系统版本，你可以采取以下几种方法


### 方法1：提取setup.exe查看产品版本

1. 将ISO镜像文件挂载到虚拟光驱或者直接解压ISO文件。
2. 找到`sources`文件夹内的`setup.exe`文件。
3. 右键点击`setup.exe`，选择“属性”，然后切换到“详细信息”选项卡。
4. 在详细信息中，你会看到“产品版本”，通过查看这个字段可以判断Windows镜像的版本号。

### 方法2：使用第三方工具

- **Dism++**：
  Dism++是一款强大的Windows映像管理工具，可以直接读取ISO镜像中的系统版本信息，无需解压或安装镜像。

  - ![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/147937dc93824447bbf8a6c18194f343.png)

- **命令提示符**：
  通过命令行工具也可查看，但通常需要先将ISO镜像挂载到一个虚拟驱动器或解压后访问到install.wim文件。

  ```shell
  dism /Get-WimInfo /WimFile:<path_to_install.wim>
  ```

  运行上述命令，路径指向ISO中`sources`文件夹内的`install.wim`文件，此命令将会列出WIM文件内各个图像的信息，包括版本号。

### 方法3：查看ISO文件名

- ISO文件名有时也会包含版本信息，如“Windows 10 Pro 21H2”之类的字样，但这种方法不准确，因为文件名有可能并未严格按照官方命名规则。


### 方法4：解压ISO并查阅内部文档

- 如果你有权限解压ISO镜像，可以打开`sources`文件夹里面的`ei.cfg`文件（如果有的话），查看里面关于SKU和版本的相关配置信息。

- 请注意，随着时间推移，Windows的版本不断更新，具体的版本号会有新的对应关系，请结合当时的最新Windows版本信息进行解读。

## 镜像下载工具推荐

- 如果镜像来源于云盘,那么就按云盘推荐的方法下载
- 而镜像经常是ed2k或bt链接,因此对于下载工具需要进行一定的选择
  - 国内的话就用迅雷好了,其他的不是那么省心
  - 而且迅雷的手机端有比较多的操作空间,迅雷电脑版的客户端也可以考虑,正常链接可以离线下载到云盘后再下载可能速度会更稳定
  - 如果用迅雷手机端下载,那么下载到手机后用局域网协议或者数据线传输到电脑上

## 自行精简镜像



- 工具和镜像准备
  1. [The Official AnyBurn Website](https://www.anyburn.com/)
  2. [GitHub - cschneegans/unattend-generator: .NET Core library to create highly customized autounattend.xml files](https://github.com/cschneegans/unattend-generator/)
     - [Generate autounattend.xml files for Windows 10/11 (schneegans.de)](https://schneegans.de/windows/unattend-generator/)
  3. 然后自行下载windows原版镜像(windows10/windows11)
- 操作方法:[最爽 Windows 安装！全自动、无人值守、高度自定义，系统清爽流畅，让电脑瞬间起飞！ | 零度解说_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV1Fz421i7bp/?spm_id_from=333.1350.jump_directly&vd_source=c0a3b17a665cd2d32431213df84cd3ce)

## 第三方镜像

- [Z-OS新版 (cn-virtual-z.github.io)](https://cn-virtual-z.github.io/zosnew.html)
  优化好的镜像：https://www.123pan.com/s/PJv7Vv-MgMr.html 提取码:6y0v
  精简方案：https://www.123pan.com/s/PJv7Vv-bnMr.html
- [不忘初心系统博客-精简版系统官网 (pc528.net)](https://www.pc528.net/)
  - 大多是收费镜像,精简的镜像文件比较小
  - 可以看看其他来源分享,比如论坛用户提供的网盘链接
- 有些包是打包好补丁了的

## 跳过联网登录微软账号

- 在引导环节使用快捷键`shift+F10`输入以下命令

  - `OOBE\BYPASSNRO`重启,然后**断网**/选择跳过网络连接以使用本地账户
  - [计算机首次开机如何跳过Microsoft 帐户登录，快速进入Windows桌面 | 华为官网 (huawei.com)](https://consumer.huawei.com/cn/support/content/zh-cn15898703/)
  - [[技巧\] 微软禁止通过OOBE跳过Windows 11账户登录 下面是最新可用办法 – 蓝点网 (landiannews.com)](https://www.landiannews.com/archives/96628.html)
  - `no@thankyou.com`

## 系统激活

在powershell中执行以下命令尝试激活(弹出的窗口中一般选择1即可)

```powershell
Invoke-RestMethod https://massgrave.dev/get | Invoke-Expression
```



## 虚拟机上安装注意事项

- 虚拟机软件能够识别系统镜像文件时,可能会推荐你使用快速安装
- 然而快速安装可能并不像听上去的那么好,例如我安装win7虚拟机时,下载的是Ultimate(旗舰版),vmware检测出来是win7,然而用快速安装后竟然安装的是普通家庭版,我又检查了一下镜像里的版本确实是旗舰版(或者说包含了多个版本,可以用Dism++导出)
- 然而为了避免翻车,我有下载了一个体积大一些的包,想着这回总该是旗舰版,没想到快速安装又给我装的家庭版
- 后来用不同安装(创建虚拟机后再选择镜像)才安装上正确的旗舰版

### win7这类老系统的现状

- 首先是UI界面流畅,响应迅速,尤其是资源管理器等系统自带的应用,Aero模糊很优雅,当年喜欢的不得了
- 其他就基本都是缺点了
  - 新硬件不在支持win7,驱动问题比较硬伤
    - UEFI引导可能会翻车
  - 虚拟机虽然可以安装,但是vmware tools 要求带有sp1包之后的win7,早期的win7还装不了
    - 即使是win7 sp1,在安装vmware tools 时也不是那么顺利,许多驱动安装不上,导致回滚
  - 停止维护,系统更新无法正常进行,需要到其他地方下载补丁或者安全卫士来打补丁
  - win7不激活的话直接黑屏给你看,新系统不激活问题不大
  - 新软件逐渐放弃了对win7的支持
    - 例如powershell7(经过试验最低要求windows8.1),最新版edge等
    - ![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/66bac51548944c60851c6b4f38c9fbb3.png)

### 360卫士安装补丁

- 可以用360安装更新补丁(数量可能高达上百个,每个补丁虽然下载快,但是安装却比较慢)
- 安装完补丁后可以顺利运行wmware tools 安装程序,把驱动安装上(直接安装vmware tools可能会失败)
- 最后可以把分辨率调整一下

### 经典游戏扫雷

- 困难模式需要有一定运气才能通过,中等难度才是最好玩的,可以做几乎全部的推理

- ![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/f656bafd8a924a4fa2a9eebfd42b272e.png)

博客园:63105e53-f4bd-4cb0-9257-ae85decf52b1

