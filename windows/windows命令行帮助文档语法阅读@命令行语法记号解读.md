[toc]

## abstract

- [命令行语法项 | Microsoft Learn](https://learn.microsoft.com/zh-cn/windows-server/administration/windows-commands/command-line-syntax-key)

- [Command-Line Syntax Key | Microsoft Learn](https://learn.microsoft.com/zh-cn/previous-versions/windows/it-pro/windows-server-2012-R2-and-2012/cc771080(v=ws.11))

- [How to Read Command Syntax in Windows (lifewire.com)](https://www.lifewire.com/how-to-read-command-syntax-2618082)

### 内容

下表描述用于指示命令行语法的表示法。

| 表示法                          | 说明                              |
| :------------------------------ | :-------------------------------- |
| 不含方括号或大括号的文本        | 必须按所显示键入的项。            |
| `<Text inside angle brackets>`  | 必须为其提供值的占位符。          |
| `[Text inside square brackets]` | 可选项。                          |
| `{Text inside braces}`          | 一组必需的项。 你必须选择一个。   |
| 竖线 (`|`)                      | 互斥项的分隔符。 你必须选择一个。 |
| 省略号 (`…`)                    | 可重复使用多次的项。              |

| Notation                        | Description                                        |
| :------------------------------ | :------------------------------------------------- |
| Text without brackets or braces | Items you must type as shown                       |
| <Text inside angle brackets>    | Placeholder for which you must supply a value      |
| [Text inside square brackets]   | Optional items                                     |
| {Text inside braces}            | Set of required items; choose one                  |
| Vertical bar (\|)               | Separator for mutually exclusive items; choose one |
| Ellipsis (…)                    | Items that can be repeated                         |