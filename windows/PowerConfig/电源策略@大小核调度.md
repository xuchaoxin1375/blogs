[toc]



## 启用隐藏的电源选项

[基于windows隐藏电源计划的CPU低功耗功耗调节指南 V1.2 - 哔哩哔哩](https://www.bilibili.com/opus/788773063444398086)

[Powercfg 命令行选项 | Microsoft Learn](https://learn.microsoft.com/zh-cn/windows-hardware/design/device-experiences/powercfg-command-line-options)

使用powershell或者cmd执行以下内容

```cmd
powercfg -attributes SUB_PROCESSOR 7f2f5cfa-f10c-4823-b5e1-e93ae85f46b5 -ATTRIB_HIDE
powercfg -attributes SUB_PROCESSOR 93b8b6dc-0698-4d1c-9ee4-0644e900c85d -ATTRIB_HIDE
powercfg -attributes SUB_PROCESSOR bae08b81-2d5e-4688-ad6a-13243356654b -ATTRIB_HIDE
cmd /c 'powercfg.cpl ,3'

```

`powercfg` 是 Windows 操作系统中的一个命令行工具，用于管理和配置电源设置。`powercfg -attributes` 是其中一个子命令，用于显示或修改电源方案和子组设置的可见属性。

以下是 `powercfg -attributes` 的详细文档和用法：

### 用法

```sh
powercfg -attributes <sub_GUID> <setting_GUID> <attribute>
```

### 参数

- `<sub_GUID>`：子组的 GUID，表示电源设置的类别。
- `<setting_GUID>`：设置的 GUID，表示特定的电源设置选项。
- `<attribute>`：属性值，可以是以下两种之一：
  - `0`：将设置标记为可见。
  - `1`：将设置标记为隐藏。

### 示例

假设我们有一个特定的电源设置项的 GUID 是 `ABCDE123-4567-8901-2345-67890ABCDE12`，并且它属于一个电源子组，其 GUID 是 `12345678-90AB-CDEF-1234-567890ABCDEF`。以下是如何使用 `powercfg -attributes` 来显示或隐藏这个设置项：

#### 显示设置项

```sh
powercfg -attributes 12345678-90AB-CDEF-1234-567890ABCDEF ABCDE123-4567-8901-2345-67890ABCDE12 0
```

#### 隐藏设置项

```sh
powercfg -attributes 12345678-90AB-CDEF-1234-567890ABCDEF ABCDE123-4567-8901-2345-67890ABCDE12 1
```

### 获取 GUID

要获取电源方案、子组和设置项的 GUID，可以使用以下命令：

#### 列出所有电源方案

```sh
powercfg -list
```

#### 列出特定电源方案的所有子组

```sh
powercfg -query <scheme_GUID>
```

#### 列出特定子组的所有设置项

```sh
powercfg -query <scheme_GUID> <sub_GUID>
```

### 示例

```sh
# 列出所有电源方案
powercfg -list

# 假设我们得到一个电源方案的 GUID 为 SCHEME_GUID

# 列出该电源方案的所有子组
powercfg -query SCHEME_GUID

# 假设我们得到一个子组的 GUID 为 SUB_GUID

# 列出该子组的所有设置项
powercfg -query SCHEME_GUID SUB_GUID

# 假设我们得到一个设置项的 GUID 为 SETTING_GUID

# 显示该设置项
powercfg -attributes SUB_GUID SETTING_GUID 0

# 隐藏该设置项
powercfg -attributes SUB_GUID SETTING_GUID 1
```

### 注意事项

- 需要以管理员身份运行命令提示符或 PowerShell 来使用 `powercfg -attributes` 命令，因为修改电源设置需要管理员权限。
- 修改电源设置可能会影响系统的电源管理行为，请谨慎操作。

### 总结

`powercfg -attributes` 命令允许用户显示或隐藏特定电源设置项，通过修改其可见属性。这对于定制电源管理方案和优化系统性能非常有用。