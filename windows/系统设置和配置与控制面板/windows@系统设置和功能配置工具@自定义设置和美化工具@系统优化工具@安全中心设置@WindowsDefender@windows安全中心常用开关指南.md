[toc]

## abstract
- windows@安全中心@windows security
- windwos defender误删文件恢复@添加信任免扫描域

## windows defender

- Windows Defender是微软推出的内置防病毒和反恶意软件工具，预装于Windows操作系统中，主要功能包括实时防护、定期扫描、云端威胁情报更新、恶意软件清除以及网络浏览保护等。
- 它专注于安静高效地保护用户的电脑免受病毒、间谍软件及其他形式恶意软件的侵害，尤其在Windows 10中，与防火墙整合并增强了安全功能，同时为企业用户提供更高级别的端点防护服务。
- Windows Defender 最初设计用于对抗间谍软件，随着时间的发展，特别是在Windows 8及后续版本中，其功能逐渐增强成为一个全面的安全解决方案，提供针对各种恶意软件（包括病毒、蠕虫、特洛伊木马等）的防护。
- Windows Defender 简称可以写作 "Defender" 或 "MS Defender"，在正式场合或文档中有时也称为 "Microsoft Defender"。随着其功能扩展到更全面的终端防护解决方案，现在常被提及为 "Microsoft Defender Antivirus" 

### 称呼说明

为了方便讨论,下面简称**Windows Defender** 为**WD**或**Defender**

### 相关服务👺

- windows antimalware service executable
  - 这个服务大约会吃掉200M内存,而且是不是会有比较高的cpu占用,风扇呼呼转
  - 虽然现在电脑普遍是16GB或更大内存,但是我认为该服务不仅仅会占用内存资源,还会影响使用体验
  - 如果关闭安全防护(比如防火墙)还会时不时弹出通知(无法关闭WD的通知)
  - 更重要的是windows defender会无意间将某些被怀疑的软件或文件删除,造成困扰
- [How to Fix Antimalware Service Executable High CPU Usage on Windows (makeuseof.com)](https://www.makeuseof.com/fix-antimalware-service-executable-high-cpu-usage-on-windows-10/)

## 停用windows Defender👺

分为两类,一类是临时停用,一类是长期停用,本文主要介绍后者

### 临时关闭实时防护

- 我们可以手动关闭实时防护,但是这只能暂时关闭
  - 打开windows defender 选择**病毒与威胁防护**,再选择"病毒与威胁防护"设置下面的**管理设置**

- 如果需要长期关闭,需要其他的进一步操作


### 彻底关闭@长期停用Defender

详见后续章节

## 使用软件工具修改系统设置👺

### windows optimizer

[hellzerg/optimizer: The finest Windows Optimizer (github.com)](https://github.com/hellzerg/optimizer)

### windows轻松设置

- [Windows11轻松设置（最新版） - 哔哩哔哩 (bilibili.com)](https://www.bilibili.com/opus/904672369138729017)

#### Ultimate Windows Tweaker

- [Ultimate Windows Tweaker 5 for Windows 11 (thewindowsclub.com)](https://www.thewindowsclub.com/ultimate-windows-tweaker-5-for-windows-11)

- [Ultimate Windows Tweaker 4 for Windows 10 (thewindowsclub.com)](https://www.thewindowsclub.com/ultimate-windows-tweaker-4-windows-10)
- 软件为英文,没有语言相关设置,有的有汉化版

#### RyTuneX

- [rayenghanmi/RyTuneX: RyTuneX is a cutting-edge optimizer built with the WinUI 3 framework, designed to amplify the performance of Windows devices. Crafted for both Windows 10 and 11. (github.com)](https://github.com/rayenghanmi/RyTuneX)

- [RyTuneX - Windows 10 and 11 Optimizer | Boost Performance & Privacy (rayenghanmi.github.io)](https://rayenghanmi.github.io/rytunex/index.html)

## UI修改方面的工具

### windHawk

[GitHub - ramensoftware/windhawk: The customization marketplace for Windows programs: https://windhawk.net/](https://github.com/ramensoftware/windhawk)

[Mods - Windhawk](https://windhawk.net/mods)

Note:如果任务栏相关的设置发现字体颜色和背景接近导致难以阅读,可以尝试到系统设置里切换一下windows主题颜色(比如从浅色切换到深色,然后切换回来)

部分Mod,可能引发问题,例如部分类型的弹窗无法弹出导致UI被阻塞而无法继续操作,设置卡死而无法正常退出,必须强杀

### ExplorerPatcher

[GitHub - valinet/ExplorerPatcher: This project aims to enhance the working environment on Windows](https://github.com/valinet/ExplorerPatcher)

[All features · valinet/ExplorerPatcher Wiki · GitHub](https://github.com/valinet/ExplorerPatcher/wiki/All-features)

- 微软逐渐封杀这种修改任务栏,资源管理器和开始菜单样式的修改软件,比如win11 24H2开始的版本,此类软件的兼容性和可用性逐渐降低
- 如果您的系统不是很新,那么软件可以较好工作
- 相关概念:[Symbols · valinet/ExplorerPatcher Wiki · GitHub](https://github.com/valinet/ExplorerPatcher/wiki/Symbols)

### DWMBlurGlass

[GitHub - Maplespe/DWMBlurGlass: Add custom effect to global system title bar, support win10 and win11.](https://github.com/Maplespe/DWMBlurGlass)

这个项目将windows的窗口设置毛玻璃效果/aero;

类似于ExplorerPatcher,对于最新的系统或者预览版系统可能无法及时兼容

### MyDockFinder👺

- 软件早期是免费软件,2021年开始的版本上架到steam上是收费软件
  - 经过近些年更新,软件的稳定性有所提高,并且推出了许多新功能,特别窗口动画,很优雅,搭配高刷屏幕,效果更佳
  - 或者网络上有一些学习版可以使用,体验也还可以,有些老版本动画更加流畅,但是部分新功能会缺失
  - 如果您不确定是否购买,可以用steam平台购买体验,2小时内可以退款
    - [Steam 客服 :: 常见退款问题 (steampowered.com)](https://help.steampowered.com/zh-cn/faqs/view/5fde-ba65-acce-a411)
    - 由于需要通过steam下载,这很不轻便
  - 由于最新版虽然在功能丰富性上满足我的需要,我也购买了此软件,但是安装时候后发现动画流畅性上对我的设备不太友好(不如旧版本流畅) 
  - 我就不怎么用这个steam上的原版软件(不超过2小时),几天之后申请了退款,我此次退款整个过程就几个小时就退款成功了,成功后如果你开着steam客户端,那么弹出退款通知,确认后您购买的软件将从您的库中移除(原来的软件界面可能出现错误,重新打开steam刷新库就可以看到产品被移出了,那么检查你的账户(比如微信支付),应该会收到一笔退款))
- mydockfinder的窗口通话可能会引起少数软件运行不正常
- 幸运的是mydockfinder支持指定排除某个应用的动画效果,这样可以提高兼容性!
- 如果这种弹窗是一次性的话,那么临时关闭mydockfinder(退出),然后重新启动出问题的程序看看是否能够解决该问题

## 彻底禁用Windows Defender👺

- [如何关闭 Windows Defender 病毒和威胁防护（临时或永久） - 系统极客](https://www.sysgeek.cn/disable-windows-defender/#7-验证-windows-defender-运行状态)
- [Defender Module | Microsoft Learn](https://learn.microsoft.com/zh-cn/powershell/module/defender/?view=windowsserver2022-ps)
  - Microsoft提供一个关于Defender的powershell模块

### 相关工具

#### windows defender remover

- [GitHub - ionuttbara/windows-defender-remover: A tool which is uses to remove Windows Defender in Windows 8.x, Windows 10 (every version) and Windows 11.](https://github.com/ionuttbara/windows-defender-remover)
- 移除后无法恢复!使用镜像升级系统后,可能导致defender页面进不去但是后台运行相关服务误删你的文件,需要慎用

#### defender control(慎用)

- !!!我用了这个软件后,发现其他软件启动的速度大幅度下降,安装软件更是一卡一卡的,如果你的设备出现了这种情况,请恢复windows defender,改用其他方式禁用👺
  - 平台是windows11(intel 12700H+RTX4060)
  - 重新启用安全中心后,速度恢复正常

- 使用开源工具:
  - [Release Defender Control v1.5 · pgkt04/defender-control · GitHub](https://github.com/pgkt04/defender-control/releases/tag/v1.5)
  - [qtkite/defender-control: An open-source windows defender manager. Now you can disable windows defender permanently. (github.com)](https://github.com/qtkite/defender-control)
  - 这个工具是命令行方式运行
  - 包含了关闭和启用两个独立的程序
- 使用关闭程序(`disable-defender`)前先手动打开安全中心,关闭病毒和威胁防护的实时防护的开关
- 这可能是设置了策略组
- ![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/92ce20637af34695b97ead7b5e2b0569.png)

#### dism++

- [Chuyu-Team/Dism-Multi-language: Dism++ Multi-language Support & BUG Report (github.com)](https://github.com/Chuyu-Team/Dism-Multi-language)

- [Chuyu Team](https://chuyu.me/en-US/index.html)
- dism++提供的系统优化中的安全设置有一个开关,功能为关闭windows defender
- 还提供了用不检查更新的功能,有效性自测

#### dControl

- [Defender Control v2.1 (sordum.org)](https://www.sordum.org/9480/defender-control-v2-1/)
  - 软件可能会报毒,毕竟它试图关闭安全中心
  - 可以从其他论坛下载,但是没必要,名字和第一个软件很像,但是这里是GUI软件

- ![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/44cf342232dd4559bff56a7f9101b2eb.png)

### UWT

[Ultimate Windows Tweaker 5 for Windows 11](https://www.thewindowsclub.com/ultimate-windows-tweaker-5-for-windows-11)

uwt有2个大版本,一个是4(对于win10),一个是5(对于win11)

软件内由一个关闭windows defender的开关,不知道有效性如何.

#### Scoop 上的相关软件

- ```powershell
  PS> scoop-search defender
  'spc' bucket:
      SordumDefenderExclusionTool-Portable (1.4)
      defender-control (2.1)
      windows-defender-control (2.869)
      windows-defender-remover (release_def_12_8_1)
  ```

  

### Microsoft defender 运行状态检查

- The `Get-MpComputerStatus` cmdlet gets the status of antimalware software installed on the computer.[Get-MpComputerStatus (Defender) | Microsoft Learn](https://learn.microsoft.com/zh-cn/powershell/module/defender/get-mpcomputerstatus?view=windowsserver2022-ps)

- ```powershell
  Get-MpComputerStatus | select AMRunningMode
  ```

  彻底关闭成功,则显示:
  
  ```powershell
  PS> Get-MpComputerStatus | select AMRunningMode                                                                         
  AMRunningMode
  -------------
  Not running
  ```
  
  

#### 杀毒软件

- 安装第三方杀毒软件后,可以关闭windows defender

#### 注册表修改

- 使用注册表的方法容易失效(随着windows的更新而失效)

  - ```bash
    PS>reg query "HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows Defender"
    
    HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows Defender\Policy Manager
    PS>reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows Defender" /v DisableAntiSpyware /t REG_DWORD /d 1
    The operation completed successfully.
    PS>reg query "HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows Defender"
    
    HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows Defender
        DisableAntiSpyware    REG_DWORD    0x1
    
    HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows Defender\Policy Manager
    ```

- 使用策略组对系统版本有要求(专业版或企业版)



## 保护历史
![在这里插入图片描述](https://img-blog.csdnimg.cn/b22eeb367fdc4f7da3089b1bdc87c03a.png)
### 恢复被认为是有病毒的文件
![windows](https://img-blog.csdnimg.cn/f34566f08a4d47ad8d4bc3c8215a6cbc.png)

### 添加信任目录,文件,文件类型或进程
- ![在这里插入图片描述](https://img-blog.csdnimg.cn/a56dbfb6a2984d21af38ed5f48d18e70.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/704f946cae224893801f2110e76367a1.png)



## windows 遥测👺

[隐私至上：5 招轻松关闭 Windows 11 遥测，防止数据收集 - 系统极客 (sysgeek.cn)](https://www.sysgeek.cn/disable-windows-11-telemetry/)

[Configure Windows diagnostic data in your organization (Windows 10 and Windows 11) - Windows Privacy | Microsoft Learn](https://learn.microsoft.com/en-us/windows/privacy/configure-windows-diagnostic-data-in-your-organization)



## TrustedInstaller权限和获取

- [NSudo/Manual/用户手册.md at master · M2TeamArchived/NSudo · GitHub](https://github.com/M2TeamArchived/NSudo/blob/master/Manual/用户手册.md)
- [Releases · M2TeamArchived/NSudo (github.com)](https://github.com/M2TeamArchived/NSudo/releases)
