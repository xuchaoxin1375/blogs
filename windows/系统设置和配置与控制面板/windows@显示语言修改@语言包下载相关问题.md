[toc]

## 显示语言混乱

- ![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/7e0a1b87bd7747b098374eedec4053d1.png)

##  删除指定目录
-  Go to the following directory to delete this folder
`C:\Windows\SoftwareDistribution\Download`
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210330091712255.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3h1Y2hhb3hpbjEzNzU=,size_16,color_FFFFFF,t_70)
##  注销或重启您的计算机
-  logout/reboot your computer
then to reinstall the  language package
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210330091847386.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3h1Y2hhb3hpbjEzNzU=,size_16,color_FFFFFF,t_70)
## windows10@11@设置搜索框的异常(总是搜不出任何结果)问题
###  显示语言配置
- 相关问题可能是显示语言设置为英文导致的(如果您的镜像是中文,而通过语言包来切换(UI显示)语言的话)
- 这或许和显示语言的设置有关
您可以考虑在显示语言来回切换以下,再尝试搜索功能
![在这里插入图片描述](https://img-blog.csdnimg.cn/dddda07fa67a4d148680d13fef8c66a4.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBAeHVjaGFveGluMTM3NQ==,size_20,color_FFFFFF,t_70,g_se,x_16)





