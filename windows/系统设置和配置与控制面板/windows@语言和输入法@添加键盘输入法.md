[toc]

#  win11


##  输入法设置

- `time&language`->`language&region`

## 添加语言设置
- 设置中搜索:`add language`
  - `time&language`->`language`->`language&option`->`[add a language]`

##  默认输入法覆盖设置👺

- `Time&language`->`typing`->`Advanced keyboard settings`

## 删除输入法

- [Remove unwanted keyboard or language from Windows 11 - Microsoft Support](https://support.microsoft.com/en-gb/surface/remove-unwanted-keyboard-or-language-from-windows-11-883fbe1c-fcf8-44bc-ba42-a834f486c058)
- time&language->language&regin->(language)options->keyboards->[your keyboard(eg.Microsoft Pinyin)]
  - 例如,可以用来临时移除搜狗输入法而不卸载输入法


## MIcrosoft Pinyin（微软拼音）

###  主题设置

- 设置中搜索`text input theme`

### 标点符号设置

- `Time & Language`>`Language & regin`>`Microsoft Pinyin`>`General`
  - Using English punctuation when in Chinese input mode
  - 在中文输入模式下使用英文标点符号。

## 添加输入法到列表中👺

有时默认输入法虽然已经安装,但是我们可以设置启用的输入法列表

对输入法列表,可以添加已安装输入法或者删除已安装输入法

通过移除输入法然后再添加回来,可以将指定输入法移动到列表末尾

![在这里插入图片描述](https://i-blog.csdnimg.cn/direct/b9ddc76c6c3044218b7f0df369050a8b.png)



- ![在这里插入图片描述](https://i-blog.csdnimg.cn/direct/186bb71e78534e808f7c37492b312538.png)

# win10部分

##  添加windows输入法键盘

- add the keyboard like this ⌨ :
- 在搜索中搜索
  - `keyboard`(英文)
    - edit language and  keyboard options

  - `输入法`(中文)
    - 编辑语言和键盘选项


## 半角切换
- 由于win10自带的输入法bug多，有时候会触发半角和全角的转换
- `Time & Language`>`Language & regin`>`Microsoft Pinyin`>`keys`

# OEM键盘

- OEM键盘是指由计算机制造商（Original Equipment Manufacturer，OEM）生产的键盘，通常配套使用于计算机系统中。OEM键盘通常没有品牌标识，外观和功能也相对简单，而且价格相对较低。

- 与OEM键盘相对的是一些知名品牌生产的键盘，它们通常有更高的质量、更多的功能和更好的用户体验，但价格也相对更高。

- 在使用OEM键盘时，可能会发现某些按键的位置或布局与其他键盘有所不同，并且可能缺少一些额外的功能键或快捷键。但OEM键盘通常可以正常工作，能够满足基本的打字和常规计算机操作的需求。

- 在vscode中配置键盘快捷键的时候经常会遇到`oem_`这类代号
  - eg:`oem_period`就是`.`键





