[toc]



## 高级重启@高级启动选项

- [启动进入 Windows 11 和 Windows 10 中的高级启动选项菜单 ](https://www.dell.com/support/kbdoc/zh-cn/000147155/booting-to-the-advanced-startup-options-menu-in-windows-10)

- [在 Windows 10 中查看安全模式和其他启动设置 - Microsoft 支持](https://support.microsoft.com/zh-cn/windows/在-windows-10-中查看安全模式和其他启动设置-7551aac3-21b5-c646-06ee-31e0e6a5e4dc)

## 查看windows登录密码

- 一般是跑字典的方法(只能跑出弱密码)
- 相关软件有`Proactive System Password Recovery`