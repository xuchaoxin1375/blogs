[toc]

## abstract

- 现在的cpu的核显基本已经比较强大了,可以满足很多场景下的需求
- 对于早一点的设备,比如2020年之前的设备,cpu的核显可能不是那么强大,渲染某些动画时会有卡顿现象,这时候如果设备带有独立显卡,那么可以考虑调用独立显卡来渲染

## windows 图形设置

- 这是英文版教程[How to choose the default GPU for gaming or apps in Windows 10 (digitalcitizen.life)](https://www.digitalcitizen.life/set-which-video-cards-are-used-apps-games-windows-10/#:~:text=How to choose the default GPU for gaming,what graphics card a game or app uses)

## 例

- 例如powerpoint的某些花哨动画,单靠cpu可能是难以流畅播放,这时候可以设置用gpu加速
- ![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/31d2523d92cf45e4b4676f098e7ec74f.png)

- powerpoint的参考路径:`C:\Program Files\Microsoft Office\root\Office16\Powerpnt.exe`