[toc]



## Android FOSS应用市场F-Droid

### F-Droid 是什么

F-Droid 是一个针对 `Android` 平台的 `FOSS` (Free and Open Source Software 免费和开源软件)应用程序的可安装目录。

客户端使浏览、安装和跟踪设备上的更新变得容易。

- [F-Droid - Free and Open Source Android App Repository](https://f-droid.org/) 可能打不开

我们可以在F-droid中下载到termux,tailscale 等开源免费软件

#### 客户端下载

- [f-droid/fdroidclient: Android client application. (github.com)](https://github.com/f-droid/fdroidclient)

- 您可以通过github release下载客户端(使用推荐国内的镜像加速连接来下载)
- 或者国内论坛下载F-Droid android客户端

#### 加速源

自带的源下载可能很慢,甚至连更新存储库都无法成功

- [F-Droid 软件仓库镜像使用帮助 - MirrorZ Help (cernet.edu.cn)](https://help.mirrors.cernet.edu.cn/fdroid/)

- [fdroid | 镜像站使用帮助 | 清华大学开源软件镜像站 | Tsinghua Open Source Mirror](https://mirrors.tuna.tsinghua.edu.cn/help/fdroid/)