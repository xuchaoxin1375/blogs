[toc]



##  live preview

![在这里插入图片描述](https://i-blog.csdnimg.cn/blog_migrate/7a509b60ef0b3a5e363f250210a8f498.png)
[download](https://marketplace.visualstudio.com/items?itemName=ms-vscode.live-server)
插件不错,支持实时预览,还支持调试工具

###  预览窗口

command palette:`show preview`

支持vscode内部预览和调用外部浏览器预览,支持随源码自动更新预览

![在这里插入图片描述](https://i-blog.csdnimg.cn/blog_migrate/c7277f96cba5164a85e9c5a40275f6fc.png)
或者用command palette

![在这里插入图片描述](https://i-blog.csdnimg.cn/blog_migrate/c4a4dda8a56c0a308096438ff890b794.png)



###  vscode内启动浏览器dev tool窗口
command palette:
输入`webview`
![在这里插入图片描述](https://i-blog.csdnimg.cn/blog_migrate/347de5aa03e9eff2545a2d94dcdae50e.png)

或者(open devtools pane)
![在这里插入图片描述](https://i-blog.csdnimg.cn/blog_migrate/b34891c0fe2ce7bd0b0a5c3c79b116df.png)
![在这里插入图片描述](https://i-blog.csdnimg.cn/blog_migrate/7587cdc306f607903d9e5995765ff92f.png)

###  限制

文件路径中不可以包含中文,否则无法成功预览

##  live server preview

允许您在路径名中存在中文.但是不支持浏览器的dev tool

![在这里插入图片描述](https://i-blog.csdnimg.cn/blog_migrate/6ab23c9393b0a672bb0efbd904d8ff6f.png)

启动

![在这里插入图片描述](https://i-blog.csdnimg.cn/blog_migrate/3713315d82934d4d0ad83b23b5da68b4.png)

##  live server

![在这里插入图片描述](https://i-blog.csdnimg.cn/blog_migrate/893d48e609478ce3e972c8305623c9c6.png)

浏览器内打开,支持实时更新(自动刷新)

启动

<i><code>alt+l alt+o<code></i>
![在这里插入图片描述](https://i-blog.csdnimg.cn/blog_migrate/ee24a78565f26d20b6ea211f9c0e3e17.png)

浏览器中浏览文件夹Go live:
![在这里插入图片描述](https://i-blog.csdnimg.cn/blog_migrate/afb4270ee4f8d7b8a44503d94843e579.png)