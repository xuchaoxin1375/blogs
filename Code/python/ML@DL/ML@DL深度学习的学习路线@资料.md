- [https://ailearning.apachecn.org](https://ailearning.apachecn.org/#/)
- [jindongwang/MachineLearning: 一些关于机器学习的学习资料与研究介绍 (github.com)](https://github.com/jindongwang/MachineLearning)

- [Mikoto10032/DeepLearning: 深度学习入门教程, 优秀文章, Deep Learning Tutorial (github.com)](https://github.com/Mikoto10032/DeepLearning)
- [datawhalechina/pumpkin-book: 《机器学习》（西瓜书）公式详解 (github.com)](https://github.com/datawhalechina/pumpkin-book)
- [fengdu78/Coursera-ML-AndrewNg-Notes: 吴恩达老师的机器学习课程个人笔记 (github.com)](https://github.com/fengdu78/Coursera-ML-AndrewNg-Notes)
- [d2l-ai/d2l-zh: 《动手学深度学习》：面向中文读者、能运行、可讨论。中英文版被60多个国家的400多所大学用于教学。 (github.com)](https://github.com/d2l-ai/d2l-zh)

## 路线

- [三个月从零入门深度学习，保姆级学习路线图_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV1yg411K72z/?spm_id_from=333.999.0.0&vd_source=c0a3b17a665cd2d32431213df84cd3ce)
- [(25 封私信 / 83 条消息) github上有哪些适合入门的机器学习或者深度学习项目？ - 知乎 (zhihu.com)](https://www.zhihu.com/question/350457961)

## 基础数学

- 视频:[可能是全网最好的《概率统计》期末速成，2小时不到冲刺60分，概率论与数理统计_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV1TJ411y7Zp/?vd_source=c0a3b17a665cd2d32431213df84cd3ce)
- 文章:[可能是全网最好的《概率统计》期末速成，2小时不到冲刺60分，概率论与数理统计 - 哔哩哔哩 (bilibili.com)](https://www.bilibili.com/read/cv14725865)

###  《概率统计》期末速成 

视频地址： [可能是全网最好的《概率统计》期末速成，2小时不到冲刺60分，概率论与数理统计](https://www.bilibili.com/video/BV1TJ411y7Zp)

## 分布函数：

![img](https://i0.hdslb.com/bfs/note/f320073195f8e567a3ea60b875c76de4b82865aa.png)





### 二项分布和泊松分布：

![img](https://i0.hdslb.com/bfs/note/8ce8c4810bebd7c5667dcd3b0049b6df12f9500b.png)

- $P(X=k)=\frac{\lambda{^k}}{k!e^{\lambda}}$

### 一维连续型随机概率：

![img](https://i0.hdslb.com/bfs/note/b60822609aa0d3652d0dfaf1970fa38da64e5985.png)

 



#### 例一：

![img](https://i0.hdslb.com/bfs/note/811f8dbb53d393d03f60f16ce8c4b60cafd18757.png)



![img](https://i0.hdslb.com/bfs/note/1b98ddcfd1a9dd80428b62cea96fc544404acb32.png)



![img](https://i0.hdslb.com/bfs/note/07e673ff92eda6b38d12a307a818ab7a4067e1dd.png)





#### 均匀分布：

![img](https://i0.hdslb.com/bfs/note/776ebbc44179cec90746d17afec91ecc9766173d.png)





#### 正态分布：

![img](https://i0.hdslb.com/bfs/note/403074a2a7d665ddd7ab9490788e27485b83c0fe.png)

- 注意：F(x) + F(-x) = 1

![img](https://i0.hdslb.com/bfs/note/d0d8a1ca8c5cfd60d44a56d83a94b4388aeb1fe6.png)

##### 标准化

![img](https://i0.hdslb.com/bfs/note/28b5a9bef70b785bbe5f7b827d0b4f07aa32e7eb.png)

例题：

![img](https://i0.hdslb.com/bfs/note/4a068c1777afcc625b35cc03523d3d82d2b8cc9f.png)

- 








#### 离散随机变量函数分布：

![img](https://i0.hdslb.com/bfs/note/787ebdcb62174a4b612fde1f8b9187058f000c6f.png)

- 注意g(X)项可能出现合并问题

![img](https://i0.hdslb.com/bfs/note/af5b0f49ffdaf8a2cb5b6502e8724c848e51d9fb.png)

#### 连续型随机变量函数分布：

![img](https://i0.hdslb.com/bfs/note/6bc1a1cf32c1681501dbd29f85df64a17764aa19.png)



例一：

![img](https://i0.hdslb.com/bfs/note/e554f3c960b8c4eee751bc2be1f1727ef2802a27.png)



![img](https://i0.hdslb.com/bfs/note/6b409c814be0255123d46b96d9a5eb36d85d4cc7.png)

例二：

![img](https://i0.hdslb.com/bfs/note/1a3d7edb20b0dd30c477cc25325822ce6af84a5a.png)



![img](https://i0.hdslb.com/bfs/note/e3bc13e8ec44dac4efada36c1a0bb856770578ad.png)



![img](https://i0.hdslb.com/bfs/note/fdad851b2543a45724bb42e6f1160e1a32cce642.png)

 

![img](https://i0.hdslb.com/bfs/note/570f22264a17f9f301efc29ca9c5fe2171204a30.png)

### 二维连续型随机变量的分布

![img](https://i0.hdslb.com/bfs/note/2b803953e1455268ad01de455230deebfa606bdf.png)





例一：

![img](https://i0.hdslb.com/bfs/note/a1ffd674a04361b426e259939b8532c93328df9c.png)

- $$
  f(x,y)=\begin{cases}
  c(6-x-y),&0<x<2,2<y<4
  \\
  0,&else
  \end{cases}
  $$

  

- 
  $$
  S=\int_{0}^{2}\int_{2}^{4}c(6-x-y)dxdy
  =c(\int_{0}^{2}\int_{2}^{4}(6-x-y)dydx)
  \\
  =c(\int_{0}^{2} ((6-x)y|_{2}^{4}-\frac{1}{2}y^2|_{2}^{4})dx)
  \\=c(\int_{0}^{2}(12-2x)-\frac{1}{2}(16-4))dx
  =c\int_{0}^{2}(6-2x)dx=c(6x-x^2)|_{0}^{2}=8c
  \\由规范性:S=8c=1\Rightarrow{c=\frac{1}{8}}
  $$

- 求$P(X+Y<4)$

  - $$
    P(X+Y<4)=\iint\limits_{X+Y<4}f(x,y)dxdy=\int_{0}^{2}dx\int_{2}^{4-x}\frac{1}{8}(6-x-y)dy=\frac{2}{3}
    $$

- 

![img](https://i0.hdslb.com/bfs/note/88487f410fed3df2fddf52d7b681159cc77204af.png)



![img](https://i0.hdslb.com/bfs/note/55e842a893a5a6c9e773594a9cdd11ddd028333e.png)

例二：

![img](https://i0.hdslb.com/bfs/note/8e3931590b47c0856ff7ceb2b72b35bd59b28c88.png)



![img](https://i0.hdslb.com/bfs/note/5a8a9b391955037c36de1bc07fb17ee1ef0e5f61.png)





![img](https://i0.hdslb.com/bfs/note/38f23991bfd3440d21b2c117ce101d3c92da66d9.png)



连续型随机变量函数的分布：

![img](https://i0.hdslb.com/bfs/note/42e71e9cb9ebed708d0174e52448316358eed4fd.png)

Z = max(X, Y):

![img](https://i0.hdslb.com/bfs/note/6d740b3daeee6fc07fabe230cbc0e2369b0516b0.png)

Z = min(X, Y):

![img](https://i0.hdslb.com/bfs/note/a3b5cc6f6fc12ac43f2d22c5c3ab2ba4677176c2.png)

 

![img](https://i0.hdslb.com/bfs/note/a8f8ec05c084193cb5ce01e109c23e4f11ac6a4d.png)



例一：

![img](https://i0.hdslb.com/bfs/note/151490f52e2be5ac56b710c25a1c1becf7ab1fd7.png)

- $F_U=\frac{x-a}{b-a}=\frac{x-0}{3-0}=\frac{x}{3}$

  - $$
    P(X>1)=1-P(X\leqslant{1})=1-F_{U}(1)=1-\frac{1}{3}=\frac{2}{3}
    $$

    





例二：

![img](https://i0.hdslb.com/bfs/note/b10be298ff9b882f363ccd99245ab875089467ec.png)



![img](https://i0.hdslb.com/bfs/note/cb6482bca18f374fcb0dc17f1f854ae8f9dd9b21.png)



### 离散数学期望：

![img](https://i0.hdslb.com/bfs/note/7bcd689f283155894eee726e65e8147e920124a0.png)

例题：

![img](https://i0.hdslb.com/bfs/note/0ccae1ac79a54d0f91f0b53e8e1f998a9eda152d.png)



### 方差和标

![img](https://i0.hdslb.com/bfs/note/dcfe8e4323ec172c1bd28d513590e5cef7615036.png)

例一：

![img](https://i0.hdslb.com/bfs/note/ee87edb833e796b9103838e69f04a8ee10277239.png)



![img](https://i0.hdslb.com/bfs/note/839172697495ae188cf6fc6d7150f16671b0d8a0.png)

- $$
  E(X^2)=\int_{-\infin}^{+\infin}x^2f(x)dx=\int_{1}^{2}x^2\frac{2}{3}xdx
  \\=\frac{1}{6}x^4|_{1}^{2}=\frac{1}{6}\times15=\frac{5}{2}
  \\
  E(X)=\int_{1}^{2}x\frac{2}{3}xdx=\frac{2}{9}x^3|_{1}^{2}=\frac{14}{9}
  D(X)=E(X^2)-E^2(X)=\frac{5}{2}-(\frac{14}{9})^2
  \\
  D(X+5)=D(X)
  $$

  





常见分布的期望和方差：

![img](https://i0.hdslb.com/bfs/note/48e63fc8d93c2dce616bc736fb0e63d4f35d5ed4.png)

例一：

如需求E(X^2)，可以先求E(X), D(X), 再套公式

![img](https://i0.hdslb.com/bfs/note/2a60296071f799f5346870a5cdf6eefb3114c3de.png)

例二：

![img](https://i0.hdslb.com/bfs/note/c841c423a50cd05b2e32c3a9b72bf1c71baa7e11.png)

例三：

![img](https://i0.hdslb.com/bfs/note/cb4426629e1c3af0437ca8fa5dcb1828e29530d2.png)

### 协方差和关系系数：

![img](https://i0.hdslb.com/bfs/note/2f295bfcdd3a4fd6a96d233233b16d4d6ab4e4ed.png)

例题：

![img](https://i0.hdslb.com/bfs/note/8239881360b57a42b28c077866b15b8e89960d05.png)



![img](https://i0.hdslb.com/bfs/note/98c6cf11d61d7123a517aafdd5d94d09b2fbb629.png)



![img](https://i0.hdslb.com/bfs/note/06fc0dea93920f0013e6fc0a862ee331d301c007.png)



![img](https://i0.hdslb.com/bfs/note/c675f1c938f1d9587f03f7da80821b6ac7895b68.png)













中心极限定理：





![img](https://i0.hdslb.com/bfs/note/74bdd42445c728ccecd94ed1ca8e86f60a7d29a5.png)

例题：

![img](https://i0.hdslb.com/bfs/note/79ded3643a39959b1dfc59756df54357d40c2cf6.png)



![img](https://i0.hdslb.com/bfs/note/b78dac4ade8478a5a288f7f3686efff0229b99c8.png)



