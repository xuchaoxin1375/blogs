

## ref

- [相对熵 - 维基百科，自由的百科全书 (wikipedia.org)](https://zh.wikipedia.org/wiki/相对熵)

## KLD

- **KL散度**（**Kullback-Leibler divergence**，简称**KLD**）[[1\]](https://zh.wikipedia.org/wiki/相对熵#cite_note-:0-1)，在讯息系统中称为**相对熵**（relative entropy），在连续时间序列中称为随机性（randomness），在统计模型推断中称为讯息增益（information gain）。也称讯息散度（information divergence）。

- **KL散度**是两个几率分布P和Q差别的非对称性的度量。 KL散度是用来度量使用基于Q的分布来编码服从P的分布的样本所需的额外的平均比特数。典型情况下，P表示数据的真实分布，Q表示数据的理论分布、估计的模型分布、或P的近似分布。[[1\]](https://zh.wikipedia.org/wiki/相对熵#cite_note-:0-1)