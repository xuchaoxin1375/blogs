## 资源下载

### 深度学习(花书)相关资源

- 下载《深度学习》的中文版 [pdf](https://github.com/MingchaoZhu/DeepLearning/releases/download/v0.0.1/DL_cn.pdf) 和英文版 [pdf](https://github.com/MingchaoZhu/DeepLearning/releases/download/v0.0.0/DL_en.pdf) 直接阅读。
  - 附加资源
    - 来自[MingchaoZhu/DeepLearning: Python for《Deep Learning》，该书为《深度学习》(花书) 数学推导、原理剖析与源码级别代码实现 (github.com)](https://github.com/MingchaoZhu/DeepLearning))
      - [深度学习_原理与代码实现.pdf](https://github.com/MingchaoZhu/DeepLearning/releases/download/v1.1.1/default.pdf) 
    - 其他翻译版本:[Releases · exacity/deeplearningbook-chinese (github.com)](https://github.com/exacity/deeplearningbook-chinese/releases)
- [ The Matrix Cookbook - Mathematics https://www.math.uwaterloo.ca › ~hwolkowi › ma...](https://www.math.uwaterloo.ca/~hwolkowi/matrixcookbook.pdf)
  - 这份公式归纳包含高于本科线性代数的范畴