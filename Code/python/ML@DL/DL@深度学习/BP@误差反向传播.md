[toc]

## 前向传播和反向传播

### 正向传播(前向传播)

- 假设学习已经全部结束，我们使用学习到的参数，先实现神经网络的“推理处理”。这个推理处理也称为神经网络的**前向传播**（forward propagation）。

### 反向传播

- [反向传播算法  (wikipedia.org)](https://zh.wikipedia.org/wiki/反向传播算法)
- 反向传播（英语：Backpropagation，意为<u>误差反向传播</u>，缩写为BP）是对多层人工神经网络进行梯度下降的算法，也就是用链式法则<u>以网络每层的权重为变数</u>计算损失函数的梯度，以<u>更新权重</u>来<u>最小化损失函数</u>。

### 从计算图的角度理解正向传播和反向传播

- 从左向右进行计算是一种正方向上的传播，简称为**正向传播**（forward propagation）。
  - 正向传播是从计算图出发点到结束点的传播。
- 考虑反向（从图上看的话，就是从右向左）的传播。这种传播称为**反向传播**（backward propagation）。

### 形式化描述

- 当我们使用前馈神经网络接收输入 x 并产生输出 $\hat{y}$ 时，信息通过网络向前流动。输入 x 提供初始信息，然后传播到每一层的隐藏单元，最终产生输出$\hat{y}$。这称之为 **前向传播**（forward propagation）。
  - 在训练过程中，前向传播可以持续向前直到它产生一个标量代价函数$J(\theta)$。
-  反向传播（back propagation）算法 (Rumelhartet al., 1986c)，经常简称为**backprop**，允许**来自代价函数的信息**通过网络向后流动，以便**计算梯度(向量)**。
  - 计算梯度的解析表达式是很直观的，但是数值化地求解这样的表达式在计算上的代价可能很大。
  - 反向传播算法使用简单和廉价的程序来实现这个目标。

### 反向传播的应用

- **反向传播**这个术语经常被误解为用于多层神经网络的整个学习算法。
  - 实际上，反向传播**仅指用于计算梯度的方法**.
  - 而另一种算法，例如<u>随机梯度下降，使用梯度来进行学习</u>。
- 此外，反向传播经常被误解为仅适用于多层神经网络，但是原则上它可以计算**任何函数的导数**（特别的,对于一些函数，正确的响应是报告函数的<u>导数是未定义</u>的）。
- 特别地，我们会描述如何计算一个任意函数 f 的梯度 $\nabla_xf(x, y)$，
  - 将函数f的输入分为了两类
    - 其中 $x$ 是一组变量组(而不是一个标量)，我们求f关于需要x的导数，
    - 而 $y$ 是函数的另外一组<u>输入变量</u>，但我们并不需要它们的导数。
    - Note:可以用向量$x_1,x_2$分别代替字母$x,y$
- 在学习算法中，我们最常需要的梯度是**代价函数关于参数的梯度**，即$\nabla_{\theta}f(\theta)$。
- 许多机器学习任务需要计算其他导数，来作为学习过程的一部分，或者用来分析学得的模型。反向传播算法也适用于这些任务，不局限于计算代价函数关于参数的梯度。通过在网络中传播信息来计算导数的想法非常普遍，它还可以用于计算诸如多输出函数 f 的 Jacobian 的值。我们这里描述的是最常用的情况，其中 f只有单个输出。

## 计算图

- 为了更精确地描述反向传播算法，使用计算图（computational graph）语言是很有帮助的。

- ```mermaid
  flowchart LR
  	a_num --->|2|x1
  	apple --->|100|x1((x)) 
  	x1 -->|200| +1((+))
  	pear --->|150|x2((x))
      p_num --->|3|x2
  	x2 -->|450| +1((+))
  	+1-->|650|x3((x))
  	tax----->|1.1|x3
  	x3-->|715|price
  
  ```

  - 正向传播

- ```mermaid
  flowchart RL
  	x1((x))
  	x2((x))
  	x3((x))
  	+1((+))
  	
  	price-..->|1|x3
  	x3-..->|1.1|+1
  	+1-..->|1.1|x1
  	+1-..->|1.1|x2
  	x1-..->|110|a_num
  	x1-..->|2.2|apple
  	x2-..->|3.3|pear
  	x2-..->|165|p_num
      x3-......->|650|tax
  ```

  - 反向传播

## 二元运算节点的反向传播

### 加法节点

- 加法节点的反向传播只乘以 1，所以输入的值会原封不动地流向下一个节点。

- $$
  z=x+y
  \\
  \frac{\partial{z}}{\partial{x}}=1
  \\
  \frac{\partial{z}}{\partial{y}}=1
  $$

  

### 乘法节点

- $$
  设z=xy
  \\
  \frac{\partial{z}}{\partial{x}}=y
  \\
  \frac{\partial{z}}{\partial{y}}=x
  $$

  

- 乘法的反向传播会将上游的值乘以正向传播时的输入信号的“翻转值”后传递给下游。
  - 原理是复合函数求导:链式法则
- 翻转值表示一种翻转关系,正向传播时信号是 x的话，反向传播时则是y；
- 正向传播时信号是y的话，反向传播时则是x。
- 因为乘法是二元运算,所以一个操作数的翻转值就是另一个操作数

### 运算层

- 把构建神经网络的“层”实现为一个类。
- 这里所说的“层”是神经网络中功能的单位。
- 比如，负责 sigmoid 函数的Sigmoid、负责矩阵乘积的 Affine等，都以层为单位进行实现。
- 计算图的实现可以层为单位来实现**乘法节点**和**加法节点**

### 激活函数层

- 将激活函数作为运算层(节点)

#### ReLU层

- $$
  y=\begin{cases}
  x,&(x>0)
  \\
  0,&(x\leqslant{0})
  \end{cases}
  \\
  \frac{\partial{y}}{\partial{x}}
  =\begin{cases}
  1,&(x>0)
  \\
  0,&(x\leqslant{0})
  \end{cases}
  $$

- 如果正向传播时的输入 x大于 0，则反向传播会将上游的值原封不动地传给下游。

- 反过来，如果正向传播时的 x小于等于 0，则反向传播中传给下游的信号将停在此处

- $$
  y=\frac{1}{1+\exp{(-x)}}
  \\
  \frac{\partial{y}}{\partial{x}}=\frac{1}{(1+\exp{(-x)})^2}\exp{(-x)}
  =\frac{1}{1+\exp{(-x)}}\frac{\exp(-x)}{1+\exp{(-x)}}
  \\=y(\frac{\exp(-x)+1-1}{1+\exp{(-x)}})=y(1-y)
  $$

### Affine层(矩阵乘法仿射层)

- 神经网络的**正向传播**中进行的**矩阵的乘积运算**在<u>几何学领域</u>被称为“**仿射变换**”.
- 几何中，仿射变换包括一次线性变换和一次平移，分别对应神经网络的加权和运算与加偏置运算。
- 将进行仿射变换的处理实现为“Affine 层”

- $$
  Y=X\cdot{W}+B
  \\
  设X为1\times{n}(向量);
  \\W为n\times{m}矩阵;(W^T为m\times{n}的矩阵)
  \\XW,B,Y均为1\times{m}向量;
  \\
  (矩阵导数的规格和矩阵一致:\frac{\partial{L}}{\partial{\Phi}}和\Phi规格一致)
  \\L为损失函数(目标h)
  \\
  X=(x_1,\cdots,x_n)
  \\
  \frac{\partial{L}}{\partial{X}}
  =(\frac{\partial{L}}{\partial{x_1}},
  \frac{\partial{L}}{\partial{x_2}},
  \cdots,\frac{\partial{L}}{\partial{x_n}})
  \\\\
  \frac{\partial{L}}{\partial{X}}=\frac{\partial{L}}{\partial{Y}}W^T(1\times{n}的向量)
  \\
  \frac{\partial{L}}{\partial{W}}=X^T\frac{\partial{L}}{\partial{Y}}(n\times{m}的矩阵)
  $$













