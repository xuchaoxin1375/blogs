[toc]

## 朴素贝叶斯分类器

- 朴素贝叶斯分类器是与上一节介绍的线性模型非常相似的一种分类器，但它的训练速度往往更快。这种高效率所付出的代价是，朴素贝叶斯模型的泛化能力要比线性分类器（如 LogisticRegression和 LinearSVC ）稍差。
  朴素贝叶斯模型如此高效的原因在于，它通过单独查看每个特征来学习参数，并从每个特征中收集简单的类别统计数据。scikit-learn 中实现了三种朴素贝叶斯分类器： GaussianNB 、BernoulliNB 和 MultinomialNB 。GaussianNB 可应用于任意连续数据，而 BernoulliNB 假定输入数据为二分类数据，MultinomialNB 假定输入数据为计数数据（即每个特征代表某个对象的整数计数，比如一个单词在句子里出现的次数）。BernoulliNB 和 MultinomialNB 主要用于文本数据分类。
- 另外两种朴素贝叶斯模型（MultinomialNB 和 GaussianNB ）计算的统计数据类型略有不同。
  MultinomialNB 计算每个类别中每个特征的平均值，而 GaussianNB 会保存每个类别中每个特征的平均值和标准差。
  要想做出预测，需要将数据点与每个类别的统计数据进行比较，并将最匹配的类别作为预测结果。有趣的是，MultinomialNB 和 BernoulliNB 预测公式的形式都与线性模型完全相同。不幸的是，朴素贝叶斯模型 coef_ 的含义与线性模型稍有不同，因为 coef_ 不同于 w 。
- 优点、缺点和参数MultinomialNB 和 BernoulliNB 都只有一个参数 alpha ，用于控制模型复杂度。alpha 的工作原理是，算法向数据中添加 alpha 这么多的虚拟数据点，这些点对所有特征都取正值。这可以将统计数据“平滑”（smoothing）。alpha 越大，平滑化越强，模型复杂度就越低。算法性能对 alpha 值的鲁棒性相对较好，也就是说，alpha 值对模型性能并不重要。但调整这个参数通常都会使精度略有提高。GaussianNB 主要用于高维数据，而另外两种朴素贝叶斯模型则广泛用于稀疏计数数据，比如文本。MultinomialNB 的性能通常要优于 BernoulliNB ，特别是在包含很多非零特征的数据集（即大型文档）上。朴素贝叶斯模型的许多优点和缺点都与线性模型相同。它的训练和预测速度都很快，训练过程也很容易理解。该模型对高维稀疏数据的效果很好，对参数的鲁棒性也相对较好。朴素贝叶斯模型是很好的基准模型，常用于非常大的数据集，在这些数据集上即使训练线性模型可能也要花费大量时间。