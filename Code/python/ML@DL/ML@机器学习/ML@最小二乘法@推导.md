[toc]



# 偏导方程组

![在这里插入图片描述](https://img-blog.csdnimg.cn/2021011216061655.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3h1Y2hhb3hpbjEzNzU=,size_16,color_FFFFFF,t_70)

# 斜率a

- ![在这里插入图片描述](https://img-blog.csdnimg.cn/20210112160036518.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3h1Y2hhb3hpbjEzNzU=,size_16,color_FFFFFF,t_70)


- ![](https://img-blog.csdnimg.cn/20210112155941306.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3h1Y2hhb3hpbjEzNzU=,size_16,color_FFFFFF,t_70)


- ![在这里插入图片描述](https://img-blog.csdnimg.cn/20210112183123925.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3h1Y2hhb3hpbjEzNzU=,size_16,color_FFFFFF,t_70)

- ![在这里插入图片描述](https://img-blog.csdnimg.cn/20210112182156347.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3h1Y2hhb3hpbjEzNzU=,size_16,color_FFFFFF,t_70)

## 另一种形式

### 转换依据

|                                                              |                                                              |                                                              |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![在这里插入图片描述](https://img-blog.csdnimg.cn/20210112185739457.png) | ![在这里插入图片描述](https://img-blog.csdnimg.cn/2021011219092242.png) | ![在这里插入图片描述](https://img-blog.csdnimg.cn/20210112181736239.png) |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |




- | ![在这里插入图片描述](https://img-blog.csdnimg.cn/20210112191510940.png) | ![在这里插入图片描述](https://img-blog.csdnimg.cn/202101121608509.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3h1Y2hhb3hpbjEzNzU=,size_16,color_FFFFFF,t_70) |      |
  | ------------------------------------------------------------ | ------------------------------------------------------------ | ---- |
  |                                                              |                                                              |      |
  |                                                              |                                                              |      |
  |                                                              |                                                              |      |

  



# 截距

同理推得


# 前奏

![在这里插入图片描述](https://img-blog.csdnimg.cn/2021011216143692.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3h1Y2hhb3hpbjEzNzU=,size_16,color_FFFFFF,t_70)

