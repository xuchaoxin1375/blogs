[toc]

## 二分类

### 混淆矩阵

- [Confusion matrix - Wikipedia](https://en.wikipedia.org/wiki/Confusion_matrix)

- 在机器学习领域和统计分类问题中，**混淆矩阵**（**英语：confusion matrix**）是可视化工具，特别用于监督学习，在无监督学习一般叫做**匹配矩阵**。

- 矩阵的每一列代表一个类的**实例预测**，而每一行表示一个实际的类的实例。

- 之所以如此命名，是因为通过这个矩阵可以方便地看出机器是否将两个不同的类混淆了（比如说把一个类错当成了另一个）。

- 混淆矩阵（也称误差矩阵 ）是一种<u>特殊的, 具有两个维度的（实际和预测）**列联表**</u>（**英语：contingency table**），并且两维度中都有着一样的类别的集合。

  - | 预测输出\真实值 | P    | N    |
    | --------------- | ---- | ---- |
    | P'              | TP   | FP   |
    | N'              | FN   | TN   |

    - 熟悉这个表格,就容易理解相关概念和公式

  - | 表1                                                          | 表2                                                          |
    | ------------------------------------------------------------ | ------------------------------------------------------------ |
    | ![在这里插入图片描述](https://img-blog.csdnimg.cn/26999fa980d84ec4911215dc1c7617b6.png) | ![在这里插入图片描述](https://img-blog.csdnimg.cn/6a2e5f753102447b8a95239ee2f96d98.png) |

- 如果已经训练好了一个系统用来区分猫和狗，那混淆矩阵就可以概括算法的测试结果以便将来的检查。假设一个13个动物的样本，8只猫和5只狗，那混淆矩阵的结果可能如下表所示

  - 在这个混淆矩阵中，系统预测了8只实际的猫(8只猫作为输入)，而分类预测系统事先是不知道这8只输入具体是什么,预测计算后"认为"其中3只是狗(即错误了3只,系统将猫分类为了狗)
  - 类似的,5只狗中，则预测有2只是猫。(系统将5只狗中的某两只错误地分类为猫)
  - 所有正确的预测都位于表格的对角线上（以粗体突出显示），因此很容易从视觉上检查表格中的预测错误，因为它们将由对角线之外的值表示。
  - 如果减少一些细节,只统计将输入的8猫5狗预出7猫6狗,这看上去好像预测的还行(最好的情况下只判错了一只)
    - 然而根据混淆矩阵揭示的细节可知,出现了许多类似`负负得正`的若干预测,实际的预测效果实际并不好

### 混淆表🎈

- 在预测分析中，混淆表（有时也称为混淆矩阵）是具有两行两列的表，该表报告假阳性，假阴性，真阳性和真阴性的数量。
- 这不仅可以进行正确分类（准确度）的分析，还可以进行更详细的分析。
- 对于分类器的**真实性能**，准确性不是可靠的指标，因为如果<u>数据集不平衡</u>（即，当不同类别中的观察数发生很大变化时），它将产生误导性结果。
  - 例如，如果数据中有95只猫，只有5条狗，
  - 特定的分类器可能会将所有观察结果归为猫。总体准确度为95％
  - 但更详细地，分类器对猫类别的识别率为100％（敏感性），<u>**对狗类别的识别率为0％**</u>。

### Precision vs F1 score vs Youden's  J statistic

- 在这种情况下，F1得分（英语：F1 score）甚至更加**不可靠**，在这种情况下，F1得分将超过97.4％，

  - 将猫作为阳性

  - $$
    P=\frac{TP}{TP+FP}=\frac{95}{95+5}=0.95
    \\
    R=\frac{TP}{TP+FN}=\frac{95}{95+0}=1.00
    \\
    F_1\ score=\frac{2}{P^{-1}+R^{-1}}= 2/(1/0.95+1/1)=
    0.974...>97.4\%
    $$

  - 若,将狗作为阳性,会遇到分母为0的情况

    

- 约登指数(后续部分介绍)则消除了这种偏见，并且将0作为乱猜情况下**能增加信息量的决定**（英语：informed decision）的概率（例如,这里总是猜测猫）。

## 灵敏度(召回率)和特异度

- 灵敏度和特异度（英语：Sensitivity and specificity），或分别称敏感性和特异性，是统计学中用来表征二项分类测试特征的数据，在统计学中也被称为统计分类，在医学中广为使用。

### 灵敏度Se

- 灵敏度（Sensitivity，也称为真阳性率、**召回率**（Recall） ）

  - $$
    R=Se=\frac{TP}{TP+FN}
    $$

    

### 特异度Sp

- 特异度（Specificity，也称为**真阴性率**）是指实际为阴性的样本中，判断为阴性的比例

  - （例如真正未生病的人中，被医院判断为未生病者的比例），计算方式是真阴性除以真阴性+假阳性（实际为阴性，但判断为阳性）的比值

- $$
  Sp=\frac{TN}{TN+FP}
  $$

  

## Youden's J statistic约登指数

- [Youden's J statistic - Wikipedia](https://en.wikipedia.org/wiki/Youden's_J_statistic)

- **Youden's J statistic** (also called **Youden's index**) is a single statistic that captures the performance of a [dichotomous](https://en.wikipedia.org/wiki/Dichotomy) diagnostic test. **Informedness** is its generalization to the multiclass case and estimates the probability of an informed decision.

  - dichotomous diagnostic test指的是二元诊断测试，即测试结果只有两种可能性的诊断测试。在医学领域中，常见的二元诊断测试包括阳性和阴性测试结果、患病和未患病的结果等。

  - 二元诊断测试通常用于评估某种疾病或者症状的存在或者缺失，或者判断某种治疗方法是否有效等。常见的二元诊断测试方法包括血液检测、尿液检测、X光检查等。

  - 二元诊断测试在机器学习领域中也有广泛的应用，如二元分类问题中的模型评估和选择最佳阈值等。

  - Youden's J statistic is:

    - ${\displaystyle J={\text{sensitivity}}+{\text{specificity}}-1}$

    - *J* = [灵敏度](https://zh.wikipedia.org/wiki/靈敏度和特異度) +[特异度](https://zh.wikipedia.org/wiki/特异度) − 1

    - $$
      \text{sensitivity}={\frac {\text{true positives}}{{\text{true positives}}+{\text{false negatives}}}}
      =\frac{TP}{TP+FN}
      \\
      \text{specificity}={\frac {\text{true negatives}}{{\text{true negatives}}+{\text{false positives}}}}
      =\frac{TN}{TN+FP}
      $$

    - 其中TP+FN表示实际阳性的数量;TN+FP表示实际阴性的数量

  - with the two <u>right-hand quantities</u>(等式右侧) being sensitivity and specificity. Thus the expanded formula is:

    - $$
      {\displaystyle J={\frac {\text{true positives}}{{\text{true positives}}+{\text{false negatives}}}}
      +{\frac {\text{true negatives}}{{\text{true negatives}}+{\text{false positives}}}}-1}
      $$

    - $J\in[-1,1]$

      - 证明:

      - $$
        TP,NP,TN,FN
        \\
        TP\leqslant{TP+FN}
        \\
        TN\leqslant{TN+FP}
        \\
        {0}\leqslant\frac{TP}{TP+FN}\leqslant{1}
        \\
        0\leqslant{\frac{TN}{TN+FP}}\leqslant{1}
        \\
        \therefore{0\leqslant{\frac{TP}{TP+FN}+\frac{TN}{TN+FP}}\leqslant{2}}
        \\
        -1\leqslant{J}\leqslant{1}
        $$

        

    - 在Youden's J统计量的计算公式中，sensitivity和specificity是指在二元分类问题中的两个重要指标，分别表示真实标签为正类的样本被正确地预测为正类的比例和真实标签为负类的样本被正确地预测为负类的比例。 

    - 在Youden's J统计量的计算公式中，sensitivity和specificity被称作右侧的两个量，是指在公式中出现在右侧的两个量。

  - Youden's J统计量（也称为Youden指数）是捕捉二元诊断测试性能的单个统计量。在二元分类问题中，Youden's J统计量是真阳性率和真阴性率之差的绝对值的最大值。

  - 在实际应用中，Youden's J统计量可以作为评价模型性能的一个指标。


### eg

- 以上面的猫狗分类为例:
  - ${\displaystyle J={\frac {95}{95+0}}+{\frac {0}{0+5}}-1=0}$ （总是猜测猫的约登指数）
    - 将猫视为阳性,将狗视为阴性
    - TP=95,TF=0(预测正确的两类情况及其次数)
      - 把猫预测为猫的次数为95
      - 把狗预测为狗的次数为0
    - NP=5,NF=0(预测错误的两类情况及其次数)
      - 把狗预测为猫的次数为5
      - 把猫预测为狗的次数为0
    - 能熟练的将四个变量和含义对应起来,是正确计算$J$的关键,将简单的例子反复练习也很有用
      - 也就是,系统若预测的对,具体怎么对,是把猫预测为猫,还是把狗预测为狗
      - 系统若预测错了,具体怎么错,是把狗预测为猫还是把猫预测为狗
- 约登指数为0的系统或测试**不具有任何作用**

## F-score

- [F-score - Wikipedia](https://en.wikipedia.org/wiki/F-score)

- In [statistical](https://en.wikipedia.org/wiki/Statistics) analysis of [binary classification](https://en.wikipedia.org/wiki/Binary_classification), the **F-score** or **F-measure** is a measure of a test's [accuracy](https://en.wikipedia.org/wiki/Accuracy_and_precision#In_binary_classification). It is calculated from the [precision](https://en.wikipedia.org/wiki/Precision_(information_retrieval)) and [recall](https://en.wikipedia.org/wiki/Recall_(information_retrieval)) of the test, where :

- 精确率是指预测为正例的样本中真正为正例的比例

- 召回率是指所有真正为正例的样本中被预测为正例的比例


### 精确率P(阳性预测值)

- 也叫**阳性预测值**

- the **precision** is the number of <u>true positive results</u> divided by the number of <u>all positive results</u>, including those not identified correctly

  - $$
    P=\frac{TP}{TP+FP}
    $$


- P=真阳例数/预测阳性总数

### 召回率R

- 也叫**灵敏度**

- the **recall** is the number of <u>true positive results</u> divided by the number of all samples that <u>should have been identified as positive</u>. 

- Precision is also known as [positive predictive value](https://en.wikipedia.org/wiki/Positive_predictive_value), and recall is also known as [sensitivity](https://en.wikipedia.org/wiki/Sensitivity_and_specificity) (零名度)in diagnostic binary classification.

  - $$
    R=\frac{TP}{TP+FN}
    $$

- R=真阳/实际阳性总数



### relevant instance 和 retrieved instance

- 在信息检索领域中，通常会使用相关实例（relevant instance）和检索实例（retrieved instance）这两个概念。
  - 相关实例是指与查询相关的文档或信息，也称为相关文档或相关信息。在信息检索任务中，我们需要从文本库中检索出与查询相关的文档或信息，这些文档或信息就是相关实例。
  - 检索实例是指通过检索算法检索到的文档或信息，也称为检索文档或检索信息。在信息检索任务中，我们通常会使用检索算法来从文本库中检索出一些文档或信息，这些文档或信息就是检索实例。

- 相关实例和检索实例的关系可以用以下四个术语来描述：

  1. True Positive（TP）：检索出的文档或信息是相关实例。
  2. False Positive（FP）：检索出的文档或信息不是相关实例。
  3. True Negative（TN）：未检索出的文档或信息不是相关实例。
  4. False Negative（FN）：未检索出的文档或信息是相关实例。

- | ![在这里插入图片描述](https://img-blog.csdnimg.cn/e333f47c43a04c869f0eabf4efa55188.png) | $\large{P=\frac{\text{true positives}}{\text{retrived elemnts}}}$ | $\large{R=\frac{\text{true positives}}{\text{relevant elemnts}}}$ |
  | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
  |                                                              | retrieve elements是分类系统预测出来的TP总数                  | relevant elements是样本实际的实际阳性的总数                  |

- retrived=TP+NP

- relevant=TP+FN

### 其他称呼🎈

- Precision（又称为**阳性预测值**）是指检索到的**相关实例**数在<u>所有检索到的实例</u>中所占的比例
- Recall（也称为**敏感性**）是指检索到的**相关实例**数在<u>所有相关实例</u>中所占的比例。
- 因此，精确率和召回率都是基于所涉及的相关实例而计算的。

- **Precision** (also called [positive predictive value](https://en.wikipedia.org/wiki/Positive_predictive_value)) is the fraction of **relevant** instances among the **retrieved** instances,

  - 所有被预测为阳性的样本中真实为阳性的样本的占比
- **recall** (also known as [sensitivity](https://en.wikipedia.org/wiki/Sensitivity_and_specificity)) is the fraction of relevant instances that were retrieved. 

  - 所有真实阳性样本中,被正确预测(为阳性)的样本数占比
  - 灵敏度=特异度=真阳性率=召回率
- Both precision and recall are therefore based on [relevance](https://en.wikipedia.org/wiki/Relevance_(information_retrieval)).

- 精确率和召回率都是基于“相关实例”的概念而计算的，其中“相关实例”是指在某个特定的场景中被认为是重要或有意义的实例。在信息检索和文本分类等任务中，相关实例通常是指与查询或主题相关的文档或文本片段。

  这两个指标通常用于评估二元分类模型的性能，特别是在不平衡类别的情况下。在这种情况下，模型可能倾向于预测数量较多的类别，而忽略数量较少但更重要的类别。精确率和召回率可以帮助我们评估模型在处理不平衡类别时的表现，并帮助我们在模型和任务之间做出权衡。

- Consider a computer program for recognizing dogs (the **relevant** element) in a digital photograph. Upon processing a picture which contains ten cats and twelve dogs, the program identifies eight dogs. Of the eight elements identified as dogs, only five actually are dogs (true positives), while the other three are cats (false positives). Seven dogs were missed (false negatives), and seven cats were correctly excluded (true negatives). The program's precision is then 5/8 (true positives / selected elements) while its recall is 5/12 (true positives / relevant elements).

- The $F_1$ score is the <u>harmonic mean</u> of the precision and recall. It thus symmetrically represents both precision and recall in one metric. The more generic $F_{\beta }$ score applies additional weights, valuing one of precision or recall more than the other.
  The highest possible value of an F-score is 1.0, indicating perfect precision and recall, and the lowest possible value is 0, if either precision or recall are zero.
- **F值**，亦被称做**F-measure**，是一种量测算法的[精确度](https://zh.wikipedia.org/wiki/精確度)常用的[指标](https://zh.wikipedia.org/wiki/指標_(電腦科學))，经常用来判断[算法](https://zh.wikipedia.org/wiki/算法)的精确度。目前在辨识、侦测相关的算法中经常会分别提到**精确率**（precision）和**召回率**（recall），F-score能同时考虑这两个数值，平衡地反映这个算法的[精确度](https://zh.wikipedia.org/wiki/精確度)。



### $F_1$

- The traditional F-measure or balanced F-score (F1 score) is the harmonic mean of precision and recall:
  ${\displaystyle F_{1}={\frac {2}{\mathrm {recall} ^{-1}+\mathrm {precision} ^{-1}}}=2{\frac {\mathrm {precision} \cdot \mathrm {recall} }{\mathrm {precision} +\mathrm {recall} }}={\frac {2\mathrm {tp} }{2\mathrm {tp} +\mathrm {fp} +\mathrm {fn} }}}$.
- 一般上来说，提到F-score且没有特别的定义时，是指$\beta =1$时的F-score，亦有写作$F_1$-score。
- 代表使用者同样的注重precision和recall的这两个指标。其分数可以说是precision和recall的调和平均

### sklearn中的F1_score

- [sklearn.metrics.f1_score — scikit-learn documentation](https://scikit-learn.org/stable/modules/generated/sklearn.metrics.f1_score.html)
- [3.3. Metrics and scoring: quantifying the quality of predictions — scikit-learn documentation](https://scikit-learn.org/stable/modules/model_evaluation.html#precision-recall-f-measure-metrics)
- [sklearn.metrics.precision_recall_fscore_support — scikit-learn  documentation](https://scikit-learn.org/stable/modules/generated/sklearn.metrics.precision_recall_fscore_support.html)

  - 这个函数是其他衍生函数(`fbeta_score`和`f1_score`)的基础,包含了计算公式的运用
  - 通过查看`f1_score`源码可以追踪到`precision_recall_fscore_support`的源码实现
- 以下主要讨论多分类下的`f1_score`的计算行为
- In the multi-class and multi-label case, this is the average of the F1 score of each class with weighting depending on the average parameter.

  在多类别和多标签的情况下，f1_score的计算方式与二分类的情况略有不同。具体来说，它是每个类别的F1分数加权平均，其中权重取决于average参数。
- 在sklearn中，可以使用`sklearn.metrics.f1_score`函数来计算多类别和多标签的f1_score。该函数具有以下参数：

  - `y_true`：真实标签数组或稀疏矩阵
  - `y_pred`：预测标签数组或稀疏矩阵
  - `labels`：类别列表（可选）
  - `average`：加权方式，可选值为None、'micro'、'macro'、'weighted'和'samples'，默认为None
  - `sample_weight`：每个样本的权重数组（可选）

#### labels参数

The set of labels to include when average != 'binary', and their order if average is None. Labels present in the data can be excluded, for example to calculate a multiclass average ignoring a majority negative class, while labels not present in the data will result in 0 components in a macro average. For multilabel targets, labels are column indices. By default, all labels in y_true and y_pred are used in sorted order.

- 该参数用于指定在计算精确度、召回率和F1分数时要包含的标签集合。

- 当`average != 'binary'`时，通过`labels`参数指定标签集合；当`average=None`时，通过`labels`参数指定标签集合的顺序。

- 如果数据中存在某些标签不需要考虑，可以通过`labels`参数将其排除在外。
- 例如，可以计算一个多分类问题的平均值，忽略一个主要的负类别；而对于数据中不存在的标签，在宏平均中将会产生0的结果。

对于多标签目标，标签是列索引。默认情况下，`y_true`和`y_pred`中的所有标签都按照排序顺序使用。

- 以下是一个示例，演示如何使用`labels`参数在多分类问题中指定要包含的标签集合：

```python
from sklearn.metrics import precision_recall_fscore_support,classification_report
import numpy as np

y_true = np.array([0, 1, 2, 0, 1, 2])
y_pred = np.array([0, 2, 1, 0, 0, 1])
#从y_true中可以看出,输入的数组包含3个类别[0,1,2]
# 以类别0为例,tp=2,fp=1,fn=0从而P=tp/(tp+fp)=2/(2+1)=2/3;R=tp/(tp+fn)=2/(2+0)=1
#类别1,2由于他们的tp值都是0,所以他们的P,R值也都是0
print(classification_report(y_true=y_true,y_pred=y_pred))
print('use labels param in `precision_recall_fscore_support`function:')
labels = [0,1,2]
def labels_demo(labels=None):
    print(f"{np.unique(y_true)=}")
    print(labels,"@{labels}set done!")

    precision, recall, f1_score, support = precision_recall_fscore_support(
        y_true, y_pred, average=None, labels=labels)

    print("Precision:", precision)
    print("Recall:", recall)
    print("F1 score:", f1_score)
    print("Support:", support)
    print()
labels_demo(labels=[0,1,2])
labels_demo(labels=[0,2])

```

- 在这个例子中，我们指定了`labels`参数为`[0, 2]`，表示只计算标签0和标签2的精确度、召回率和F1分数。
- 为了便于比较,我在例程前调用classification_report综合展示各个metric

- 输出结果为：

```bash
              precision    recall  f1-score   support

           0       0.67      1.00      0.80         2
           1       0.00      0.00      0.00         2
           2       0.00      0.00      0.00         2

    accuracy                           0.33         6
   macro avg       0.22      0.33      0.27         6
weighted avg       0.22      0.33      0.27         6

use labels param in `precision_recall_fscore_support`function:
np.unique(y_true)=array([0, 1, 2])
[0, 1, 2] @{labels}set done!
Precision: [0.66666667 0.         0.        ]
Recall: [1. 0. 0.]
F1 score: [0.8 0.  0. ]
Support: [2 2 2]

np.unique(y_true)=array([0, 1, 2])
[0, 2] @{labels}set done!
Precision: [0.66666667 0.        ]
Recall: [1. 0.]
F1 score: [0.8 0. ]
Support: [2 2]
```

- 这意味着在这个例子中，我们只计算了标签0和标签2的精确度、召回率和F1分数，而标签1被排除在外。
- 例如，标签0的精确度为1.0，召回率为0.6667，F1分数为0.8。
- 标签2的精确度为0.5，召回率为0.3333，F1分数为0.4。

#### average参数

- `average{‘micro’, ‘macro’, ‘samples’, ‘weighted’, ‘binary’} or None, default=’binary’`
  - This parameter is required for multiclass/multilabel targets. 
- If `None`, the scores for each class are returned. Otherwise, this determines the type of averaging performed on the data:

- `'binary'`:

  Only report results for the class specified by `pos_label`. This is applicable only if targets (`y_{true,pred}`) are binary.

- `'micro'`:

  Calculate metrics **globally** by **counting** the total `true positives`, `false negatives` and `false positives`.

- `'macro'`:

  Calculate metrics for each label, and find their **unweighted mean**. This does not take label imbalance into account.

  计算每个标签的指标，并找到它们的未加权平均值。这并没有考虑标签的不平衡

- `'weighted'`:

  Calculate metrics for each label, and find their **average weighted by <u>support</u>** (the number of true instances for each label). This alters ‘macro’ to account for label imbalance; it can result in an F-score that is not between precision and recall.计算每个标签的度量指标，并按每个标签的真实实例数加权计算它们的平均值。这个过程中考虑了标签的不平衡，从而修改了“macro”方法；这可能会导致F1分数不在精确率和召回率之间。

- `'samples'`:

  Calculate metrics for each instance, and find their average (only meaningful for multilabel classification where this differs from [`accuracy_score`](https://scikit-learn.org/stable/modules/generated/sklearn.metrics.accuracy_score.html#sklearn.metrics.accuracy_score)).

#### eg

- `precision_recall_fscore_support`是Scikit-learn库中的一个函数，用于计算分类模型的精确度（precision）、召回率（recall）、F1分数（F1-score）和支持度（support）。该函数的使用方式为：

  ```python
  from sklearn.metrics import precision_recall_fscore_support
  
  precision, recall, f1_score, support = precision_recall_fscore_support(y_true, y_pred, average=None)
  ```

  其中，`y_true`和`y_pred`分别是真实标签和预测标签。`average`参数可以为None、'binary'、'micro'、'macro'或'weighted'，表示计算精确度、召回率和F1分数时采用的方法，具体含义如下：

  - None：对每个类别分别计算精确度、召回率和F1分数。
  - 'binary'：仅在二分类问题中使用，返回正例（positive）的精确度、召回率和F1分数。
  - 'micro'：将所有类别的真实标签和预测标签**合并**后计算精确度、召回率和F1分数。
  - 'macro'：对每个类别分别计算精确度、召回率和F1分数，并对它们取平均值。
  - 'weighted'：对每个类别分别计算精确度、召回率和F1分数，并按照每个类别的样本数加权平均。
    - Formally, the weighted mean of a non-empty finite tuple of data ${\displaystyle \left(x_{1},x_{2},\dots ,x_{n}\right)}$, with corresponding non-negative weights ${\displaystyle \left(w_{1},w_{2},\dots ,w_{n}\right)}$ is
      ${\displaystyle {\bar {x}}={\frac {\sum \limits _{i=1}^{n}w_{i}x_{i}}{\sum \limits _{i=1}^{n}w_{i}}},}$
      which expands to:
      ${\displaystyle {\bar {x}}={\frac {w_{1}x_{1}+w_{2}x_{2}+\cdots +w_{n}x_{n}}{w_{1}+w_{2}+\cdots +w_{n}}}.}$

  函数返回的四个值分别为每个类别的精确度、召回率、F1分数和支持度（即每个类别的样本数）。

- ```python
  import numpy as np
  from sklearn.metrics import precision_recall_fscore_support
  y_true = np.array(['cat', 'dog', 'pig', 'cat', 'pig','pig'])
  y_pred = np.array(['cat', 'pig', 'dog', 'cat', 'dog','pig'])
  prfs=precision_recall_fscore_support
  r0=prfs(y_true, y_pred)
  
  r1=prfs(y_true, y_pred, average='macro')
  
  r2=prfs(y_true, y_pred, average='micro')
  #tp:(cat,dog,pig)=(2,0,1)
  #fp:(cat,dog,pig)=(0,2,1)
  #fn:(cat,dog,pig)=(0,1,2)
  #total:(tp,fp,fn)=(3,3,3)@就是对每一行元组求和
  #P=tp/(fp+fn)=3/(3+3)=0.5,R=tp/(tp+nf)=3/(3+3)=0.5
  r3=prfs(y_true, y_pred, average='weighted')
  print(f"{r0=}@None\n{r1=}@macro\n{r2=}@micro\n{r3=}@weighted")
  ```

  - ```bash
    r0=(array([1. , 0. , 0.5]), array([1.        , 0.        , 0.33333333]), array([1. , 0. , 0.4]), array([2, 1, 3], dtype=int64))@None
    r1=(0.5, 0.4444444444444444, 0.4666666666666666, None)@macro
    r2=(0.5, 0.5, 0.5, None)@micro
    r3=(0.5833333333333334, 0.5, 0.5333333333333333, None)@weighted
    ```

- 手动计算三个不同的average参数下的结果🎈:

  ```bash
  # macro
  ma=[item.mean() for item in r0][:-1]
  # micro
  tp, fp, fn = [np.count_nonzero(y_true == y_pred), np.count_nonzero(
      y_true != y_pred), np.count_nonzero(y_true != y_pred)]
  P, R = tp/(tp+fp), tp/(tp+fn)
  mb=[P, R, 2/(1/P+1/R)]
  #weighted1(method1)
  wt=[(item*(r0[3]/r0[3].sum())).sum() for item in r0][:-1]
  print(f"{ma=}\n{mb=}\n{wt=}")
  #weighted2
  [((item*r0[3])/r0[3].sum()).sum() for item in r0][:-1]
  #weighted3
  [((item*r0[3]).sum() / r0[3].sum())for item in r0][:-1]
  ```

  - ```bash
    ma=[0.5, 0.4444444444444444, 0.4666666666666666]
    mb=[0.5, 0.5, 0.5]
    wt=[0.5833333333333333, 0.5, 0.5333333333333333]
    ```

- 随机化版本

  ```python
  import numpy as np
  from sklearn.metrics import precision_recall_fscore_support
  
  categories=['dog','cat','pig']
  y_true = np.random.choice(categories,size=10)
  y_pred = np.random.choice(categories,size=10)
  print(y_true,"@{y_true}\n",y_pred,"@{y_pred}")
  prfs = precision_recall_fscore_support
  r0 = prfs(y_true, y_pred)
  
  r1 = prfs(y_true, y_pred, average='macro')
  
  r2 = prfs(y_true, y_pred, average='micro')
  
  r3 = prfs(y_true, y_pred, average='weighted')
  print(f"{r0=}@None\n{r1=}@macro\n{r2=}@micro\n{r3=}@weighted")
  
  ```

  - ```bash
    r0=(array([0.66666667, 0.        , 0.        ]), array([1., 0., 0.]), array([0.8, 0. , 0. ]), array([2, 2, 2], dtype=int64))
    r1=(0.2222222222222222, 0.3333333333333333, 0.26666666666666666, None)
    r2=(0.3333333333333333, 0.3333333333333333, 0.3333333333333333, None)
    r3=(0.2222222222222222, 0.3333333333333333, 0.26666666666666666, None)
    ```

  - ```bash
    ['pig' 'dog' 'cat' 'cat' 'cat' 'cat' 'dog' 'cat' 'dog' 'cat'] @{y_true}
     ['dog' 'cat' 'cat' 'dog' 'dog' 'pig' 'dog' 'cat' 'cat' 'dog'] @{y_pred}
    r0=(array([0.5, 0.2, 0. ]), array([0.33333333, 0.33333333, 0.        ]), array([0.4 , 0.25, 0.  ]), array([6, 3, 1], dtype=int64))@None
    r1=(0.2333333333333333, 0.2222222222222222, 0.21666666666666667, None)@macro
    r2=(0.3, 0.3, 0.3, None)@micro
    r3=(0.36, 0.3, 0.31500000000000006, None)@weighted
    ```

    

#### eg

以下是一个示例，演示如何使用`sklearn.metrics.f1_score`函数计算多类别和多标签的f1_score：

```python
from sklearn.metrics import f1_score

# 假设模型的预测结果保存在y_pred变量中，真实标签保存在y_true变量中
y_true = [[1, 0, 0],
          [0, 1, 1],
          [1, 0, 1],
          [0, 0, 1]]
y_pred = [[1, 0, 1],
          [1, 1, 0],
          [0, 0, 1],
          [0, 0, 1]]

# 计算不同加权方式下的f1_score
f1_micro = f1_score(y_true, y_pred, average='micro')
f1_macro = f1_score(y_true, y_pred, average='macro')
f1_weighted = f1_score(y_true, y_pred, average='weighted')

print("f1_score (micro):", f1_micro)
print("f1_score (macro):", f1_macro)
print("f1_score (weighted):", f1_weighted)
```

- 在这个例子中，我们假设模型对4个样本进行了标签预测，每个样本都有3个可能的标签。真实标签和预测标签分别保存在y_true和y_pred变量中。然后，我们使用`f1_score`函数计算不同加权方式下的f1_score。

- 输出结果为：

  - ```bash
    f1_score (micro): 0.6666666666666666
    f1_score (macro): 0.7222222222222222
    f1_score (weighted): 0.6666666666666666
    ```

- 这意味着，在micro加权方式下，所有类别的F1分数被平均，得到了一个总体的F1分数；
- 在macro加权方式下，每个类别的F1分数被计算并加权平均，而不考虑类别的样本数量；
- 在weighted加权方式下，每个类别的F1分数被计算并加权平均，考虑类别的样本数量。在本例中，模型的f1_score值相对较低，但可能取决于所选择的加权方式。

### precision_recall_curve@thresholds

- [sklearn.metrics.precision_recall_curve — scikit-learn documentation](https://scikit-learn.org/stable/modules/generated/sklearn.metrics.precision_recall_curve.html)

- The `precision_recall_curve` computes a precision-recall curve from the ground truth label and a score given by the classifier by varying a decision threshold.

  `precision_recall_curve`函数可以从分类器给出的得分和真实标签中，通过改变决策阈值来计算出一个精度-召回率曲线。

- 在机器学习中，分类器通常会输出一个得分（score），用于表示样本属于某个类别的概率。通过将得分与决策阈值进行比较，可以将每个样本分配到不同的类别中。在二分类问题中，通常将得分大于阈值的样本视为正类，将得分小于阈值的样本视为负类。

- 但是，决策阈值的选择会影响到分类器的性能指标，如准确率和召回率。因此，为了评估分类器在不同决策阈值下的性能，可以使用精度-召回率曲线。

### $F_β$ score🎈

- A more general F score, $F_{\beta }$, that uses a positive real factor $\beta$, where $\beta$ is chosen such that recall is considered $\beta$ times as important as precision, is:

  - $$
    F_\beta = (1 + \beta^2) \cdot \frac{\mathrm{precision} \cdot \mathrm{recall}}{(\beta^2 \cdot \mathrm{precision}) + \mathrm{recall}}
    $$

  - $F_1$-Score就是$\beta=1$时的$F_\beta$一种特殊情况

- In terms of <u>Type I and type II errors</u> this becomes:(用TP/FN/FP)表示

  - $$
    F_\beta = \frac {(1 + \beta^2) \cdot \mathrm{true\ positive} }{(1 + \beta^2) \cdot \mathrm{true\ positive} + \beta^2 \cdot \mathrm{false\ negative} + \mathrm{false\ positive}}\,
    $$

- Two commonly used values for $\beta$ are 2, which weighs recall higher than precision, and 0.5, which weighs recall lower than precision.

## 调和平均数@harmonic mean

- 调和平均数（英语：harmonic mean），在数学中，调和平均数是几种平均数之一，特别的,是毕达哥拉斯平均数之一。当需要计算平均速率时，调和平均数是一个合适的选择。In [mathematics](https://en.wikipedia.org/wiki/Mathematics), the **harmonic mean** is one of several kinds of [average](https://en.wikipedia.org/wiki/Average), and in particular, one of the [Pythagorean means](https://en.wikipedia.org/wiki/Pythagorean_means). It is sometimes appropriate for situations when the average [rate](https://en.wikipedia.org/wiki/Rate_(mathematics)) is desired.

- 调和平均数是将<u>所有数值取倒数</u>并求其算术平均数后，再将此算术平均数取倒数而得，其结果等于数值的个数除以数值倒数的总和。

- 一组正数${\displaystyle x_{1},x_{2},\cdots ,x_{n}}$的调和平均数$H$其计算公式为：

  - $$
    H={\frac  {n}{{\frac  {1}{x_{1}}}+{\frac  {1}{x_{2}}}+...+{\frac  {1}{x_{n}}}}}
    =({\frac{1}{n}{\sum _{{i=1}}^{n}{\frac  {1}{x_{i}}}}})^{-1}
    ={\frac  {n}{\sum _{{i=1}}^{n}{\frac  {1}{x_{i}}}}}
    $$

#### 直观解释和例子

- 可以将调和平均数看作是一组数据的倒数的平均值。如果一组数据表示的是速度或者频率等与时间有关的物理量，那么对它们求调和平均数可以得到平均速度或平均频率。
- 举个例子，如果一个人在往返两地的路上以 4m/s 的速度去，以 6m/s 的速度回来，那么这个人的平均速度就可以用这两个速度的调和平均数计算得出。
  - 我们可以先从小学的角度计算这个来回平均速度
  - 设A,B两地距离为S,从A到B以$4m/s$的速度前进,耗时$t_1$;B到A的速度以$6m/s$速度前进,耗时$t_2$
  - 则$S=4t_1=6t_2$
    - $t_1=\frac{1}{4}S$
    - $t_2=\frac{1}{6}S$
  - $\overline{v}=\frac{2S}{t_1+t_2}=\frac{2S}{\frac{1}{4}S+\frac{1}{6}S}$
    - $=\frac{24}{3+2}=4.8m/s$
  - 如果不稍加思考,容易得出平均速度为$\frac{1}{2}(4+6)=5m/s$的错误结论
    - 因为$t_1:t_2=3:2$,$t_1\neq{t_2}$,不能够直接计算速度的平均值
    - 并且对比上述形式,符合调和平均数的形式
    - 由于S会被消掉,所以可以设路程为1,并且将两个速度设为$x_1,x_2$,则$\overline{v}=\frac{S}{x_1^{-1}+x_2^{-1}}$



## ROC曲线

- [Receiver operating characteristic - Wikipedia](https://en.wikipedia.org/wiki/Receiver_operating_characteristic)

  - 在[信号检测理论](https://zh.wikipedia.org/wiki/信号检测理论)中，**接收者操作特征曲线**，或者叫**ROC曲线**（英语：Receiver operating characteristic curve），是一种坐标图式的分析工具，用于选择最佳的信号侦测模型、舍弃次佳的模型或者在同一模型中设置最佳[阈值](https://zh.wikipedia.org/wiki/阈值)。
  - 在做决策时，ROC分析能不受成本／效益的影响，给出客观中立的建议。
  - 数十年来，ROC分析被用于[医学](https://zh.wikipedia.org/wiki/医学)、[无线电](https://zh.wikipedia.org/wiki/无线电)、[生物学](https://zh.wikipedia.org/wiki/生物學)、[犯罪心理学](https://zh.wikipedia.org/wiki/犯罪心理學)领域中，而且最近在[机器学习](https://zh.wikipedia.org/wiki/机器学习)（machine learning）和[数据挖掘](https://zh.wikipedia.org/wiki/数据挖掘)（data mining）领域也得到了很好的发展。

- ROC（Receiver Operating Characteristic）曲线是一种用于评估二分类模型性能的可视化工具。

- | 预测输出\真实值 | P    | N    |
  | --------------- | ---- | ---- |
  | P'              | TP   | FP   |
  | N'              | FN   | TN   |

- ROC曲线（Receiver Operating Characteristic Curve）是用于评估二分类模型性能的常用工具，它可以展示在给定阈值下真阳性率（true positive rate，TPR）与假阳性率（false positive rate，FPR）之间的关系。通常情况下，TPR表示所有正例中被正确预测为正例的比例，FPR表示所有负例中被错误预测为正例的比例。

- 特异度:specificity, selectivity or true negative rate (TNR)

  - ${\displaystyle \mathrm {TNR} ={\frac {\mathrm {TN} }{\mathrm {N} }}={\frac {\mathrm {TN} }{\mathrm {TN} +\mathrm {FP} }} }$

- 在ROC曲线上，

  - **横轴**表示**假**阳性率（False Positive Rate，FPR），即被错误地预测为正类的负样本数(FP)占总负样本数(TN+FP)的比例；
    - fall-out or false positive rate (FPR)
      ${\displaystyle \mathrm {FPR} ={\frac {\mathrm {FP} }{\mathrm {N} }}={\frac {\mathrm {FP} }{\mathrm {FP} +\mathrm {TN} }}=1-\mathrm {TNR} }$
    - 这反映的是分类器对阴性样本预测的错误率(FPR的计算不依赖于阳性样本的预测情况)
  - **纵轴**表示**真**阳性率（True Positive Rate，TPR），即被正确地预测为正类的正样本数占总正样本数的比例。
    - sensitivity, **recall**, hit rate, or true positive rate (TPR)(回召率|命中率)
      ${\displaystyle \mathrm {TPR} ={\frac {\mathrm {TP} }{\mathrm {P} }}={\frac {\mathrm {TP} }{\mathrm {TP} +\mathrm {FN} }}=1-\mathrm {FNR} }$
    - 反映分类器对阳性样本预测的正确率(TPR的计算不依赖于阴性样本的预测情况)

- ROC曲线能够展示在不同的分类阈值下，模型的真阳性率和假阳性率的变化情况，从而帮助我们更好地理解模型在不同阈值下的性能表现。

### 分析ROC曲线

- 在ROC曲线上，理想的分类器应该尽可能地靠近左上角，**此时真阳性率很高，而假阳性率很低**。

- 如果分类器在ROC曲线上的表现是一条从左下角到右上角的直线，那么它的预测能力等同于随机猜测，即没有预测能力。

- 下面是如何解读ROC曲线的步骤：

  1. 观察曲线的形状：ROC曲线通常是一个二维图形，横轴表示FPR，纵轴表示TPR。可以通过观察曲线的形状来初步判断模型的性能。如果曲线接近左上角，说明模型具有较好的性能，如果曲线接近对角线，则说明模型性能较差。
  2. 查看AUC值：AUC（Area Under the Curve）是ROC曲线下的面积，其取值范围是0到1，AUC值越大，模型的性能越好。通常将AUC值大于0.5的模型视为有一定预测能力。
  3. 确定最佳阈值：ROC曲线可以帮助我们找到最佳的阈值。在ROC曲线上，最佳阈值是指**距离左上角最近的点**，通常可以通过将不同阈值下的TPR和FPR画在ROC曲线上来确定最佳阈值。
  4. 比较不同模型：ROC曲线可以用于比较不同模型的性能。如果两个模型的ROC曲线都接近左上角，但一个模型的ROC曲线在另一个模型的上方，那么可以认为前者的性能更好。

- 总之，ROC曲线可以帮助我们评估模型的性能、找到最佳阈值、比较不同模型的性能等。同时，ROC曲线也具有可视化的特点，可以直观地展示模型的性能。

- ROC（Receiver Operating Characteristic）曲线是一种用于评估分类模型性能的工具。通过改变分类器的阈值，我们可以得到一系列不同的真阳性率（True Positive Rate，TPR）和假阳性率（False Positive Rate，FPR）组合，从而绘制出ROC曲线。ROC曲线下的面积（Area Under the Curve，AUC）可以用来衡量分类器的整体性能。

- 找到ROC曲线上的最佳阈值意味着在这个阈值下，分类器的性能达到了某种最优。这个最优可以是根据某种标准，例如最小化错误率、最大化正确率、最大化准确率等。在实际应用中，确定最佳阈值通常需要根据问题的具体需求来决定。

  最佳阈值的意义主要有以下几点：

  1. **平衡TPR和FPR**：在不同的阈值下，TPR和FPR的值会发生变化。找到最佳阈值可以帮助我们在这两者之间找到一个平衡点，从而实现在某种意义上的最优性能。
  2. **调整分类器性能**：通过改变阈值，我们可以调整分类器的灵敏度和特异性，使其适应不同的应用场景。例如，在某些情况下，我们可能希望降低FPR，即使这意味着TPR也会降低。通过找到最佳阈值，我们可以实现这种需求。
  3. **评估模型的泛化能力**：最佳阈值可以帮助我们评估分类器在新的数据集上的性能。如果一个模型在训练集上的最佳阈值在测试集上也能取得较好的性能，说明该模型具有较好的泛化能力。
  4. **指导模型优化**：最佳阈值可以为我们提供有关模型性能的信息，从而指导我们进行进一步的模型优化。例如，我们可以根据最佳阈值的变化来调整模型的参数，以实现更好的性能。

### AUC

- ROC曲线下面积（Area Under Curve，AUC）也是评估模型性能的一个重要指标，AUC的取值范围在0.5到1之间，AUC越接近1，表示模型性能越好。

#### 例

- **分类模型**（又称[分类器](https://zh.wikipedia.org/w/index.php?title=分类器&action=edit&redlink=1)，或[诊断](https://zh.wikipedia.org/wiki/診斷)）是将一个实例映射到一个特定类的过程。

- ROC分析的是[二元分类模型](https://zh.wikipedia.org/w/index.php?title=二元分類模型&action=edit&redlink=1)，也就是输出结果只有两种类别的模型，例如：（阳性／阴性）（有病／没病）（垃圾邮件／非垃圾邮件）（敌军／非敌军）。

- 当**信号侦测**（或**变量测量**）的结果是一个[连续值](https://zh.wikipedia.org/w/index.php?title=連續值&action=edit&redlink=1)时，类与类的边界必须用一个[阈值](https://zh.wikipedia.org/wiki/阈值)（英语：threshold）来界定。举例来说，用血压值来检测一个人是否有[高血压](https://zh.wikipedia.org/wiki/高血压)，测出的血压值是连续的实数（从0~200都有可能），以收缩压140／舒张压90为阈值，阈值以上便诊断为有高血压，阈值未满者诊断为无高血压。二元分类模型的个案预测有四种结局：
  1. **真阳性**（TP）：**诊断为有**，**实际上也有**高血压。
  2. **伪阳性**（FP）：**诊断为有**，**实际却没有**高血压。
  3. **真阴性**（TN）：**诊断为没有**，**实际上也没有**高血压。
  4. **伪阴性**（FN）：**诊断为没有**，**实际却有**高血压。

### sklearn_demo

- ```python
  from sklearn.datasets import make_classification
  from sklearn.model_selection import train_test_split
  from sklearn.linear_model import LogisticRegression
  from sklearn.metrics import roc_curve, roc_auc_score
  import matplotlib.pyplot as plt
  
  # 生成一个二分类数据集
  X, y = make_classification(n_samples=1000, n_classes=2, random_state=1)
  
  # 将数据集分为训练集和测试集
  X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1)
  
  # 训练逻辑回归模型
  lr = LogisticRegression()
  lr.fit(X_train, y_train)
  
  # 预测测试集
  y_pred_prob = lr.predict_proba(X_test)[:, 1]
  
  # 计算ROC曲线和AUC
  fpr, tpr, thresholds = roc_curve(y_test, y_pred_prob)
  auc = roc_auc_score(y_test, y_pred_prob)
  
  # 绘制ROC曲线
  plt.plot(fpr, tpr, label='ROC curve (area = %0.2f)' % auc)
  plt.plot([0, 1], [0, 1], 'k--')
  plt.xlim([0.0, 1.05])
  plt.ylim([0.0, 1.05])
  plt.xlabel('False Positive Rate')
  plt.ylabel('True Positive Rate')
  plt.title('Receiver operating characteristic')
  plt.legend(loc="lower right")
  plt.show()
  ```

  - 在这个例子中，我们使用了make_classification函数生成了一个二分类数据集，然后将数据集分为训练集和测试集。接着，我们训练了一个逻辑回归模型，并在测试集上进行了预测。
  - 使用roc_curve函数计算了ROC曲线的假正率（false positive rate）和真正率（true positive rate），并使用roc_auc_score函数计算了AUC。
  - 最后，我们使用Matplotlib绘制ROC曲线，并在图例中显示AUC的值。

