[toc]

- [numpy@array_like](https://numpy.org/doc/stable/glossary.html#term-array_like)

- [How to check if a variable is either a python list, numpy array or pandas series - Stack Overflow](https://stackoverflow.com/questions/43748991/how-to-check-if-a-variable-is-either-a-python-list-numpy-array-or-pandas-series)

## array_like

- [numpy.array — NumPy Manual](https://numpy.org/doc/stable/reference/generated/numpy.array.html#numpy-array)

- array_like:An array, any object exposing the array interface, an object whose __array__ method returns an array, or any (nested) sequence. If object is a scalar, a 0-dimensional array containing object is returned.

### 判断某个对象是否为array_like



