

## 表达式|运算符|数据对象

C语言中的对象(数据对象)不同于面向对象语言中的对象概念



##  变量

- 用机器语言编程，存取数据的时候，程序员要指定数据在内存中的地址，以及需要存取多少个字节的数据。用高级语言编程，程序员不必关心数据存放的内存地址，只要使用“变量”来存取数据即可。
- 变量就是一个代号，程序运行时系统会自动为变量分配内存空间，于是变量就代表了系统分配的那片内存空间，对变量的访问，就是对其代表的内存空间的访问。
- 变量有**名字和类型**两种属性
  - 不同变量的名字就对应了内存中的不同地址（即不同位置）
  - 变量的类型决定了一个变量占用多少个字节。
- 在C/C++语言中，变量要先定义然后才能使用，“使用”有时也称为“引用”。
  - 读取或修改一个变量的值，都称为使用(访问)这个变量。
  - 定义变量的语句，要出现所有使用该变量的语句之前。

### 基础变量类型

C/C++变量类型

[基础类型 - cppreference.com](https://zh.cppreference.com/w/cpp/language/types)

| 类型名             | 含义                         | 字节数 | 取值范围                                     |
| ------------------ | ---------------------------- | ------ | -------------------------------------------- |
| int                | 整型，表示整数               | 4      | $-2^{31} \sim 2^{31}-1$                      |
| long               | 长整型，表示整数             | 4      | $-2^{31} \sim 2^{31}-1$                      |
| short              | 短整型，表示整数             | 2      | $-2^{15} \sim 2^{15}-1$                      |
| unsigned int       | 无符号整型，表示非负整数     | 4      | $0 \sim 2^{32}-1$                            |
| unsigned long      | 无符号长整型，表示非负整数   | 4      | $0 \sim 2^{32}-1$                            |
| unsigned short     | 无符号短整型，表示非负整数   | 2      | $0 \sim 2^{16}-1$                            |
| long long          | 64位整型，表示整数           | 8      | $-2^{63} \sim 2^{63}-1$                      |
| unsigned long long | 无符号64位整型，表示非负整数 | 8      | $0 \sim 2^{64}-1$                            |
| float              | 单精度实数型，表示实数       | 4      | $-3.4\times10^{-38} \sim 3.4\times10^{38}$   |
| double             | 双精度实数型，表示实数       | 8      | $-1.7\times10^{-308} \sim 1.7\times10^{308}$ |
| char               | 字符型，表示字符             | 1      | $-128 \sim 127$                              |
| unsigned char      | 无符号字符型                 | 1      | $0 \sim 255$                                 |
| bool               | 布尔型，表示真假             | 1      | true 或 false                                |

1. **char** 和 **unsigned char**：
   - `char` 类型变量占用一个字节，并且有符号位，因此其取值范围是 `-128` 到 `127` (一个字节8bit,共有256个合法取值)。
   - `unsigned char` 类型变量没有符号位，因此其取值范围是 `0` 到 `255`。(同样有256个合法取值,但是取值范围和char类型不同,没有负值)
2. **int**、**short**、**long** 和 **long long**：
   - 这些类型用于表示整数。
   - `int` 和 `long` 在大多数系统上都是相同的大小（通常是32位），但有些系统可能有所不同。
   - `short` 的长度通常为16位。
   - `long long` 是一种64位整数类型。
3. **float** 和 **double**：
   - 这些类型用于表示浮点数。
   - `float` 通常是一个单精度浮点数，而 `double` 是双精度浮点数，具有更高的精度。
4. **bool**：
   - `bool` 类型变量的取值只有两种：`true` 和 `false`，分别表示逻辑“真”和逻辑“假”。

## 数据类型自动转换

在赋值语句中，如果等号左边的变量类型为T1,等号右边的变量或常量类型为T2,T1和T2不相同，那么在T1和T2类型能够兼容的情况下，编译器会将等号右边的变量或常量的值，自动转换为一个T1类型的值，再将此值赋给等号左边的变量。

这个过程称为"**自动类型转换**”。自动类型转换**不会改变赋值符号右边的变量**。

上面提到的所有类型，正好都是两两互相兼容的，所以可以互相自动转换。

表示整数的类型(short,int,long long等)和char、unsigned char类型统称为“整数类型”。它们之间可以互相赋值。

但是如果自动转换会导致信息丢失，那么编译器也会给予警告。

例如，用一个double类型的变量`f`给int类型的变量`n`赋值，虽然是允许的，但是由于自动转换后f的小数部分被舍弃，只留下整数部分赋值给，这造成了信息丢失，所以编译器对这样的赋值语句给出警告信息。

而如指针类型、结构类型，它们和上述所有的类型都不兼容。如果等号左边是一个整型变量，等号右边是一个“结构类型”的变量，这样的赋值语句在编译的时候就会报错。

下面以一段程序来说明上述数据类型之间的自动转换以及截断现象：

```cpp
#include <iostream>
#include <iomanip> //提供非十进制数形式输出
using namespace std;

void print_int_bin(int number);

int main()
{
  long long l = 123456789011LL;
  long long l1 = 123456789011;
  long long l2 = 0xffff000000000001;
  long long l3 = 0xffff000080000000;
  long long l4 = 0x0000000000000001;

  int i1 = l1;
  int i2 = l2;
  int i3 = l3;
  int n = -1;
  int b = 0b1010;
  cout << "l=" << l << endl
       << "l1=" << l1 << endl
       << "l2=" << l2 << endl // l2=-281474976710655
       << "l3=" << l3 << endl // l3=-281472829227008
       << "l4=" << l4 << endl // l4=1
       //  << "i1=" << i1 << endl
       << "i2=" << i2 << endl                                       // i2=1
       << "i3=" << i3 << endl;                                      // i3=-2147483648=-2^{31}
  cout << "i2=" << setw(8) << setfill('0') << hex << i2 << endl     // i2=00000001
       << "i3=" << setw(8) << setfill('0') << hex << i3 << endl;    // i3=80000000
  cout << "n=-1=0x" << setw(8) << setfill('0') << hex << n << endl; // 查看-1的补码
  cout << "b=" << dec << b << endl<<endl;                                 // b=10

  print_int_bin(i2);
  print_int_bin(i3);

  return 0;
}

void print_int_bin(int number)
{
  for (int i = 31; i >= 0; --i)
  {
    std::cout << ((number >> i) & 1);
  }
  std::cout << std::endl;
}

```

输出

```bash
l=123456789011
l1=123456789011
l2=-281474976710655                                                                                              
l3=-281472829227008                                                                                              
l4=1
i2=1
i3=-2147483648
i2=00000001
i3=80000000
n=-1=0xffffffff
b=10

00000000000000000000000000000001
10000000000000000000000000000000
```

### 由算数运算引起的类型转换和位丢弃

- 例如一个int类型的变量和一个long long类型变量相加,以操作数中较高精度的一方为准,让两个操作数的类型相同后相加,结果也是以高精度的类型为准
- 然而两个同样类型的低精度变量相加的值可能会得到一个需要更高精度类型的变量来存储,但是C/C++不会自动将结果的类型提升精度,高出的位会被丢弃,导致结果错误;又比如两个整数相除,比如5/2得到的结果会被保存在整型值,小数部分会被丢弃
- 而且即便能够提升精度也仍然很有限,这中情况下需要用其他结构来解决,比如使用数组



## 常量表示

### 整型常量

在 C/C++ 中，整型常量可以使用以下格式表示：

1. **十进制常量**：没有前缀，直接写数字。
	* 例：`123`
2. **八进制常量**：以 `0` 开头，后面跟数字。
	* 例：`0123`
3. **十六进制常量**：以 `0x` 或 `0X` 开头，后面跟数字和字母（A-F 或 a-f）。
	* 例：`0x123`、`0X123`
4. **二进制常量**：以 `0b` 或 `0B` 开头，后面跟数字（0 和 1）。
	* 例：`0b1010`、`0B1010`

注意：

* 在 C++ 中，二进制常量的前缀是 `0b` 或 `0B`，而在 C 中不支持二进制常量。
* 在 C/C++ 中，整型常量可以使用后缀来指定类型，例如 `L`、`LL`、`U`、`UL` 等。
	+ 例：`123L`、`123LL`、`123U`、`123UL`

整型常量的后缀可以指定类型，如下：

* `L`：长整型（`long`)
* `LL`：长长整型（`long long`)
* `U`：无符号整型（`unsigned`)
* `UL`：无符号长整型（`unsigned long`)
* `ULL`：无符号长长整型（`unsigned long long`)

例如：

* `123L`：长整型常量
* `123LL`：长长整型常量
* `123U`：无符号整型常量
* `123UL`：无符号长整型常量
* `123ULL`：无符号长长整型常量

### 浮点型常量(实数型常量)

**实数型常量**有普通表示法和科学记数法两种。

- 普通表示法就是最常用的“整数.小数”格式，如12.34、0.05、一8等。

- 科学记数法的格式是“小数$e$整指数$p$”，其大小是小数×$10^{p}$。例如：

  - 3.14e2,即3.14×102

  - -23.078e-12,即-23.078×10-12

- 科学记数法表示的常量中，字母“e”换成大写"E"的也可以。

在 C/C++ 编程语言中，数据类型和常量类型的定义和使用对于编程的基本逻辑结构至关重要。以下是对 `bool` 型、字符型、字符串型常量和符号常量的详细介绍：

### bool 类型常量
- `bool` 是 C++ 引入的数据类型（在 C99 中也有 `_Bool`）。
- `bool` 类型有两个值：`true` 和 `false`，用来表示布尔逻辑。
- C 中使用 `0` 表示 `false`，非零值表示 `true`，但 C++ 中提供了明确的 `bool` 类型。
- `#include <stdbool.h>` 头文件（C99）定义了 `true` 和 `false`，但 C++ 无需包含头文件，因为 `bool` 是内置类型。
  

示例：
```cpp
bool isTrue = true;
bool isFalse = false;
```

### 字符型常量
- 字符型常量在 C/C++ 中通常是单引号 `' '` 括起来的单个字符，如 `'A'`、`'b'`、`'1'` 等。
- 它的类型是 `char`，在底层存储中通常是一个 `1` 字节的整数，表示 ASCII 或 Unicode 编码中的数值。
- 字符常量实际上是 `int` 类型的一个值，例如 `'A'` 的 ASCII 值是 `65`。
  

示例：
```cpp
char ch = 'A'; // 一个字符型常量
```

- 特殊字符可以使用转义序列，如：
  - `'\n'` 表示换行符
  - `'\t'` 表示制表符
  - `'\0'` 表示空字符（字符串结束符）

###  字符串型常量

- 字符串常量是用双引号括起来的一串字符，如"a"、"abc"、"1234567"等。
  - "a"和'a'是不一样的，前者是只有一个字符的字符串，后者是一个字符，不能用前者给一个char类型的变量赋值。(这和一些shell语言不同)
  - "1234567"当然也和1234567是不一样的，不能用前者给一个int类型变量赋值。

- 字符串常量是由双引号 `"` 包围的字符序列，例如 `"Hello, World!"`。
- 它们在 C 中被表示为一个字符数组，以空字符 `'\0'` 结尾，用于标识字符串的结束。
- 字符串常量的类型是 `const char*`，是一个指向字符串第一个字符的指针。
  

示例：
```cpp
const char* greeting = "Hello, World!";
```

- 在 C++ 中，可以使用 `std::string` 来表示字符串（需要 `#include <string>`），提供更多功能和更直观的操作方法。

### 符号常量
- 符号常量是通过 `#define` 预处理指令或 `const` 关键字来定义的不可变值。
- 使用符号常量可以提高代码的可读性和可维护性，避免使用硬编码的字面值。

#### 使用 `#define` 定义符号常量（预处理宏）
- 这种方式在预处理阶段直接替换常量值。
- 没有数据类型，不受作用域控制。
```c
#define PI 3.14159
#define MAX_BUFFER_SIZE 1024
```

#### 使用 `const` 关键字定义常量
- `const` 定义的常量有类型信息，并且受到作用域控制，通常推荐使用这种方式。
```cpp
const double pi = 3.14159;
const int maxBufferSize = 1024;
```

 `constexpr` 关键字（C++11 引入）

- `constexpr` 表示编译时常量，比 `const` 更加严格，它确保值在编译期就确定下来。
- 适用于需要高效的编译时计算场景。
```cpp
constexpr int maxItems = 100;
```

#### 最佳实践

在 C 语言中，多使用符号常量（也称为宏常量）而少使用数值常量是好的编程习惯，因为符号常量可以提高代码的可读性和维护性。

例如，下面的代码使用了数值常量：

```c
if (age > 18) {
    printf("成年人\n");
}
```

如果我们想改变这个年龄限制，我们需要找到所有使用这个数值常量的地方并修改它。

而使用符号常量可以避免这种问题：

```c
#define ADULT_AGE 18

if (age > ADULT_AGE) {
    printf("成年人\n");
}
```

这样，我们只需要修改 `ADULT_AGE` 的值就可以改变年龄限制。

但是，在 C++ 语言中，符号常量已经不再是最佳选择。C++ 提供了更好的选择：枚举常量（enum）和常变量（const）。

枚举常量可以定义一组相关的常量值，例如：

```cpp
enum class Color {
    RED,
    GREEN,
    BLUE
};
```

枚举常量可以提高代码的可读性和安全性。

```cpp
#include <iostream>

enum class Color
{
    RED,
    GREEN,
    BLUE
};

void printColor(Color color)
{
    switch (color)
    {
    case Color::RED:
        std::cout << "红色" << std::endl;
        break;
    case Color::GREEN:
        std::cout << "绿色" << std::endl;
        break;
    case Color::BLUE:
        std::cout << "蓝色" << std::endl;
        break;
    }
}

int main()
{
    Color myColor = Color::GREEN;
    printColor(myColor);
    printColor(Color::BLUE);
    return 0;
}
```

常变量（const）可以定义一个不能被修改的变量，例如：

```cpp
const int ADULT_AGE = 18;
```

常变量可以提供比符号常量更好的类型安全性和可读性。

因此，在 C++ 语言中，应该尽可能使用枚举常量和常变量，而少用符号常量。

总结：

* 在 C 语言中，多使用符号常量而少使用数值常量是好的编程习惯。
* 在 C++ 语言中，应该尽可能使用枚举常量和常变量，而少用符号常量。

##  对象和左值 

- **对象是一个命名的存储区域**
- **左值**（lvalue）是**引用某个对象的表达式**。
- 具有合适<u>类型与存储类</u>的标识符便是左值表达式的一个明显的例子。
  - 某些**运算符可以产生左值**。
    - 例如，如果`E`是一个指针类型的表达式，`*E`则是一个左值表达式，它引用**由E指向的对象**。
    - 名字“左值”来源于赋值表达式E1=E2，其中，左操作数E1必须是一个左值表达式。

- **对每个运算符的讨论**需要说明此运算符是否需要一个左值操作数以及它是否产生一个左值。 



#### 数据对象

> 变量和常量是程序处理的两种基本**数据对象**。

- 对象的类型决定该对象可取值的集合以及可以对该对象执行的操作
- 声明语句说明变量的名字及类型，也可以指定变量的初值。
- 运算符指定将要进行的操作。

- 所有整型都包括signed（带符号）和unsigned（无符号）两种形式，
- 且可以表示无符号常量与十六进制字符常量。
- 浮点运算可以以单精度进行，还可以使用更高精度的long double 类型运算。
- 字符串常量可以在编译时连接。

> **对象**有时也称为**变量**，它是一个存储位置。

- 对它的解释依赖于两个主要属性：存储类和类型。
- 存储类决定了与该标识对象相关联的存储区域的生存期，类型决定了标识对象中值的含义。
- 名字还具有一个作用域和一个连接。
  - 作用域即程序中可以访问此名字的区域，
  - 连接决定另一作用域中的同一个名字是否指向同一个对象或函数

#### 变量的声明/定义

- 初始化声明符表中的声明符包含被声明的标识符；
- 声明说明符由一系列的类型和存储类说明符组成。 
- 声明说明符： (pass)
  - ![image-20220419194130559](https://cdn.jsdelivr.net/gh/xuchaoxin1375/pictures@main/images/image-20220419194130559.png)


Declarations 

- **Declarations** specify the interpretation given to each **identifier**; 
- they do not necessarily reserve storage(预留存储空间) associated with the identifier. 
- Declarations that reserve storage are called **definitions**. 

##### 类型限定符 

- 对象的**类型**可以通过附加的限定符进行**限定**。
- 声明为const的对象表明**此对象的值不可以修改**；
- 声明为volatile 的对象表明它具有与优化相关的特殊属性。限定符既不影响对象取值的范围，也不影响其算术属性。

#### 声明的形式

`  declaration-specifiers init-declarator-list_opt;`(文法:句型)

- declaration-specifiers:声明`说明符` 
- init-declarator-list:初始化`声明符`表_opt; 
- 相关文法:(产生式Production)
  - 编译原理中提到

```c
declaration-specifiers: 
      storage-class-specifier declaration-specifiers_opt 
      type-specifier declaration-specifiers_opt 
      type-qualifier declaration-specifiers_opt  
init-declarator-list: (非终结符)
      init-declarator 
      init-declarator-list , init-declarator  
init-declarator: 
      declarator 
      declarator = initializer(终结符)  
```

```
[declaration-specifiers]: 
      storage-class-specifier declaration-specifiers_opt 
      type-specifier declaration-specifiers_opt 
      type-qualifier declaration-specifiers_opt  
[init-declarator-list]: 
(文法:产生式)
      [init-declarator]
      init-declarator-list , [init-declarator]  
[init-declarator]: 
      declarator 
      declarator = initializer  
```



- `declarator`([dɪ'klærətə] )声明符(与一下几种`符`不同,较为抽象)

  - 从文法分析的递归定义来理解(opt表示可选)
  - ![image-20220419193112784](https://s2.loli.net/2022/04/19/lWG2CwB5sEuIhac.png)

- `specifiers`说明符(eg.static,registor)

- `identifier`标识符(eg. word1,abc,...)

- `qualifier` 限定符(eg. const)

- The **declarators** in **the init-declarator list** contain the **identifiers**(标识符) being declared; 

- the **declaration-specifiers** consist of a sequence of **type and storage class specifiers**

  

#####  Storage Class Specifiers(存储类型说明符)

The storage class specifiers are:  

> - ​      auto 
> - ​      register 
> - ​      static 
> - ​      extern 
> - ​     typedef  

##### The type-specifier(类型说明符)

The type-specifiers are 

> - type specifier: 
> - void 
> - char 
> - short 
> - int 
> - long 
> - float 
> - double 
> - signed 
> - unsigned 
> - struct-or-union-specifier 
> - enum-specifier 
> - typedef-name  

##### Declarator(声明符)

- Meaning of Declarators 

- **A list of declarators** appears after **a sequence of type and storage class specifiers**. 
- Each **declarator** declares **a unique main identifier**, the one that <u>appears as the first alternative of the production for direct-declarator.</u> 
- The **storage class specifiers** apply directly to this **identifier**, but its **type** depends on the form of its **declarator**.
-  A declarator is read as an **assertion** that when its **dentifier** appears in an expression of the same form as the declarator, it <u>yields an object of the specified type</u>.  
- Considering only **the type parts** of **the declaration specifiers** (Par. A.8.2) and a particular declarator, a declaration has the form T D, where `T` is a type and `D` is a declarator. 
  - The type attributed to the identifier in the various forms of declarator is described inductively using this notation.  
  - In a declaration T D where D is an unadored identifier, the type of the identifier is T.  
  - In a declaration T D where D has the form  

```
  ( D1 ) 
```
- then the type of the identifier in D1 is **the same as that of D**. 			

- The parentheses do not alter the type, but may change **the binding of complex declarators**.  

> - **声明符表**出现在**类型说明符**和**存储类**说明符序列之后。
> - 每个**声明符**声明一个唯一的**主标识符**，该标识符是**直接声明符产生式的第一个候选式**。
>   - 存储类说明符可直接作用于该标识符，但其类型由声明符的形式决定。
>   - 当声明符的标识符出现在与该声明符形式相同的表达式中时，该声明符将被作为一个断言，其结果将产生一个指定类型的对象。

##### 声明符的例子

- 一个例子。下列声明： 
-  `int i, *pi, *const cpi = &i;`
  - 声明了一个整型i和一个指向整型的指针pi。
  - 不能修改常量指针cpi的值，该指针总是指向同一位置，但它所指之处的值可以改变。
- ` const int ci = 3, *pci; ` 
  - 整型ci是常量，也不能修改（可以进行初始化，如本例中所示）。
  - pci 的类型是“指向const int 的指针”，pci 本身可以被修改以指向另一个地方，但它所指之处的值不能通过pci赋值来改变

### 表达式

- **表达式**则把<u>变量与常量组合起</u>来生成<u>新的值。</u>

#### 初等表达式

- 初等表达式包括
  - 标识符、
  - 常量、
  - 字符串
  - 带括号的表达式。 
- 常量是初等表达式，其类型同其形式有关。
- 字符串字面值是初等表达式。
  - 它的初始类型为“char类型的数组”类型（对于宽字符字符串，则为“wchar_t类型的数组”类型）
  - 它通常被修改为“指向char类型（或wchar_t类型）的指针”类型，其结果是<u>指向字符串中第一个字符</u>的指针。
    - 某些初始化程序中不进行这样的转换
  - 用括号括起来的表达式是初等表达式，它的类型和值<u>与无括号的表达式相同</u>。
  - 此表达式是否是左值不受括号的影响。 

