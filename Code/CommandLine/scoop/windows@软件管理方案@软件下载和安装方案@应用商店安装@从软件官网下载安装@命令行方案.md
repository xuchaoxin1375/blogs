[toc]



## abstract

windows上的软件管理方案@软件下载和安装方案@应用商店安装@从软件官网下载安装@命令行方案

### 命令行软件包安装管理器方案

- 一般而言，命令行安装是最快捷的，windows上常见的有 **scoop,choco,winget**
  - 其中scoop使用最简单方便，并且可以配置国内加速版镜像（主要针对github上的软件，国内软件可以直接下载）
    - scoop主要面向不需要高权限的软件，软件库内的软件数量受限,但也不少,常见软件基本都有,只是安装过程中便携化安装可能会遇到错误,下载国内软件时需要知道英文包名(可以配合模糊关键词搜索)
    - 即便软件安装过程中失败,也可以当做软件下载器
    - 在版本控制方面相比普通的应用商店有优势,譬如可以同时管理多个python版本
    - 我为此整合了一个国内加速版的scoop部署脚本,并且做了配套的powershell 函数来快速配置scoop,详情另见它文
  - 其次是choco,下载和安装速度比较慢
  - winget有加速镜像，但是只能加速部分内容
  - 总之考虑到国内的网络环境,使用命令行安装软件还是存在一定的不方便和门槛

### 应用商店方案👺

- 对于一般用户,使用应用商店或许是最简单的方式了

  - 国内比较好的应用商店有
    1. 联想应用商店 [联想应用商店 (lenovo.com)](https://lestore.lenovo.com/)
      - 可以在线直接下载软件,软件库也算丰富,可以下载到许多编程软件,比如powershell,vscode,devC++,小熊猫C++等(部分软件更新有所滞后,不一定是最新版)
    2. 火绒应用商店:[应用商店-火绒安全 (huorong.cn)](https://www.huorong.cn/appstore)
    3. 其他
      - 360软件管家和腾讯软件中心则要逊色,尤其是前者广告多,软件少/旧,编程开发类软件尤为如此,不值得用

  - 其实应用商店建议还是使用客户端,方便管理软件,比如更新/卸载等操作
  - 此外,各类电脑管家或者安全卫士一般有携带应用商店插件,这中全家桶容易推送各种广告弹窗,需要谨慎使用,目前火绒在这方面比较友好
- 感觉联想商店和火绒商店的软件源一样,来更新的版本都一致,不过前者支持网页版直接下载软件包,当然推荐使用客户端,便于管理软件(卸载或更新)

### 官网下载方案

- 下载软件最直接的是到官网下载,然而国内的搜索引擎容易引导你下载错误的文件,对于小白不友好
- 现在虽然有去广告脚本(tampermonkey),但是增加了操作门槛,而且许多国外软件下载很慢甚至下载不下来,官网也可能都打不开,因此使用可靠的应用商店下载成了首选

### 软件卸载

- 一般windows上安装的软件可以从`appwiz.cpl`控制面板中找到并卸载
- 而命令行工具`winget`也提供了卸载管理
- 对于win10之后的系统,可以通过设置或开始菜单中搜索`apps`来管理软件,还可以看到uwp等软件
- 第三方应用商店一般也会提供软件卸载和更新功能
- 此外还有许多专业的卸载工具,用来对付难以卸载干净的软件,比如geek等

### 小结

上述几种方案(命令行软件包管理工具,应用商店,软件官网)其实可以互补使用,他们各有优势,通常普通用户我们选择成功率最高的一种即可

命令行方式便于脚本化部署,应用商店操作门槛低,下载速度快,软件官网可以下载到最新版本,尤其是分支版本和开发版