[toc]

[badmotorfinger/z: Save time typing out directory paths in PowerShell by jumping around instead. (github.com)](https://github.com/badmotorfinger/z)

[vors/ZLocation: ZLocation is the new Jump-Location (github.com)](https://github.com/vors/ZLocation)