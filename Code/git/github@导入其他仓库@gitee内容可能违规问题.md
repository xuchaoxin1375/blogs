[toc]



## abstract

- 在github导入gitee中的仓库可能有点滑稽,我们通常是在gitee中导入github上的仓库,但是这种反向操作有时还是有用的
  - 平时推送仓库推送到gitee上会比较方便
  - 在需要的时候查看github上的备份仓库
- 因为gitee对内容比较敏感,经常会误报内容违规,需要clone下来才能查看,或者需要仓库主人账号登陆才能自己在线查看
- 这种情况下我们希望用github来在线查看被误报的内容

### 参考链接

- [使用 GitHub 导入工具导入仓库 - GitHub 文档](https://docs.github.com/zh/migrations/importing-source-code/using-github-importer/importing-a-repository-with-github-importer)