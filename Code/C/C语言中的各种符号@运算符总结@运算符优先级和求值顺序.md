[toc]



## 运算符优先级与求值次序 

总结C语言中所有运算符的优先级与结合性。

运算符的优先级十分重要,从简单的运算赋值等操作,到类型的复杂声明以及高可读性代码的编写技巧,都离不开对运算符优先级和结合顺序的深刻理解

### 参考文档

- [表达式 - cppreference.com](https://zh.cppreference.com/w/c/language/expressions)

- [C 运算符优先级 - cppreference.com](https://zh.cppreference.com/w/c/language/operator_precedence)

- [C++ 运算符优先级 - cppreference.com](https://zh.cppreference.com/w/cpp/language/operator_precedence)
- K&R The C

### 运算符的高级应用

部分运算符和数学中的运算符相挂钩,容易理解,但是另一部分是C语言标准定义的,需要额外的记忆和时间来掌握它们的用法

`f(int (*daytab)[13]) { ... }` 
这种声明形式表明函数`f`的**参数`daytab`是一个指针**，它指向具有13 个整型元素的一维数组。

- 因为方括号`[]`的优先级高于`*`的优先级，所以上述声明中必须使用圆括号。

  - 这里方括号通常用来访问给定地址/指针为首地址的数组的第`i`个元素,例如`a[i]`
  - 上面的例子中`(*daytab)`结果应该是一个地址(指针),而且是数组的元素的地址,结合`[ ]`来访问数组元素,而且数组元素是整型`int`; `(*daytab)`可以被`[i]`访问元素,`i`的范围是`0...12`这13个种取值
  - 因此,`daytab`是一个指针,并且指向的一个(一维)数组,并且这种数组是含有13个整型元素的数组
    - 有时我们称`*daytab`是二维数组(考虑一个线性代数矩阵,本例子中会是一个列数为13的矩阵,行向量长度为13)的**列指针**,例如**列指针**`c`增加1,并且假设`c`取当前行向量第`j`个元素的地址,那么`c+1`相当于行向量中从原来地址变为`j+1`个元素的地址;
    - 对应的,`daytab`就是行指针,对它增加1,相当于一下偏移一个行向量的地址,本例中,会偏移13个int类型的元素的地址
    - 也就是说,行指针`daytab`前使用间接寻址运算符`*daytab`,就成为列地址

- 如果去掉括号，则声明变成 `int *daytab[13]` 这相当于声明了一个数组，该数组有13个元素，其中每个元素都是一个指向整型对象的指针。

- 上面的例子旨在说明认识运算符不同含义和优先级的高级应用和重要性

- 附上代码

  ```c
  #include <stdio.h>
  static char mat[2][10] = {{0, 11, 12, 13, 14, 15, 16, 17, 18, 19},
                            {0, 21, 22, 23, 24, 25, 26, 27, 28, 29}};
  
  int main()
  {
      printf("%d\n", (*mat)[2]);
        //下面两个写法等价
      printf("%d\n", (*(mat + 1))[2]);
      printf("%d\n", mat[1][2]);
      // #下面几种写法效果一致,因为*间接寻址运算符优先级高于+
      printf("%d\n", (*mat + 1)[2]);
      printf("%d\n", ((*mat) + 1)[2]);
      printf("%d\n", (*(*mat + 3)));
      
      // 比较地址
      printf("%p,%p\n", mat, *mat);//行指针和列指针地址一样
      printf("%p,%p\n", mat + 1, *mat + 1);//行指针和列指针地址分别+1,结果不一样,前者偏移量是行向量的元素个数的地址,后者是列向量的单个元素地址
  }
  ```
  
  

### 符号总结

下表中同一行中的各运算符具有相同的优先级，各行间从上往下优先级逐行降低。

| 运算符                                                       | 结合性   | 描述                                                         |
| ------------------------------------------------------------ | -------- | ------------------------------------------------------------ |
| `()` , `[]` , `->` , `.`                                     | 从左至右 | 函数调用、数组下标、成员访问(`->`,`.`)结构运算符)(优先级最高的4个运算符) |
| `!` , `~` , `++` , `--` , `+` , `-` , `*` , `(type)` , `sizeof` | 从右至左 | 逻辑非、按位取反、自增、自减、正负号、指针解引用(间接寻址/间接引用)、类型转换、对象长度(单目运算符优先级第二梯队)<br />例如`*M++`等价于`*(M++)`,两个运算符优先级相同但是结合性从有到左 |
| `*` , `/` , `%`                                              | 从左至右 | 乘法、除法、取模                                             |
| `+` , `-`                                                    | 从左至右 | 加法、减法                                                   |
| `<<` , `>>`                                                  | 从左至右 | 按位左移、按位右移                                           |
| `<` , `<=` , `>` , `>=`                                      | 从左至右 | 小于、小于等于、大于、大于等于                               |
| `==` , `!=`                                                  | 从左至右 | 相等、不相等                                                 |
| `&`                                                          | 从左至右 | 按位与                                                       |
| `^`                                                          | 从左至右 | 按位异或                                                     |
| `|`                                                          | 从左至右 | 按位或                                                       |
| `&&`                                                         | 从左至右 | 逻辑与                                                       |
| `||`                                                         | 从左至右 | 逻辑或                                                       |
| `?:`                                                         | 从右至左 | 三元条件运算符                                               |
| `=` , `+=` , `-=` , `*=` , `/=` , `%=` , `&=` , `^=` , `|=` , `<<=` , `>>=` | 从右至左 | 赋值及复合赋值运算符                                         |
| `,`                                                          | 从左至右 | 逗号运算符，用于分隔表达式                                   |

- 一元运算符`+`,`-`,`&`,`*`比它们作为相应的二元运算符的优先级高。 
- 位运算符`&、^，|`的优先级比运算符`==`与`!=`的低。
  - 这意味着，位测试表达式，如 `if ((x & MASK) == 0) ...` 必须用圆括号括起来才能得到正确结果。 
- sizeof运算符
  - C 语言提供了一个**编译时**（compile-time）**一元运算符`sizeof`**，它可用来计算任一对象的长度。
  - 表达式 `sizeof 对象 `或`sizeof(类型名)`将返回一个**整型值**，它等于指定**对象或类型**占用的**存储空间字节数**。(简单将就是sizeof的操作对象不引起歧义或编译器的误解时,可以省略括号,例如字符串可以省略括号,但是类型名不能)
    - 严格地说，`sizeof` 的返回值是**无符号整型值**，其类型为`size_t`，该类型在头文件`<stddef.h>`中定义。
    -  **对象**可以是**变量、数组或结构**；
    - **类型**可以是**基本类型**，如int、double，也可以是**派生类型**，如结构类型或指针类型。 


### 求值顺序

- C 语言没有指定**同一运算符中多个操作数的计算顺序**（`&&、||、?:`和`,`4个运算符除外）。
  - 例如，在形如 `x = f() + g();` 的语句中，`f()`可以在g()之前计算，也可以在`g()`之后计算。
  - 因此，如果函数f 或g 改变了另一个函数所使用的变量，那么x 的结果可能会依赖于这两个函数的计算顺序。为了保证特定的计算顺序，可以把中间结果保存在临时变量中。 
- C 语言也没有指定**函数各参数的求值顺序**。因此，下列语句 

```c
printf("%d %d\n", ++n, power(2, n)); /* 错 */ 
```

在不同的编译器中可能会产生不同的结果，这取决于n的自增运算在power调用之前还是之 
后执行。解决的办法是把该语句改写成下列形式： 

```c
++n; 
printf("%d %d\n", n, power(2, n)); 
```

函数调用、嵌套赋值语句、自增与自减运算符都有可能产生“副作用”——在对表达式 求值的同时，修改了某些变量的值。在有副作用影响的表达式中，其执行结果同表达式中的 变量被修改的顺序之间存在着微妙的依赖关系

下列语句就是一个典型的令人不愉快的情况： 
`a[i] = i++;` 
问题是：数组下标i 是引用旧值还是引用新值？对这种情况编译器的解释可能不同，并因此产生不同的结果。C 语言标准对大多数这类问题有意未作具体规定。

表达式何时会产生这种副作用（对变量赋值），将由编译器决定，因为最佳的求值顺序同机器结构有很大关系。（ANSI C 标准明确规定了所有对参数的副作用都必须在函数调用之前生效，但这对前面介绍的printf函数调用没有什么帮助。） 
在任何一种编程语言中，如果代码的执行结果与求值顺序相关，则都是不好的程序设计风格。很自然，有必要了解哪些问题需要避免，但是，如果不知道这些问题在各种机器上是 如何解决的，就最好不要尝试运用某种特殊的实现方式。 



## 重点运算符

### 1. `++` 和 `--`（自增和自减）
- **前缀形式** (`++i` 和 `--i`)：
  - 在使用变量之前先进行自增/自减操作。
  - 例如：`int i = 0; int j = ++i;` 这里 `j` 的值为 `1`，`i` 的值也为 `1`。

- **后缀形式** (`i++` 和 `i--`)：
  - 先使用变量的当前值，然后进行自增/自减操作。
  - 例如：`int i = 0; int j = i++;` 这里 `j` 的值为 `0`，`i` 的值为 `1`。

### 2. `*`（指针解引用和乘法）
- **指针解引用**：
  - 用于获取指针所指向的内存地址中的值。
  - 例如：`int a = 10; int *p = &a; int b = *p;` 这里 `b` 的值为 `10`。

- **乘法**：
  - 用于数学乘法运算。
  - 例如：`int a = 5; int b = 3; int c = a * b;` 这里 `c` 的值为 `15`。

### 3. `&`（按位与,取地址）

- **按位与**：
  - 用于按位逻辑与操作。
  
  - 例如：
  
    - ```c
      int a = 5; // 二进制 0101 
      int b = 3; // 二进制 0011 
      int c = a & b; // 二进制 0001
      ```
  
    -  这里 `c` 的值为 `1`。
  
- **取地址**：
  - 用于获取变量的内存地址。
  - 例如：`int a = 10; int *p = &a;` 这里 `p` 存储的是 `a` 的内存地址。

### 4. `=`（赋值和比较）

- **赋值**：
  - 用于将一个值赋给一个变量。
  - 例如：`int a = 5;` 这里 `a` 的值为 `5`。

- **比较**：
  - 使用 `==` 进行相等比较，使用 `!=` 进行不相等比较。
  - 例如
  
    - C语言中
  
      ```c
      
      int a = 5;
      int b = 5;
      printf("%d\n", a == b);#结果为1
      ```
  
    - C++中：`int a = 5; int b = 5; bool result = (a == b);` 这里 `result` 的值为 `true`。

### 5. `||` 和 `&&`（短路逻辑运算符）👺

- **短路逻辑与** (`&&`)：
  - 如果第一个操作数为 `false`，则不会评估第二个操作数。
  - 例如：`int a = 0; int b = 5; if (a != 0 && b / a > 1) { ... }` 这里不会发生除零错误，因为 `a != 0` 为 `false`，所以 `b / a > 1` 不会被评估。

- **短路逻辑或** (`||`)：
  - 如果第一个操作数为 `true`，则不会评估第二个操作数。
  - 例如：`int a = 5; int b = 0; if (a != 0 || b / a > 1) { ... }` 这里 `a != 0` 为 `true`，所以 `b / a > 1` 不会被评估。

### 6. `?:`（三元条件运算符）
- **语法**：`condition ? expr1 : expr2`
  - 如果 `condition` 为 `true`，则返回 `expr1` 的值；否则返回 `expr2` 的值。
  - 例如：`int a = 5; int b = 10; int max = (a > b) ? a : b;` 这里 `max` 的值为 `10`。

### 7. `=` 和复合赋值运算符（如 `+=`, `-=`, `*=`, `/=`, `%=`, `&=`, `^=`, `|=`, `<<=`, `>>=`）

- **赋值**：
  - 例如：`int a = 5;`

- **复合赋值**：
  - 例如：`int a = 5; a += 3;` 等价于 `a = a + 3;`，这里 `a` 的值为 `8`。

### 8. `,`（逗号运算符）
- **用途**：
  - 用于在一个表达式中执行多个操作，最后一个表达式的值作为整个表达式的值。
  - 例如：`int a = 0, b = 0; int c = (a++, b++, a + b);` 这里 `a` 和 `b` 都为 `1`，`c` 的值为 `2`。

这些运算符在不同的上下文中可能会有不同的行为，因此在使用时需要特别小心，以避免潜在的错误。希望这些解释对您有所帮助！如果还有其他问题，请随时提问。

## C语言中的其他符号

### 花括号

花括号 `{}` 在 C 语言中是一个非常重要的语法元素，用于定义代码块和控制代码的作用域。

在以下几种情况下会使用花括号：

1. **函数体**：定义函数的主体内容。
2. **条件语句（如 `if`, `else`, `switch`）**：包含条件语句执行的代码块。
3. **循环（如 `for`, `while`, `do-while`）**：包含循环执行的代码块。
4. **结构体、联合体和枚举**：定义结构体、联合体和枚举的成员。

### 分号(;)

标志一个语句的结束

### 井号(#)

用于宏定义

### 省略号

在C语言中，省略号（`...`）主要用于函数参数列表中，表示这个函数可以接受可变数量的参数。这类函数被称为**可变参数函数（variadic functions）**。例如，C标准库中的 `printf` 函数就是一个典型的可变参数函数，因为它可以接受不定数量的参数。

使用方法

在C语言中，要声明一个可变参数函数，通常使用如下语法：
```c
返回类型 函数名(固定参数, ...);
```

例如：
```c
#include <stdio.h>
#include <stdarg.h>  // 引入可变参数宏库

void print_numbers(int count, ...) {
    va_list args;                 // 定义一个可变参数列表变量
    va_start(args, count);        // 初始化参数列表，第二个参数是最后一个固定参数

    for (int i = 0; i < count; i++) {
        int number = va_arg(args, int);  // 从参数列表中提取下一个参数，指定类型为 int
        printf("%d ", number);
    }

    va_end(args);                 // 清理参数列表
    printf("\n");
}

int main() {
    print_numbers(3, 10, 20, 30); // 调用函数，实际传入三个可变参数
    return 0;
}
```

可变参数函数的工作原理

在 `print_numbers` 函数中，省略号（`...`）表示可以接收任意数量的参数，具体过程如下：
1. **va_list**：声明一个 `va_list` 类型的变量来处理参数列表。
2. **va_start**：使用 `va_start(args, count)` 初始化参数列表，其中 `count` 是最后一个固定参数。
3. **va_arg**：通过 `va_arg(args, int)` 提取下一个参数，并指定参数类型（例如，这里是 `int`）。
4. **va_end**：最后，用 `va_end(args)` 结束参数处理，避免内存泄漏。

可变参数函数的注意事项

1. **最后一个固定参数**：`va_start` 的第二个参数必须是固定参数列表中的最后一个参数，以便告诉编译器从哪里开始寻找可变参数。
   
2. **类型安全问题**：`va_arg` 需要指定提取参数的类型，但没有编译时检查机制。如果类型不匹配可能导致未定义行为，因此在使用时需特别小心。

3. **常见用途**：C语言中常见的可变参数函数包括 `printf`、`scanf`、`fprintf` 等，它们通常用于处理格式化的字符串和不同数量的输入参数。

### 注释符号

- 单行注释`//`
- 多行注释`/* */`

## 分类总结

好的，以下是C语言中所有运算符的分类总结，每个类别都包含相应的运算符及其简要说明：

### 1. 算术运算符
用于基本的数学运算。
- `+`：加法
- `-`：减法
- `*`：乘法
- `/`：除法
- `%`：取模（求余）

### 2. 关系运算符
用于比较两个值之间的关系。
- `<`：小于
- `<=`：小于等于
- `>`：大于
- `>=`：大于等于
- `==`：等于
- `!=`：不等于

### 3. 逻辑运算符
用于组合多个条件表达式。
- `&&`：逻辑与
- `||`：逻辑或
- `!`：逻辑非

### 4. 位运算符
用于对整数的二进制表示进行操作。
- `&`：按位与
- `|`：按位或
- `^`：按位异或
- `~`：按位取反
- `<<`：左移
- `>>`：右移

### 5. 赋值运算符
用于将值赋给变量。
- `=`：简单赋值
- `+=`：加法赋值
- `-=`：减法赋值
- `*=`：乘法赋值
- `/=`：除法赋值
- `%=`：取模赋值
- `&=`：按位与赋值
- `|=`：按位或赋值
- `^=`：按位异或赋值
- `<<=`：左移赋值
- `>>=`：右移赋值

### 6. 条件运算符
用于三元条件表达式。
- `?:`：三元条件运算符

### 7. 逗号运算符
用于在单个表达式中执行多个操作。
- `,`：逗号运算符

### 8. 特殊运算符
用于特殊目的。
- `()`：函数调用
- `[]`：数组下标
- `.`：结构体成员访问
- `->`：指向结构体成员的指针
- `sizeof`：计算数据类型的大小
- `&`：取地址
- `*`：指针解引用
- `type`：类型转换（如 `(int)`）

### 9. 自增和自减运算符
用于增加或减少变量的值。
- `++`：自增
- `--`：自减

## 按操作数数量分类

### 单目运算符

这些运算符只作用于一个操作数。

- `!`：逻辑非
- `~`：按位取反
- `++`：自增
- `--`：自减
- `+`：正号
- `-`：负号
- `*`：指针解引用
- `&`：取地址
- `(type)`：类型转换
- `sizeof`：计算大小

### 双目运算符

这些运算符作用于两个操作数。

- `+`：加法
- `-`：减法
- `*`：乘法
- `/`：除法
- `%`：取模
- `<<`：左移
- `>>`：右移
- `<`：小于
- `<=`：小于等于
- `>`：大于
- `>=`：大于等于
- `==`：等于
- `!=`：不等于
- `&`：按位与
- `^`：按位异或
- `|`：按位或
- `&&`：逻辑与
- `||`：逻辑或
- `=`：简单赋值
- `+=`：加法赋值
- `-=`：减法赋值
- `*=`：乘法赋值
- `/=`：除法赋值
- `%=`：取模赋值
- `&=`：按位与赋值
- `|=`：按位或赋值
- `^=`：按位异或赋值
- `<<=`：左移赋值
- `>>=`：右移赋值
- `,`：逗号运算符

### 三目运算符

这些运算符作用于三个操作数。

- `?:`：三元条件运算符

### 特殊运算符

这些运算符具有特殊用途，通常涉及多个操作数

- `()`：函数调用
- `[]`：数组下标
- `.`：结构体成员访问
- `->`：指向结构体成员的指针

 