[toc]

## 中考英语必背高频短语汇总

### 动词固定搭配

#### act

act as担任......职务；起……作用

act out 表演（对话、故事等）

#### break

break away from脱离，逃离

break down 出故障，抛锚；分解

break in闯进，打断；使顺服；插嘴

break into闯入；强行进入

break out（战争，疾病等）爆发

break off 打断，中断

break the record破纪录

break one's promise 食言

break through 冲破

break up 破碎；解散

#### bring

bring about 引起；导致

bring back 使想起

bring forth 产生；引起；结果

bring in 收获；获利；引进

bring out 显示出

bring up 提出；抚养；呕吐

#### call

call at (a place) 访问某地

call in 叫某人进来，邀请

call back 回电话

call on (upon) 号召，拜访

#### catch

be caught doing 被发现做某事

be caught in the rain淋雨

catch a bus / train 赶汽车/火车

catch a cold伤风；感冒

catch sight of发现；瞥见

catch up with赶上；追上

#### carry

carry out 贯彻，落实（计划，命令等）

carry on 继续，坚持下去

#### come

come about 产生，发生

come across偶遇

come along进步；进展；出现

come down倒下；降落；跌落

come from来自，起源于

come into being出现；形成

come into use开始使用

come out 出来；传出；出版；结果是

come to苏醒；复原；共计；达到

come to an end终止；结束

come to know开始了解到

come true实现；成为现实

#### cut

cut down 砍倒；减少

cut in 插嘴

cut off 切除；断绝

cut out 删去；略去

cut up 切碎

#### die

die away 消失

die down 逐渐平息

die of 死于（外因，比如车祸）

die from 死于（内因，比如心脏病）

die out 灭绝

#### get

get about徘徊；（消息）流传

get accustomed to习惯于

get along with与……相处

get away离开；逃脱

get down to认真对待；静下心来

get familiar with熟悉

get on with 进展；相处

get over越过；克服

get rid of除去；去掉；摆脱

get through完成；通过；及格

get together积聚；积累

get used to习惯于

#### give

give away 赠送；泄露

give back 归还

give in屈服；让步

give in to 向……让步

give off 发出（烟，气味）

give out 分发；公布

give rise to引起；导致

give up放弃；停止

give way to让步；退却；屈服于

#### go

go about 着手处理；忙于

go away 走开；离开

go against 违反；反对

go ahead 进行；进展；干吧；说吧

go around 流传；四处走动

go bad （食物等）变坏

go by 走过；经过

go in for 从事（某种事业或活动）

go over 审阅；检查

go through 审阅；检查；经历；经过

go up 上涨；上升

go wrong 出毛病

#### hand

hand in 交上；交付

hand in hand 手拉手，联合

hand out 分发

hand over 移交

#### hold

hold…back 阻止（经常是眼泪）

hold on 等一等（电话用语）

hold out 伸出

hold up 举起；阻挡

#### keep

keep away from 避开；不接近

keep a record 做好记录

keep back 阻止；隐瞒不讲

keep off 使……不接近

keep up 坚持；不使（斗志）低落；保持；维持

keep up with 跟上

keep in touch with 与……联系

keep fit 保持健康

#### look

look about四下环顾；查看

look after照顾；看管

look around东张西望

look at注视，着眼于

look back回顾

look for寻找

look down on轻视

look forward to盼望，期待

look into调查；

look like看起来像

look out 向外看；注意；当心；提防

look up to 仰望；尊敬

#### make

be made from 由……原料制成

be made of 由……材料制成

be made up of 由……组成

make a fool of 愚弄，欺骗

make a mistake 弄错

make use of使用；利用

make out 理解；辨认出；填写；开支票

make the best of 尽量利用

make up编造；化妆

make up for 弥补

#### pass

pass away 去世

pass by 从旁边经过

pass down 传下来；流传

pass on (to sb) 传给 (某人)

pass out 失去知觉；昏厥

#### pull

pull down 拆毁

pull in 车停下；车进站

pull off 完成；扯下

pull over 靠边停车

#### put

put aside 把……放在一边；搁置

put away 把……放好

put back把……放回原处

put down 放下；镇压；制止；记下

put forward 提出

put off 推迟；延期

put on 上演；穿上

put one's heart into 全神贯注；专心致志

put up with 忍受，容忍

put through 电话接通

#### run

run across 偶遇

run after 追捕；追求

run against 违反；不利于

run away 逃跑

run into 偶遇；撞上

run out 用完（主语是被用的那个物）

run out of 用完（主语是用东西的那个人）

#### see

see about 负责处理

see through 看穿；识破

see to 注意；照料；负责

#### send

send away 把……打发走

send for 派人去请（医生什么的）

send out 派遣；发出 (光亮等)

#### set

set about 着手干

set aside 存蓄；留出

set down 放下

set off 动身出发；引起

set out 出发

set up 创立

#### show

show off 炫耀

#### stand

stand by 和……一起；支持

stand for 代表

satnd out 显眼；引人注目

#### take

take in 吸收；收容；理解；领悟；欺骗

take it easy 放轻松；慢慢来

take off 起飞；脱去

take on 承担；从事；呈现

take out 拿出；取出；拔去

take over 继承；接管；最终取代

take up 开始从事；着手处理；拿起；占据

take a seat 就坐

take a shower 淋浴

take care of照顾；处理

take one's temperature量体温

take part in参与；参加

take place 发生；举行

take the place of代替

#### throw

throw away 抛弃；浪费

throw up 举起；抛起；呕吐

#### turn

turn on 打开（自来水，电器开关）

turn off 关上（自来水，电器开关）

turn up 出现；露面；音量调高；查阅；参考

turn down 音量调小；拒绝

turn upside down 颠倒过来；使陷入混乱

turn into 变成；变为

turn out 证明是

turn out to be原来是；证明是；结果是

turn to 求助于

take one's turn to do轮流到做

take turns 轮流

turn a blind eye to 对……视而不见

#### work

work at(on) 致力于；从事于

work out 算出；制定出;成功,顺利进行;锻炼,健身

out of work 失业

### 介词短语

#### at 短语

at last 最后，终于

laugh at 嘲笑

look at 看；注视

at the moment现在；此时

point at / to指向

at times不时

#### after 短语

look after照看；照顾

name after以……的名字命名

run after追赶；追求

#### on 短语

agree on(通过协商) 达成共识

call on拜访；看望

come on快点儿；加油;拜托,请求你;来吧

on display在展出

hang on稍等；别挂断

on holiday度假；休假

play a joke on和……开玩笑，戏弄……

keep on继续

live on以……为食，靠……生活

put on穿上；戴上

turn on打开（收音机、电灯、煤气等）

work on致力于

#### to 短语

agree to同意；答应；接受

compare...to...把……与……作比较

from...to...从……到……

get to 到达

do harm to对……有害处

lead to通往；导致

pay attention to注意

to one's surprise使某人吃惊的是

take...to...把……带到 / 给……

write to...写信给……

#### in 短语

in danger在危险中

hand in交上；上交

join in参加

take part in参加

take pride in以……为荣

in surprise吃惊地；惊讶地

#### of 短语

be afraid of害怕

take care of照顾；处理

make fun of嘲笑……

instead of代替；而不是

hear of听说……

speak of 谈到；提起

think of 考虑；想出；认为

#### about 短语

care about担心；关心

be worried about担心

think about考虑

#### for 短语

call for需要；要求；提倡；号召

care for关怀；照顾

except for除了……之外

fight for为……而战

leave for...前往……

look for寻找

pay for sth,

send for派人去请　

#### away 短语

give away分发；赠送

put away收好；放好

take away拿走；带走

throw away扔掉　

#### out 短语

break out （火灾、战争等) 突然发生，爆发

out of breath上气不接下气

find out发现

give out分发

go out出去

make out理解；明白

point out指出

run out用完

sell out卖完

send out发出

set out动身；出发

turn out证明是；结果是

work out算出；解决　

#### with 短语

agree with sb同意某人

be angry with sb生某人的气

keep up with赶上

catch up with赶上；追上

come up with提出；想出

be covered with被……覆盖

deal with处理；对待

be filled with充满

make friends with sb与某人交朋友

get along / on (well) with sb 与某人相处（融洽）

help sb with sth 帮助某人做某事

#### over 短语

come over顺便来访

fall over跌倒

go over复习；温习；检查

look over检查

think over仔细考虑

turn over把……翻过来；移交；转交

#### down 短语

break down损坏；中断

cut down砍倒；削减

get down 下来

go down 下落；下降

pull down拆毁

put down放下；写下

set down放下；记下

shut down把……关上；关闭

take down记下；拆卸

turn down关小；调低

write down写下；记下　

#### from 短语

across from...在......的对面

break away from...脱离......

be different from...与......不同

hear from...收到......的来信

learn from...向......学习

tell...from...区分 / 辨别......和......

#### up 短语

bring up提出；呕吐；养育

call up打电话；想起

cheer up使……振奋

fill up填满；装满

fix up修理；安装

go up上升；上涨

grow up长大（成人）

hurry up赶快；赶紧

look up查阅；查找；向上看

make up one's mind下定决心

pick up (sb)拾起；搭载 / 接载（某人）

put up举起；挂起；张贴；搭建

save up存钱；积蓄

set up创立；建立

show up出席；露面

sit up坐起来

stay up不睡；熬夜

shut up住嘴

speak up大声说

think up想出

tidy up整理

turn up开大；调大（音量）

#### off 短语

fall off从……跌落

get off下车

give off发出（光、热、气味等）

hurry off匆匆离去

put off推迟

see sb off为某人送行

set off出发；启程

show off显示；夸耀

take off脱下;起飞

turn off关掉 (收音机、电灯、煤气等)　

### 重叠式短语

again and again再三地；反复地

arm in arm臂挽着臂

day after day日复一日

from door to door挨家挨户

from house to house挨家挨户

face to face面对面

here and there到处；处处

less and less越来越少

all day and all night整日整夜

one by one一个接一个地

hand by hand手把手

side by side肩并肩；一个接一个

step by step逐步