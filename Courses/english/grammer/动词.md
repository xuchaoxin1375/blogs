[toc]

## 动词

动词是表示动作(action)或状态(state)的词,是一种非常重要的词类,用法也较复杂。

动词在句中主要作谓语,有人称,数、时态、语态、语气的变化。

不同时态和语态的动词可以组成动词短语,在句中可作主语、宾语、定语、状语、宾语补足语等。

句子成分中的谓语的充当者只能是动词(短语),而谓语是英语句子中必要的,(和主语一样都是必须的成分),因此了解动词是十分重要的,谓语/非谓语也是所有句子成分中最灵活多变的.

## 动词分类👺

- 按照用法，动词可分为**及物动词和不及物动词**
- 按照本身意义，动词可分为**实义动词、系动词、 助动词和情态动词**。
- 按照在句中的语法功能，动词可以分为**限定动词 和非限定动词**。

### 根据意义与句法作用分类👺

根据意义与句法作用,英语动词可分为四类

| 类别                        | 距离             |
| --------------------------- | ---------------- |
| 行为动词(Dative Verb)       | work,study,play  |
| 连系动词(Link Verb/Copular) | be，seem，become |
| 助动词(Auxiliary Verb)      | shall，will，do  |
| 情态动词(Modal Verb)        | can,may，must    |

谓语的核心是动词，分为简单动词和复合动词。

例如：

**She sings**.她唱歌

**He has been studying**.他一直在学习

#### 情态动词

情态动词（Modal verb），本身有一定的词义，**表示语气**的单词。

但是情态动词**不能独立作谓语，只能和动词原形一起构成谓语**。(情态动词后面不能跟**非动词**)

情态动词用在行为动词前，**表示说话人对这一动作或状态的看法或主观设想**。 

情态动词虽然数量不多，但用途广泛，英文中常见的有： can (could), may (might), must, need, ought to, dare (dared), shall (should), will (would)。

情态动词是指表达说话者的情绪或者态度的词。情态动词本身有一 定的含义，但需要和实义动词一起使用才能表达完整的意义。 

1. 情态动词一般没有人称和单复数的变化，大部分也没有时态变化。
2. 情态动词之后跟动词原形。

##### 情态动词分类归纳

1. 表示能力：can / could
2. 表示允许/可能性：may / might
3. 表示必须或义务：must
4. 表示建议：shall / should
5. 表示意愿：will / would
6. ....

| 情态动词                   | 用法                                             | 同义表达                       | 示例                                                         |
| -------------------------- | ------------------------------------------------ | ------------------------------ | ------------------------------------------------------------ |
| can/could                  | 能够，会，可以，可能，表“请求、许可”             | be capable of, be competent in | These intensely powerful mental events can be **harnessed**.这些强大的心理活动是可以被利用的。<br />Could I have a look at your new phone? |
| may/might                  | 许可，可能性                                     | be likely to                   | Dreaming may not entirely belong to the unconscious.         |
| must/have to               | 必须（强调客观），“不得不”（强调主观）           | be bound to, be obliged to     | We must concentrate more on co-productions, the exchange of news, documentary services and training. |
| should/ought to/had better | 应该                                             | be supposed to, be obliged to  | Death should be accepted as a fact of life.                  |
| will/would                 | 将会；想要                                       | intend to                      | This would increase the oil import bill in rich economies by only 0.25%~0.5% of GDP. |
| need                       | 需要（实义动词：need to do）                     |                                | We need take full advantage of the Internet to construct the energy and telecom infrastructures. |
| dare                       | 敢                                               |                                | No regular advertiser dare promote a product that fails to live up to the promise of his advertisements. |
| would rather/sooner        | 宁愿                                             |                                | I would rather (或would sooner) begin at the bottom and climb to the top.<br/>我宁愿从底部开始爬到顶点。<br/>In order to pursue light and heat, people would rather (或would sooner) lose their lives. 为了追求光和热，人宁愿舍弃自己的生命。 |
| used to                    | 表示过去的事实或状态，而且暗示现在已经不再如此。 |                                | I used to be a coward, but now I’m not. <br/>我过去是个懦夫，但现在不是了。<br/>My father used to say, “It is important to learn from your mistakes.”<br/>父亲过去常说：“从过去的错误中学习很重要。”<br/>He used to smoke, but knowing what a bad habit it is, he stopped three years ago. <br/>他过去常吸烟，但是了解到这是一个坏习惯之后，在三年前就戒了。 |

##### 情态动词构成结构



| 情态动词后接形式 | 功能                   | 例句                                                         |
| ---------------- | ---------------------- | ------------------------------------------------------------ |
| 动词原形         | 常规使用               | She **can play** the piano.（她会弹钢琴。）<br />You **[ought to] go** to bed early.（你应该早点睡。） |
| have + 过去分词  | 对过去的推测或虚拟情况 | They **must have left**.（他们肯定已经离开了。）             |
| be + 现在分词    | 表示动作正在进行       | He **might be sleeping** now.（他现在可能正在睡觉。）        |
| be + 过去分词    | 表示被动语态           | The work **should be finished** soon.（这项工作应该快完成了。） |

值得注意的是,有些结构是情态动词性质的短语(情态动词短语),比如`ought to`,把它当做一个情态动词来使用,而不应该分开看待

##### 情态动词的完成时表示推测

情态动词+现在完成时的用法,四个表示推测的情态动词完成时。

很多时候，情态动词之后跟现在完成时，**表示对过去事情的猜测或思考**等。

情态动词之后的现在完成时其基本表达为have done，其中的 have没有人称和数的变化

- must have done:一定做过某事
- needn't have done:本没必要做某事，但是做了
- could have done:本能够做某事，但没有做
- should have done:本应该做某事

（can 或could+现在完成时表示“本可以做某事”，而实际上当时并没有 做）
（may或might+现在完成时表示“可能做过某事”，是对过去的猜 测）
（must +现在完成时表示 “一定做过某事”，是对过去语气非常肯定的猜测）
（should或ought to +现在完成时表示“本该做某事”， 而实际上当时没有做）
（should not或ought not to +现在完成时表示“本不该做某事”，而实际上当时做过）
（need not +现在完成时表示“本来不必做 某事”，而实际上做过）

#### 实义动词(行为动词)

实义动词（实意动词）与系动词是相对的，系动词亦称连系动词（Linking Verb），作为系动词，它本身有词义，但不能单独用作谓语，后边必须跟表语，构成系表结构说明主语的状况、性质、特征等情况。

实义动词意思完全相反，能独立用作谓语。实义动词有**及物动词和不及物动词**（及物动词是指后面要求有直接宾语的动词；不及物动词指后面不需要跟宾语的动词，如果跟宾语要加介词。） 即**行为动词**,表示动作的动词。

- 及物动词：后面直接加宾语，并且必须加宾语。比如：accept the idea,buy a book.
- 不及物动词：后面不能直接加宾语，必须要加了介词以后才能加宾语。比如：I agree with you

那如何区分及物动词和不及物动词呢？
1)根据意思：

- eat
- kiss

2)“动词+介词”中的动词为不及物动词：

- arrive in 抵达，到达
- abound in 大量地出产

3)根据词典或单词表的词形直接记住：
V:既可以作及物动词，也可以作不及物动词
Vt.:及物动词
Vi:不及物动词

#### 系动词

系动词，也称连系动词（Linking verb），是用来辅助主语的动词。

它本身有词义，但不能单独用作谓语，其后必须跟表语，构成系表结构说明主语的状况、性质、特征等情况。

在英语中，系动词的功能主要是把表语（名词、形容词、某些副词、非谓词、介词短语、从句）和它的主语联系在一起，说明主语的性质、特征、品性或状态。

系动词它有自己的但不完全的词义，不能在句中独立作谓语，必须和后面的表语一起构成句子的谓语。
英语连系动词(LinkVerb/Copular)本身有词汇意义,但不能单独构成动词词组，必须同表语结合，构成“系表结构”。

连系动词可分为两类：完全连系动词和不完全连系动词。

- 完全连系动词只有be,通常表示“是，存在”。
- 不完全连系动词共有40多个，表示感觉、变化等，描述主语的特征、性质、身份或状态。连系动词be后跟的表语可以是名词、代词、副词、-ing分词、-ed分词、介词短语和从句。



##### 词汇意义

不完全连系动词分析：连系动词的三种词汇意义
（1）表示某种持续状态，表语的作用是现状属性（CurrentAttribute）。
这类动词以remain为代表，还包括：rest依然是/保持，stand，lie，keep，continue，stay，loom，sit，hold等

- It stayed fresh.它依然新鲜
- He stayed young.他仍然年轻
- The paper stayedblank.这纸依然空白

（2）表示具有某种性质、特征、外表、感觉或处于某种状态或情况，表语的作用是现状属性（Current
Attribute)。

这类动词有：smell，sniff，live，look，sound，marry，mean，break，bleed，lick，feel，eat;
seem，play 假装是，ring 听上去/听起来是，moan，die，cut，buy，appear，shine，taste，awake,
read，act，stand等。

- I dare say I stand innocent of any wrong.我敢说，我没有任何过错。
- The trees stood red，orange and yellow all over the hills.满山遍野到处都是红色的、橙色的和黄色的树。

（3）表示动词的动作和过程所产生的结果，或状态的开始，逐渐具有某种性质，这类动词后的表语通常具有结果属性（ResultingAttribute）。

这类动词（短语)有：become，burst，cook，go，grow，stop，run，rise，wear，prove，pass，wash，turn， fly，flush,fall,drop,dawn,draw,blush,burn，stain，take,work，grow，get, frecze,come，plead，slam，turn out，come in，come off,end up，wind up 等。

- The leaves have turned red in the hills.山里的树叶红了。
- It's becoming a serious problem.它正成为一个严重的问题。
- He became very fond of her.他变得非常喜欢她。

##### 常见类型归纳

1)状态：be动词（am，is，are）(连接主语和表语，无意义，用于构成句子)
2)感官：feel,sound,smell,taste,look
3)变化：get，become，turn，grow，make，come，go，fall，run
4)保持：remain，keep，stay，continue，stand，rest，lie，hold
5)表象(似乎)：seem,appear
6)终止或结果：prove

可带名词作表语：become，make，look，sound，fall，turn，prove，remain。其中turn后接的是单数名词大多不用冠词（例：He turned teacher）

##### 区分

连系动词多有自己的意思，但不能独立作谓语，必须与表语（名词、形容词，或与之相当的词类、短语、从句）一起构成合成谓语。最常用的连系动词为be（系）动词，即完全连系动词，另外还有look，turn，feel，get，become，sound，smell，stand，go，remain等半连系动词。

无论是完全连系动词还是半连系动词、后面都有表语。半连系动词是由实意动词变来的，分辨该动词为实义动词还是半连系动词可以尝试用[比较法](https://baike.baidu.com/item/比较法/5108391?fromModule=lemma_inlink)和[替换法](https://baike.baidu.com/item/替换法/4994416?fromModule=lemma_inlink)。

##### 结构



| 语法结构         | 描述                                                         | 例子                                                         |
| ---------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 系动词+形容词    | 用于描述主语的状态或性质                                     | She is beautiful. （她很漂亮。）                             |
| 系动词+名词/代词 | 用于说明主语的身份、职业、角色或特征                         | He is a doctor. （他是一名医生。）                           |
| 系动词+副词      | 副词作为表语，用于描述主语的状态或程度                       | The door is already closed. （门已经关了。）                 |
| 系动词+分词      | 现在分词表示主动或正在进行的动作，过去分词表示被动或完成的动作 | The window is broken. （窗户破了。）（过去分词）<br />He is interesting. （他很有趣。）（现在分词，但已形容词化）<br />The cake is being baked. （蛋糕正在被烤制。）（现在分词，表示正在进行的动作） |
| 系动词+介词短语  | 介词短语作为表语，描述主语的位置、状态或与其他事物的关系     | The book is on the table. （书在桌子上。）                   |

#### 助动词👺

协助主要动词构成谓语的词叫助动词（Auxiliary Verb），也叫辅助动词。

- 被协助的动词称作主要动词（Main Verb）。
- 助动词用来构成**时态和语态**。 
- 助动词具有语法意义，但除情态助动词外没有词汇意义

助动词是指**与实义动词连用，表示各种[时态、语态、否定和疑问]句型， 其本身没有实际含义**。

助动词主要有三个，分别是：**be, do,have** (另外还有情态助动词)

- **主要的助动词**包括：
  - be的各种形式（am, is, are, was, were, been, being）
  - do的各种形式（do, does, did）
  - have的各种形式（have, has, had）
- 详情见相应章节(三类主要助动词)

这三个助动词除了作助动词外，**be**还可以作系动词，**do和have**还可以作实义动词

##### 常见助动词分类

**1. 一般助动词**：

- do, does, did：用于一般现在时和一般过去时的否定句和疑问句。
  - 例如：Do you like coffee? He doesn't like tea. Did you go to the park?


**2. 情态助动词**：

- can, could：表示能力或可能性。例如：She can swim. Could you help me?
- may, might：表示可能性或许可。例如：It may rain tomorrow. Might I ask a question?
- will, would：表示将来或意愿。例如：I will call you tomorrow. Would you like some coffee?
- shall, should：表示建议或义务。例如：Shall we dance? You should eat more vegetables.
- must：表示必须或必要。例如：You must finish your homework.

**3. 完成时助动词**：

- have, has, had：用于表示完成时态。例如：She has finished her work. They had left before I arrived.

##### 总结表格

以下是常见助动词分类的表格：

| 助动词类别   | 助动词         | 用途说明                                   | 示例句                                                       |
| ------------ | -------------- | ------------------------------------------ | ------------------------------------------------------------ |
| 一般助动词   | do, does, did  | 用于一般现在时和一般过去时的否定句和疑问句 | Do you like coffee? He doesn't like tea. Did you go to the park? |
| 情态助动词   | can, could     | 表示能力或可能性                           | She can swim. Could you help me?                             |
|              | may, might     | 表示可能性或许可                           | It may rain tomorrow. Might I ask a question?                |
|              | will, would    | 表示将来或意愿                             | I will call you tomorrow. Would you like some coffee?        |
|              | shall, should  | 表示建议或义务                             | Shall we dance? You should eat more vegetables.              |
|              | must           | 表示必须或必要                             | You must finish your homework.                               |
| 完成时助动词 | have, has, had | 用于表示完成时态                           | She has finished her work. They had left before I arrived.   |



| 助动词                  | 用法                                                         | 举例                                                         |
| ----------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| be                      | 1\. 帮助构成进行时<br>2\. 被动语态                           | As the cartoon describes, a hen is making a promise.<br>The story is claimed to be very instructive. |
| do/did/does             | 1\. 一般现在时、一般过去时的否定和疑问<br>2\. 强调谓语动词（强调谓语只能用do/did/does）<br>3\. 倒装 | The picture did not show us profound meaning.<br>The picture did show us profound meaning.<br>Only yesterday did he find out that his watch was missing. |
| have/has                | 帮助构成完成时                                               | Republicans left in the recent Euro-elections have forced him to eat his words and stand down. |
| will/shall/should/would | 帮助构成将来时                                               | Imitation of behaviors will mislead behavioral studies.      |

## 三类主要助动词

### A.助动词be

助动词be有is, am, are, was, were, been和being等形式，**在句中可以和实义动词一起构成进行时态和被动语态**，如：

#### be构成进行时态

Still I am learning.
我仍在学习。
Believe that pleasantness is coming. 
相信吧，愉快的日子正在来临。
A learning organization is a group of people who are continually enhancing their capability to create their future. 
学习型组织是一群不断加强自身能力、创造未来的人。

#### be构成被动语态

A hero cannot be judged by success and failure.
不能以成败论英雄。
Character is made in the small moments of our lives. 
性格是在我们人生的各种无关紧要的时刻形成的。 
Life is truly known to those who endure adversity. 
忍受苦难的人真正了解生活。

### B. 助动词do

助动词do有do, does和did三种形式，在句中可以**和实义动词一起构成否定和疑问**，**也可以用于对动词的强调以及代替前面出现过的动词以避免重复**。

#### do构成否定

Don’t value money for its own sake, but for what you can do with it.
不要看重金钱本身，而是看重它的应用价值。
The past doesn’t equal the future.
过去不等同于未来。
If you don’ t learn how to use pain and pleasure, life controls you. 
假如学不会如何利用痛苦和快乐，你就只能任由生活摆布。

#### do构成疑问句

Do you believe that you will succeed by luck?
你相信自己会凭运气成功吗？
Does he believe in himself? 
他相信自己吗？
Why do many of us struggle with work? 
为什么我们有很多人工作起来勉为其难呢？

#### do用于代替前面出现过的动词以避免重复

Difficulties strengthen the mind, as labor does the body.
困难磨练意志，劳动增强体质。（does 代替strengthens）
When you expect things to happen, often they do. 
你期望事情的发生，往往事情就会发生。(do 代替happen）
It takes less time to make a better decision than it does to correct 
a poor decision. (does 代替takes） 
做出更好的决定所用的时间比改正糟糕的决定所用的时间少。

### C. 助动词have

助动词have有has, have和had三种形式，**和实义动词一起构成完成时态**。

He has achieved success who has lived well, laughed often, and loved much. 
善于生活，时常欢笑，充满爱心的人已经取得了成功。
If you have made mistakes, there is always another chance for you.
即使你犯了错误，你总还有机会。
Once we have clearly defined our thoughts, resources come to us.
一旦我们对想法有了明确的界定，资源就会出现。
By that time, the biggest lesson they had learned was the importance of thinking.

到那时为止，他们最大的收获就是明白了思考的重要性。

## 助动词

### 助动词分类

英语中的助动词可分为两类：**基本助动词(Primary Auxiliary)** 和 **半助动词(Semi-Auxiliary)**。

### 半助动词

半助动词是兼有**实义动词**和**助动词**双重特征的动词。

半助动词有一定的词汇意义,既像**实义动词**一样**前面**可以有**助动词、情态动词或其他半助动词**

又像**助动词**一样可以和实义动词一起构成**复合谓语**。

(1) **基本助动词的形式：**

基本助动词本身没有词义，不能单独作谓语，只用于辅助主动词构成各种时态、语态、语气、疑问式和否定式等。

| 基本助动词 | 现在时    | 过去时   |
| ---------- | --------- | -------- |
| be         | am/is/are | was/were |
| do         | do/does   | did      |
| have       | have/has  | had      |
| shall      | shall     | should   |
| will       | will      | would    |

(2) **半助动词的构成方式：**  
半助动词本身有词义，但又不同于定式连用构成复合谓语，才能表达完整意义。

**半助动词有如下两种构成方式：**

| 结构                                 | 用法                                                         |
| ------------------------------------ | ------------------------------------------------------------ |
| be + adj./adv./v-ed + to-infinitive; | be able to, be about to, be afraid to, be apt to, be bound to, be certain to, be/feel disinclined to, be/feel disposed to, be due to, be eager to, be fit to, be going to, be likely to, be inclined to, be obliged to, be liable to, be prone to, be ready to, be reluctant to, be supposed to, be sure to, be unable to, be unlikely to, be unwilling to, be willing to 等。 |

| 结构                                                         | 用法                                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| appear/chance/happen/seem/come/get/tend/turn out, etc. + to-infinitive | appear, seem, happen, chance 后的不定式可用一般式、完成式和进行式，并可用于 there appear/seem/happen/chance to do 结构。come to 和 get to 表示过程或状态的变化，tend to 表示“倾向于，易于”，turn out 表示“结果是”。 |

### 以下为常见的半助动词

- be型半助动间: be (un)wiling to、 be (un)able to、 be about to、 be going to、 be to、 be supposed to、 be (un)likelto、be sure to、be apt to, be bound to、 be certain to、 be due to、 be liable to、 be obliged to 等。
- 其他型半助动词:tend to、seem to、appear to、happen to、come to  fail to ,get to、turn out to 等。

以下是两组常见的半助动词及其短语集合的解释和例句，以两个表格展示。

####  be型半助动词

| 半助动词短语      | 含义                     | 例句                                                         |
| ----------------- | ------------------------ | ------------------------------------------------------------ |
| be (un)willing to | (不)愿意做某事           | She is unwilling to attend the meeting. （她不愿意参加会议。） |
| be (un)able to    | (不)能够做某事           | He is able to solve the problem. （他能解决这个问题。）      |
| be about to       | 即将做某事               | I am about to leave. （我即将离开。）                        |
| be going to       | 打算做某事；将要做某事   | She is going to start a new job. （她打算开始新工作。）      |
| be to             | 注定要做某事；将要做某事 | The conference is to begin at 9 a.m. （会议将于早上9点开始。） |
| be supposed to    | 应该做某事               | You are supposed to finish your homework. （你应该完成作业。） |
| be (un)likely to  | (不)可能做某事           | She is likely to pass the exam. （她可能会通过考试。）       |
| be sure to        | 一定会做某事             | He is sure to win the game. （他一定会赢得比赛。）           |
| be apt to         | 倾向于做某事             | People are apt to forget things when they are busy. （人们在忙碌时容易忘事。） |
| be bound to       | 必定会做某事             | They are bound to succeed. （他们一定会成功。）              |
| be certain to     | 一定会做某事             | She is certain to arrive late. （她肯定会迟到。）            |
| be due to         | 预计会做某事；因为       | The train is due to arrive at 6 p.m. （火车预计6点到达。）   |
| be liable to      | 有可能做某事；有...倾向  | He is liable to make mistakes when tired. （他累了时容易犯错。） |
| be obliged to     | 有义务做某事；必须做某事 | She is obliged to follow the rules. （她必须遵守规则。）     |

#### 其他型半助动词

| 半助动词短语 | 含义           | 例句                                                         |
| ------------ | -------------- | ------------------------------------------------------------ |
| tend to      | 倾向于做某事   | People tend to gain weight as they age. （人们随着年龄增长容易发胖。） |
| seem to      | 似乎做某事     | He seems to be tired. （他似乎很累。）                       |
| appear to    | 看起来像做某事 | She appears to be happy. （她看起来很高兴。）                |
| happen to    | 碰巧做某事     | I happened to see him at the mall. （我碰巧在商场见到了他。） |
| come to      | 开始做某事     | He came to realize his mistake. （他开始意识到自己的错误。） |
| fail to      | 未能做某事     | She failed to submit the report on time. （她未能按时提交报告。） |
| get to       | 得以做某事     | You will get to meet him tomorrow. （你明天会见到他。）      |
| turn out to  | 结果是做某事   | The event turned out to be a success. （结果活动很成功。）   |

#### 双重特性说明

半助动词既具备**实义动词（Lexical Verb）**的功能，又可以像**助动词（Auxiliary Verb）**一样与其他动词一起构成复合谓语。以下是一些半助动词在两种用法下的示例，帮助说明这种双重特性：

例如: tend to

- **作为实义动词**：表示“倾向于某种行为或特征”。
  - **例句**：He **tends** to be late for meetings.（他开会往往迟到。）
    - 这里的 "tend" 是**实义动词**，表示某种习惯或倾向。
  
- **作为助动词**：与其他动词（如"to be"）一起构成复合谓语。
  - **例句**：People tend to **forget** names easily.（人们容易忘记名字。）
    - 这里的 "tend" 像**助动词**一样，帮助 "forget" 形成复合谓语。

以下是半助动词既作为**实义动词**又作为**助动词**使用的示例：

| 半助动词        | 作为实义动词的含义及例句                                     | 作为助动词的含义及例句                                       |
| --------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| **tend to**     | **倾向于某种行为或特征** <br> He tends to be late for meetings.（他开会往往迟到。） | **与动词形成复合谓语** <br> People tend to forget names easily.（人们容易忘记名字。） |
| **seem to**     | **看起来像，似乎** <br> He seems happy today.（他今天看起来很高兴。） | **与动词形成复合谓语** <br> She seems to know the answer.（她似乎知道答案。） |
| **happen to**   | **偶然发生** <br> Something strange happened yesterday.（昨天发生了一些奇怪的事情。） | **偶然做某事，与动词结合** <br> I happened to meet her at the mall.（我碰巧在商场遇见了她。） |
| **be about to** | **即将做某事** <br> I am about to leave.（我正要离开。）     | **表即将发生的动作** <br> She is about to start her new job.（她即将开始新工作。） |
| **fail to**     | **失败，未能完成** <br> He failed in his attempt to climb the mountain.（他登山的尝试失败了。） | **未能做某事** <br> She failed to submit her report on time.（她未能按时提交报告。） |



### 根据与宾语和补语的关系分类

英语动词从其与宾语和补语的关系上看,还可分为四类

1. 完全及物动词(Complete Transitive Verb)
2. 不完全及物动词(Incomplete Transitive Verb)
3. 完全不及物动词(Complete Intransitive Verb)
4. 不完全不及物动词(Incomplete Intransitive Verb)

### 根据语法结构分类

按语法结构,动词分为限定动词(Finite Verb)和非限定动词(Non-finite Verb)两种

#### 限定动词

限定动词在句中作谓语时,要受主语的限定,在人称和数上必须同主语保持一致,因而有人称、数、时态、语态和语气的变化。

例

I'm hoping for an interview next week.我盼望着下星期有一次面试
You are always complaining.你总是抱怨。
Their baby bears a strong resemblance to its grandfather.他们的婴儿与祖父十分相像,
Although he was badly injured,his heart was still beating.虽然他伤得很重,但他的心脏仍在跳动
The hills are alive with the sound of music.山丘随着音乐之声而生机盎然。
I'd have crashed my car but for your warning.要不是你的提醒,我就把车撞坏了。
This problem is now being looked at by some of the ablest minds in the country.现在全国一些最有
才干的人正在研究这一问题。

#### 非限定动词

非限定动词包括:动词不定式(Infinitive)、分词(Participle)和动名词(Gerund)。

在形式上,非限定动词不受主语的人称和数的制约。

在句法功能上,非限定动词不能独立作谓语,但可与一定的助动词结合构成复合谓语,或在句中充当主语、表语、宾语、定语、状语或补足语。

例

 He's never been able to admit his mistakes.他从不能承认错误。
You can't expect to get everything you need at the push ofa button.你不能期望能轻而易举地得到
你想要的每样东西。
I have so much to tell you;Idon't know where to begin,我有很多话跟你说,简直不知从何说起
She’ll never buy that story about you getting lost!她决不会相信你迷路的鬼话。
I love the house - it's wonderful being able to see the sea from my window.我喜欢这房子--从
我的窗口可以看见大海,太好了。
The point is worth mentioning.这一点值得提及。



### 根据在作谓语的动词短语中的功能分类

英语句子的谓语,是由动词短语(Verb Phrase)来体现的。

作谓语的动词短语,可以是一个或一个以上的动词构成。根据在动词短语中的功能,动词可分为两大类。

1. 主动词(Main Verb),又称为“全义动词”(Full Verb),或“实义动词”(Lexical Verb),是动词短语的核心,决定着句子的基本格局,如:leave，study，contribute，succeed 等。
2. 助动词(Auxiliary Verb)。



## 补充

### 利用助动词和情态动词改为倒装句

所有助动词和情态动词都可以帮助句子改装为倒装句

Under the table was lying a half-conscious young man.桌子底下躺着一个神志不清的年轻人
Only after things disappear will we cherish them.人们只有到失去了才懂得珍惜。
Nor did he let the disease stop him from living the kind of life he has always dreamt about.
疾病没有使他放弃过上梦想中的生活。
Under no circumstances should you lend Paul any money..在任何情况下你都不应该借钱给保罗。
Only by studying hard can students pass the exams.只有努力学习，学生们才能通过考试。

