$$
\begin{array}{lcccc} 
& \text { 符号位 } S & \text { 阶码 } & \text { 尾数 } & \text { 总位数 } \\
\text { 短实数 } & 1 & 8 & 23 & 32 \\
\text { 长实数 } & 1 & 11 & 52 & 64 \\
\text { 临时实数 } & 1 & 15 & 64 & 80
\end{array}
$$

