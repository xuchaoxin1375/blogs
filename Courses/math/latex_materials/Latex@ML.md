[toc]

- 数据集:$D=\{(\boldsymbol{x}_1,y_1),(\boldsymbol{x}_2,y_2),\cdots,(\boldsymbol{x}_m,y_m)\}$
  - 简写:$D=\{(\boldsymbol{x}_i,y_i)\}_{i=1}^{m}$,表示数据集由m个**样例**
  - 如果数据集中样例只有一个属性:$D=\{({x}_i,y_i)\}_{i=1}^{m}$
  - $\boldsymbol{x}_i=(x_{i1};x_{i2};\cdots;x_{id})$,$y_i\in\mathbb{R}$
- 
- 

