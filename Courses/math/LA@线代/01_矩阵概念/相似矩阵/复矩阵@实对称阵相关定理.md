[toc]



## 复矩阵和复向量

- 元素是复数的矩阵和向量分别称为**复矩阵**和**复向量**



### 共轭矩阵

- 设$a_{ij}$是**复数**,$A=(a_{ij})_{m\times{n}},\overline{A}=(\overline{a_{ij}})_{m\times{n}}$,$\overline{a_{ij}}$和$a_{ij}$互为共轭复数,则称$A,\overline{A}$互为**共轭矩阵**
- 类似的设复矩阵:$B=(b_{ij})_{n\times{l}}$

### 性质

1. 对于复数$z_1,z_2,$有$\bar{z_1}\bar{z_2}=\overline{z_1z_2}$

   1. $a\in\mathbb{R},\bar{a}=a$
   2. $a\cdot\overline{z_1}=\overline{az_1}$
      - $-\overline{z_1}=\overline{-z_1}$
      - $+\overline{z_1}=\overline{+z_1}$

   1. $\overline{z_1+z_2}$=$\overline{z_1}+\overline{z_{2}}$

2. $\overline{\overline{A}}=A$

3. $\overline{A^T}=\overline{A}^T$

4. 如果$A$是**实矩阵**,则$\overline{A}=A$

5. 如果$A$是**实对称阵**,则$\overline{A^T}=A$
  - 证明:任意对称阵:满足$A^T=A$,而若$A$还是实矩阵,则由性质(3):$\overline{A}$=$A$
  - 从而$\overline{A^{T}}$=$\overline{A}$=$A$,也就是得到性质(4)
  - 这说明,对实对称阵取共轭运算和转置运算都不改变原矩阵

6. $\overline{kA}=\overline{k}\cdot\overline{A}$
  - $k,a_{ij}\in\mathbb{C}$
  - $\overline{ka_{ij}}$=$\overline{k}\cdot\overline{a_{ij}}$,将$\overline{k}$提出到矩阵外即得本性质

7. $\overline{A+B}$=${\overline A+\overline B}$

8. $\overline{AB}$=$\overline A\;\overline B$

  - 记$C$=$\overline{AB}$=$(c_{ij})_{m\times{l}}$
    - $c_{ij}=\sum_{i}^{l}a_{ik}b_{kj}$
    - $\overline{c_{ij}}=\sum_{i}^{n}\overline{a_{ik}b_{kj}}$=$\sum_i^n{\overline {a_{ik}}\cdot\overline{b_{kj}}}$;

  - 记$D$=$\overline A\;\overline B$
    - $d_{ij}$=$\sum_i^n{\overline {a_{ik}}\cdot\overline{b_{kj}}}$

  - 显然$\overline{c_{ij}}$=$\overline{d_{ij}}$

9. $\overline{(AB)^T}$=$\overline{B^TA^T}$=$\overline{B^T}\ \overline{A^T}$

10. 若$A$可逆,则$\overline{A^{-1}}=(\overline{A})^{-1}$

  - 从验证等式是否满足可逆的定义的角度证明该结论
    - 对于可逆矩阵相关结论,采用对结论式两边分别乘以统一矩阵,这个矩阵通过观察结论式两边来选取
    - 结论翻译为文字描述即$\overline{A}$的逆矩阵为$\overline{A^{-1}}$
      - 结论等号右边右乘$\overline{A}$:则:$\overline{A}(\overline{A})^{-1}=E$
      - 等号左边也右乘$\overline{A}$,则:$\overline{A^{-1}}\;\overline{A}$=$\overline{A^{-1}A}$=$\overline{E}$=$E$

    - 再根据矩阵逆的唯一性,可以确定原结论成立

11. $\large |\overline{A}|=\overline{|A|}$

   - $$
     |\overline{A}|=
     \sum\limits_{k}{(-1)}^{\tau(p_k)}\prod_{i=1}^{n}{\overline{\theta_{i}}}
     =\overline{\sum\limits_{k}{(-1)}^{\tau(p_k)}\prod_{i=1}^{n}{\theta_{i}}}
     =\overline{|A|}
     $$

## 定理

### 实对称阵特征值性质定理

- **实对称阵**的特征值都是**实数**

- 证明

  - 对称阵是方阵

  - 设$\lambda$是实对称阵A的任意一个特征值,则

    - $A\alpha=\lambda\alpha$,$(\alpha\neq{0})$`(1)`
      - $\bar\alpha\neq{0}$`(1-1)`
      - $(\bar\alpha)^T\alpha$=$\sum_{i}^{n}(a_i^2+b_i^2)>0$`(1-2)`
    - $\overline{A}=A$`(2)`,$A^T=A$`(3)`
    - $(\overline{A})^T=\overline{A^T}$`(4)`

  - 需要证明的内容是$\overline\lambda=\lambda$`(5)`

    - 构造式$(\bar\alpha)^T(A\alpha)$`(6)`,由式(3),式(6)变为$(\bar\alpha)^TA^T\alpha
      $`(6-1)`
      - 这里式(6)左乘的是行向量$(\bar{\alpha})^T$而不是列向量$\bar{\alpha}$是为了使得乘法可以执行(规格)
    - 再由矩阵乘法的转置公式,式(6-1)变为$(A\bar\alpha )^T\alpha$`(6-2)`
    - 再利用式(2),式(6-2)变为$(\bar{A}\bar\alpha)^T\alpha$`(6-3)`
    - 逆用复矩阵乘法共轭公式,式(6-3)变为$(\overline{A\alpha})^T\alpha$`(6-4)`
    - 两端分别用式(1)代入,$(\bar{\alpha})^T\lambda\alpha$=$(\overline{\lambda\alpha})^T\alpha$`(7)`
    - 调整常数$\lambda$位置,式(7)左边变为$\lambda(\bar\alpha)^T\alpha$`(7-1)`;式(7)右边变形为$(\overline{\lambda}\bar\alpha)^T\alpha$=$\overline{\lambda}(\bar\alpha)^T\alpha$`(7-2)`(常数$\overline\lambda$的转置等于其本身
    - 比较(7-1,7-2) $(\lambda-\bar\lambda)(\bar\alpha)^T\alpha=0$

    - 由(1-2)式,$(\bar\alpha)^T\alpha>0$,所以$\lambda-\bar{\lambda}=0$,即$\lambda=\bar\lambda$


### 实对称阵的特征向量性质定理

- **实对称阵**的关于<u>不同特征值的特征向量</u>**彼此正交**🎈

  - 设$\lambda,\mu$是**实对称阵**$\bold{A}$,$(\bold{A^{T}=A})$`(0)`的两个不同的特征值($\lambda\neq{\mu}$),即:$A\alpha=\lambda\alpha$`(1)`;$A\beta=\mu\beta$`(2)`


  - 两向量正交是指两向量的内积为实数0


  - $\lambda(\alpha,\beta)$=$(\lambda\alpha,\beta)$=$(A\alpha,\beta)$=$(A\alpha)^T\beta$=$\alpha^TA^T\beta$=$\alpha^TA\beta$=$\alpha^T(A\beta)$=$(\alpha,A\beta)$=$(\alpha,\mu\beta)$=$\mu(\alpha,\beta)$
    - 两端做差,得$(\lambda-\mu)(\alpha,\beta)=0$;
    - 因为${\lambda}\neq{\mu}$,所以$(\alpha,\beta)=0$

  - 类似地:$A\alpha_i=\lambda_i\alpha_i$,$(i=1,\cdots,s)$,均有$(\alpha_i,\alpha_j)=0,(\lambda_i\neq{\lambda_j})$

    - s表示A有s个互异的特征值

### 实对称阵的对角化

- **实对称阵**一定可**对角化**(并且是**正交相似对角化**)

  - 一定存在**正交矩阵Q**($Q^TQ=E)$使得**实对称阵**$A$满足$Q^{-1}AQ=\Lambda$($\Lambda$为某个对角阵)

- $n$阶实对称阵一定有$n$个**正交的单位特征向量**$\Phi:\alpha_1,\cdots,\alpha_n$

  - 因为可以将<u>可对角化实对称阵</u>的n个线性无关向量进行
    - Gram-**Schmidt** **orthogonalization**方法**正交化**
    - 再进行单位化
  - 记$Q=(\Phi)$,则:$Q^{-1}AQ=\Lambda$=$\mathrm{diag}(\lambda_1,\cdots,\lambda_n)$

  - $\forall{A},A^T=A, \exist{Q},Q^TQ=E,s.t.\:Q^{-1}AQ=\Lambda$
  - 也可用**合同**的概念描述该定理:实对称阵A合同于对角阵$\Lambda$

- 如果**实矩阵**$A$和某个<u>对角阵$\Lambda$</u>**正交相似**($\exist{Q}, Q^TQ=E$, s.t.$Q^{-1}AQ=\Lambda$),则A一定是**对称阵**

  - 当$A$正交相似于对角阵$\Lambda$时,即$Q^TAQ=\Lambda$`(1)`
    - 等式两边同时左乘$(Q^{T})^{-1}$;再同时右乘$Q^{-1}$,得:$A=(Q^T)^{-1}\Lambda{Q^{-1}}$=$(Q^{-1})^T\Lambda{Q^{-1}}$`(2)`
    - 而$\Lambda^T=\Lambda$`(3)`则对(2)两边转置,得$A^T=(Q^{-1})^T\Lambda^TQ^{-1}$`(4)`,从而$A^{T}$=$(Q^{-1})^T\Lambda Q^{-1}$`(5)`
    - 比较式(2),(5),可见$A=A^T=(Q^{-1})^T\Lambda Q^{-1}$,说明A是一个**对称阵**

- 方阵$A$**正交相似**于对角阵$\Lambda$的充要条件是$A$为对称阵(当且仅当$A^T=A$)

  - 换句话说,方阵$A$可**正交相似对角化**当且仅当$A$是个**对称阵**



















