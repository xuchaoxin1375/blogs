

[toc]



## 矩阵乘法@矩阵标准乘积🎈



- 给定$m\times{l}$矩阵$A=(a_{ij})_{m\times{l}}$和$l\times{n}$矩阵$B=(b_{ij})_{l\times{n}}$

- 记A,B的乘积为$C=AB=(c_{ij})_{m\times{n}}$,C中的元素$c_{ij}$计算公式如下

  - $$
    c_{ij}=\sum\limits_{k=1}^{l}a_{ik}b_{kj}
    $$

  - A,B的规格保证了A的列数等于B的行数,<u>A的**行向量**中包含的元素数和B的**列向量**包含的元素都是$l$个</u>

  - 矩阵乘积的结果是一个矩阵,且规格(行数和列数)分别由第一个矩阵的行数和第二个矩阵的列数决定

- 特别地:

  - 如果是两个同n维向量(行向量乘以相同维数的列向量),结果是一个仅包含一个元素的矩阵,这种情况下可以视结果为一个标量)
  - 如果两个同n维向量(列向量乘以行向量),结果是一个n阶方阵

### 矩阵乘法和线性变换

- 利用矩阵乘法可以方便的计算线性变换乘法(嵌套)



## Hadamard 乘积

- [Hadamard product (matrices) - Wikipedia](https://en.wikipedia.org/wiki/Hadamard_product_(matrices))

- Hadamard乘积必矩阵运算更容易执行,只需要执行$n\times m$次乘法运算即可

- 需要注意的是，两个矩阵的**标准乘积**不是指两个矩阵中对应元素的乘积。

- 不过，那样的矩阵操作确实是存在的，被称为 **元素对应乘积**（element-wise product）或者 Hadamard 乘积（Hadamard product），记为 $A\odot B$,其结果是一个与连个因子同型的矩阵。

  - $$
    c_{ij}=a_{ij}b_{ij}
    $$


## 向量运算

- 向量是矩阵的特殊形式,有许多内容可以展开,详见向量运算章节

## 矩阵乘法各种形式小结👺

- 矩阵乘法运算具有相当的重要性


### 定义矩阵A,B

- 设矩阵A为$m\times{n}$的

  - $$
    A=\begin{pmatrix}
    	a_{11}  &a_{12}  &\cdots  &a_{1n}  	\\
    	a_{21}  &a_{22}  &\cdots  &a_{2n}  	\\
    	\vdots  &\vdots  &        &\vdots  	\\
    	a_{m1}  &a_{m2}  &\cdots  &a_{mn}  	\\
    \end{pmatrix}
    \\记列向量\alpha_j
    =\begin{pmatrix}
    	a_{1j}  	\\
    	a_{2j}  	\\
    	\vdots		\\
    	a_{mj}  	\\
    \end{pmatrix},j=1,2,\cdots,n
    \\记行向量\beta_i^T=(a_{i1},a_{i2},\cdots,a_{in}),i=1,2,\cdots,m
    $$
    
  - 注意$\alpha_i$和$\beta_i$不是转置关系

- 设矩阵$B$为$n\times{s}$的

  - $$
    B=(b_{ij})_{n\times{t}}
    =\begin{pmatrix}
    	b_{11}  &b_{12}  &\cdots  &b_{1n}  	\\
    	b_{21}  &b_{22}  &\cdots  &b_{2n}  	\\
    	\vdots  &\vdots  &        &\vdots  	\\
    	b_{n1}  &b_{n2}  &\cdots  &b_{ns}  	\\
    \end{pmatrix}
    $$

    


### 按向量分块👺

- 设矩阵$A$为$m\times{n}$的矩阵,将矩阵写作行向量组和列向量组的分块矩阵形式:

  - $$
    A=
    \begin{pmatrix}
    	\alpha_{1}&\alpha_{2}&\cdots&\alpha_{n}	\\
    \end{pmatrix}
    =\begin{pmatrix}
    	\beta_{1}^T	\\
    	\beta_{2}^T	\\
    	\vdots		\\
    	\beta_{m}^T	\\
    \end{pmatrix}
    $$

- 类似的,设矩阵B为$n\times{s}$的矩阵,$\delta_j,j=1,2,\cdots,s$为列向量,$\theta_i^T,i=1,2,\cdots,n$为行向量

  - $$
    B=(\delta_1,\delta_2,\cdots,\delta_s)
    =\begin{pmatrix}
    	\theta_{1}^T\\
    	\theta_{2}^T\\
    	\vdots		\\
    	\theta_{n}^T	\\
    \end{pmatrix}
    $$
    
    

### 矩阵乘法的基础形式

- $$
  C_{m\times{s}}=A_{m\times{n}}B_{n\times{s}},
  \\
  c_{ij}=\sum\limits_{k=1}^{n}a_{ik}b_{kj},(i=1,2,\cdots,m;j=1,2,\cdots,s)
  $$

- 矩阵C可以记为$C=(c_{ij})_{m\times{s}}$

### 向量形式

#### 行向量分块乘列向量分块

- 手工计算的基础操作

- $$
  C=AB=
  \begin{pmatrix}
  	\beta_{1}^T\\
  	\beta_{2}^T\\
  	\vdots		\\
  	\beta_{m}^T	\\
  \end{pmatrix}
  (\delta_1,\delta_2,\cdots,\delta_s)
  =\begin{pmatrix}
  \beta_1^T\delta_1&\beta_1^T\delta_2&\cdots&\beta_1^T\delta_s	\\
  \beta_2^T\delta_1&\beta_2^T\delta_2&\cdots&\beta_2^T\delta_s	\\
  \vdots&\vdots&&\vdots\\
  \beta_m^T\delta_1&\beta_m^T\delta_2&\cdots&\beta_m^T\delta_s	\\
  \end{pmatrix}_{m\times{s}}
  $$

  - $\beta_{i}^T$和$\delta_j$分别是$1\times{n},n\times{1}$的矩阵(向量)

  - $\beta_{i}^T\delta_j$是一个仅含有一个数值元素的矩阵,视为标量

  - 矩阵C的元素可以用**向量内积**描述
    - $c_{ij}=\beta_{i}^T\delta_j$=$\sum\limits_{k=1}^{n}a_{ik}b_{kj}$,$(i=1,2,\cdots,m;j=1,2,\cdots,s)$


#### 列向量分块乘行向量分块

- $$
  C=AB=\begin{pmatrix}
  	\alpha_{1}&\alpha_{2}&\cdots&\alpha_{n}	\\
  \end{pmatrix}
  \begin{pmatrix}
  	\theta_{1}^T\\
  	\theta_{2}^T\\
  	\vdots		\\
  	\theta_{n}^T	\\
  \end{pmatrix}
  =\sum\limits_{i=1}^{n}\alpha_i\theta_i
  $$

- $C,\alpha_i\theta_i^T$都是$m\times{s}$的矩阵,这中方式相当于执行$n$个$m\times{s}$的矩阵叠加

### 半矩阵半向量形式

#### 形式1

- $$
  C_{m\times{n}}=AB
  =\begin{pmatrix}
      \alpha_{1}&\alpha_{2}&\cdots&\alpha_{n} \\
  \end{pmatrix}
  \begin{pmatrix}
      b_{11}  &b_{12}  &\cdots  &b_{1n}   \\
      b_{21}  &b_{22}  &\cdots  &b_{2n}   \\
      \vdots  &\vdots  &        &\vdots   \\
      b_{n1}  &b_{n2}  &\cdots  &b_{ns}   \\
  \end{pmatrix}\\
  =\begin{pmatrix}
  \sum_{k=1}^{n}\alpha_{k}b_{k1}&\sum_{k=1}^{n}\alpha_{k}b_{k2}&
  \cdots&\sum_{k=1}^{n}\alpha_{k}b_{ks}
  \end{pmatrix}
  $$

  - 第1列:$\sum_{k=1}^nb_{k1}(a_{1k},a_{2k},\cdots,a_{mk})^T$=$(\sum_{k=1}^{n}a_{1k}b_{k1},\sum_{k=1}^{n}a_{2k}b_{k1},\cdots,\sum_{k=1}^{n}a_{mk}b_{k1})^T$=$(c_{11},c_{21},\cdots,c_{m1})^T$
  - 第$j$列$\sum_{k=1}^nb_{kj}(a_{1k},a_{2k},\cdots,a_{mk})^T$=$(\sum_{k=1}^{n}a_{1k}b_{kj},\sum_{k=1}^{n}a_{2k}b_{kj},\cdots,\sum_{k=1}^{n}a_{mk}b_{kj})^T$=$(c_{1j},c_{2j},\cdots,c_{mj})^T$

#### 形式2

- $$
  C_{m\times{n}}=AB
  =\begin{pmatrix}
      a_{11}  &a_{12}  &\cdots  &a_{1n}   \\
      a_{21}  &a_{22}  &\cdots  &a_{2n}   \\
      \vdots  &\vdots  &        &\vdots   \\
      a_{m1}  &a_{m2}  &\cdots  &a_{mn}   \\
  \end{pmatrix}
  \begin{pmatrix}
      \theta_{1}^T\\
      \theta_{2}^T\\
      \vdots      \\
      \theta_{n}^T    \\
  \end{pmatrix}
  =\begin{pmatrix}
      \sum\limits_{k=1}^{m}a_{1k}\theta_{k}^T     \\
      \sum\limits_{k=1}^{m}b_{2k}\theta_{k}^T     \\
      \vdots      \\
      \sum\limits_{k=1}^{m}b_{mk}\theta_{k}^T     \\
  \end{pmatrix}
  $$

  

- 第$i$行:

- $$
  \\
  \begin{aligned}
  \sum\limits_{k=1}^{m}b_{ik}A_{k}
      &=\sum_{k=1}^{m}b_{ik}(a_{k1},a_{k2},\cdots,a_{kn})
      \\
      &=\sum_{k=1}^{m}(b_{ik}a_{k1},b_{ik}a_{k2},\cdots,b_{ik}a_{kn})
      \\
      &=(\sum_{k=1}^{m}b_{ik}a_{k1},
      \sum_{k=1}^{m}b_{ik}a_{k2},
      \cdots,
      \sum_{k=1}^{m}b_{ik}a_{kn})
      \\
      &=(c_{i1},c_{i2},\cdots,c_{in})
  \end{aligned}
  \quad i=1,2,\cdots,m
  $$

### 分块形式👺

- 设$A\in\mathbb{R}^{m\times{n}}$,$B\in\mathbb{R}^{n\times{s}}$



### 点积形式

- 在掌握点积和矩阵-向量积的知识后，那么**矩阵-矩阵乘法**（matrix-matrix multiplication）应该很简单。


- 假设有两个矩阵$\mathbf{A} \in \mathbb{R}^{n \times k}$和$\mathbf{B} \in \mathbb{R}^{k \times m}$：

  - $$
    \mathbf{A}=\begin{bmatrix}
     a_{11} & a_{12} & \cdots & a_{1k} \\
     a_{21} & a_{22} & \cdots & a_{2k} \\
    \vdots & \vdots & \ddots & \vdots \\
     a_{n1} & a_{n2} & \cdots & a_{nk} \\
    \end{bmatrix},\quad
    \mathbf{B}=\begin{bmatrix}
     b_{11} & b_{12} & \cdots & b_{1m} \\
     b_{21} & b_{22} & \cdots & b_{2m} \\
    \vdots & \vdots & \ddots & \vdots \\
     b_{k1} & b_{k2} & \cdots & b_{km} \\
    \end{bmatrix}.
    $$

    

- 用行向量$\mathbf{a}^\top_{i} \in \mathbb{R}^k$表示矩阵$\mathbf{A}$的第$i$行，并让列向量$\mathbf{b}_{j} \in \mathbb{R}^k$作为矩阵$\mathbf{B}$的第$j$列。要生成矩阵积$\mathbf{C} = \mathbf{A}\mathbf{B}$，最简单的方法是考虑$\mathbf{A}$的行向量和$\mathbf{B}$的列向量:

  - $$
    \mathbf{A}=
    \begin{bmatrix}
    \mathbf{a}^\top_{1} \\
    \mathbf{a}^\top_{2} \\
    \vdots \\
    \mathbf{a}^\top_n \\
    \end{bmatrix},
    \quad \mathbf{B}=\begin{bmatrix}
     \mathbf{b}_{1} & \mathbf{b}_{2} & \cdots & \mathbf{b}_{m} \\
    \end{bmatrix}
    $$

    

- 当我们简单地将每个元素$c_{ij}$计算为点积$\mathbf{a}^\top_i \mathbf{b}_j$:
  $$
  \mathbf{C} = \mathbf{AB} = \begin{bmatrix}
  \mathbf{a}^\top_{1} \\
  \mathbf{a}^\top_{2} \\
  \vdots \\
  \mathbf{a}^\top_n \\
  \end{bmatrix}
  \begin{bmatrix}
   \mathbf{b}_{1} & \mathbf{b}_{2} & \cdots & \mathbf{b}_{m} \\
  \end{bmatrix}
  = \begin{bmatrix}
  \mathbf{a}^\top_{1} \mathbf{b}_1 & \mathbf{a}^\top_{1}\mathbf{b}_2& \cdots & \mathbf{a}^\top_{1} \mathbf{b}_m \\
   \mathbf{a}^\top_{2}\mathbf{b}_1 & \mathbf{a}^\top_{2} \mathbf{b}_2 & \cdots & \mathbf{a}^\top_{2} \mathbf{b}_m \\
   \vdots & \vdots & \ddots &\vdots\\
  \mathbf{a}^\top_{n} \mathbf{b}_1 & \mathbf{a}^\top_{n}\mathbf{b}_2& \cdots& \mathbf{a}^\top_{n} \mathbf{b}_m
  \end{bmatrix}.
  $$

- [**我们可以将矩阵-矩阵乘法$\mathbf{AB}$看作简单地执行$m$次矩阵-向量积，并将结果拼接在一起，形成一个$n \times m$矩阵**]。



### 分块乘法

- $$
  P=(\alpha_1,\cdots,\alpha_n)
  \\
  A\in{\mathbb{R}^{n\times{n}}},P\in\mathbb{R}^{n\times{n}}
  ,AP\in\mathbb{R}^{n\times{n}},
  A\alpha_i\in\mathbb{R}^{n\times{1}}
  \\根据矩阵分块乘法:
  \\
  AP=A(\alpha_1,\cdots,\alpha_n)=(A\alpha_1,\cdots,A\alpha_n)
  $$

  

### 互为转置的矩阵乘积

- $$
  \\
  A=\begin{pmatrix}
  \alpha_1&\alpha_2&\cdots &\alpha_{n}
  \end{pmatrix}
  \\
  A^T=\begin{pmatrix}
  \alpha_1^T\\
  \alpha_2^T\\
  \vdots  \\
  \alpha_{n}^T
  \end{pmatrix}
  \\
  A^TA=\begin{pmatrix}
  \alpha_{1}^T\alpha_1&\alpha_{1}^T\alpha_2&\cdots&\alpha_{1}^T\alpha_n\\
  \alpha_{2}^T\alpha_1&\alpha_{2}^T\alpha_2&\cdots&\alpha_{n}^T\alpha_n\\
  \vdots&\vdots&&\vdots\\
  \alpha_{n}^T\alpha_1&\alpha_{n}^T\alpha_2&\cdots&\alpha_{n}^T\alpha_n\\
  \end{pmatrix}
  $$

  

- $c_{ij}=\alpha_{i}^T\alpha_{j}$

- $c_{ji}=\alpha_{j}^T\alpha_{i}$

- 可以看出$c_{ij}=c_{ji}$,即$A^TA$是一个对称阵

- 或者通过计算$(A^TA)^T=A^TA$可知,$A^TA$是对称阵

  

  

## 方阵行列式和特征值

- 行列式，记作 det(A)，是一个将方阵 A 映射到实数的函数。
- 行列式等于**方阵特征值的乘积**。$|A|=\prod_{i=1}{\lambda_i}$
- 行列式的**绝对值**可以用来衡量<u>矩阵参与矩阵乘法后</u>空间扩大或者缩小了多少。
  - 如果行列式是 0，那么空间至少沿着某一维完全收缩了，使其失去了所有的体积。
  - 如果行列式是 1，那么这个转换保持空间体积不变。



