[toc]



## 基本定理👺

### $n$维欧式空间中任意一个正交向量组能够扩充为正交基

- $n$维欧式空间$V$中任意一个**正交向量组**能够扩充为**正交基**

#### 证明

- 设$A_0:\alpha_1,\cdots,\alpha_m$是一组**正交向量组**,对$p=n-m$作数学归纳法
  - 首先,$n$维空间$V$的基包含的向量个数是$n$
  - $p=0$时,$A$本身就是$V$的正交基
  - 设$p=k$时定理成立,即存在$B:\beta_1,\cdots,\beta_k$使得:$\alpha_1,\cdots,\alpha_m,\beta_1,\cdots,\beta_k$是$V$的**正交基**
  - 因为$p=k+1$时,有$m<n$,一定存在向量$\beta$不能被$A_0$线性表示
    - 显然$\sum_{j=1}^{m}k_j\alpha_j=\beta$无解,即$\beta\neq{\sum_{j=1}^{m}k_j\alpha_j}$`(1)`
      - 将(1)变形,即有:$\beta-\sum_{j=1}^{m}k_j\alpha_j\neq{\bold 0}$`(1-1)`
    - 构造向量$\bold{\alpha}_{m+1}$=$\beta-\sum_{j=1}^{m}k_j\alpha_j$ `(2)`,则$\alpha_{m+1}\neq{\bold{0}}$
  - 则$(\alpha_i,\alpha_{m+1})$=$(\beta,\alpha_i)-k_i(\alpha_i,\alpha_i)$,$(i=1,2,\cdots,m)$`(3)`
    - 令$(\alpha_i,\alpha_{m+1})$=$0$,则$k_i=\frac{(\beta,\alpha_i)}{(\alpha_i,\alpha_i)}$`(4)`,$(i=1,2,\cdots,m)$
    - 这就是说,待定系数$k_i$按照式(4)的方式计算,可得到满足$(\alpha_i,\alpha_{m+1})=0$,$(i=1,2,\cdots,m)$,的$\alpha_{m+1}$
    - 即$\alpha_{m+1}$和向量组$A_0$的所有向量正交,所以$A_1:\alpha_1,\cdots,\alpha_m,\alpha_{m+1}$构成**正交向量组**
  - 由归纳法假设,$A_1$可以**扩充**成**正交基**
  - 所以定理得证

#### 扩充正交向量组到正交基

- 上述定理的证明过程给出了一个具体的扩充正交向量组的方法:
  - 即从任意一个非零向量出发,扩充出向量空间的一个**正交基**
  - 只需要按照证明中的步骤逐个扩充,最后得到的正交向量组就是一个正交基
  - 对正交基做**单位化**,就得到一组**标准正交基**

### 标准正交基存在定理👺

- 一组基$A$可以产生一个向量空间;那么是否存在另一组基$B$,$B$是正交基并且$A,B$产生的向量空间相等?事实上,问题的答案是肯定的,且有如下定理

- 对于$n$维欧式空间中任意一组基$A_n:\bold{a}_1,\cdots,\bold{a}_n$,总可以找到一组**标准正交基**$B_n:\bold{b}_1,\cdots,\bold{b}_n$,使得$L(A_i)=L(B_i)$`(0)`,$(i=1,\cdots,n)$,其中$L$表示**向量空间**

#### 证明

- 从先易后难的思路:先证明$n$值较小时定理成立,再讨论$n$值较大的情形
- 这里逐个地求$\bold{b}_{1},\bold{b}_{2},\cdots,\bold{b}_{n}$
- 首先,取单位向量:$\bold{b}_1=\frac{1}{|\bold{a}_1|}\bold{a}$`(1)`,假设已经求出向量组$B_m$,它是**单位正交**的,且:$L(A_i)$=$L(B_i)$`(2)`,$(i=1,\cdots,m)$
- 再求$\bold{b}_{m+1}$;对于向量$\bold{a}_{m+1}$,满足$\bold{a}_{m+1}\in{A_n}$,$\bold{a}_{m+1}\not\in{A_m}$,而$A_{n}$内的向量线性无关(无法相互表示),$L(A_m)=L(B_m)$`(2-1)`,即$\bold{a}_{m+1}$不能由$A_m,B_m$线性表示
- 构造向量$\xi_{m+1}=\bold{a}_{m+1}-\sum_{j=1}^{m}k_j\bold{b}_{j}$`(3)`;
  - 这里$k_j$,$(j=1,\cdots,m)$是待定系数
  - 对等式两边同时取内积运算:$(\xi_{m+1},\bold{b}_i)$=$(\bold{a}_{m+1},\bold{b}_{i})-\sum_{j=1}^{m}k_j(\bold{b}_i,\bold{b}_j)$=$(\bold{a}_{m+1},\bold{b}_{i})-k_i(\bold{b}_i,\bold{b}_i)$`(4)`
    - 这里用到了$B_{m}$是正交向量组的特点,$(\bold{b}_{i},\bold{b}_{j})=0$,$(i\neq{j})$`(4-1)`
  - 令上式(4)为0,即$(\bold{a}_{m+1},\bold{b}_{i})-k_i(\bold{b}_i,\bold{b}_i)=0$`(4-2)`,可得:$k_i=\frac{(\bold{a}_{m+1},\bold{b}_i)}{(\bold{b}_i,\bold{b}_i)}$`(5)`,$(i=1,\cdots,m)$
- 将式(5)代入(3),得$\xi_{m+1}=\bold{a}_{m+1}-\sum_{j=1}^{m}\frac{(\bold{a}_{m+1},\bold{b}_j)}{(\bold{b}_j,\bold{b}_j)}\bold{b}_{j}$`(6)`时:
  - 显然$\xi_{m+1}\neq{0}$且$(\xi_{m+1},\bold{b}_i)$=0,$i=1,\cdots,m$
  - 这说明$\bold{\xi}_{m+1}$与$\bold{b}_1,\cdots,\bold{b}_m$都正交
- 再作标准化,得到单位向量$\bold{b}_{m+1}$:令$\bold{b}_{m+1}=\frac{\xi_{m+1}}{|\xi_{m+1}|}$
- $\bold{b}_1,\cdots,\bold{b}_{m},\bold{b}_{m+1}$是一单位正交向量组,同时$L(A_{m+1})=L(B_{m+1})$
- 由归纳法原理,定理成立



## 标准正交化

- 基的标准正交化:设$V_1$是$V$的一个基,寻找$V$的另一个与$V_1$等价的标准正交基$V_2$的过程称为基的**标准正交化**

### 正交化(schmidt正交化)方法

- 一个向量组**线性无关**是该向量组成为**正交向量组**的**必要条件**(不充分条件)
  - 将一个**基**(线性无关线向量组)中的各个向量**单位化**是容易的,利用公式$\bold{a}=\frac{\bold{a}}{||\bold a||}$即可
  - 但是将它们**正交化**比较复杂,这里介绍一种方法能够对一组正交向量组进行正交化
  - 本方法的原理参考标准正交基存在定理👺
    - $\bold{b}_{m+1}=\bold{a}_{m+1}-\sum_{j=1}^{m}\frac{(\bold{a}_{m+1},\bold{b}_j)}{(\bold{b}_j,\bold{b}_j)}\bold{b}_{j}$;即$\bold{b}_{m}=\bold{a}_{m}-\sum_{j=1}^{m-1}\frac{(\bold{a}_{m},\bold{b}_j)}{(\bold{b}_j,\bold{b}_j)}\bold{b}_{j}$
  
- 对于一个线性无关组$A$,可以通过**施密特正交化方法**,求出一个**等价**的正交向量组$B$,

  - **schmidt正交化**方法是一种递推计算的方法,可以将一组基正交化地**正交化方法**
  - **标准化**(单位化)只需要利用正交公式计算即可

- 特点
  - 从几何的角度,正交化的过程是被正交化的向量被正交分解(投影到各个相互垂直的轴上)的过程
  - 被正交化的向量维数越高,或向量越多(尤其是向量数量),正交分解的过程就越繁琐,计算量越大
  - 并且公式中的求和部分除了分母$(\beta_{j},\beta_{j})$可以重复利用,其余都必须重新计算
  - 幸运的是,公式中包含的向量内积计算和向量线性运算都是比较简单的运算,相较于大型矩阵运算算是简单的了
  - 推荐的计算方式是
    - 将公式写出,减少错误率
    - 分部计算:根据公式,将每个需要计算的内积运算都列出来算,然后将内积结果代入回公式,可以减少错误率
    - 列向量计算通常转换为行向量转置的形式书写和计算

### 正交化公式

- 设$A:\alpha_1,\alpha_2,\cdots,\alpha_n$
  - 令$\beta_1=\alpha_1$

  - $\beta_{i}$=$\alpha_i-\sum_{k=1}^{i-1}\frac{(\alpha_i,\beta_{k})}{(\beta_{k},\beta_{k})}\beta_{k}$=$\alpha_i-\sum_{k=1}^{i-1}\frac{(\alpha_i,\beta_{k})}{||\beta_{k}||^2}\beta_{k}
        \quad (i=2,\cdots,n)$`(1)`
  - 构造向量组:$B:\beta_1,\cdots,\beta_n$,则$B$**正交向量组**
- 公式结构和特点
  - 公式右端的元素中出现了$\alpha_{i}$和$\beta_{k}$,分别出现了$2,4$次但是如果展开求和式可发现,计算$\beta_{i}$时,公式右端出现了$\alpha_{i}$,$\beta_{1},\cdots,\beta_{i-1}$
  - 因此,如果使用公式(1)计算$\beta_{i}$,必须要将$\beta_{1},\cdots,\beta_{i-1}$全部先求出来,然后代入公式(1)计算;但$\alpha_{i}$始终是那个$\alpha_{i}$
  - 而一般手工计算的正交化向量不超过四个,因此可以把公式展开
    - $\beta_1$=$\alpha_1$
    - $\beta_2$=$\alpha_2-\frac{(\alpha_{2},\beta_{1})}{(\beta_{1},\beta_{1})}\beta_{1}$
    - $\beta_{3}$=$\alpha_{3}$-$\frac{(\alpha_{3},\beta_{1})}{(\beta_{1},\beta_{1})}\beta_{1}$-$\frac{(\alpha_{3},\beta_{2})}{(\beta_{2},\beta_{2})}\beta_{2}$
    - $\beta_{4}$=$\alpha_{4}$-$\frac{(\alpha_{4},\beta_{1})}{(\beta_{1},\beta_{1})}\beta_{1}$-$\frac{(\alpha_{4},\beta_{2})}{(\beta_{2},\beta_{2})}\beta_{2}$-$\frac{(\alpha_{4},\beta_{3})}{(\beta_{3},\beta_{3})}\beta_{3}$
    - $\cdots$
    - $\beta_{s}$=$\alpha_{s}$-$\frac{(\alpha_{s},\beta_{1})}{(\beta_{1},\beta_{1})}\beta_{1}$-$\frac{(\alpha_{s},\beta_{2})}{(\beta_{2},\beta_{2})}\beta_{2}$-$\cdots$-$\frac{(\alpha_{s},\beta_{s-1})}{(\beta_{s-1},\beta_{s-1})}\beta_{s-1}$
- 正交性证明:(todo)

  - $\beta_j$=$\beta_{j}
    =\alpha_j-\sum_{k=1}^{j-1}
    \frac{(\alpha_j,\beta_{k})}{(\beta_{k},\beta_{k})}\beta_{k}$
  - 为了便于讨论和书写,令:
    - $F_i=\sum_{k=1}^{i-1}\frac{(\alpha_i,\beta_{k})}{||\beta_{k}||^2}\beta_{k}
      \quad (i=2,\cdots,n)$
    - $\beta_i=\alpha_i-F_i$
    - $(\beta_i,\beta_j)$=$(\alpha_i,\alpha_j)-(\alpha_i,F_j)-(F_i,\alpha_j)+(F_i,F_j)$=0
  - 或者,令$B_m:\beta_1,\cdots,\beta_m$,依次证明$\beta_{m+1}$与正交向量组$B_m$正交

### 解析几何角度解释正交化过程

- 以三维向量为例

- ![在这里插入图片描述](https://img-blog.csdnimg.cn/299772b1b7fd4dd0baecd0c779f94db8.png)

- $\bold{a_1=b_1}$
- $\bold{b_2=a_2-c_2}$,$\bold{c}_2$是$\bold{a}_2$在$\bold{b}_1$上的**投影向量(分量)**,由分量公式:$\bold{c}_2$=$\text{Prj}_{\bold{b}_1}\bold{a}_2\cdot{\bold{\frac{b_1}{|b_1|}}}$=$\bold{\frac{(a_2,b_1)}{|b_1|^2}b_1}$
- $\bold{b_3=a_3-c_3}$,而$\bold{c}_3$是$\bold{a}_3$在平行于$\bold{b_1,b_2}$的平面上的投影向量,由于$\bold{b_1\perp{b_2}}$,所以$\bold{c}_3$等于分别在$\bold{b_1,b_2}$上的投影向量$\bold{c_{31},c_{32}}$之和:$\bold{c_3=c_{31}+c_{32}=\frac{(a_3,b_1)}{|b_1|^2}b_1}$+$\bold{\frac{(a_3,b_2)}{|b_2|^2}b_2}$

## 应用

### 例

- 设$\bold{a}_1,\bold{a}_2,\bold{a}_3$分别是$(1,2,-1)^T$,$(-1,3,1)^T$,$(4,-1,0)^T$,试用施密特方法正交化它们
  - 令$F_i=\sum_{k=1}^{i-1}\frac{(\bold{a}_i,\bold{b}_k)}{|\bold{b}_k|^2}\bold{b}_k$,$i=2,\cdots,r$
- 解
  - 取$\bold{b}_1=\bold{a}_1$=$(1,2,-1)^T$
  - $\bold{b}_2=\bold{a_2}-F_2$
    - $|\bold{b}_1|^2=6$
    - $(\bold{a_2,b_1})=-1+6-1=4$
    - $\bold{b}_2=(-1,3,1)^T-\frac{4}{6}(1,2,-1)^T$=$\frac{5}{3}(-1,1,1)^{T}$

  - $\bold{b}_3=\bold{a}_3-F_3$
    - $|\bold{b}_2|^2=\frac{25}{9}(1+1+1)$=$\frac{25}{3}$
    - $(\bold{a_3,b_1})=4-2=2$,$(\bold{a_3,b_2})=\frac{5}{3}(-4-1+0)=-\frac{25}{3}$
    - $F_3=\frac{2}{6}(1,2,-1)^T$+$-\frac{25}{3}\cdot\frac{3}{25}\cdot\frac{5}{3}(-1,1,1)^{T}$=$\frac{1}{3}((1,2,-1)^T-5(-1,1,1)^T)$=$\frac{1}{3}(6,-3,-6)$=$(2,-1,-2)$
    - $\bold{b}_3=(4,-1,0)^T-(2,-1,-2)^T=(2,0,2)^T$=$2(1,0,1)^T$

  - 标准化(单位化):$\bold{e}_1=\frac{\bold{b}_1}{|\bold{b}|}$=$\frac{1}{\sqrt{6}}(1,2,-1)^T$;$\bold{e}_2=\frac{\sqrt{3}}{5}\frac{5}{3}(-1,1,1)^{T}$=$\frac{\sqrt{3}}{3}(-1,1,1)^{T}$;$\bold{e}_3$=$\frac{1}{2\sqrt{2}}2(1,0,1)^T$=$\frac{\sqrt{2}}{2}(1,0,1)^T$




### 例:扩充正交基

- 已知$\bold{a}_1=(1,1,1)^T$;求一组非零向量$\bold{a_2,a_3}$,使$\bold{a_1,a_2,a_3}$两两正交
- 解:
  - 两两正交等价于:$(\bold{a_1,a_2})=0$,$(\bold{a_1,a_3})=0$,$(\bold{a_2,a_3})=0$
  - 先处理前两个方程,它们可以用同一个线性方程建模:$\bold{a_1^{T}x=0}$,即$(x_1+x_2+x_3)=0$
  - 显然该方程组仅包含一个线性方程,并且系数矩阵的秩为1,基础解系包含的向量个数为$n-r=3-1=2$,自由未知数的个数也是2
  - 为自由未知数数组取2组线性无关的值:$q_1=(1,0)^T,q_2=(0,1)^T$,对应的,基础解系取值为:
    - $\xi_1=(-1,1,0)^T$;$\xi_2=(-1,0,1)^T$
- 然而$\xi_1,\xi_2$此时不是正交的,需要对其正交化:
  - 取$\bold{c}_1=\xi_1$
  - $\bold{c}_2$=$\xi_2-\frac{(\xi_2,c_1)}{|c_1|^2}c_1$=$(-1,0,1)^T-\frac{1}{2}(-1,1,0)^T$=$(-\frac{1}{2},-\frac{1}{2},1)^T$=$\frac{1}{2}(-1,-1,2)^T$
  - 显然,,$\bold{c_1,c_2}$是基础解系的线性组合,它们仍然满足$\bold{a_1^{T}x=0}$
  - 所以可以取$\bold{a}_2=(-1,1,0)^T$,$\bold{a}_3=\frac{1}{2}(-1,-1,2)^T$



