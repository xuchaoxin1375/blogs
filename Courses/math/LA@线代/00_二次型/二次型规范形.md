[toc]

## 规范形

- 如果二次型$f(x_1,x_2,\cdots,x_n)$=$f(\bold{x})$=$\bold{x^{T}Ax}$的标准形$f=\sum_{i=1}^{n}k_iy_i^2$的系数$k_i\in G=\{-1,0,1\}$,则$f$可以表示为$f$=$\sum_{i=1}^py_i^2-\sum_{j=1}^{r-p}y_{p+j}^2$,则这个形式称为二次型$f$的**规范形**
- 另一种描述:如果$f(\bold{x})=\bold{x^{T}Ax}$可以通过线性变换$\bold{x=Cy}$化为:$f(\bold{x})$=$g(\bold{y})$=$\sum_{i=1}^py_i^2-\sum_{j=p+1}^{r}y_j^2$,
  - $r=r(A)=r(f)$即二次型$f$的秩

- 使用矩阵乘法表示规范形二次型

  - $$
    \begin{aligned}
        f(y_1,\cdots,y_n)
        &=\sum_{i=1}^py_i^2-\sum_{j=1}^{q}y_{p+j}^2\\
        &=(y_1,y_2,\cdots,y_n)
        \begin{pmatrix}
            1&&&&&&&&  	\\
             &\ddots&&&&&&  &\\
             &&1&&&&&&&  	\\
             &&&-1&&&&&\\
             &&&&&\ddots&&&\\
             &&&&&&-1&&\\
          	 &&&&&&&0&\\
             &&&&&&&&\ddots\\
             &&&&&&&&&0\\
        \end{pmatrix}
        \begin{pmatrix}
            y_{1}\\
            y_{2}\\
            \vdots\\
            y_{n}	\\
        \end{pmatrix}
        \\&=\bold{{y}^{T}(\Lambda_{G}){y}}
    \end{aligned}
    $$

  - $\bold{\Lambda_G}$表示规范形对角阵矩阵$(1_{1},\cdots,1_{p},-1_{p+1},\cdots,-1_{r},0,\cdots,0)$


### 规范形的矩阵

- $$
  \Lambda_{G}
  =\begin{pmatrix}
  E_{p}&&\\
  &-E_{q}&\\
  &&O_{n-r}
  \end{pmatrix}
  \\p+q=r\leqslant{n}
  $$

- 其中$r=R(f)$,表示二次型的秩



### 二次型标准形间的关系

- 同一个二次型的标准形不唯一
- 但二次型的标准形中所含的项数是确定的,其等于二次型的**秩**
- 若限定变换为**实变换**,则所有标准形具有相同的正系数个数,同时有相同的负系数个数
- 这个结论可以表述为如下的惯性定理

### 惯性定理

- 设二次型$f=\bold{x^{T}Ax}$的秩为$r$,且有可逆变换$\bold{x=Cy}$,$\bold{x=Pz}$使$f=\sum_{i=1}^{r}k_iy_i^2$,$(k_i\neq{0})$,$f=\sum_{i=1}^{r}\lambda_iz_i^2$,$(\lambda_i\neq{0})$
- 则$k_1,\cdots,k_r$中正数的个数与$\lambda_1,\cdots,\lambda_r$中正数的**个数相等**(负数个数也相等)

#### 惯性指数

- 二次型的规范形$f=\sum_{i=1}^py_i^2-\sum_{j=1}^{q}y_{p+j}^2$($p+q=r$)中,
  - 正系数个数$p$称为**正惯性指数**
  - 负系数个数$q=r-p$称为**负惯性指数**

  - $p-q=p-(r-p)=2p-r$称为**符号差**

- 正负惯性指数统称**惯性指数**



#### 推论:惯性指数和特征值个数

- 正(负)惯性指数是二次型矩阵$\bold{A}$的正(负)特征值个数
- 借助符号描述:
  - 设$p,q$分别是二次型$f=\bold{x^{T}Ax}$的标准形的正惯性指数和负惯性指数
  - 则$\bold{A}$的特征值$\lambda_1,\cdots,\lambda_n$中有$p$个为正数,$q$个为负数





## 标准形可以转换为规范化定理👺

- 定理:对于任意$n$元二次型$f(x_1,\cdots,x_n)=\bold{x^{T}Ax}$,$(\bold{A^{T}=A})$,一定存在一个**正交(可逆)线性变换**$\bold{x=Cz}$,使得$f$可以化为**规范形**$g(\bold{z})=f(\bold{Cz})$
- 本定理作为二次型可规范化推论:**二次型总是可以规范化**

### 证明

- 由二次型可标准化定理可知,存在线性变换$\bold{x=Py}$使得$f(\bold{x})$=$f(\bold{Py})$=$\bold{y^{T}\Lambda{y}}$=$\sum_{i=1}^{n}k_iy_i^2$
- 设$R(f)=r$,则$\bold\Lambda$=$\text{diag}(\lambda_1,\cdots,\lambda_n)$的**特征值**$\lambda_i,i=1,\cdots,n$中恰好有$r$个不为0,其余$n-r$个为0,不妨设$\lambda_1,\cdots,\lambda_r=0$,$\lambda_{r+1},\cdots,\lambda_{n}\neq{0}$,从而$\bold{\Lambda}$=$\text{diag}(\lambda_1,\cdots,\lambda_r,0,\cdots,0)$
- 不妨令矩阵$\bold{K}$=$\text{diag}(k_1,\cdots,k_n)$,其中$k_i$=$\begin{cases}\frac{1}{\sqrt{|\lambda_i|}}&i\leqslant{r}\\1&i>r\end{cases}$,即构造可逆对角阵$\bold{K}$=$\text{diag}(\frac{1}{\sqrt{|\lambda_1|}},\cdots,\frac{1}{\sqrt{|\lambda_r|}},1,\cdots,1)$
  - 显然$\bold{|K|}\neq{0}$,从而$\bold{K}$可逆,构造线性变换$\bold{y=Kz}$,则$\bold{x=Py=P(Kz)}$
    - 线性变换$\bold{y=Kz}$,即
      - $y_1=\frac{1}{\sqrt{|\lambda_1|}}z_1$
      - $\vdots$
      - $y_r=\frac{1}{\sqrt{|\lambda_r|}}z_r$
      - $y_{r+1}=z_{r+1}$
      - $\vdots$
      - $y_{n}=z_{n}$
  - 从而$f(\bold{x})$=$f(\bold{PKz})$=$\bold{z^{T}K^{T}P^{T}APKz}$=$\bold{z^{T}K^{T}(P^{T}AP)Kz}$=$\bold{z^{T}K^{T}\Lambda{K}z}$
  - 而$\bold{C}=\bold{K^{T}\Lambda{K}}$=$\text{diag}(\frac{\lambda_1}{|\lambda_1|},\cdots,\frac{\lambda_r}{|\lambda_r|},0,\cdots,0)$
    - 注意对角阵连乘计算公式:$c_j=\prod_{i=1}^{n}d_{ij}$,$j=1,\cdots,n$,$d_{ij}$表示第$i$个对角阵的第$j$个对角元素
    - $c_{j}=\frac{\lambda_i}{|\lambda_{i}|}\in\{-1,1\}$,$i=1,\cdots,r$
  - $f(\bold{x})$=$\bold{z^{T}Cz}$是一个规范形
- 记$\bold{C=PK}$,即有可逆变换$\bold{x=Cz}$能使$f$化为**规范形**$f(\bold{x})$=$f(\bold{Cz})$=$\sum_{i=1}^{n}\frac{\lambda_i}{|\lambda_i|}z_i^{2}$

- 附:为什么要这么构造$\bold{K}$?
  - $|k_iy_i^2|$=$z_i^2$`(1)`,解得$y_i^2={\frac{z_i^2}{|k_i|}}$,即$y_i=\pm{\sqrt{\frac{z_i^2}{|k_i|}}}$=$\pm\frac{|z_i|}{\sqrt{|k_i|}}$
  - 这说明$a=\frac{|z_i|}{\sqrt{|k_i|}}$,$b=-\frac{|z_i|}{\sqrt{|k_i|}}$都满足`(1)`,而$c=\frac{z_i}{\sqrt{|k_i}}$的取值是$a,b$中的一个,因此$c$满足`(1)`
  - 虽然$a,b,c$都满足`(1)`,甚至可以在$i$取不同值时混用也可以,即$\bold{K}$不是唯一的
  - 但是统一方便起见,我们采用$y_i=\frac{z_i}{\sqrt{|k_i|}}$,$i=1,\cdots,r$作为规范化线性变换,而$y_{r+1},\cdots,y_{n}$这部分变换可以随意(但是要保持变换的可逆性),为简单起见,通常取$y_j=z_j,j=r+1,\cdots,n$



### 实对称阵间相互合同的充要条件

- 实对称阵$A,B$合同的充要条件它们有相同的秩$r(A)=r(B)$和正惯性指数$p$



## 二次型规范化步骤

- 先将二次型标准化为$f=g(\bold{y})=\sum_{i=1}^{n}k_iy_i^2$=$\bold{y^{T}\Lambda{y}}$,
  - 若二次型的秩$r=R(f)$,则$\bold{\Lambda}$的对角元素包含$n-r$个0,$f=\sum_{i=1}^{n}k_iy_i^2$=$\sum_{i=1}^{r}k_iy_i^2+\sum_{i=r+1}^{n}0$=$\sum_{i=1}^{r}k_iy_i^2$
  - 设$\bold{\Lambda}=\text{diag}(\lambda_1,\cdots,\lambda_n)$=$\text{diag}(\lambda_1,\cdots,\lambda_r,0,\cdots,0)$,
- 由惯性定理,规范化后的二次型的矩阵为$\bold{\Lambda_{G}}$=$\text{diag}(\frac{\lambda_1}{|\lambda_1|},\cdots,\frac{\lambda_r}{|\lambda_r|},0,\cdots,0)$
- $\bold{y=(\Lambda_{G})z}$能使$f=\bold{y^{T}\Lambda{y}}$规范化为$f=\bold{z^{T}(\Lambda_{G})z}$

### 小结

- 从上述规范化步骤可以看出,若已求得而次形的标准形,只需要抽取各个系数的符号代替标准形的原系数,即得到规范形
- 但是如果需要给出规范化所用的线性变换(矩阵),则要用公式$\text{diag}(\frac{1}{\sqrt{|\lambda_1|}},\cdots,\frac{1}{\sqrt{|\lambda_r|}},1,\cdots,1)$计算

## 例

- 化二次型$f=x_1x_2+x_1x_3+2x_2x_3$为规范形
- 解:
  - 将$f$标准化,可得$f=z_1^2-z_2^2-2z_3^2$(过程在此处不是重点,略去)
    - 其矩阵为$\bold{\Lambda}=\text{diag}(1,-1,-2)$
    - 规范化后的矩阵$\bold{\Lambda_{G}}$=$\text{diag}(1,-1,-1)$
    - 用到的可逆线性变换$\bold{z=Kw}$,其中$\bold{K}=\text{diag}(1,-1,-\frac{1}{\sqrt{2}})$(或$\bold{K}=\text{diag}(1,1,\frac{1}{\sqrt{2}})$)
  - $f=w_1^2-w_2^2-w_3^2$

















