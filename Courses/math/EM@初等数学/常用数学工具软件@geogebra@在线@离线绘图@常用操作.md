[toc]

## abstract

- 常用数学工具软件
- geogebra@在线@离线绘图@常用操作

## 数学计算工具(免费)

- geogebra(可以离线使用)
- math solver(微软数学)
- 国内的**作业帮**这类解析软件(有时还是很好用的)

### 其他的大型专业软件

- maple,
- mathmatics,
- matlab

### 能够给出计算步骤的软件

- wolfram
- symbolab
- photomath

### 移动端app

- 上述给出步骤的软件有些特殊版本是免费的,可以在某些论坛找到(主要是android版本)

## geogebra

### 在线资源

#### geogebra 在线使用记录和账号管理

- [ Resources – GeoGebra](https://www.geogebra.org/u/cxxu1375)

- [GeoGebra - the world’s favorite, free math tools used by over 100 million students and teachers](https://www.geogebra.org/)

#### 在线作图计算页面直达链接

- [Calculator Suite - GeoGebra](https://www.geogebra.org/calculator)

#### 官方入门培训资料

- [Learn GeoGebra Classic – GeoGebra](https://www.geogebra.org/m/XUv5mXTm)

- | ![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/f9c6ff0bf620435ba30d5f839bbe59d9.png) | [学习计算器套件手册 在线 – GeoGebra](https://www.geogebra.org/m/vbyxxtwu) |
  | ------------------------------------------------------------ | ------------------------------------------------------------ |

#### 其他使用手册

- [GeoGebra使用指南[中文资料,可下载pdf] – GeoGebra](https://www.geogebra.org/m/xy83dFeY)
- [GeoGebra 手册EN](https://wiki.geogebra.org/en/Manual)

#### 直接用搜索引擎查询geogebra功能

- 相比直接查询手册,也可以考虑用网络搜索引擎或AI引擎查询特定功能

#### 软件自带功能提示

- ![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/c220eb1683a64d85b83d4818e8d29d54.png)

## 常用操作

### 批量对象操作

- ![在这里插入图片描述](https://img-blog.csdnimg.cn/5fe3f15d23c54e22a974d6e3c0dc0c53.png)

### 移动标题

- 移动标题前,需要确保处于**普通模式**(视图)
  - 按下ESC键跳转到普通模式
    - ![在这里插入图片描述](https://img-blog.csdnimg.cn/f7ababc13ca1427bba778916be15a05c.png)
  - 蓝色指示图标箭头消失,才可以移动标题/文字
  - 不过有些模式允许您直接拖动文字,比如文本(text)

### latex 公式文本段或标题👺

- | ![在这里插入图片描述](https://img-blog.csdnimg.cn/5842350122134c6fbe97437073f71a0a.png) | 选择Media->Text元素,插入文本 |      |
  | ------------------------------------------------------------ | ---------------------------- | ---- |



###  坐标轴刻度和单位修改

- 坐标轴显示修改:`xAxis`和`yAxis`分别是对$x$轴和$y$轴的设置
- 刻度间显示间隔(Distance),例如$\frac{\pi}{2}$
- 刻度单位(Unit),比如弧度$\pi$或$\degree$或1



![在这里插入图片描述](https://img-blog.csdnimg.cn/51907e3ebece48008f4a88bd8a276117.png)



