[toc]

# 低维柯西不等式

- 从$a^2+b^2\geqslant2ab$出发可以得到柯西不等式(齐次不等式)

## 柯西不等式的代数形式👺

- 设$a_1,a_2,b_1,b_2\in\mathbb{R}$,则$(a_1^2+a_2^2)(b_1^2+b_2^2)\geqslant{(a_1b_1+a_2b_2)^2}$;当且仅当$a_1b_2=a_2b_1$时取等号
- 证明:用作差比较法
  - $(a_1^2+a_2^2)(b_1^2+b_2^2)-(a_1b_1+a_2b_2)^2\geqslant{0}$
  - $\Leftrightarrow$ $a_1^2b_1^2+a_1^2b_2^2+a_2^2b_1^2+a_2^2b_2^2$ -$(a_1^2b_1^2+2a_1a_2b_1b_2+a_2^2b_2^2)\geqslant{0}$
  - $\Leftrightarrow$ $a_1^2b_2^2+a_2^2b_1^2-2a_1a_2b_1b_2\geqslant{0}$
  - $\Leftrightarrow$ $(a_1b_2-a_2b_1)^2\geqslant{0}$
  - 最后一个不等式的成立说明原命题的成立
- 结论的证明不难,关键在于人们如何得到这个结论(易于证明的结论不一定容易被发现,很多定理是先通过猜测(提出)结论假设,然后试图证明它)

## 柯西不等式的向量和几何形式

- 柯西不等式有直观的几何背景,可以借助向量来联系代数和几何

### 柯西不等式的向量形式

- 由平面向量的知识,设$\boldsymbol{\alpha,\beta}$为平面上的两个向量且$\boldsymbol{\alpha}=(a_1,a_2)$,$\boldsymbol{\beta}=(b_1,b_2)$
  - 记$\boldsymbol{\alpha,\beta}$的夹角为$\theta=<\boldsymbol{\alpha,\beta}>$,$|\boldsymbol\alpha|=\sqrt{a_1^2+a_2^2}$,$|\boldsymbol{\beta}|=\sqrt{b_1^2+b_2^2}$,$\boldsymbol{\alpha\cdot{\beta}}=a_1b_1+a_2b_2$
  - 则$\boldsymbol{\alpha,\beta}$的夹角余弦$\cos\theta$=$\frac{\boldsymbol{\alpha\cdot\beta}}{\boldsymbol{|\alpha||\beta|}}$

- 显然$\cos\theta\leqslant{1}$,若$\boldsymbol{\alpha,\beta\neq{0}}$即有$\frac{\boldsymbol{\alpha\cdot\beta}}{\boldsymbol{|\alpha||\beta|}}$ $\leqslant{1}$所以:$\boldsymbol{\alpha\cdot\beta}$ $\leqslant$ ${\boldsymbol{|\alpha||\beta|}}$

  - 向量形式直接对应于:$a_1b_1+a_2b_2\leqslant{\sqrt{a_1^2+a_2^2}\sqrt{b_1^2+b_2^2}}$
  - 也等价于:$(a_1b_1+a_2b_2)^2\leqslant{{(a_1^2+a_2^2)}{(b_1^2+b_2^2)}}$

- 事实上:

  - 当$\boldsymbol{\alpha,\beta}$均为非零向量,由$|\cos\theta|\leqslant{1}$可知,$\frac{\boldsymbol{|\alpha\cdot\beta|}}{\boldsymbol{|\alpha||\beta|}}$ $\leqslant{1}$ ,即${\boldsymbol{|\alpha||\beta|}}$ $\geqslant$ $|\boldsymbol{\alpha\cdot\beta}|$

    - 等号成立的条件是:向量$\boldsymbol{\alpha,\beta}$共线(平行) $\Leftrightarrow$ $\exist \lambda\neq{0}$ s.t.$\boldsymbol{\alpha}=\lambda\boldsymbol{\beta}$


    - 特别的,零向量和任何相邻平行.当$\boldsymbol{\alpha,\beta}$中至少一个为零向量时,即$\boldsymbol{\alpha\cdot{\beta}}$=0,不等式成立且取得等号


### 三角不等式形式

- 这种形式也时有几何背景的

- 延续上一节中的记号说明,$\boldsymbol{\alpha}=(a_1,a_2)$,$\boldsymbol{\beta}=(b_1,b_2)$

- 由向量加法法则,$\boldsymbol{\alpha,\beta,\alpha+\beta}$的端点分别为$A,B,C$,由向量加法的**三角形法则**(两边之和大于第三边):

  - $|\boldsymbol{\alpha}|+|\boldsymbol{\beta}|\geqslant{|\boldsymbol{\alpha+\beta}|}$
  - 等号成立的条件:$\boldsymbol{\alpha,\beta}$共线

  - 代入代数公式,有
    $$
    \sqrt{a_1^2+a_2^2}+\sqrt{b_1^2+b_2^2}\geqslant{\sqrt{(a_1+b_1)^2+(a_2+b_2)^2}}
    $$
    

#### 三角形式的讨论

- $\sqrt{a_1^2+a_2^2}+\sqrt{b_1^2+b_2^2}\geqslant{\sqrt{(a_1+b_1)^2+(a_2+b_2)^2}}$;**取等条件**:当且仅当(下面三种**等价**描述成立(任意一条))
  - 构造平面向量$\boldsymbol{\alpha}=(a_1,a_2)$,$\boldsymbol{\beta}=(b_1,b_2)$,条件为$\boldsymbol{\alpha,\beta}$共线(平行)
  - $\exist{\mu,\lambda\in{\mathbb{R}}}$ s.t.$\mu{a_1}=\lambda{b_1},\mu{a_2}=\lambda{b_2}$
  - 若$a_1,a_2\neq{0}$,条件可以写作$\frac{b_1}{a_1}=\frac{b_2}{a_2}=\frac{\mu}{\lambda}$;(否则必然能够取等号,此时$\mu,\lambda$有无穷多解)
- 证明:
  - 该不等式可由几何角度三角不等式容易得出
    - 若$\boldsymbol{|\alpha||\beta|\neq 0}$则$|\boldsymbol{\alpha}|+|\boldsymbol{\beta}|$>${|\boldsymbol{\alpha+\beta}|}$
    - 若$\boldsymbol{|\alpha||\beta|}=0$,该不等式等号成立,即$|\boldsymbol{\alpha}|+|\boldsymbol{\beta}|$=${|\boldsymbol{\alpha+\beta}|}$
    - 反之,若等号成立,$\boldsymbol{\alpha,\beta}$中至少有一个为零向量,此时显然满足**取等条件**

#### 柯西三角形式和绝对值的三角不等式

- 令$\sqrt{a_1^2+a_2^2}+\sqrt{b_1^2+b_2^2}\geqslant{\sqrt{(a_1+b_1)^2+(a_2+b_2)^2}}$中的$a_2=b_2=0$,此时维度被压缩到一维(平面向量在$x$轴上的投影),不等式变为$\sqrt{a_1^2}+\sqrt{b_1^2}\geqslant\sqrt{(a_1+b_1)^2}$,即$|a_1|+|b_1|\geqslant{|a_1+b_1|}$,即$\forall{a,b\in\mathbb{R}}$,$|a|+|b|\geqslant{|a+b|}$,取等号条件$ab\geqslant0$

### 平面三角不等式

#### 代数形式

- 分别用$a_1-b_1$,$a_2-b_2$,$b_1-c_1$,$b_2-c_2$代替$a_1,a_2,b_1,b_2$代入到$\sqrt{a_1^2+a_2^2}+\sqrt{b_1^2+b_2^2}\geqslant{\sqrt{(a_1+b_1)^2+(a_2+b_2)^2}}$得

  - $$
    \sqrt{(a_1-b_1)^2+(a_2-b_2)^2}+\sqrt{(b_1-c_1)^2+(b_2-c_2)^2}\geqslant{\sqrt{(a_1-c_1)^2+(a_2-c_2)^2}}
    $$

  - 等号成立条件:

    - 构造平面点$A(a_1,a_2),B(b_1,b_2),C(c_1,c_2)$,条件描述为:$A,B,C$三点共线($|AB|+|BC|\geqslant|AC|$)
    - 构造向量$\boldsymbol{\alpha}=(a_1,a_2)$,$\boldsymbol{\beta}=(b_1,b_2)$,$\boldsymbol{\gamma}=(c_1,c_2)$,取等条件描述为:$\exist{\lambda>0}$ s.t. $\boldsymbol{\alpha-\beta}=\lambda{(\boldsymbol{\beta-\gamma})}$
    - 借助向量共线的概念描述:$\exist{\lambda,\mu>0}$ s.t. $\mu(a_1-b_1)=\lambda(b_1-c_1)$,$\mu(a_2-b_2)=\lambda(b_2-c_2)$
      - 可以把参数减少一个(例如两边同时除以$\mu$,$\mu(a_1-b_1)=\lambda(b_1-c_1)$ $\Rightarrow$ $(a_1-b_1)=\frac{\lambda}{\mu}(b_1-c_1)$,
      - 令$\nu=\frac{\lambda}{\mu}$,则$\exist{\nu}>0$ s.t $(a_1-b_1)=\nu(b_1-c_1)$,$(a_2-b_2)=\nu(b_2-c_2)$

#### 向量形式

- 若$\boldsymbol{\alpha,\beta,\gamma}$为平面向量,则$\boldsymbol{|\alpha-\beta|+|\beta-\gamma|\geqslant{|\alpha-\gamma|}}$;取等条件:
  - $\boldsymbol{|\alpha-\beta||\beta-\gamma|}\neq{0}$
  - 向量$\boldsymbol{\alpha-\beta,\beta-\gamma}$共线
- 证明
  - 设$\boldsymbol{\alpha}=(a_1,a_2)$,$\boldsymbol{\beta}=(b_1,b_2)$,$\boldsymbol{\gamma}=(c_1,c_2)$
  - $\boldsymbol{\alpha-\beta}$=$(a_1-b_1,a_2-b_2)$
  - $\boldsymbol{\beta-\gamma}$=$(b_1-c_1,b_2-c_2)$
  - $\boldsymbol{\alpha-\gamma}$=$(a_1-c_1,a_2-c_2)$
  - $\boldsymbol{|\alpha-\beta|}$=$\sqrt{(a_1-b_1)^2+(a_2-b_2)^2}$
  - $\boldsymbol{|\beta-\gamma|}$=$\sqrt{(b_1-c_1)^2+(b_2-c_2)^2}$
  - $\boldsymbol{|\alpha-\gamma|}$=$\sqrt{(a_1-c_1)^2+(a_2-c_2)^2}$

- 由平面三角不等式(代数形式)有$\boldsymbol{|\alpha-\beta|+|\beta-\gamma|\geqslant{|\alpha-\gamma|}}$;取等条件:$\exist{\lambda>0}$ s.t. $\boldsymbol{\alpha-\beta}=\lambda{(\boldsymbol{\beta-\gamma})}$



### 基本不等式和柯西不等式

- $$
  (a_1^2+a_2^2)+(b_1^2+b_2^2)\geqslant{2(a_1b_1+a_2b_2)}
  $$

- 由基本不等式:$(a_1^2+a_2^2)+(b_1^2+b_2^2)\geqslant{2\sqrt{(a_1^2+a_2^2)(b_1^2+b_2^2)}}$

- 再有柯西不等式:$(a_1^2+a_2^2)(b_1^2+b_2^2)\geqslant{(a_1b_1+a_2b_2)^2}$

- 所以有$(a_1^2+a_2^2)+(b_1^2+b_2^2)\geqslant{2|a_1b_1+a_2b_2|}$

  

## 小结👺

- 代数形式:$(a_1^2+a_2^2)(b_1^2+b_2^2)\geqslant{(a_1b_1+a_2b_2)^2}$;向量形式$\boldsymbol{|\alpha|^2|\beta|^2\geqslant{|\alpha\cdot{\beta}|^2}}$
- 几何积形式:${\boldsymbol{|\alpha||\beta|}} \geqslant \boldsymbol{|\alpha\cdot\beta|}$,对应于代数形式${\sqrt{a_1^2+a_2^2}\sqrt{b_1^2+b_2^2}} \geqslant |a_1b_1+a_2b_2|$
- 几何和形式(三角不等式形式):$|\boldsymbol{\alpha}|+|\boldsymbol{\beta}|\geqslant{|\boldsymbol{\alpha+\beta}|}$;对应于代数形式$\sqrt{a_1^2+a_2^2}+\sqrt{b_1^2+b_2^2}\geqslant{\sqrt{(a_1+b_1)^2+(a_2+b_2)^2}}$





# 柯西不等式的一般形式

- 设$a_1,\cdots,a_n;b_1,\cdots,b_n\in{\mathbb{R}}$,则$(\sum_{i=1}^{n}a_i^2\sum_{i=1}^{n}b_i^2)^{\frac{1}{2}}$ $\geqslant$ $|\sum_{i=1}^{n}a_ib_i|$;令集合$D=\{1,2,\cdots,n\}$
- 等价形式:$(\sum_{i=1}^{n}a_i^2)(\sum_{i=1}^{n}b_i^2)$ $\geqslant$ $(\sum_{i=1}^{n}a_ib_i)^2$;
- 取等条件:$a_i=\lambda{b_i},i\in{D}$;(若用除式描述:$\frac{a_i}{b_i}=\lambda,i\in{D}$,若$b_j=0,j\in\{1,2,\cdots,n\}$,则解释为$a_i=b_i=0$)

## 特例

- 取$n=2$时,$(a_1^2+a_2^2)(b_1^2+b_2^2)\geqslant{(a_1b_1+a_2b_2)^2}$

## 参数配方法证明一般形式的柯西不等式

- 证明:若$a_1=a_2=\cdots=a_n=0$,不等式显然成立(0=0)
  - 若$a_1,\cdots,a_n$至少有一个不是0,则:$\sum_{i=1}^{n}a_{i}^2>0$
  - 构造关于$x$的一元二次三项式:y=$(\sum_{i=1}^{n}a_{i}^2)x^2$+$2(\sum_{i=1}^{n}a_{i}b_{i})x$+$\sum_{i=1}^{n}b_{i}^2$ 则$y=\sum_{i=1}^{n}(a_ix+b_i)^2\geqslant{0}$
  - 从图形的角度来看$y$开口向上,且最小值为非负数,即$y\geqslant0$的解集为$\mathbb{R}$;$y=0$的实根个数为0或1,因此,若设y的判别式$\Delta$,则$\Delta\leqslant0$
  - 而$\Delta$=$4(\sum_{i=1}^{n}a_{i}b_{i})^2-4(\sum_{i=1}^{n}a_{i}^2)(\sum_{i=1}^{n}b_{i}^2)$,则$4[(\sum_{i=1}^{n}a_{i}b_{i})^2-(\sum_{i=1}^{n}a_{i}^2)(\sum_{i=1}^{n}b_{i}^2)]\leqslant{0}$,即$(\sum_{i=1}^{n}a_{i}b_{i})^2-(\sum_{i=1}^{n}a_{i}^2)(\sum_{i=1}^{n}b_{i}^2)\leqslant0$,即$(\sum_{i=1}^{n}a_{i}b_{i})^2$ $\leqslant$ $(\sum_{i=1}^{n}a_{i}^2)(\sum_{i=1}^{n}b_{i}^2)$
  - 所以  $|\sum_{i=1}^{n}a_ib_i|$ $\leqslant$ $(\sum_{i=1}^{n}a_i^2\sum_{i=1}^{n}b_i^2)^{\frac{1}{2}}$
  - 等号成立的条件:为了确定该条件,考察$y=\sum_{i=1}^{n}(a_ix+b_i)^2\geqslant{0}$这一形式,可知$a_ix+b_i=0$,$(i=1,2\cdots,n)$就是$\forall{x\in\mathbb{R}},y=0$的条件,此时判别式$\Delta=0$,也就是 $|\sum_{i=1}^{n}a_ib_i|$ $\leqslant$ $(\sum_{i=1}^{n}a_i^2\sum_{i=1}^{n}b_i^2)^{\frac{1}{2}}$取等号的条件;若用除式描述:
    - $\frac{a_i}{b_i}=-x^{-1},\forall i\in{D}$,即$\frac{a_1}{b_1}=\frac{a_2}{b_2}=\cdots=\frac{a_n}{b_n}$
      - 若$\exist{j}\in{D},b_j=0$,则$a_i=0,\forall{i}\in{D}$,
      - 若$\forall{i}\in{D},b_i\neq 0$,
- 上述的证明方法称为**参数配方法**

## 柯西不等式的应用

### 例

- 设$(r_1,r_2)$是$a_1,\cdots,a_4$为共面4顶点的多边形各边上的两个顶点下标组合之一,$S$是所有可能的组合,共有4中可能
- 求证:$\sum_{i=1}^{4}a_i^2\geqslant{\sum_{(r_1,r_2)\in{S}}}a_{r_1}a_{r_2}$,

- 证明:构造$f_1=a_1^2+a_2^2+a_3^2+a_4^2$,$f_2=a_2^2+a_3^2+a_4^2+a_1^2$,且$f_1=f_2$
  - 则$f_1^2f_2^2\geqslant{(a_1a_2+a_2a_3+a_3a_4+a_4a_1)^2}$,所以$\sum_{i=1}^{4}a_i^2\geqslant{\sum_{(r_1,r_2)\in{S}}}a_{r_1}a_{r_2}$

### 例

- 设$a,b,c>0$,且$a+b+c=1$,求证$a^{-1}+b^{-1}+c^{-1}\geqslant{9}$
- 证明:构造两组数
  - $\sqrt{a},\sqrt{b},\sqrt{c}$
  - $\sqrt{a}^{-1},\sqrt{b}^{-1},\sqrt{c}^{-1}$
  - 由柯西不等式(n=3形式):$(a+b+c)(a^{-1}+b^{-1}+c^{-1})\geqslant{(1+1+1)^2}$
  - 所以$a^{-1}+b^{-1}+c^{-1}\geqslant{9}$









