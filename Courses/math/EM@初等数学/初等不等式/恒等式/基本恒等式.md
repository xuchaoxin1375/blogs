[toc]

## 乘法公式恒等式

**基本**[乘法公式](https://zh.wikipedia.org/wiki/乘法公式)

- 分配律:$(a+b)(c+d)= ac+ad+bc+bd$

- 和差平方:${\displaystyle (a\pm b)^{2}=a^{2}\pm 2ab+b^{2}}$

- 平方和、平方差延伸：${\displaystyle a^{2}+b^{2}=(a+b)^{2}-2ab=(a-b)^{2}+2ab\,\!}$

- 和差立方:${\displaystyle (a\pm b)^{3}=a^{3}\pm 3a^{2}b+3ab^{2}\pm b^{3}}$

- 三数和平方:$(a + b + c)^2 = a^2 + b^2 + c^2 + 2ab + 2bc + 2ca \,\!$

- 三数差平方：${\displaystyle (a-b-c)^{2}=a^{2}+b^{2}+c^{2}-2ab+2bc-2ca\,\!}$

- 三数和立方：${\displaystyle (a+b+c)^{3}=a^{3}+b^{3}+c^{3}+3(a+b)(b+c)(a+c)\,\!}$

- 
  等幂和差

  - 平方和$a^2+b^2=(a+bi)(a-bi)$

  - 
    平方差${\displaystyle a^{2}-b^{2}=\left(a+b\right)\left(a-b\right)}$

- 
  立方和差$a^{3}\pm b^{3}=(a\pm b)(a^{2}\mp ab+b^{2})$

- 
  等幂和差逆定理$a^4+a^2b^2+b^4 = (a^2+ab+b^2)(a^2-ab+b^2)\,\!$

- 对称多项式:

  - $a^2+b^2+c^2-ab-bc-ac$=$\frac{1}{2}[(a-b)^2+(b-c)^2+(c-a)^2]$ $\geqslant{0}$
  - $a^3+b^3+c^3$=$(a+b+c)^3+3(a+b+c)(-ab-bc-ca)+3abc$
    - =$(a+b+c)[(a+b+c)^2-3(ab+ac+bc)]+3abc$
  - 三次等幂求和:$a^3+b^3+c^3-3abc$=$(a+b+c)[(a+b+c)^2-3(ab+ac+bc)]$
    - $=(a+b+c)(a^2+b^2+c^2+2(ab+ac+bc)-3(ab+ac+bc))$
    - $=(a+b+c)(a^2+b^2+c^2-ab-ac-bc)$

## 平方和公式(因式分解公式)

平方和公式是一个比较常用公式，用于求连续自然数的平方和（Sum of squares），其和又可称为四角锥数，或金字塔数（square pyramidal number）也就是正方形数的级数。
此公式是**冯哈伯公式**(Faulhaber's formula)(**正整数的幂和**通用公式)的一个特例。

详细介绍参考:

[平方和公式](https://baike.baidu.com/item/平方和公式/3264126)

许多期刊文献给出更多的证明/推导角度和思路

证明或推导该公式的方法有多种,包括初等数学方法和高等数学方法,但是总体上看,平方和公式的推导并不是容易的事情,需要对数学比较敏感以及足够的运算经验,有一定的技巧性.

下面给出简单的几种推导方法

### 拆分法

在学习等差数列和的时候,我们有$1+3+5+\cdots+(2n-1)$=$n^2$`(1)`,或者说$\sum\limits_{i=1}^{n}(2i-1)$=$n^2$

在学习数列的一般性质时,有$S_{n}-S_{n-1}=a_{n}$,在这里$a_{n}=n^2$,如果能够得到$S_{n},S_{n-1},n$构成的等式,将其整理有望得到$S_{n}$关于的$n$的表达式

$S_{n}$=$\sum\limits_{i=1}^{n}i^2$=$\sum\limits_{i=1}^{n}\sum\limits_{k=1}^{i}(2k-1)$

对于式(1),分别令$n=1,2,\cdots,n$得到$n$个等式,两边对应相加得到的等式记为`(2)`,等式(2)左边进行变形:
$$
\begin{align*} 
    &= [1+3+5+\cdots+(2 \times n-1)] \times n
    -\{0 \times 1+1 \times 3+2 \times 5+3 \times 7+\cdots
    +(n-1) \times(2 \times n-1)\} \\ 
    &= n^{2} \times n
    -\sum_{i=1}^{n}\left[(i-1) \times(2 \times i-1)\right] \\ 
    &= n^{3}-\sum_{i=1}^{n}\left[(i-1) \times[2 \times(i-1)+1]\right]
    =n^{3}-2 \sum_{i=1}^{n}(i-1)^{2}-\sum_{i=1}^{n}(i-1)  \\ 
\end{align*}
$$
即$n^{3}-2 \sum_{i=1}^{n}(i-1)^{2}-\sum_{i=1}^{n}(i-1)$=$S_{n}$`(3)`,其中 $$S_{n-1}=\sum_{i=1}^{n}(i-1)^{2}$$ ,从而$n^3-2S_{n-1}-\frac{n(n+1)}{2}+n$=$S_{n}$`(4)`

因为前n项平方和与前n-1项平方和差为$n^2$;即$S_{n}-S_{n-1}=n^2$

带入(4)整理并因式分解,得:$S_{n}$= $\sum_{i=1}^{n} i^{2}=\frac{n(n+1)(2n+1)}{6}$

### 排列组合法

#### 方案1

由于$i^2 = i^2 + i - i = i(i-1) + i = 2C_i^2 + C_i^1$，

因此有$1^2 + 2^2 + \cdots + n^2 = 1 + 2(C_2^2 + \cdots + C_n^2) + (C_2^1 + \cdots + C_n^1)$

$= 2(C_2^2 + \cdots + C_n^2) + (C_1^1 + C_2^1 + \cdots C_n^1)$

由于$C_2^2 + \cdots C_n^2 = C_{n+1}^3 = \frac{(n+1)n(n-1)}{6}$，$C_1^1 + C_2^1 + \cdots + C_n^1 = C_{n+1}^2 = \frac{(n+1)n}{2}$，

于是有$1^2 + 2^2 + \cdots + n^2 = 2 \times \frac{(n+1)n(n-1)}{6} + \frac{(n+1)n}{2} = \frac{n(n+1)(2n+1)}{6}$

#### 方案2

则根据排列数公式，我们可以利用这些公式对 $n^2$ 进行适当的分解。易得：
$$
\begin{aligned}
&A_2^2 + A_3^2 + A_4^2 + A_5^2 + \cdots + A_{n+1}^2 \\
&= 1 \times 2 + 2 \times 3 + 3 \times 4 + 4 \times 5 + \cdots + n(n+1)\\
&= (1 + 1^2) + (2 + 2^2) + (3 + 3^2) + (4 + 4^2) + (5 + 5^2) + \cdots + (n + n^2)\\
&= 1^2 + 2^2 + 3^2 + 4^2 + \cdots + n^2 + 1 + 2 + 3 + 4 + \cdots + n
\end{aligned}
$$

上述推导中第一个等号尾项$n(n+1)$=$n^2+n$是每一项的通项公式,原式为$\sum_{i=1}^{n}i(i+1)$=$\sum_{i=1}^{n}(i^2+i)$

因为 $A_2^2 + A_3^2 + A_4^2 + A_5^2 + \cdots + A_{n+1}^2 = A_2^2 \left( C_2^2 + C_3^2 + C_4^2 + C_5^2 + \cdots + C_{n+1}^2 \right)$

根据组合数性质(朱世杰恒等式)，得

$$C_2^2 + C_3^2 + C_4^2 + C_5^2 + \cdots + C_{n+1}^2 = C_{n+2}^3$$

所以

$$A_2^2 + A_3^2 + A_4^2 + A_5^2 + \cdots + A_{n+1}^2 = A_2^2 C_{n+2}^3$$

$$= 2 \times 1 \times \frac{(n+2) \times (n+1) \times n}{3 \times 2 \times 1}$$

$$= \frac{(n+2)(n+1)n}{3}$$

根据以上运算，可以得到

$$1^2 + 2^2 + 3^2 + 4^2 + \cdots + n^2 = A_2^2 + A_3^2 + A_4^2 + A_5^2 + \cdots + A_{n+1}^2 - (1 + 2 + 3 + 4 + \cdots + n)$$

$$= \frac{(n+2)(n+1)n}{3} - \frac{n \times (n+1)}{2}$$

$$= \frac{n(n+1)(2n+1)}{6}$$

## 函数类恒等式

### 基础篇

- [对数恒等式](https://zh.wikipedia.org/wiki/对数恒等式)
- [指数恒等式](https://zh.wikipedia.org/wiki/指数恒等式)
- [三角恒等式](https://zh.wikipedia.org/wiki/三角恒等式)

### 高阶篇

- [双曲线函数恒等式](https://zh.wikipedia.org/wiki/雙曲函數恆等式)
- [超几何函数恒等式](https://zh.wikipedia.org/w/index.php?title=超几何函数恒等式&action=edit&redlink=1)
- [组合恒等式](https://zh.wikipedia.org/w/index.php?title=组合恒等式&action=edit&redlink=1)



### refs

- [恒等式  (wikipedia.org)](https://zh.wikipedia.org/zh-cn/恒等式)





