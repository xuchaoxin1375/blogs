[toc]

## abstract

- 从$n$次方根引入分数指数幂(有理指数幂),实数指数幂及其运算
- 介绍根式以及根式的幂式表示

## 方根和分数指数幂👺

- 下面讨论**实数范围**内的方根概念
- 和根的**个数**相关的结论都限定在实数范围内
- 复数范围内在此不讨论

### 简单低次方根

本节介绍平方根和立方根,平方根是重点;

### 回顾初中教材

一般地，如果一个**正数** $x$ 的平方等于 $a$，即 $x^2 = a$，那么这个**正数** $x$ 叫做 $a$ 的 **算术平方根**（arithmetic square root）。

$a$ 的算术平方根记为 $\sqrt{a}$，**读作“根号 $a$”**，$a$ 叫做**被开方数**（radicand）。

规定：0 的算术平方根是 0。

一般地，如果一个数的平方等于 $a$，那么这个数叫做 $a$ 的 **平方根**（square root）或 **二次方根**．

- 这就是说，如果 $x^2 = a$，那么 $x$ 叫做 $a$ 的平方根。

- 例如，3 和 -3 是 9 的平方根，简记为 $\pm 3$ 是 9 的平方根。


求一个数 $a$ 的平方根的运算，叫做 **开平方**（extraction of square root）

求一个数的立方根的运算，叫做**开立方**（extractionof cuberoot)

一般地，我们把形如$\sqrt{a}$ ($a \geqslant 0$)的式子叫做**二次根式**（quadratic radical），“$\sqrt{\quad}$”称为**二次根号**。

把满足以下两个条件的二次根式叫做**最简二次根式**（simplest quadratic radical）。

(1) 被开方数不含分母；

 (2) 被开方数中不含能开得尽方的**因数或因式**。

在二次根式的运算中，一般要把最后结果化为最简二次根式，并且分母中不含二次根式。

比如$\sqrt{8}=2\sqrt{2}$,有利于合并化简根式,比如$\sqrt{8}+\sqrt{2}$=$3\sqrt{2}$

### 回顾高中教材

平方根和立方根相关内容:

- $x^2=a$,则$x$称为$a$的**平方根**或**二次方根**
  - 实数范围内的平方根情况分析:
    - 若$a>0$,则$x=\pm{\sqrt{a}}$,共2个平方根
    - 若$a=0$,则$x=0$,仅一个平方根
    - 若$a<0$,则实数范围内没有平方根
- $x^3=a$,则$x$称为$a$的**立方根**或**三次方根**
  - 实数范围内,$x^3=a$有且仅有1个根$\sqrt[3]{a}$
  - Note:根据代数学基本定理,复数范围内有3个立方根(包含重根)

### $n$次方根和开方运算

本节对比这两个概念,$n$次方根借助方程的来定义;

在数学中，一数$b$为数$a$的**$n$次方根**，则$b^n=a$。

在提及实数$a$的$n$次方根的时候，若指的此数的**主$n$次方根**，则可以用根号（$\sqrt[n]{\quad}$）表示成$\sqrt[n]{a}$。

> 例如：1024的主10次方根为2，就可以记作$\sqrt[10]{1024}=2$。当$n=2$时，则$n$可以省略。

### 主$n$次方根

定义实数$a$的**主$n$次方根**$b$为:$a$的$n$次方根中有与$a$相同的正负号的唯一实数$b$。

在$n$是偶数时，负数没有主$n$次方根。

方根也是幂的分数指数，即数$b$为数$a$的一次方：

$$b = \sqrt[n]{a} = a^{\frac{1}{n}}$$

### $n$次方根👺

一般地,给定大于1的**正整数$n$和实数$a$**,若存在实数$x$,使得$x^{n}=a$,则**$x$称为$a$的$n$次方根**

- 若$\exist{x}$ s.t. $x^{n}=a$,$(a\in{\mathbb{R}},n\in{\mathbb{N}_{+}})$,则$x$是实数**$a$的$n$次方根**

Note:$n$次方根是幂$x^{n}$的指数$n$为**正整数**的范围内讨论的

例如，因为方程 $x^4=81$ 的实数解为 3 与 -3，所以 3 与 -3 都是 81 的 4 次方根；因为 $2^5=32$，而且 $x^5=32$ 只有一个实数解，所以 32 的 5 次方根为 2。

## $n$次方根相关概念和性质👺

根据方程 $x^n=a$ 解的情况：

- 0 的任意正整数次方根均为 0，记为 $\sqrt[n]{0}=0$。
- 正数 $a$ 的**偶数次方根**有两个，它们**互为相反数**，其中
  - **正的方根**称为 $a$ 的 $n$ 次**算术根**，记为 $\sqrt[n]{a}$，
    - 例如$\sqrt{a}$称为$a$的**算数平方根**

  - **负的方根**记为 $-\sqrt[n]{a}$；负数的偶数次方根在实数范围内不存在，即当 $a<0$ 且 $n$ 为偶数时，$\sqrt[n]{a}$ 在实数范围内没有意义。

- 任意实数的**奇数次方根**都有且只有一个，记为 $\sqrt[n]{a}$。
  - 正数的奇数次方根是一个正数，负数的奇数次方根是一个负数。

- 当 $\sqrt[n]{a}$ 有意义的时候，$\sqrt[n]{a}$ 称为**根式**，$n$ 称为**根指数**，$a$ 称为**被开方数**。




### 根式性质👺

- 根据$n$次方根的定义,有
  1. $(\sqrt[n]{a})^{n}=a$,$(n\in{\mathbb{N}_{+}})$
  2. $\sqrt[n]{a^{n}}$=$\begin{cases}a&n为奇数\\|a|&n为偶数\end{cases}$
    - $\sqrt[n]{a^{n}}$,$(n\in\mathbb{N_{+}})$这个表达式总是有意义的
      - 当$n$为奇数时该根式显然有意义
      - 当$n$为偶数时,$a^{n}$一定是非负的,所以仍然有意义
    - $|\sqrt[n]{a^{n}}|$=$|a|$

公式应用

- $\sqrt[7]{(-2)^{7}}$=$-2$
- $\sqrt[4]{(-3)^{4}}=|-3|=3$
- $(\sqrt[5]{2})^{5}=2$

#### 根号@根式和开方运算

- 求$a$的$n$次方根,称为"把$a$开$n$次方",称为**开方运算**

- 为了把$a$的$n$次方根$x$表示为形如$x=f(a)$的形式,引入记号$\sqrt[n]{a}$,称为$a$的**主$n$次方根**
  - 若$b=\sqrt[n]{a}$则$b^n=a$,若$b^n=a$不可能成立,则称$\sqrt[n]{a}$无意义,比如$n$为偶数,$a<0$的情况

- "把$a$开$n$次方"不同于$\sqrt[n]{a}$,后者是前者运算的一个结果的一种表示

#### 偶次方根和奇次方根

- 当$a>0$且$n$为偶数时,$\sqrt[n]{a}$表示的是$a$的两个$n$次方根中的**正根**
  - 正数的**偶次方根**(共2个)使用该符号的算式表示为$\sqrt[n]{a}$,$-\sqrt[n]{a}$
  - 负数偶次方根**没有意义**(指实数范围内,$a<0$,$n$为偶数时,$x^n=a$无实数根,不存在$\sqrt[n]{a}$)
- 当$a<0$时,其只有奇次方根,表示为$\sqrt[n]{a}$
- $n$为奇数时,$a$的$n$次方根只有一个,也记为$\sqrt[n]{a}$



- 总之$\sqrt[n]{a}$表示的是$a$的唯一奇数次方根或者两个互为相反数的偶次方根中的正根(非负根),具体要视$a,n$的取值而定

#### n次方根的表示

任意数可以开**奇次方**,但不是任意数都可以开**偶次方**

1. 偶次方根:正数可以开偶次方根
   - 正数$a$的偶次方根有2个互为相反数的根,,它们分别表示为$\sqrt[n]{a}$,$-\sqrt[n]{a}$,($a$为偶数)
   - 负数$a$的偶次方根没有意义(没有实根,但是在复数范围内有意义)
2. 奇次方根:任何实数可以开奇次方根
   - $\forall a\in\mathbb{R}$与其唯一的奇次方根$\sqrt[n]{a}$**同号**,即$a\sqrt[n]a\geqslant{0}$,($n$为奇数)



## 符号

- [Radical symbol - Wikipedia](https://en.wikipedia.org/wiki/Radical_symbol)

- 在数学中，根号$\sqrt{\quad}$ 被用来表示一个数 $x$ 的平方根或 $n$ 次方根，分别写作 $\sqrt{x}$ 及 $\sqrt[n]{x}$，分别读作
  - “二次根号 $x$”
  - “$n$ 次根号 $x$”。
- 有时在文字系统不支持数学式的时候，会使用“√”来代替平方根,或`sqrt(x,n)`来表示$x$的主$n$次方根。

## 将整数指数幂推广到分数指数幂(有理数幂)

我们希望推广后，有关的运算性质仍然能保持。

比如 $(a^m)^n=a^{mn}$ 在指数是分数时仍然成立。

举例来说，$5^{\frac{1}{2}}$ 应该满足$(5^{\frac{1}{2}})^2 = 5^{\frac{1}{2} \times 2} = 5^1 = 5$,这表示 $5^{\frac{1}{2}}$ 应该是 5 的平方根，但是 5 的平方根有两个，即 $\sqrt{5}$ 和 $-\sqrt{5}$。为了方便起见，我们规定 $5^{\frac{1}{2}}=\sqrt{5}$。

分数指数幂运算可以像整数指数幂那样运算。

### 分数指数幂和根式表示

为了方便起见，我们约定底数 $a>0$。

#### 正分数指数幂👺

当 $a>0$ 时，规定$a^{\frac{1}{n}} = \sqrt[n]{a}$,从而有**正分数指数幂**
$$
a^{\frac{m}{n}} = (\sqrt[n]{a})^m = \sqrt[n]{a^m} \quad (n, m \in \mathbb{N}_+, \text{且 } \frac{m}{n} \text{为既约分数}).
$$

**歧义问题和既约分数指数**:若上式在 $\frac{m}{n}$ 不是**既约分数**（即 $m$, $n$ 有大于 1 的公因数）时可能会有歧义。

例如，$(-8)^{\frac{2}{6}} = \sqrt[6]{(-8)^2}$ 是有意义的，而 $(-8)^{\frac{4}{6}} = (\sqrt[6]{-8})^2$ 是没有意义的。因此，以后无特别说明时，我们都认为分数指数幂中的指数都是既约分数。

#### 负分数指数幂

**负分数指数幂**的定义与**负整数指数幂**类似，即 $a>0$ 时，规定
$$
a^{-\frac{m}{n}} = \frac{1}{a^{\frac{m}{n}}} \quad (n, m \in \mathbb{N}_+).
$$

现在我们已经将整数指数幂推广到了分数指数幂（即有理数指数幂）。

#### 运算法则

一般情况下，当 $s$ 与 $t$ 都是有理数时，有运算法则：

- $a^s a^t = a^{s+t}$,
- $(a^s)^t = a^{st}$,
- $(ab)^t = a^t b^t$

更一般的

- $a^{r_1}a^{r_2}\cdots{a^{r_n}}$=$a^{\sum_{i=1}^{n}a_i}$
- $(((a^{r_1})^{r_2})^{\cdots})^{r_n}$=$a^{\prod_{i=1}^{n}r_{i}}$
- $(a_1a_2\cdots{a_n})^{r}=\prod_{i=1}^{n}a_{i}^{r}$

## 有理数幂推广到实数幂

一般地，当 $a \geqslant 0$ 且 $t$ 是**无理数**时，$a^t$ 都是一个**确定的实数**

我们可以用夹逼的的方法找出它的任意精度的近似值。

因此，当 $a \geqslant 0$，$t$ 为**任意实数**时，可以认为**实数指数幂 $a^t$ 都有意义**。

可以证明，对任意实数 $s$ 和 $t$，类似前述有理指数幂的**运算法则仍然成立**。

## 补充

### 判定一个分数指数幂是否有意义👺

- $a=(-2)^{\frac{4}{3}}$=$\sqrt[3]{(-2)^{4}}$,而$b=(-2)^{\frac{8}{6}}$=$\sqrt[6]{(-2)^{8}}$,显然$a=b$
  - 第一个式子$a$我们只需要看到分数指数$\frac{4}{3}$中分母为奇数$3$,就可以断言$a$一定有意义
  - 第二个式子$b$,其分数指数$\frac{8}{6}$的分子式偶数,那么也可以确定$b$必定有意义

- 总之对于$a^{\frac{m}{n}}=\sqrt[n]{a^{m}}$,$(a<0)$;分子$m$为奇数,分母$n$为偶数,则$a^{\frac{m}{n}}$无意义,其余情况都有意义
  - 其中分母$n$和偶次方根挂钩,负数的偶次方根无意义
  - 分子$m$若为偶数,那么$a^{n}$非负,其任意次方根均有意义



### 幂的底数的讨论范围

- 通常我们不讨论任意底数的任意指数幂,主要讨论底数$a$在$(a>0,a\neq{1})$的情况
- 底数为负数的幂是否有意义依赖于指数,中学阶段和非数学专业一般就研究**正底数幂**
- 0的幂也是比较特殊,在指数$\alpha$为正数的情况下,$0^{\alpha}=0(\alpha>0)$;而0的非正数幂没有定义

