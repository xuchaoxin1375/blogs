[toc]

# 诱导公式

- [诱导公式  (wikipedia.org)](https://zh.wikipedia.org/wiki/诱导公式)

##  单位圆坐标和三角函数

- | ![在这里插入图片描述](https://img-blog.csdnimg.cn/888ebf4f0a3944578db97cdebf3a52df.jpeg) | ![在这里插入图片描述](https://img-blog.csdnimg.cn/8b0764db1187490f9edfd26f9954c294.png) |
  | ------------------------------------------------------------ | ------------------------------------------------------------ |
  | ![在这里插入图片描述](https://img-blog.csdnimg.cn/688dca26cbfa433f9c5d8b5c85492c5f.png) | ![image-20220621152858967](https://img-blog.csdnimg.cn/img_convert/7537432a58a9a7646ba7751694340023.png) |

- 例如,$sin(\theta+\pi)=- sin(\theta);这里\phi(\theta)=\pi+\theta$
- $途中各个点的横纵坐标分值分别对应p(cox(\phi(\theta)),sin(\phi(\theta)))$
- 途中设定了两个超级点(主超级点为$A(cos\theta,sin\theta),副超级点B(sin\theta,cos\theta)$
  - $所有的其他角度都可以由超级点关于x轴或者y轴或者圆心原点(或者\theta=\frac{\pi}{2})对称$
  - 比如$\phi(\theta)=\theta-\frac{\pi}{2};则sin(\phi(\theta))=-cos\theta;cos(\phi(\theta))=sin\theta$

###  记忆口诀

* 对于$k\frac{\pi}{2}\pm\alpha(k\in \mathbb{Z})$的三角函数值，

#### 符号看象限

- 口诀总是把$\alpha$看作锐角,例如$2π-α∈(270°，360°)$,弧度角$2\pi-\alpha$终边落在第4象限，$\sin(2π-α)<0$，符号为“-”

####  奇变偶不变

- 当k是偶数时，得到α的同名函数值，即函数名不改变；
- 当k是奇数时，得到α相应的余函数值，即
  - sin→cos;cos→sin;tan→cot,cot→tan

- 然后在前面加上把α看成锐角时原函数值的符号

- 对于$\tan,\sec,\csc,\cot$可以转化为$\cos,\sin$处理

### 例

- $sin(2π-α)=sin(4·\frac{\pi}{2}-α)$，k=4为偶数，所以函数名(绝对值部分)是$\sin\alpha$。
- 所以$sin(2π-α)=-sinα$

## 常用诱导公式🎈

#### 常用部分(5对)

- $\sin(-\alpha)=-\sin{\alpha}$
- $\cos(-\alpha)=\cos{\alpha}$
- $\sin(\frac{\pi}{2}-\alpha)=\cos{\alpha}$
- $\cos(\frac{\pi}{2}-\alpha)=\sin{\alpha}$
- $\sin(\frac{\pi}{2}+\alpha)=\cos{\alpha}$
- $\cos(\frac{\pi}{2}+\alpha)=-\sin{\alpha}$
- $\sin{(\pi-\alpha)}=\sin{\alpha}$
- $\cos{(\pi-\alpha)}=-\cos{\alpha}$
- $\sin(\pi+\alpha)=-\sin{\alpha}$
- $\cos{(\pi+\alpha)}=-\cos{\alpha}$

- <u>总之,第一象限全是正的,第三象限全是负的</u>



####  倒数关系

$$
 正弦(sine)\times余割(co-secant)=1
 \\正割(secant)\times余弦(co-sine)=1
\\ 正切(tangent)\times余切(co-tangent)=1
$$

| tan·gent   | co·tan·gent  | se·cant             | co·se·cant  |
| ---------- | ------------ | ------------------- | ----------- |
| /ˈtanjənt/ | /kōˈtanjənt/ | /ˈsēˌkant,ˈsēˌkənt/ | /kōˈsēkənt/ |
| 正切       | 余切         | 正割                | 余割        |

####  六种三角函数间的转换关系

- 正弦余弦&正割余割&正切余切间的转换($\frac{\pi}{2}$)
  ![image-20220617203146092](https://img-blog.csdnimg.cn/img_convert/9813733cc3f82528be4040626596e500.png)

## 小结

- $\frac{\pi}{2}-\alpha$:关于$y=x$对称

- 关于$y=x$对称的两点$P_1=(x_1,y_1),P2=(x_2,y_2)$坐标关系:
  - $x_1=y_2$
  - $x_2=y_1$


#### Reflections

- ![在这里插入图片描述](https://img-blog.csdnimg.cn/63574d9c2ce2453bbfb2a446d75f698b.png)

#### Shifts and periodicity

![image-20220621130245743](https://img-blog.csdnimg.cn/img_convert/e02874ea1e8d4853964d924aecbf337b.png)