[toc]

## abstract

- 在一些实际问题中,常会遇到**积分区间为无穷区间**,或者**被积函数为无界函数的积分**,它们已经**不属于定积分**了.
- 因此,我们对定积分作如下两种**推广**,借助极限定义反常积分的概念.

- 反常积分包括**无穷限反常积分**和**无界函数的反常积分**(**瑕积分**)

## 无穷限反常积分

1. 设函数$f(x)$在$[a,+\infin)$上连续,任取$t>a$,作**定积分**$\int_{a}^{t}f(x)\mathrm{d}x$,再求**变上限定积分**极限:$A=\lim\limits_{t\to{+\infin}}\int_{a}^{t}f(x)\mathrm{d}x$`(1)`,式(1)称为函数$f(x)$在无穷区间$[a,+\infin)$上的**反常积分**记为$\int_{a}^{+\infin}f(x)\mathrm{d}x$=$\lim\limits_{t\to{\infin}}\int_{a}^{t}f(x)\mathrm{d}x$`(2)`

2. 类似的,设$f(x)$在$[-\infin,b]$上连续,任取$t<b$,算式$\lim\limits_{t\to{-\infin}}{\int_{t}^{b}f(x)\mathrm{d}x}$`(1-1)`称为$f(x)$在$(-\infin,b]$上的反常积分,记为$\int_{-\infin}^{b}f(x)\mathrm{d}x$=$\lim\limits_{t\to{-\infin}}{\int_{t}^{b}f(x)\mathrm{d}x}$

3. 综合前两种情形,设函数$f(x)$在$(-\infin,+\infin)$上来连续,反常积分$\int_{-\infin}^{b}f(x)\mathrm{d}x$和$\int_{0}^{+\infin}f(x)\mathrm{d}x$之和称为$f(x)$在无穷区间$(-\infin,+\infin)$上的反常积分,记为$\int_{-\infin}^{+\infin}f(x)\mathrm{d}x$

   

### 敛散性

- 若式极限(1)存在,则称(2)**收敛**,并称此极限值$A$为(2)的**反常积分值**
  - 否则,(2)**发散**
- 类似的,有另一侧和双侧无穷限反常积分的敛散性和反常积分值定义

### 无穷限反常积分运用微积分基本公式

- 设$F(x)$为$f(x)$在$[a,+\infin]$上的一个**原函数**
  - 若$R=\lim\limits_{x\to{+\infin}}F(x)$存在,则$\int_{a}^{+\infin}f(x)\mathrm{d}x$=$R-F(a)$=$\lim\limits_{x\to{+\infin}}F(x)-F(a)$`(3)`
  - 若$R$不存在,则$\int_{a}^{+\infin}f(x)\mathrm{d}x$发散
- 若记$F(+\infin)$=$\lim\limits_{x\to{+\infin}}F(x)$,$[F(x)]_{a}^{+\infin}$=$F(+\infin)-F(a)$,`(3-0)`
  - 当$F(+\infin)$存在时,式(3)可以作$\int_{a}^{+\infin}f(x)\mathrm{d}x$=$[F(x)]_{a}^{+\infin}$`(3-1)`
  - 当$F(+\infin)$不存在时,反常积分$\int_{a}^{+\infin}f(x)\mathrm{d}x$发散
- 类似的,另外两种情形的反常积分有相仿的表示方法
  - $\int_{-\infin}^{b}f(x)\mathrm{d}x$=$[F(x)]_{-\infin}^{b}$
  - $\int_{-\infin}^{+\infin}f(x)\mathrm{d}x$=$[F(x)]_{-\infin}^{+\infin}$
- Note:$F(x)=\int{f(x)}\mathrm{d}x$

### 例

- $\int_{-\infin}^{+\infin}{\frac{1}{x^2+1}\mathrm{d}x}$=$[\arctan{x}]_{-\infin}^{+\infin}$=$\lim\limits_{x\to{+\infin}}\arctan{x}$-$\lim\limits_{x\to{-\infin}}\arctan{x}$=$\frac{\pi}{2}-(-\frac{\pi}{2})$=$\pi$
- 这个反常积分的几何意义表明,虽然曲线$\frac{1}{x^2+1}$与$x$轴围成的区域是无限延申的,但它的面积却不是无限大的,而是有一个极限值$\pi$
- 这类情况在概率统计的连续型随机变量的密度函数求概率问题中经常遇到

### 例

- $\int_{0}^{+\infin}te^{-pt}\mathrm{d}t$,其中$p$为常数,$p>0$
  - $\int_{0}^{+\infin}te^{-pt}\mathrm{d}t$=$[\int te^{-pt}\mathrm{d}t]_{0}^{+\infin}$
    - =$[-\frac{1}{p}\int t\mathrm{d}(e^{-pt})]_{0}^{+\infin}$ 分部积分法
    - =$-\frac{1}{p}[te^{-pt}-\int{e^{-pt}\mathrm{d}t}]_{0}^{+\infin}$
    - =$-\frac{1}{p}[te^{-pt}|_{0}^{+\infin}+\frac{1}{p}e^{-pt}|_{0}^{+\infin}]$
    - =$-\frac{1}{p}(\lim\limits_{t\to{+\infin}}te^{-pt}-0)$-$\frac{1}{p^2}(0-1)$=$0-(-\frac{1}{p})$=0
  - 其中$\lim\limits_{t\to{+\infin}}te^{-pt}$是$\infin\cdot{0}$型未定式$\lim\limits_{t\to{+\infin}}te^{-pt}$=$\lim\limits_{t\to{+\infin}}t/e^{pt}$=$\lim\limits_{t\to{+\infin}}\frac{1}{pe^{pt}}$=0

- 证明$\int_{a}^{+\infin}\frac{1}{x^{p}}\mathrm{d}x$,$(a>0)$当$p>1$时收敛,当$p\leqslant{1}$时发散

  - 当$p=1$时,$\int_{a}^{+\infin}\frac{1}{x^{p}}\mathrm{d}x$=$\int_{a}^{+\infin}\frac{1}{x}\mathrm{d}x$=$[\ln{|x|}]_{a}^{+\infin}$=$[\ln{x}]_{a}^{+\infin}$=$+\infin$,发散
  - 当$p\neq{1}$
    - $\int_{a}^{+\infin}\frac{1}{x^{p}}\mathrm{d}x$=$\frac{1}{1-p}x^{1-p}|_{a}^{+\infin}$
      - =$+\infin$,$(p<1)$,发散
      - =$\frac{1}{1-p}(0-a^{1-p})$=$\frac{a^{1-p}}{p-1}$,$(p>1)$,收敛

  - 综上,欲证命题成立





## 无界函数的反常积分

- 若函数$f(x)$在点$a$的**任意一邻域**内**都无界**,则点$a$称为$f(x)$的**瑕点**(无界间断点)
- 无界函数的反常积分又称为**瑕积分**
- 设函数$f(x)$在区间$(a,b]$上**连续**,点$a$为$f(x)$的瑕点,
  - 任取$t>a$,作定积分$\int_{t}^{b}f(x)\mathrm{d}x$`(0)`,求其极限$\lim\limits_{t\to{a^{+}}}{\int_{t}^{b}f(x)\mathrm{d}x}$`(1)`
  - 这个对变下限的定积分求极限的算式(1)称为函数$f(x)$在区间$(a,b]$上的反常积分,仍然记为$\int_{a}^{b}f(x)\mathrm{d}x$`(3)`,即$\int_{a}^{b}f(x)\mathrm{d}x$=$\lim\limits_{t\to{a^{+}}}{\int_{t}^{b}f(x)\mathrm{d}x}$`(3-1)`
- 类似地,$f(x)$在$[a,b)$上连续,点$b$为$f(x)$的瑕点,任取$t<b$,可以定义:$\lim\limits_{t\to{b^{-}}}\int_{a}^{t}f(x)\mathrm{d}x$为函数$f(x)$在$[a,b)$上的**反常积分**,仍然记为:$\int_{a}^{b}f(x)\mathrm{d}x$,即$\int_{a}^{b}f(x)\mathrm{d}x$=$\lim\limits_{t\to{b^{-}}}\int_{a}^{t}f(x)\mathrm{d}x$`(3-2)`
- 综合上述两种情形,$f(x)$在$(a,b)$上的反常积分为$[a,c)$和$(c,b]$上的反常积分之和
  - $\int_{a}^{b}f(x)\mathrm{d}x$=$\int_{a}^{c}f(x)\mathrm{d}x$+$\int_{c}^{b}f(x)\mathrm{d}x$

### 敛散性

- 对于情形$(a,b]$若极限(1)存在,则(3)收敛,并称该极限为(3)的反常积分值
  - 否则(3)发散
- 情形$[a,b)$类似
- 情形$(a,b)$要求两端反常积分都收敛,才收敛,否则发散

### 瑕积分应用微积分基本公式

- 设$x=a$为$f(x)$的瑕点,$(a,b]$上$F'(x)=f(x)$,若极限$\lim\limits_{x\to{a^{+}}}F(x)$存在,则$\int_{a}^{b}f(x)\mathrm{d}x$=$F(b)-\lim\limits_{x\to{a^{+}}}F(x)$=$F(b)-F(a^{+})$`(4)`
- 仍然用记号$[F(x)]_{a}^{b}$来表示$F(b)-F(a^{+})$,从而形式上仍然有$\int_{a}^{b}f(x)\mathrm{d}x$=$[F(x)]_{a^{+}}^{b}$`(4-1)`
- 其余情形有类似的公式:
  - $\int_{a}^{b}f(x)\mathrm{d}x$=$[F(x)]_{a}^{b^{-}}$;`(4-2)`
  - $\int_{a}^{b}f(x)\mathrm{d}x$=$[F(x)]_{a^+}^{b^{-}}$`(4-3)`

### 例

- $\int_{0}^{a}\frac{1}{\sqrt{a^2-x^2}}\mathrm{d}x$,$(a>0)$
  - 判断瑕点(根据积分限或定义域,试探($0^{+},a^{-}$)即可):由于$\frac{1}{\sqrt{a^2-x^2}}\to{+\infin}(x\to{a^{-}})$,$\frac{1}{\sqrt{a^2-x^2}}\to{(\frac{1}{a})}$;所以$a$是瑕点
  - $\int_{0}^{a}\frac{1}{\sqrt{a^2-x^2}}\mathrm{d}x$=$[\arcsin{\frac{x}{a}}]_{0}^{a}$=$\lim\limits_{x\to{a^{-1}}}{\arcsin{\frac{x}{a}}}$=$\frac{\pi}{2}-0$=$\frac{\pi}{2}$
  - 该积分的几何意义是,位于曲线$y=\frac{1}{\sqrt{a^2-x^2}}$下,以及直线$x=0$与$x=a$之间的图形区域的面积虽然不封闭,但是面积不会无限增大,而有极限值$\frac{\pi}{2}$(面积无限接近但不超过也不等于$\frac{\pi}{2}$)

- $\int_{0}^{+\infin}\frac{1}{\sqrt{x{(x+1)^3}}}\mathrm{d}x$

  - $\frac{1}{\sqrt{x{(x+1)^3}}}$=$\frac{1}{(x+1)\sqrt{x(x+1)}}$=$\frac{1}{(x+1)\sqrt{(x+\frac{1}{2})^2-(\frac{1}{2})^2}}$

  - 利用$\sec^2{t}-1=\tan^2{t}$,即$(\frac{1}{2})^{2}\sec^2{t}-(\frac{1}{2})^{2}=(\frac{1}{2})^{2}\tan^2{t}$来消去根号

  - 令$x+\frac{1}{2}=\frac{1}{2}\sec{t}$,则:
  
    - $x=\frac{1}{2}\sec{t}-\frac{1}{2}$
    - $\mathrm{d}x$=$\frac{1}{2}\sec{t}\tan{t}\mathrm{d}t$;
    - 当$x=0$时,$t=0$;当$x\to{+\infin}$,$t\to{\frac{\pi}{2}}$,则$\tan{t}>0$
  
  - $\int_{a}^{+\infin}\frac{1}{\sqrt{x{(x+1)^3}}}\mathrm{d}x$
  
    - =$\int_{0}^{\frac{\pi}{2}}\frac{1}{(\frac{1}{2}\sec{t}+\frac{1}{2})\frac{1}{2}|\tan{t}|}{\frac{1}{2}\sec{t}\tan{t}\mathrm{d}t}$
  
    - =$2\int_{0}^{\frac{\pi}{2}}\frac{1}{1+\cos{t}}\mathrm{d}t$
  
    - =$2\int_{0}^{\frac{\pi}{2}}\frac{1}{2\cos^2{\frac{t}{2}}}\mathrm{d}t$
  
    - =$2\int_{0}^{\frac{\pi}{2}}\frac{1}{\cos^2{\frac{t}{2}}}\mathrm{d}(\frac{1}{2}t)$
  
    - =$2\tan{\frac{1}{2}t}|_{0}^{\frac{\pi}{2}}$=$2$
  
      

### 隐含瑕积分

- 瑕积分问题不总是直接体现在积分区间的端点上,还可能在积分区间内部出现瑕点
- 即:某些积分看似和定积分形式相当,但是由于积分区间内函数**不连续**,特别是出现了**瑕点**(无界间断点),需要分段积分,例如$\frac{1}{x^2}$在$[-1,1]$内的积分,需要分成两个瑕积分分别计算
- 若某个区间上的积分每拆成多段积分,并且已经算得其中任何一个区间上的积分是发散的,则原来的整个积分区间内的积分是发散的

- $\int_{-1}^{1}\frac{1}{x^2}\mathrm{d}x$

  - 分析$f(x)=\frac{1}{x^2}$的定义域可知,$f(x)$在处点$x=0$处以外连续,结合积分区间$[-1,1]$,可以将实际积分区间分为两部分:$[-1,0)$,$(0,1]$

  - $\lim\limits_{x\to{0}}f(x)=\infin$,因此该积分为瑕积分:
    - $F(x)=-\frac{1}{x}$是$f(x)$的一个原函数
    - 在区间$[-1,0)$上,$\int_{-1}^{0}f(x)\mathrm{d}x$=$[-\frac{1}{x}]_{-1}^{0^{-}}$=$\lim\limits_{x\to{0^{-}}}-\frac{1}{x}-1$=$+\infin$,该积分发散,

  - 因此$\int_{-1}^{1}\frac{1}{x^2}\mathrm{d}x$发散















