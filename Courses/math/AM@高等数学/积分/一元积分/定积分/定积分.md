[toc]

## abstract

- 定积分的基本概念和性质

## 引言

### 曲边梯形面积

- 设$y=f(x)$在区间$[a,b]$上**非负连续**,由直线$x=a$,$x=b$,$y=0$以及曲线$y=f(x)$所围成的图形称为**曲边梯形**,其中**曲线弧**称为**曲边**
- 曲边梯形的面积$S_1$可以分割为一系列小区间的**窄曲边梯形**面积的和,窄曲边梯形的面积近似为**窄矩形**面积,因此可以用一系列的窄矩形面积之和$S_2$近似曲边梯形面积
- 若把$[a,b]$无限细分下去(得到无穷多个小区间),即,使得每个小区间的长度$\Delta{x}_{i}$,$i=1,2,\cdots,n$,$(n\to{\infin})$都趋于0(或者令所有小区间中最大的一个趋于0,则其余区间必然也趋于0),并把这时候所有窄矩形面积之和$S_2$的极限可以定义为曲边梯形的面积$\lim\limits_{\Delta{x}_{i}\to{0}}S_2=S_1$
  - 设小区间$\Delta{x}_i=x_i-x_{i-1}$;任取$\xi_{i}\in[x_{i-1},x_i]$,再令$\lambda=\max{(\Delta{x_1,\cdots,\Delta{x}_n})}$
  - 则这个极限值与曲边梯形面积关系表示为$\lim\limits_{\lambda\to{0}}\sum_{i=1}^{n}f{(\xi_{i})}\Delta{x_i}$
- 利用这个定义,可以计算曲边梯形的面积

### 变速直线运动的路程

- 该问题模型也是由类似与曲边梯形面积的数量关系:$\lim\limits_{\lambda\to{0}}\sum_{i=1}^{n}v{(\tau_{i})}\Delta{t_i}$

## 定积分

- 从上述两个例子中抽象出数量关系,便得到定积分的定义

### 定义

- 设函数$f(x)$在$[a,b]$上**有界**,在$[a,b]$中任意插入若干个分点$a=x_0<x_1<\cdots<x_{n-1}<x_n=b$,把$[a,b]$划分为$n$个小区间$[x_0,x_1],\cdots,[x_{n-1},x_n]$,各个小区间的长度依次为$\Delta{x_i}$=$x_{i-1}-x_{i-1}$,$i=1,2,\cdots,n$
- 每个小区间上任意取一个点$\xi_i\in[x_{i-1},x_i]$,作函数值$f(\xi_i)$与小区间长度$\Delta{x_i}$的乘积$f(\xi_i)\Delta{x_i}$,$(i=1,2,\cdots,n)$,求和$S=\sum_{i=1}^{n}f(\xi_i)\Delta{x_i}$
- 令$\lambda=\max{(\Delta{x_1,\cdots,\Delta{x}_n})}$,(或表示为$\lambda$=$\max\limits_{1\leqslant{i}\leqslant{n}}(\Delta{x}_{i})$),若$\lambda\to{0}$时,$\lim\limits_{\lambda\to{0}}\sum_{i=1}^{n}f{(\xi_{i})}\Delta{x_i}$=$I$总是存在,且与闭区间$[a,b]$的分法和$\xi_i$的取法**无关**,则该极限$I$为函数$f(x)$在**区间$[a,b]$上的定积分**,简称**积分**,记为
  - $\int_{a}^{b}f(x)\mathrm{d}x$=$\lim\limits_{\lambda\to{0}}\sum_{i=1}^{n}f{(\xi_{i})}\Delta{x_i}$

### 定积分思想四步骤小结

- 分割
- 近似
- 求和
- 取极限

### 定积分本质

- 定积分是积分和式的**极限**,因此某些求极限问题也可以用定积分来表示和计算


### 定积分式相关概念

- 其中$f(x),f(x)\mathrm{d}x,x$分别称为**被积函数**,**被积表达式**,**积分变量**,
- $a,b$分别称为**积分下限**和**积分上限**,$[a,b]$称为**积分区间**

- $\sum_{i=1}^{n}f{(\xi_{i})}\Delta{x_i}$称为**积分和**

- 若$f(x)$在$[a,b]$上的定积分存在,则称,$f(x)$在$[a,b]$上**可积**


- 由于定积分是基于极限定义的,而极限可以由$\epsilon-\delta$语言描述的,参考极限的$\epsilon-\delta$语言描述,可以给出定积分的$\epsilon-\delta$描述

### 可积的充分条件(定理)@定积分存在定理

- 设$f(x)$在区间$[a,b]$上**连续**,则$f(x)$在$[a,b]$上可积
- 设$f(x)$在区间$[a,b]$上**有界**,并且只有有限个**间断点**,则$f(x)$在$[a,b]$上可积
  - 显然,定理1是定理2的特例情况

### 定积分相等于字母替换

- 当上述极限$I$存在时,其仅与**被积函数**$f$和**积分区间**$[a,b]$有关
  - 若不改变积分函数也不改变积分区间,仅仅替换积分变量字母(比如$x$替换为$u$),定积分结果相等
  - 即,定积分的值与**积分变量**的记法无关
  - 例如:$\int_{a}^{b}f(x)\mathrm{d}x$=$\int_{a}^{b}f(u)\mathrm{d}u$

## 定积分计算补充约定

- 为了计算和应用的方便,对定积分作两点补充规定:
  1. 当$b=a$时,$\int_{a}^{b}f(x)\mathrm{d}x=0$
  2. 当$a>b$时,$\int_{a}^{b}f(x)\mathrm{d}x$=$-\int_{b}^{a}f(x)\mathrm{d}x$
     - 即交换定积分的上下限时,定积分的绝对值不变而符号相反
     - 从而,积分下限可以大于积分上限

## 定积分性质👺

- 定积分恒等式和定积分不等式

### 线性运算法则

- 设$\alpha,\beta$均为常数,则$f_{a}^{b}(\alpha{f(x)}+\beta{g(x)})\mathrm{d}x$=$\alpha\int_{a}^{b}f(x)\mathrm{d}x$+$\beta\int_{a}^{b}g(x)\mathrm{d}x$
  - 由极限运算法则容易证明
  - 对于任意有限个函数的线性组合也成立

### 可加性

- $\int_{a}^{b}f(x)\mathrm{d}x$=$\int_{a}^{c}f(x)\mathrm{d}x$+$\int_{c}^{b}f(x)\mathrm{d}x$`(1)`即,定积分对于积分区间有**可加性**
- 证明:设$a<b<c$
  - 因为函数$f(x)$在区间$[a,b]$上积分,所以无论怎样划分$[a,b]$,**积分和**的极限总是不变的
  - 在分区间时,可以使$c$永远是个分点,则$[a,b]$上的积分和等于$[a,c]$上的积分和加上$[c,b]$上的积分和,记为$\sum\limits_{[a,b]}f(\xi_i)\Delta{x_i}$=$\sum\limits_{[a,c]}f(\xi_i)\Delta{x_i}+\sum\limits_{[c,b]}f(\xi_i)\Delta{x_i}$
  - 两端同时取$\lambda\to{0}$的极限,$\int_{a}^{b}f(x)\mathrm{d}x$=$\int_{a}^{c}f(x)\mathrm{d}x$+$\int_{c}^{b}f(x)\mathrm{d}x$
  - 再由补充约定2,不论$a,b,c$相对位置如何,总有(1)式成立
    - 例如,当$a<b<c$,由可加性可知$\int_{a}^{c}f(x)\mathrm{d}x$=$\int_{a}^{b}f(x)\mathrm{d}x$+$\int_{b}^{c}f(x)\mathrm{d}x$,
    - 即$\int_{a}^{b}f(x)\mathrm{d}x$=$\int_{a}^{c}f(x)\mathrm{d}x$-$\int_{b}^{c}f(x)\mathrm{d}x$
    - 所以由补充约定2,$\int_{a}^{b}f(x)\mathrm{d}x$=$\int_{a}^{c}f(x)\mathrm{d}x$+$\int_{c}^{b}f(x)\mathrm{d}x$,式(1)仍然成立

### 常数1的定积分

- 若在区间$[a,b]$上,$f(x)=1$,则$\int_{a}^{b}1\mathrm{d}x$=$\int_{a}^{b}\mathrm{d}x$=$b-a$

### 保号性

- 若区间$[a,b]$上$f(x)\geqslant{0}$,则$\int_{a}^{b}f(x)\mathrm{d}x\geqslant{0}$,$(a<b)$
- 证明:因为$f(x)\geqslant{0}$,所以$f(\xi_{i})\geqslant{0}$,$(i=1,2,\cdots,n)$
  - 又由于$\Delta{x_i}\geqslant{0}$,$(i=1,2,\cdots,n)$,因此积分和$\sum_{i=1}^{n}f{(\xi_{i})}\Delta{x_i}\geqslant{0}$
  - 令$\lambda=\max\set{\Delta{x_1},\cdots,\Delta{x_n}}\to{0}$,即得$\int_{a}^{b}f(x)\mathrm{d}x\geqslant{0}$

- 推论:若区间$[a,b]$上,$f(x)\leqslant{g(x)}$,则$\int_{a}^{b}f(x)\mathrm{d}x\leqslant{\int_{a}^{b}g(x)\mathrm{d}x}$,$(a<b)$
  - 令$h(x)=g(x)-f(x)$,由条件$h(x)\geqslant{0}$,从而$\int_{a}^{b}h(x)\mathrm{d}x\geqslant{0}$,即$\int_{a}^{b}[g(x)-f(x)]\mathrm{d}x\geqslant{0}$,由定积分线性运算法则,$\int_{a}^{b}f(x)\mathrm{d}x\leqslant{\int_{a}^{b}g(x)\mathrm{d}x}$

- 推论:$|\int_{a}^{b}f(x)\mathrm{d}x|\leqslant{\int_{a}^{b}|f(x)|\mathrm{d}x}$ 👺
  - 因为$-|f(x)|\leqslant f(x)\leqslant{|f(x)|}$,所以$-\int_{a}^{b}|f(x)|\mathrm{d}x$ $\leqslant$ $\int_{a}^{b}f(x)\mathrm{d}x$ $\leqslant$ ${\int_{a}^{b}|f(x)|\mathrm{d}x}$`(1)`
  - 由**绝对值不等式**可(1)可以改写为$|\int_{a}^{b}f(x)\mathrm{d}x|\leqslant{\int_{a}^{b}|f(x)|\mathrm{d}x}$
  - Note:$|a|\leqslant{b}$ $\Leftrightarrow$ $-b\leqslant{a}\leqslant{b}$

### 定积分最值(定积分估计定理)

- 设$M,m$分别是函数$f(x)$在区间$[a,b]$上得最大值和最小值,则$m(b-a)\leqslant{\int_{a}^{b}f(x)\mathrm{d}x}\leqslant{M(b-a)}$,$(a<b)$
  - 由保号性和常数1的定积分易证
  - $\int_{a}^{b}m\mathrm{d}x$ $\leqslant$ $\int_{a}^{b}f(x)\mathrm{d}x$ $\leqslant$ $\int_{a}^{b}M\mathrm{d}x$
  - $m\int_{a}^{b}1\mathrm{d}x$ $\leqslant$ $\int_{a}^{b}f(x)\mathrm{d}x$ $\leqslant$ $M\int_{a}^{b}\mathrm{d}x$
  - $m(b-a)\leqslant{\int_{a}^{b}f(x)\mathrm{d}x}\leqslant{M(b-a)}$

### 定积分中值定理(函数在区间上的平均值)

- 若$f(x)$在区间$[a,b]$上连续,则$[a,b]$上至少有一点$\xi$,使得$\int_{a}^{b}f(x)\mathrm{d}x$=$f(\xi)(b-a)$,$(a\leqslant{\xi}\leqslant{b})$(该公式称为**积分中值公式**)

- 证明:

  - 由定积分估计定理,$m(b-a)\leqslant{\int_{a}^{b}f(x)\mathrm{d}x}\leqslant{M(b-a)}$,两边同时除以$b-a$,
  - 得$m$ $\leqslant$ $\frac{1}{b-a}\int_{a}^{b}f(x)\mathrm{d}x$ $\leqslant$ $M$

  - 因此,令$t=\frac{1}{b-a}\int_{a}^{b}f(x)\mathrm{d}x$,则$t\in[m,M]$,$t$是一个确定的数值;
  - 再根据闭区间上连续函数的介值定理的推论,$[a,b]$上至少有点$\xi$,使得$f(x)$在点$\xi$处的值与这个确定的数值相等,即$t=f(\xi)$,$(a\leqslant{\xi}\leqslant{b})$

  - 两端乘以$b-a$,得$t=f(\xi)(b-a)$,即得定理结论

- 由积分中值公式,$f(\xi)=\frac{1}{b-a}\int_{a}^{b}f(x)\mathrm{d}x$称为函数$f(x)$在$[a,b]$上的平均值(曲边梯形的平均高度)



## 常用定积分公式

- 关于圆的定积分公式

  - $\int_{0}^{a}\sqrt{a^2-x^2}\mathrm{d}x$=$\frac{1}{4}\pi{a^2}$

  - $\int_{-a}^{a}\sqrt{a^2-x^2}\mathrm{d}x$=$\frac{1}{2}\pi{a^2}$
  - $\int_{0}^{\pi}\sin{x}\mathrm{d}x$=$2$
  - $\int_{0}^{r}\sqrt{q+px-x^2}\mathrm{d}x$也可以化为圆的一部分的面积来计算
    - $y=\sqrt{q+px-x^2}$,则$y^2$=$q+px-x^2$;所以$x^2-px+y^2$=$q$,配方可得$(x-\frac{p}{2})^2+y^2$=$\frac{p^2}{4}+q$,这是圆心在$(\frac{p}{2},0)$,半径为$r=\sqrt{\frac{p^2}{4}+q}$的圆

- 设$f(x)$在$[-a,a]$,$(a>0)$上式一个连续的

  - 偶函数,则$\int_{-a}^{a}f(x)\mathrm{d}x$=$2\int_{0}^{a}f(x)\mathrm{d}x$
  - 奇函数,则$\int_{-a}^{a}f(x)\mathrm{d}x$=$0$

- 设$f(x)$在$(-\infin,+\infin)$内是满足$f(x+T)$=$f(x)$的连续函数(以$T$为周期),则对于任一常数$a$,恒有

  - $\int_a^{a+T}f(x)\mathrm{d}x$=$\int_{0}^{T}f(x)\mathrm{d}x$
  - $\int_{a}^{a+nT}\int{f(x)}\mathrm{d}x$=$n\int_{0}^{T}f(x)\mathrm{d}x$,$n\in{\mathbb{Z}}$

- 若$f(x)$在[0,1]上连续,则$\int_{0}^{\frac{\pi}{2}}f(\sin{x})\mathrm{d}{x}$=$\int_{0}^{\frac{\pi}{2}}f(\cos{x})\mathrm{d}{x}$
- 华理士公式
- $\int_{0}^{\pi}xf(\sin{x})\mathrm{d}x$=$\frac{\pi}{2}\int_{0}^{\pi}f(\sin{x})\mathrm{d}x$,其中$f(x)$连续
  - 令$I$=$\int_{0}^{\pi}xf(\sin{x})\mathrm{d}x$
  - 令$x=\pi-t$,则$t=\pi-x$,$\mathrm{d}x$=$-\mathrm{d}t$
  - $x:0\to{\pi}$,$t:\pi\to{0}$
  - $I$=$-\int_{\pi}^{0}(\pi-t)f(\sin(\pi-t))\mathrm{d}t$=$\int_{0}^{\pi}(\pi-t)f(\sin{t})\mathrm{d}t$
    - =$\int_{0}^{\pi}\pi{f(\sin{t})}\mathrm{d}t$-$\int_{0}^{\pi}tf(\sin{t})\mathrm{d}t$
    - =$\pi\int_{0}^{\pi}{f(\sin{t})}\mathrm{d}t-I$
  - 从而$2I$=$\pi\int_{0}^{\pi}{f(\sin{t})}\mathrm{d}t$
  - 从而$I$=$\frac{\pi}{2}\int_{0}^{\pi}f(\sin{x})\mathrm{d}x$





