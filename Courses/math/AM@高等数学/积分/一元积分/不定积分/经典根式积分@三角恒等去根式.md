[toc]

## abstract

- 第二类换元法积分的应用@经典根式积分@三角恒等去根式

### 适合使用三角换元的情形

- 含有$\sqrt{a^2-x^2}$,$\sqrt{x^2-a^2}$,$\sqrt{a^2+x^2}$函数的积分;即$s$=$\sqrt{a^2\pm{x^2}}$或$\sqrt{x^2\pm{a^2}}$
  - 上述$a^2$是常数,不过平方的形式指出了它是非负(并且应该是正的,否则$\sqrt{x^2}$=$|x|$用不着三角换元消去根号)
  - 更一般的它们的共同特点是简单的二次项$kx^2$和一个常数项$A=a^2$的和开根号的形式
  - 总结为$\sqrt{kx^2+A}$,其中$k\neq{0}$;$A\neq{0}$,都可以考虑使用三角换元


#### 拓展

- 更一般的,$ax^2+bx+c$通过配方,转换为$a(x+\frac{b}{2a})^2+\frac{4ac-b^2}{4a}$=$a[(x+\frac{b}{2a})^2+\frac{4ac-b^2}{4a^2}]$,常数项可以移到积分号外,再用$t=x+\frac{b}{2a}$代换,$\mathrm{d}x$=$\mathrm{d}t$;问题转换为上一类情形

### 基本步骤

1. 设被积函数为$\int R(x,f(x))\mathrm{d}x$`(1)`,其中$f(x)$是包含形如$\sqrt{kx^2+A}$`(2)`的根式

2. 根据$f(x)$的类型,将$x$用不同的三角代换$x=x(t)$,

3. 重新计算换元后的积分区间:$x\in[a,b]$重新计算为$t\in[\alpha,\beta]$,其中$a=x(\alpha)$;$b=x(\beta)$

   - | $f(x)$,$(a>0)$   | $D_x$                             | $f_1(x)$                    | $x$        | $D_{t}$                                      | $f(x(t))$                                                    | $\mathrm{d}x$                |
     | ---------------- | --------------------------------- | --------------------------- | ---------- | -------------------------------------------- | ------------------------------------------------------------ | ---------------------------- |
     | $\sqrt{a^2-x^2}$ | $x\in[-a,a]$                      | $a\sqrt{1-(\frac{x}{a})^2}$ | $a\sin{t}$ | $[-\frac{\pi}{2},\frac{\pi}{2}]$             | $a\cos{t}$                                                   | $a\cos{t}\mathrm{d}t$        |
     | $\sqrt{x^2+a^2}$ | $x\in{\bold{R}}$                  | $a\sqrt{(\frac{x}{a})^2+1}$ | $a\tan{t}$ | $(-\frac{\pi}{2},\frac{\pi}{2})$             | $a\sec{t}$                                                   | $a\sec^2{t}\mathrm{d}t$      |
     | $\sqrt{x^2-a^2}$ | $x\in(-\infin,-a)\cup(a,+\infin)$ | $a\sqrt{(\frac{x}{a})^2-1}$ | $a\sec{t}$ | $(0,\frac{\pi}{2})\cup{(\frac{\pi}{2},\pi)}$ | $a\tan{t}$,$(t\in(0,\frac{\pi}{2}))$<br />$-a\tan{t}$,$(t\in(\frac{\pi}{2},\pi))$ | $a\sec{t}\tan{t}\mathrm{d}t$ |

     - 其中$f_1(x)$=$f(x)$,而$f_1(x)$容易看出需要适用的三角代换
     - 或者由
       - $a^2-a^2\sin^2{t}=a^2\cos^{2}{t}$
       - $a^2\tan^{2}t+a^2$=$a^2\sec^{2}t$
       - $a^2\sec^{2}t-a^2=a^2\tan^2{t}$
     - 上述三种代换分别是:正弦,正切,正割来代换$\frac{x}{a}$
     - 第三种情形是用正割$\sec{t}$代换$\frac{x}{a}$,另一半区间通过求变量代换,利用$t\in(0,\frac{\pi}{2})$间接求得$t\in(\frac{\pi}{2},\pi)$的情形
     - 共同点:三种情形的$f(x(t))$在$x\in(0,\frac{\pi}{2})$上开根号时不需要加绝对值
     - 根式中的平方差可以映射到一个直角三角形的适当的两条边上
       - $a^2-x^2$,则令斜边位$a$,一条直角边为$a$,最后一条边由勾股定理求得$f=\sqrt{a^2-x^2}$
       - $x^2-a^2$,则令斜边为$x$,一条直角边为$a$,最后一条边由勾股定理求得$f=\sqrt{x^2-a^2}$
       - $a^2+x^2$,令两个直角边分别为$a,x$,最后一条边由勾股定理求得$f=\sqrt{a^2+x^2}$

4. 将$x=x(t)$代入式(1),得到被消去根式的三角积分式:$\int R(x(t),f(x(t)))\mathrm{d}[x(t)]$`(3)`

   - 求出$\mathrm{d}x(t)$=$x'(t)\mathrm{d}t$,代替$\mathrm{d}x$,这就将对$x$的积分问题转换为对$t$的积分问题
   - 例如$x=x(t)=a\sin{t}$,则用$a\cos{t}\mathrm{d}t$代替(1)中的$\mathrm{d}x$
   - 求出(3),结果表示为$F(t)+C$`(4)`

5. 这时关于$t$的函数,将$t=x^{-1}(x)$`(5)`回代到$(4)$,得到结果式$F(x^{-1}(x))+C$`(6)`

   - 这里$t=x^{-1}(x)$,表示$x=x(t)$的反函数
   - 有时不需要求出反函数,若能直接代入合适的式子能够转换为(6),则不需要求出(5)

#### 灵活选用方法

- 上述适用情形仅给出一个大概的指南,这并不是说三角换元总是最简便的
- 例如$S$=$\int\frac{1}{\sqrt{a^2-x^2}}\mathrm{d}x$
  - 方法1:
    - $S$=$\frac{1}{a}\int\frac{1}{\sqrt{1-(\frac{x}{a})^2}}\mathrm{d}x$=$\int\frac{1}{\sqrt{1-(\frac{x}{a})^2}}\mathrm{d}\frac{x}{a}$=$\arcsin{\frac{x}{a}}+C$,
    - 因为这个例子在基本积分公式表中有形近的公式,因此优先考虑第一类换元法积分
  - 方法2:
    - 当然也可以三角换元来消去根式:令$\sin{t}=\frac{x}{a}$`(1)`,则$x$=$a\sin{t}$,$(x\in(-\frac{\pi}{2},\frac{\pi}{2}))$;$\mathrm{d}x$=$a\cos{t}\mathrm{d}t$
    - 从而$S$=$\frac{1}{a}\int{\frac{1}{\cos{t}}}a\cos{t}\mathrm{d}t$=$t+C$`(2)`;而由(1)可以解出$t$=$\arcsin{\frac{x}{a}}$,代入(2),得$S$=$\arcsin\frac{x}{a}+C$
    - 这种方法更通用,即便不知道$\int{\frac{1}{\sqrt{1-x^2}}}\mathrm{d}x$=$\arcsin{x}+C$,也能做出来

## 三角恒等式化去根式

### 例



- $\int{\sqrt{a^2-x^2}}\mathrm{d}x$,$(a>0)$
  - 这个被积函数难以直接凑微分,困难来自于根式
    - 可以通过换元法,利用三角公式$\sin^2{t}+\cos^{2}t=1$`(0-1)`化去根式(或者说$a^2(\sin^2{t}+\cos^{2}t)=a^2$,
      $a^2\sin^2{t}+a^2\cos^{2}t=a^2$`(0-2)`
    - 即$a^2-a^2\sin^2{t}=a^2\cos^{2}t$`(0-3)`,或者$a^2-a^2\cos^2{t}=a^2\sin^2{t}$`(0-4)`,
    - 以前者为例,等号两边同取算术平方根:$\sqrt{a^2-a^2\sin^2{t}}$=$|a\cos{t}|$
  - 被积函数定义域为$x\in(-a,a)$
  - 令$x=a\sin{t}$`(0)`,则$\sin{t}\in(-1,1)$,取$t$的范围为$-\frac{\pi}{2}<t<\frac{\pi}{2}$`(1)`,则:
    - $t=\arcsin{\frac{x}{a}}$`(2)`,$\sqrt{a^2-x^2}$=$\sqrt{a^2-a^2\sin^2t^2}$=$|a\cos{t}|$=$a\cos{t}$
  - $\mathrm{d}x=a\cos{t}\mathrm{d}t$,于是${\sqrt{a^2-x^2}}\mathrm{d}x$=$a\cos{t}\cdot a\cos{t}\mathrm{d}t$=$a^2\cos^2{t}\mathrm{d}t$
  - 所以$\int{\sqrt{a^2-x^2}}\mathrm{d}x$=$\int{a^2\cos^2{t}}\mathrm{d}t$=$a^2\int\cos^2{t}\mathrm{d}t$=$a^2(\frac{t}{2}+\frac{\sin{2t}}{4})+C$ [^1-1]`(3)`
    - =$\frac{1}{2}a^2{t}+\frac{a^2}{2}\sin{t}\cos{t}+C$`(4)`
  - 反函数(2)回代:为例将式(4)化为关于$x$的函数,还需要分别计算$\sin{t}$,$\cos{t}$从而
    - $\sin{t}$=$\sin{\arcsin{\frac{x}{a}}}$=$\frac{x}{a}$
    - $\cos{t}=\sqrt{1-\sin^2{t}}$=$\sqrt{1-(\frac{x}{a})^2}$=$\frac{\sqrt{a^2-x^2}}{a}$
    - $\int{\sqrt{a^2-x^2}}\mathrm{d}x$=$\frac{1}{2}a^2{\arcsin\frac{x}{a}}$+$\frac{1}{2}x\sqrt{a^2-x^2}+C$

[^1-1]: $\int{\cos^{2}x}\mathrm{d}x$=$\frac{1}{2}\int{(1+\cos{2x})}\mathrm{d}x$=$\frac{1}{2}(x+\frac{1}{2}\sin{2x})+C$=$\frac{1}{2}x+\frac{1}{4}\sin{2x}+C$

### 例

- $\int{\frac{1}{\sqrt{x^2+a^2}}}\mathrm{d}x$,$(a>0)$
  - 利用$1+\tan{^2}t=\sec^2{t}$(即$a^2+a^2\tan^{2}t=a^2\sec^2{t}$化去根式
  - 令$x=a\tan{t}$`(1)`,$-\frac{\pi}{2}<t<\frac{\pi}{2}$[^2-0],$\sec{t}=\frac{1}{\cos{t}}>0$,从而$a\sec{t}>0$,即$\sqrt{x^2+a^2}$=$\sqrt{a^2+a^2\tan^{2}t}$=$|a\sec{t}|$=$a\sec{t}$
  - $\tan{t}=\frac{x}{a}$`(2)`,$\mathrm{d}x$=$a\sec^2{t}\mathrm{d}t$`(3)`
  - $\int{\frac{1}{\sqrt{x^2+a^2}}}\mathrm{d}x$=$\int{\frac{1}{a\sec{t}}}\cdot{a\sec^2{t}\mathrm{d}t}$=$\int{\sec{t}}\mathrm{d}t$=$\ln|\sec{t}+\tan{t}|+C$=$\ln(\sec{t}+\tan{t})+C$[^2-1]`(4)`
  - $\sec{t}$=$\sqrt{1+\tan^2{t}}$,由(2)式,得$\sec{t}=\sqrt{1+\frac{x^2}{a^2}}$=$\frac{\sqrt{x^2+a^2}}{a}$`(5)`(或者构造辅助直角三角形也可得式(5)
  - $\int{\frac{1}{\sqrt{x^2+a^2}}}\mathrm{d}x$=$\ln(\frac{x}{a}+\frac{\sqrt{x^2+a^2}}{a})+C$
    - =$\ln(x+\sqrt{x^2+a^2})+C_1$`(6)`,其中$C_1=C-\ln{a}$

[^2-0]: 因为$x\in{(-\infin,+\infin)}$,所以$a\tan{t}\in(-\infin,+\infin)$,$\tan{t}\in(-\infin,+\infin)$,可以取$t\in{(-\frac{\pi}{2},\frac{\pi}{2})}$
[^2-1]: $\sec{t}+\tan{t}$=$\frac{1+\sin{t}}{\cos{t}}$,因为$t\in(-\frac{\pi}{2},\frac{\pi}{2})$,从而$\cos{t}>0$,且$1+\sin{t}>0$,所以$\sec{t}+\tan{t}>0$

### 例

- $\int\frac{1}{\sqrt{x^2-a^2}}\mathrm{d}x$,$(a>0)$
  - 利用公式$\sec^{2}t-1=\tan^{2}t$(或$a^2\sec^{2}t-a^2=a^2\tan^{2}t$化去根式
  - 函数的定义域为$x<-a$和$x>a$两个区间,求积分要在两个区间内分别积分
    - 在各自区间内积分时,由$x$所在区间可以确定某些式子的符号
  - 当$x>a$时
    - 令$x=a\sec{t}$`(5)`,$(0<t<\frac{\pi}{2})$[^3-1],则$\sqrt{x^2-a^2}$=$\sqrt{a^2\sec-a^2}=a\sqrt{\sec^2{t}-1}$=$a\tan{t}$
    - 因为$x=a\sec{t}$,则$\mathrm{d}x$=$\mathrm{d}(a\sec{t})$=$a\sec{t}\tan{t}\mathrm{d}t$
    - $\int\frac{1}{\sqrt{x^2-a^2}}\mathrm{d}x$=$\int{\frac{1}{a\tan{t}}}\cdot{a\sec{t}\tan{t}\mathrm{d}t}$=$\int{\sec{t}\mathrm{d}t}$=$\ln|\sec{t}+\tan{t}|+C$=$\ln(\sec{t}+\tan{t})+C$`(6)`
  - 为了将式化为关于$x$的式子,需要求出以下之一
    - $x=a\sec{t}$的反函数$t=t(x)$;
    - 或直接求$\sec{t}=t_1(x)$和$\tan{t}=t_2(x)$
      - 由(5),得$\sec{t}=\frac{x}{a}$`(7)`
      - 求$\tan{t}=\frac{\sqrt{x^2-a^2}}{a}$`(8)`有多种办法:
        - 由$\tan^2{t}=\sec^2{t}-1$=$\frac{x^2-a^2}{a^2}$,以及$\tan{t}>0$可知,$\tan{t}=\frac{\sqrt{x^2-a^2}}{a}$``
        - 根据式(7)作辅助直角三角形:斜边为$x$,邻边为$a$,两边角为$t$,再由勾股定理,对边为$\sqrt{x^2-a^2}$,显然,该直角三角形可以读出$\tan{t}=t_2(x)$=$\frac{\sqrt{x^2-a^2}}{a}$
  - 回代(7),(8)到(6)中得$\int\frac{1}{\sqrt{x^2-a^2}}\mathrm{d}x$=$\ln(\frac{x}{a}+\frac{\sqrt{x^2-a^2}}{a})+C$=$\ln(x+\sqrt{x^2-a^2})-\ln{a}+C$=$\ln(x+\sqrt{x^2-a^2})+C_1$`(9)`,其中$C_1=C-\ln{a}$

[^3-1]: $x=a\sec{t}>a$,即$\sec{t}>1$,$\frac{1}{\cos{t}}>1$,即$0<\cos{t}<1$,$t\in(-\frac{\pi}{2}+2k\pi,\frac{\pi}{2}+2k\pi)$,$(k\in\mathbb{Z})$;为简单起见,去$t\in(0,\frac{\pi}{2})$即可满足需要(使得$x$的取值范围等于$x>a$)

- 再讨论另一个区间:$x<-a$
  - 此时$-x>a$,令$x=-u$,则$u>a$,$\mathrm{d}{x}$=$-\mathrm{d}u$代入到式(9),即可得
  - $\int\frac{1}{\sqrt{x^2-a^2}}\mathrm{d}x$=$\int\frac{1}{\sqrt{u^2-a^2}}(-\mathrm{d}u)$=$-\int\frac{1}{\sqrt{u^2-a^2}}\mathrm{d}u$=$-\ln(u+\sqrt{u^2-a^2})+C$=$-\ln(-x+\sqrt{x^2-a^2})+C$=$\ln(\frac{1}{-x+\sqrt{x^2-a^2}})+C$=$\ln{\frac{-x-\sqrt{x^2-a^2}}{a^2}+C}$=$\ln{({-x-\sqrt{x^2-a^2}})-2\ln{a}+C}$=$\ln{({-x-\sqrt{x^2-a^2}})+C_2}$`(10)`,其中$C_2=C-2\ln{a}$
  - 比较(9),(10)可以发现两者不同,可见,即使是同一个函数,定义域不同区间内的不定积分结果可能式不同的
  - 但因为两个区间的被积表达式都一样,因此存在联系,可以通过换元法来推导得到另一个区间内的积分
  - 式(9),(10)可以借助绝对值号合并为一个式子,$\int\frac{1}{\sqrt{x^2-a^2}}\mathrm{d}x$=$\ln|x+\sqrt{x^2-a^2}|+C$
    - 合并的关键在于$x+\sqrt{x^2-a^2}$,
      - 当$x>a$时$x+\sqrt{x^2-a^2}$=$|x+\sqrt{x^2-a^2}|$
      - 当$x<-a$时,$-x-\sqrt{x^2-a^2}$=$|x+\sqrt{x^2-a^2}|$
      - 反之亦然

## 小结与推广

- 若被积函数含有$\sqrt{a^2-x^2}$,可以作代换$x=a\sin{t}$化去根式
- 若含有$\sqrt{x^2+a^2}$,可以作代换$x=a\sin{t}$
- 若含有$\sqrt{x^2-a^2}$可以作代换$x=\pm{a\sec{t}}$
- 当被积函数含有$\sqrt{x^2\pm{a^2}}$还可以用$\operatorname{ch}^2t-\operatorname{sh}^2{t}=1$化去根式($x=a\operatorname{sh}t$,$x=\pm{a}\operatorname{ch}x$)



## 其他情形化去根式的情形和方法

### 一次项的根式

- $\int{R(x,\sqrt[n]{ax+b}),\sqrt[m]{ax+b}}\mathrm{d}x$,$(a\neq{0})$ 型的积分
  - 其特点式根号下是一个一次多项式
  - 通常令$\sqrt[mn]{ax+b}$=$t$,$x=\frac{t^{mn}-b}{a}$,$\mathrm{d}x$=$\frac{mn}{a}t^{mn-1}\mathrm{d}t$
  - 这就化成了一个常数系数的幂函数积分

### 一次项分式的根式

- $\int{R}(x,\sqrt{\frac{ax+b}{cx+d}})\mathrm{d}x$型
  - 令$t$=$\sqrt{\frac{ax+b}{cx+d}}$,则$x$=$\frac{dt^2-b}{a-ct^2}$;$\mathrm{d}x$=$\frac{2(ad-bc)t}{(a-ct^2)^2}\mathrm{d}t$,$(ad-bc\neq{0})$

### 三角万能代换

- $\int{R(\sin{x},\cos{x})}\mathrm{d}x$型

  - 令$t$=$\tan{\frac{x}{2}}$`(1)`,则
    - $\sin{x}$=$2\sin\frac{x}{2}\cos\frac{x}{2}$=$\frac{2\sin\frac{x}{2}\cos\frac{x}{2}}{\sin^2\frac{x}{2}+\cos^2\frac{x}{2}}$=$\frac{2\tan{\frac{x}{2}}}{\tan^2{\frac{x}{2}}+1}$=$\frac{2t}{t^2+1}$`(2-1)`
    - $\cos{x}$=$\cos^2\frac{x}{2}-\sin^{2}\frac{x}{2}$=$\frac{\cos^2\frac{x}{2}-\sin^{2}\frac{x}{2}}{\sin^2\frac{x}{2}+\cos^2\frac{x}{2}}$=$\frac{1-\tan^2{\frac{x}{2}}}{\tan^2{\frac{x}{2}}+1}$=$\frac{1-t^2}{t^2+1}$=$\frac{1-t^2}{1+t^2}$`(2-2)`

  - 对(1)两边同时取$\arctan$运算,$\arctan{t}$=$\frac{x}{2}$,变形得$x=2\arctan{t}$`(3)`,从而$\mathrm{d}x$=$2\frac{1}{1+t^2}\mathrm{d}t$`(4)`

- 上述代换能将三角式代换位有理式,通常会产生复杂的计算.很少使用





