[toc]

## abstract

- 在积分学中,**积分范围**先是从数轴上(**直线**)的一个区间的情形,推广到平面或看空间内的一个**闭区域**的情形
- 不仅如此,积分概念可以推广到积分范围为**一段曲线弧**或**一片曲面**的情形,分别称为曲线积分和曲面积分

## 对弧长的曲线积分

### 曲线形构件的质量

- 对弧长的曲线积分源自某些问题的研究,其中最经典的一个问题模型是曲线形构建的质量问题
  - 在讨论定积分和二重积分时,分别对应曲边梯形面积问题和曲顶柱体的体积问题
  - 而理解曲线积分时,用类似理解定积分时的几何的角度就不容易,也不太合适,而从物理意义角度就比较合适
- 通过对这个问题问题模型的抽象,定义出弧长的曲线积分(第一类曲线积分)

### 第一类曲线积分

- 定义:设$L$为$xOy$面内的一条**光滑曲线弧**,函数$f(x,y)$**在$L$上有界**,在$L$上任意插入一系列点$M_1,\cdots,M_{n-1}$,把$L$分成$n$个小段
- 设第$i$小段的**长度为$\Delta{s_i}$**,又$(\xi_i,\eta_i)$为第$i$个小段上任意取定的,作乘积$f(\xi_i,\eta_i)\Delta{s_i}$`(0)`,$(i=1,2,\cdots,n)$,并作和式$\sum_{i=1}^{n}f(\xi_i,\eta_i)\Delta{s_i}$`(1)`
- 若当各个小弧段的长度的最大值$\lambda\to{0}$时,式(1)的极限总存在,切曲线弧$L$的分法以及点$(\xi_i,\eta_i)$的取法无关,则称此极限为函数$f(x,y)$在曲线弧$L$上对弧长的曲线积分或第一类曲线积分,记为$\int_{L}f(x,y)\mathrm{d}s$,即$\int_{L}f(x,y)\mathrm{d}s$=$\lim\limits_{\lambda\to{0}} \sum_{i=1}^{n}f(\xi_i,\eta_i)\Delta{s_i}$`(3)`
- 其中$f(x,y)$称为**被积函数**,$L$称为**积分弧段**(积分弧段类似于定积分的积分区间或重积分的积分区域)
  - 而**弧段元素**$\Delta{s_i}$或$\mathrm{d}s$类似于定积分$\int_{a}^{b}f(x)\mathrm{d}x$中的区间元素$\mathrm{d}x$
- Note:$f(x,y)$和$L$的方程是相对独立的,最简单的情形是
  - $f(x,y)$为常数
  - $L$为直线方程

### 曲线积分存在性

- 当$f(x,y)$在光滑曲线弧$L$上连续时,对弧长的曲线积分$\int_{L}f(x,y)\mathrm{d}s$总是存在的

  - 在讨论曲线积分时,我们总假定$f(x,y)$在$L$上是连续的

- 函数$f(x,y)$在曲线$L$上连续表示,$(x,y)\in{L}$时,$f(x,y)$处处连续

  

### 利用曲线积分的定义描述曲线形构件质量问题

- 根据上述定义,曲线形构件的质量$m$当**线密度**$\mu(x,y)$在$L$上连续时,就有等于$\mu(x,y)$对弧长的曲线积分,即$m=\int_{L}\mu(x,y)\mathrm{d}s$`(4)`

### 推广

- 上述曲线积分的定义是**平面曲线**积分,可以类似地推广到积分弧段为**空间曲线**$\Gamma$的情形,即函数$f(x,y,z)$在曲线弧$\Gamma$上对弧长的曲线积分$\int_{\Gamma}f(x,y,z)\mathrm{d}s$=$\lim\limits_{\lambda\to{0}}\sum_{i=1}^{n}f(\xi_i,\eta_i,\xi_i)\Delta{s_i}$`(5)`
  - $f(x,y,z)$可以表示空间中坐标为$x,y,z$的点处的密度(点附近小区域密度的代表值)

### 曲线积分可加性

- 若$L$是分段光滑的(有限个点不光滑),则规定函数在$L$上的曲线积分等于函数在光滑的各段上的曲线积分之和(空间曲线$\Gamma$也类似)
- 例如设$L$可分成两段光滑曲线弧$L_1$以及$L_2$,记为$L=L_1+L_2$),就规定$\int_{L_1+L_2}f(x,y)\mathrm{d}s$=$\int_{L_1}f(x,y)\mathrm{d}s$+$\int_{L_2}f(x,y)\mathrm{d}s$`(6)`

### 闭曲线积分

- 若$L$是闭曲线,则函数$f(x,y)$在闭曲线$L$上对弧长的曲线积分记为$\oint{f(x,y)\mathrm{d}s}$

## 曲线积分性质

- 由对弧长的曲线积分的定义可知,其具有如下性质(这些性质和定积分类似)
- 线性性
  - $\int_{L}[\alpha f(x,y)+\beta{g(x,y)}]\mathrm{d}s$=$\alpha\int_{L}f(x,y)\mathrm{d}s$+$\beta\int_{L}g(x,y)\mathrm{d}s$`(7)`
- 可加性
  - 若积分弧段$L$可分为两段光滑曲线弧$L_1,L_2$,则
  - $\int_{L}f(x,y)\mathrm{d}s$=$\int_{L_1}f(x,y)\mathrm{d}s$+$\int_{L_2}f(x,y)\mathrm{d}s$`(8)`
- 设$L$上$f(x,y)\leqslant{g(x,y)}$,则
  - $\int_{L}f(x,y)\mathrm{d}s\leqslant{\int_{L}g(x,y)\mathrm{d}s}$`(9)`
  - $|\int_{L}f(x,y)\mathrm{d}s| \leqslant{\int_{L}|f(x,y)|\mathrm{d}s}$`(10)`
    - 因为$-|f(x,y)|\leqslant{f(x,y)}\leqslant{|f(x,y)|}$
    - $-\int_{L}|f(x,y)|\mathrm{d}s\leqslant\int_{L}f(x,y)\mathrm{d}s\leqslant{\int_{L}|f(x,y)|\mathrm{d}s}$
    - 改写成绝对值不等式即得证不等式

## 曲线积分的计算方法👺

### 直角坐标系下曲线参数方程的曲线积分

- 定理:设$f(x,y)$在曲线弧$L$上有定义且连续**,$L$的参数方程**为$x=\phi(t)$;$y=\psi(t)$,$t\in[\alpha,\beta]$
  - 若$\phi(t),\psi(t)$在$[\alpha,\beta]$上具有一阶连续导数,且$\phi'^2+\psi'^2(t)\neq{0}$,则曲线积分$\int_{L}f(x,y)\mathrm{d}s$存在,
  - 且:$\int_{L}f(x,y)\mathrm{d}s$=$\int_{\alpha}^{\beta}[f(\phi(t),\psi(t))]\sqrt{\phi'^2(t)+\psi'^{2}(t)}\mathrm{d}t$,$(\alpha<\beta)$`(11)`

### 证明(部分推导)

- 利用弧长公式,我们可以将第一类曲线积分化为定积分计算
  - 弧长公式我们在定积分的应用中讨论过,$s$=$\int_{\alpha}^{\beta}\sqrt{\phi'^2(t)+\psi'^2(t)}\mathrm{d}t$`(12)`
  - 令$r(t)=\sqrt{\phi'^{2}(t)+\psi'^{2}(t)}$`(12-1)`

- 假定当参数$t$由$\alpha$变至$\beta$时,曲线弧$L$上的点$M(x,y)$依点$A$运动至$B$,该过程描出曲线弧$L$
- 在$L$上取一列点:$A=M_0,M_1,\cdots,M_{n}=B$
- 设它们对应于一列单调增加的参数值$\alpha=t_0<t_1<\cdots<t_{n}=\beta$(这表明$(\alpha<\beta)$)
- 根据对弧长的曲线积分的定义:$\int_{L}f(x,y)\mathrm{d}s$=$\lim\limits_{\lambda\to{0}} \sum_{i=1}^{n}f(\xi_i,\eta_i)\Delta{s_i}$(即式(3))
  - 设点$(\xi_i,\eta_i)$对应于参数值$\tau_{i}$,即$\xi_{i}=\phi(\tau_i)$,$\eta_{i}=\phi(\tau_i)$`(13)`,这里$\tau_{i}\in[t_{i-1},t_{i}]$,
  - 记$t_{i-1}\to{t_{i}}$在$L$上对应的弧长为$\Delta{s}_{i}$,使用弧长公式(12),有$\Delta{s_{i}}$=$\int_{t_{i-1}}^{t_{i}}\sqrt{\phi'^2(t)+\psi'^2(t)}\mathrm{d}t$`(14)`
  - 由积分中值定理:$\Delta{s}_{i}$=$\sqrt{\phi'^2(\tau_{i}')+\psi'^2(\tau_{i}')}\cdot {\Delta{t_{i}}}$`(15)`;其中$\Delta{t_{i}}$=$t_{i}-t_{i-1}$,$\tau_{i}'\in[t_{i-1},t_{i}]$于是
  - Note:这里$\tau_i,\tau_i'$有联系(都是$[t_{i-1},t_{i}]$区间上的点),但并不相同
  - 将式(13),(15)代入到式(3),于是$\int_{L}f(x,y)\mathrm{d}s$=$\lim\limits_{\lambda\to{0}}\sum_{i=1}^{n}f[\phi(\tau_{i}),\psi(\tau_{i})]\sqrt{\phi'^{2}(\tau_{i}')+\psi'^{2}(\tau_{i}')}\Delta{t_{i}}$`(16)`
  - 由于函数(12-1)在闭区间$[\alpha,\beta]$上连续,可以把上式中$r_{i}'$替换为$r_{i}$,从而式(16)写作式(11)
    - 这一步要用到(12-1)函数在$[\alpha,\beta]$上的一致连续性
- 式(11)等号右端是一个定积分式子,因为其被积函数在$[\alpha,\beta]$上连续,所以这个定积分是存在的,因此式(11)等号左端$\int_{L}f(x,y)\mathrm{d}s$也存在;并且可以有式(11)计算结果

### 小结

- 公式(11)表明,计算对弧长的曲线积分$f_{L}f(x,y)\mathrm{d}s$时,只要将$x,y,\mathrm{d}s$分别替换为:$\phi(t),\psi(t),\sqrt{\phi'^2{t)+\psi'^2(t)}}\mathrm{d}t$
- 然后做$[\alpha,\beta]$上的定积分即可$(\alpha<\beta)$

## 公式其他形式

### 曲线弧显函数形式方程下的曲线积分公式

- 若曲线弧$L$由方程$y=\psi(x)$`(17)`,$(x\in[x_0,X])$给出,
  - 则可以把这种情况看作特殊的参数方程:(参数方程的简单情形)
    - $x=t$,$y=\psi(t)$,$t\in[x_{0},X]$`(17-1)`
  
  - 将其代入公式(11),得$\int_{L}f(x,y)\mathrm{d}s$=$\int_{x_0}^{X}f(x,\psi(x)) \sqrt{1+\psi'^{2}(x)}\mathrm{d}x$,$(x_0<X)$`(18)`
  
- 类似地,若曲线弧$L$由方程$x=\phi(y)$`(19)`,$(y\in[y_0,Y])$给出
  - $y=t$,$x=\phi(t)$`(19-1)`,$(y\in[y_0,Y])$
  - 则$\int_{L}f(x,y)\mathrm{d}s$=$\int_{y_0}^{Y}f(\phi(y),y) \sqrt{1+\phi'^{2}(y)}\mathrm{d}y$,$y_{0}<Y$`(20)`

### 极坐标形式的曲线方程下的曲线积分公式

- 若$L:r=r(\theta)$`(21)`,$\theta\in[\alpha,\beta]$,则$\int_{L}f(x,y)\mathrm{d}s$=$\int_{\alpha}^{\beta}f(r(\theta)\cos\theta,r(\theta)\sin\theta)\sqrt{r^2+r'^2}\mathrm{d}\theta$`(22)`

### 推广

- 若空间曲线$\Gamma$由参数方程$x=\phi(t)$,$y=\psi(t)$,$z=\omega{t}$,$t\in[\alpha,\beta]$给出的情形,这时$f_{L}f(x,y,z)\mathrm{d}s$=$\int_{\alpha}^{\beta}[f(\phi(t),\psi(t),\omega(t))]\sqrt{\phi'^2(t)+\psi'^{2}(t)+\omega'^{2}(t)}\mathrm{d}t$$(\alpha<\beta)$

## 应用

### 例

- 计算$\int_{L}\sqrt{y}\mathrm{d}s$其中$L$时抛物线$y=x^2$上点$O(0,0)$与$B(1,1)$之间的一段弧
  - 曲线$L$表示为$x$为参数的参数方程:$y=x^2$,$x\in[0,1]$
  - $\int_{L}\sqrt{y}\mathrm{d}s$=$\int_{0}^{1}\sqrt{x^2}\sqrt{1+(x^2)'^2}\mathrm{d}x$=$\int_{0}^{1}x\sqrt{1+4x^2}\mathrm{d}x$
    - 第一换元法:$\frac{1}{2}\int_{0}^{1}\sqrt{1+4x^2}\mathrm{d}x^2$=$\frac{1}{8}\int_{0}^{1}\sqrt{1+4x^2}\mathrm{d}(4x^2+1)$=$\frac{1}{8}\frac{2}{3}(1+4x^2)^{\frac{3}{2}}|_{0}^{1}$=$\frac{1}{12}[(1+4)^{\frac{3}{2}}-1]$=$\frac{1}{12}(5\sqrt{5}-1)$

### 例

- 若$L$:$x=R\cos\theta$,$y=R\sin\theta$,$\theta\in[-\alpha,\alpha]$

- $I$=$\int_{L}y^2\mathrm{d}s$=$\int_{-\alpha}^{\alpha}R^2\sin^2\theta\sqrt{(-R\sin\theta)^2+(R\cos\theta)^2}\mathrm{d}\theta$
  - =$R^3\int_{-a}^{a}\sin^2\theta\mathrm{d}\theta$=$\frac{R^3}{2}[\theta-\frac{\sin 2\theta}{2}]_{-\alpha}^{\alpha}$=$\frac{R^3}{2}(2\alpha-\sin2\alpha)$=$R^3(\alpha-\sin\alpha\cos\alpha)$

### 例

- 若$L$:$x=a\cos{t}$,$y=a\sin{t}$,$z=kt$,$t\in[0,2\pi]$,求$I$=$\int_{\Gamma}(x^2+y^2+z^2)\mathrm{d}s$
  - $I$=$\int_{\Gamma}(a^2\cos^2{t}+a^2\sin^2t+k^2t^2)\sqrt{(-a\sin{t})^2+(a\cos{t})^2+k^2}\mathrm{d}t$
  - =$\int_{0}^{2\pi}(a^2+k^2t^2)\sqrt{a^2+k^2}\mathrm{d}t$=$\sqrt{a^2+k^2}[a^2t+\frac{k^2}{3}t^3]_{0}^{2\pi}$
    - 提取与$t$无关的因式$\sqrt{a^2+k^2}$到积分号前
  - =$\sqrt{a^2+k^2}[2a^2\pi+\frac{8}{3}k^2\pi^2]$
  - =$\frac{2\pi}{3}\sqrt{a^2+k^2}(3a^2+4\pi^2k^2)$



