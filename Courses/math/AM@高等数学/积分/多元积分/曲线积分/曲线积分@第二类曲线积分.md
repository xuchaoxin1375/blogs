[toc]

## abstract

- 第二类曲线积分,即对坐标的曲线积分

## 对坐标的曲线积分

### 变力沿曲线所做的功

- 力是矢量,具有方向属性,从便利沿曲线做功的问题抽象出第二类曲线积分的定义

- 变力的表示:这里用向量的坐标分解式表示力

- 设一个**质点**在$xOy$面内受到**力**$\bold F(x,y)$=$P(x,y)\bold{i}+Q(x,y)\bold{j}$`(0)`的作用,从点$A$沿**光滑**曲线弧$L$移动到点$B$,其中**函数**$P(x,y)$和$Q(x,y)$在$L$上**连续**
  - 质点$M$从位置点$A\to{B}$的过程中位置坐标记$(x,y)$,每个位置对应的力为$\bold F(x,y)$,这就是函数$\bold{F}(x,y)$和曲线弧$L$的关系;这里假设曲线弧$L$时光滑的
  - 而函数$\bold{F}$在曲线弧$L$上连续保证了$L$上的任意位置$(x,y)$都有连续(不会出现无定义的情况或突变)
- 现在问题是计算上述移动过程中$F(x,y)$所做的**功**

### 平均功(恒力做功)

- 一种最简单的情形是$\bold{F}$为恒力,且质点从$A$沿直线移动到$B$,那么恒力$F$所做的功$W$为向量$\bold{F}$与向量$\overrightarrow{AB}$的数量积,即$\bold{W}=\bold F\cdot{\overrightarrow{AB}}$`(1)`
- 恒力:可以令式(0)中的$x,y$取得一组确定得值$(\xi_i,\eta_i)$,即$\bold{F}(\xi_i,\eta_i)$

### 变力做功

- 现在$F(x,y)$是变力,且质点沿的是曲线移动,功显然无法直接按照式(1)计算
- 这里可以采用微积分的方法来合理的应用公式(1)于更一般的情形

### 弧段微分

- 先用曲线弧上的点$M_{1}(x_1,y_1)$,$M_2(x_2,y_2)$,$\cdots$,$M_{n-1}(x_{n-1},y_{n-1})$`(2)`**把$L$分成$n$个**小弧段

- 取其中一个有向小弧段$a(M_{i-1}M_{i})$做分析
  - 由于$a(M_{i-1}M_{i})$**光滑**且很短,可以用**有向线段**$\overrightarrow{M_{i-1}M_{i}}$=$(\Delta{x}_{i})\bold{i}+(\Delta{y}_{i})\bold{j}$`(3)`近似代替
  - 其中$\Delta{x_i}$=$x_{i}-x_{i-1}$,$\Delta{y_{i}}$=$y_{i}-y_{i-1}$`(3-1)`
- 由于$P(x,y)$,$Q(x,y)$在$L$上连续,可以用$a(M_{i-1}M_{i})$上任意曲定的一点$(\xi_{i},\eta_{i})$处的力$\bold{F}(\xi_{i},\eta_{i})$=$P(\xi_{i},\eta_{i})\bold{i}+Q(\xi_{i},\eta_{i})\bold{j}$`(4)`来近似代替这小弧段上各点处的力
- 这样变力$\bold{F}(x,y)$沿着有向小弧段$a(M_{i-1}M_{i})$所做的功$\Delta{W_{i}}$可以近似地等于**恒力**$\bold{F}(\xi_i,\eta_{i})$,沿着$\overrightarrow{M_{i-1},M_{i}}$所作的功:
  - $\Delta{W}_{i} \approx{\bold F(\xi_{i},\eta_i)}\cdot{\overrightarrow{M_{i-1}M_{i}}}$`(5)`,即$\Delta{W}_{i}\approx{P(\xi_{i},\eta_{i})\Delta{x_i}+Q(\xi_{i},\eta_{i})\Delta{y_i}}$`(5-1)`

- 于是$W$=$\sum_{i=1}^{n}\Delta{W_{i}}$ $\approx$ $\sum_{i=1}^{n}[P(\xi_{i},\eta_{i})\Delta{x_i}+Q(\xi_{i},\eta_{i})\Delta{y_i}]$`(6)`

- 若用$\lambda$表示$n$个小弧段的最大长度,令$\lambda\to{0}$取式(6)的极限,所得极限自然地被认为式变力$\bold{F}$沿有**向曲线段**所做的功:
  - $W$=$\lim\limits_{\lambda\to{0}}\sum_{i=1}^{n}[P(\xi_{i},\eta_{i})\Delta{x_i}+Q(\xi_{i},\eta_{i})\Delta{y_i}]$`(7)`
- 这种和的极限在研究其他问题时也会遇到,将其抽象为第二类曲线积分

### 第二类曲线积分的定义

- 设$L$为$xOy$面内从点$A\to{B}$的一条**有向光滑曲线弧**;函数$P(x,y)$和$Q(x,y)$在$L$上有界
  - 和第一类曲线积分中曲线的无向性相比,第二类曲线积分的曲线$L$强调**有向**
- 在$L$上沿$L$的任意方向插入点列$M_{1}(x,y),\cdots,{M_{n-1}(x_{n-1},y_{n-1})}$,把$L$分成了$n$个有向小弧段
  - $a(M_{i-1}M_{i})$,$(i=1,2,\cdots,n)$;$M_{0}=A,M_{n}=B$
  - 设$\Delta{x_i}$=$x_{i}-x_{i-1}$,$\Delta{y_{i}}$=$y_{i}-y_{i-1}$
  - 点$(\xi_{i},\eta_i)$为小弧段$a(M_{i-1}M_{i})$上任意曲定的一点
- 作乘积$P(\xi_{i},\eta_{i})\Delta{x_i}$,$(i=1,2,\cdots,n)$;`(1)`作和式$\sum_{i=1}^{n}P(\xi_{i},\eta_{i})\Delta{x_i}$,`(2)`

- 若当各个小弧段长度的最大值$\lambda\to{0}$时,式(2)的**极限**总是存在,且于曲线弧$L$的分法和$(\xi_{i},\eta_{i})$的取法无关,则称此极限为函数$P(x,y)$在有向曲线弧$L$上对**坐标$x$的曲线积分**,记为$\int_{L}P(x,y)\mathrm{d}x$
- 类似地,若$\sum_{i=1}^{n}P(\xi_{i},\eta_{i})\Delta{x_i}$总是存在,且与曲线弧$L$的分法及点$(\xi_i,\eta_{i})$的取法无关,那么称此极限为**函数$Q(x,y)$在有向曲线弧$L$上对坐标$y$的曲线积分**,记为$\int_{L}Q(x,y)\mathrm{d}y$
- 综上有公式组:
  - $\int_{L}P(x,y)\mathrm{d}x$=$\lim\limits_{\lambda\to{0}}\sum_{i=1}^{n}P(\xi_{i},\eta_{i})\Delta{x}_{i}$`(3)`
  - $\int_{L}Q(x,y)\mathrm{d}x$=$\lim\limits_{\lambda\to{0}}\sum_{i=1}^{n}Q(\xi_{i},\eta_{i})\Delta{y}_{i}$`(4)`

- 两个积分称为第二类曲线积分
- 其中$P(x,y)$,$Q(x,y)$称为**被积函数**,$L$称为**积分弧段**

### 函数在曲线弧上连续

- 讨论第二类曲线积分时,总假定$P(x,y),Q(x,y)$在$L$上连续(第二类曲线积分总是存在)
- 函数$f(x,y)$在曲线$L$上连续,应当是$f(x,y)$在$D=\set{(x,y)|(x,y)\in{L}}$上处处连续
- 严格地说,向量值函数函数$\bold{F}(x,y)$在曲线$L$上连续是指:
  - 对$L$上**任意点**$M_{0}(x_0,y_0)$,当$L$上的动点$M(x,y)$沿$L$趋于$M_{0}$时,有$|\bold F(x,y)-\bold{F}(x_0,y_0)|\to{0}$
  - 若$\bold F(x,y)$=$P(x,y)\bold{i}+Q(x,y)\bold{j}$,则$\bold{F}(x,y)$在$L$上连续等价于$P(x,y)$与$Q(x,y)$均在$L$上连续

### 推广:空间曲线弧的第二类曲线积分

- 上述第二类曲线积分是在平面上定义的,可以类似地推广到积分弧段为空间有向曲线弧$\Gamma$的情形:
  - $\int_{\Gamma}P(x,y,z)\mathrm{d}x$=$\lim\limits_{\lambda\to{0}}\sum_{i=1}^{n}P(\xi_{i},\eta_{i},\zeta_{i})\Delta{x_i}$`(5)`
  - $\int_{\Gamma}Q(x,y,z)\mathrm{d}x$=$\lim\limits_{\lambda\to{0}}\sum_{i=1}^{n}Q(\xi_{i},\eta_{i},\zeta_{i})\Delta{y_i}$`(6)`
  - $\int_{\Gamma}R(x,y,z)\mathrm{d}x$=$\lim\limits_{\lambda\to{0}}\sum_{i=1}^{n}R(\xi_{i},\eta_{i},\zeta_{i})\Delta{z_i}$`(7)`

### 常用形式@向量形式@简写👺

- 两个二类曲线积分的和的形式:$\int_{L}P(x,y)\mathrm{d}x$+$\int_{L}Q(x,y)\mathrm{d}y$`(8)`可以简写为
  - $\int_{L}P(x,y)\mathrm{d}x+Q(x,y)\mathrm{d}y$`(9)`
    - 取消掉第二个积分号,而用一个积分号对两个被积表达式做二类曲线积分
  - 或者写成**向量形式**(向量内积表示):$\int_{L}\bold{F}(x,y)\cdot\mathrm{d}\bold r$,`(10)`
    - 这里$\bold{F,r}$都是向量,$\cdot$表示向量内积,不省略
    - 其中$\bold{F}(x,y)$=$P(x,y)\bold{i}+Q(x,y)\bold{j}$`(10-1)`为**向量值函数**
    - $\mathrm{d}\bold{r}$=$\mathrm{d}x\bold{i}+\mathrm{d}y\bold{j}$`(10-2)`为微分向量
- 类似地,$\int_{\Gamma}P(x,y,z)\mathrm{d}x$+$\int_{\Gamma}Q(x,y,z)\mathrm{d}y$+$\int_{\Gamma}R(x,y,z)\mathrm{d}z$可以省略第一个之后的2个"$\int_{\Gamma}$"符号,简写成
  - $\int_{\Gamma}P(x,y,z)\mathrm{d}x+Q(x,y,z)\mathrm{d}y+R(x,y,z)\mathrm{d}z$`(11)`
  - 或:$\int_{L}\bold{A}(x,y,z)\cdot\mathrm{d}\bold r$,
    - 其中$\bold{A}(x,y,z)$=$P(x,y,z)\bold{i}$+$Q(x,y,z)\bold{j}$+$R(x,y,z)\bold{k}$;
    - $\mathrm{d}\bold{r}$=$\mathrm{d}x\bold{i}+\mathrm{d}y\bold{j}+\mathrm{d}z\bold{k}$

### 利用第二类曲线积分表示变力做功

- 讨论变力$\bold{F}$做功时,$\bold{F}$所做的功可以表示为式(8)或(9)或(10)

### 性质

- 线性性质:
  - 设$\alpha,\beta$为常数,$\int_{L}[\alpha{F_{1}(x,y)}+\beta{F_{2}}(x,y)]\cdot{\mathrm{d}\bold{r}}$=$\alpha\int_{L}\bold{F}_1(x,y)\cdot{\mathrm{d}\bold{r}}$+$\beta\int_{L}\bold{F}_{2}(x,y)\cdot{\mathrm{d}\bold{r}}$`(1)`

- 可加性:
  - 若有向曲线弧$L$可分成两段光滑的有向曲线弧$L_1,L_2$,则:$\int_{L}\bold{F}(x,y)\cdot\mathrm{d}\bold r$=$\int_{L_1}\bold{F}(x,y)\cdot\mathrm{d}\bold r$+$\int_{L_2}\bold{F}(x,y)\cdot\mathrm{d}\bold r$`(2)`

- 反向弧性质:

  - 设$L$是有向光滑弧,$L^{-}$是$L$**反向曲线弧**:$\int_{L^{-}}\bold{F}(x,y)\cdot\mathrm{d}\bold r$=$-\int_{L}\bold{F}(x,y)\cdot\mathrm{d}\bold r$`(3)`

  - 证明:把$L$分成$n$小段,相应的$L^{-}$也分成$n$小段,对于每个小段弧,当曲线的方向改变时,有向弧在坐标上的**投影**,其**绝对值不变**,但是要改变符号,就得到(3)

  - 此性质表明,当积分弧段的方向改变时,对坐标的曲线积分要改变符号,因此对坐标的曲线积分要区分**积分弧段的方向**
  - 本性质是对坐标曲线积分特有的性质,对弧长的曲线积分不具有这类性质)
  - 相应的,对弧长的曲线积分也有独占的性质(被积函数大小的性质 )

## 计算方法

- 第二类曲线积分仍然是转化为定积分计算
- 设$P(x,y),Q(x,y)$在**有向曲线弧**$L$上有定义且**连续**,$L$的参数方程为
  - $x=\phi(t)$
  - $y=\psi(t)$
- 当参数$t$**单调**地从$\alpha\to{\beta}$时,点$M(x,y)$从$L$的起点$A$沿$L$运动到终点$B$,若$\phi(t),\psi(t)$在以$\alpha,\beta$为端点的**闭区间**上具有**一阶连续导数**,且$\phi'^2(t)+\psi'^{2}(t)\neq{0}$`(0)`
- 则曲线积分$\int_{L}P(x,y)\mathrm{d}x+Q(x,y)\mathrm{d}y$存在,且有公式$\int_{L}P(x,y)\mathrm{d}x+Q(x,y)\mathrm{d}y$=$\int_{\alpha}^{\beta}\{P[(\phi(t),\psi(t))]\phi'(t)+Q[\phi(t),\psi(t)]\psi'(t)\}\mathrm{d}t$`(1)`
- 特别的
  - 当$Q(x,y)=0$时,$\int_{L}P(x,y)\mathrm{d}x$=$\int_{\alpha}^{\beta}P[\phi(t),\psi(t)]\phi'(t)\mathrm{d}t$`(1-1)`
  - 当$P(x,y)=0$时:$\int_{L}Q(x,y)\mathrm{d}y$=$\int_{\alpha}^{\beta}Q[\phi(t),\psi(t)]\psi'(t)\mathrm{d}t$`(1-2)`

### 证明

- 推导过程应用微分中值定理和一致连续性,以及连续函数性质,连续函数积分存在

#### 对坐标$x$

- 在$L$上取一列点:$A=M_0,M_1,\cdots,M_{n}=B$
- 设它们对应于一列**单调变化**(递增或递减)的参数值$\alpha=t_0,t_1,\cdots,t_{n}=\beta$`(2)`
- 根据对坐标的曲线积分的定义(对坐标$x$):$\int_{L}P(x,y)\mathrm{d}x$=$\lim\limits_{\lambda\to{0}}\sum_{i=1}^{n}P(\xi_{i},\eta_i)\Delta{x_i}$`(3)`
- 设点$(\xi_{i},\eta_i)$对应于参数$\tau_i$,即$\xi_{i}=\phi(\tau_i)$,$\eta_{i}=\psi(\tau_{i})$`(4)`这里$\tau_{i}$在$t_{i-1},t_{i}$之间
- 由于$\Delta{x}_{i}=x_{i}-x_{i-1}$=$\phi(t_{i})-\phi(t_{i-1})$`(5)`应用微分中值定理,式,$\Delta{x}_{i}$=$\phi'(\tau_{i}')\Delta{t_{i}}$,$\tau_{i}'$`(5-1)`介于$t_{i-1},t_{i}$之间
- 将(4),(5-1)代入(3),于是式(3)可改写为$\lim\limits_{\lambda\to{0}}\sum_{i=1}^{n}P[\phi(\tau_{i}),\psi(\tau_i)]\phi'(\tau_{i}')\Delta{t_{i}}$`(6)`
- 因为函数$\phi'(t)$在区间$[\alpha,\beta]$或$[\beta,\alpha]$上连续,由一致连续性知识,可以把$\tau_{i}'$替换为$\tau_{i}$,从而式(6)改写为$\lim\limits_{\lambda\to{0}}\sum_{i=1}^{n} P[\phi(\tau_{i}),\psi(\tau_i)]\phi'(\tau_{i})\Delta{t_{i}}$`(7)`
- 式(7)的和的极限就是**定积分**$\int_{\alpha}^{\beta}P[\phi(t),\psi(t)]\phi'(t)\mathrm{d}t$`(8)`
- 由连续条件和连续函数的性质,被积函数$P[\phi(t),\psi(t)]\phi'(t)$连续,因此积分式(8)存在,所以$\int_{L}P(x,y)\mathrm{d}x$存在,且$\int_{L}P(x,y)\mathrm{d}x$=$\int_{\alpha}^{\beta}P[\phi(t),\psi(t)]\phi'(t)\mathrm{d}t$`(9)`

#### 对坐标$y$

- 类似上述推导,可证$\int_{L}Q(x,y)\mathrm{d}y$=$\int_{\alpha}^{\beta}Q[\phi(t),\psi(t)]\psi'(t)\mathrm{d}t$`(10)`

#### 相加

- 将式(9,10)相加:$\int_{L}P(x,y)\mathrm{d}x+Q(x,y)\mathrm{d}y$
  - =$\int_{\alpha}^{\beta}P[\phi(t),\psi(t)]\phi'(t)+Q[\phi(t),\psi(t)]\psi'(t)\mathrm{d}t$
  - =$\int_{\alpha}^{\beta}P[\phi(t),\psi(t)]\phi'(t)\mathrm{d}t$+$\int_{\alpha}^{\beta}Q[\phi(t),\psi(t)]\psi'(t)\mathrm{d}t$,`(11)`
- 这就证明了公式(1)

#### 积分限和曲线弧起止点

- 式(11)中,积分下限$\alpha$对应于$L$的起点,上限$\beta$对应于$L$的终点

### 公式的应用👺

- 公式(1)或(11)表明,计算曲线积分$\int_{L}P(x,y)\mathrm{d}x+Q(x,y)\mathrm{d}y$时,只需要将$x,y,\mathrm{d}x,\mathrm{d}y$依次替换为$\phi(t),\psi(t),\phi'(t)\mathrm{d}t,\psi'(t)\mathrm{d}t$
- 在处理有向弧$L$对应的参数:
  - 从$L$的**起点所对应的参数值$\alpha$**到$L$的**终点所对应的参数值$\beta$**做定积分即可
  - 这里$\alpha,\beta$大小关系无限制,$\alpha$不一定小于$\beta$,这和第一类曲线积分不同

### 公式的其他形式

- 若$L$有方程$y=\psi(x)$或$x=\phi(y)$给出,可以看作参数方程的特例
- 例如:当$L$有$y=\phi(x)$,$x\in[a,b]$给出时(可以视为$x=t,y=\phi(t)$或直接$x=x,y=\phi(x)$,参数为$x$,公式(1)改写为$\int_{L}P(x,y)\mathrm{d}x+Q(x,y)\mathrm{d}y$=$\int_{a}^{b}P[x,\psi(x)]+Q[x,\psi(x)]\psi'(x)\mathrm{d}x$`(12)`
  - 其中,$a,b$分别对应$L$的起点和终点

### 推广

- 公式(1)可以推广到**空间曲线**$\Gamma$由参数方程$x=\phi(t)$,$y=\psi(t)$,$z=\omega{(t)}$给出的情形
- 此时$\int_{\Gamma}P(x,y,z)\mathrm{d}x+Q(x,y,z)\mathrm{d}y+R(x,y,z)\mathrm{d}z$=$\int_{\alpha}^{\beta}P[\phi(t),\psi(t),\omega(t)]\phi'(t)+Q[\phi(t),\psi(t),\omega(t)]\psi'(t)+R[\phi(t),\psi(t),\omega(t)]\omega'(t)\mathrm{d}t$

- $\alpha,\beta$分别对应$\Gamma$的起点和终点















