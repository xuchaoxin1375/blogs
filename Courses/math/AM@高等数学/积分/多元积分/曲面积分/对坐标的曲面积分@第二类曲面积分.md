[toc]

## abstract

- 对坐标的曲面积分@第二类曲面积分

## 曲面基本概念

### 双侧曲面

- 通常,曲面是双侧的,例如
  - 不闭合的曲面$z=z(x,y)$有**上侧和下侧**之分(按照惯例,假定$z$轴铅直向上)
  - 而一张包围某一空间区域的**闭曲面**,有**外侧和内侧**之分
- 这里考虑的曲面是双侧曲面

#### 例

- 例如旋转抛物面,$z=x^2+y^2$,其不是一个封闭的区域,即便给人感觉有内外侧,但其实只有上下侧
- 明确这一点是重要的,关于第二类曲面积分的负号问题

### 有向曲面

- 讨论对坐标的曲面积分时,需要指定曲面的**侧**
- 通常可以通过曲面上**法向量**的指向来定出曲面的侧:
  - 例如,对于$z=z(x,y)$,若取它的法向量$\bold{n}$的指向朝上,则认为取定曲面的上侧
  - 又如,对于闭曲面,如果它的法向量的指向**朝外**,则认为曲定曲面的外侧
- 取定了**法向量**亦即选定了**侧**的曲面,称为**有向曲面**



### 曲面区域投影

- 设$\Sigma$时有向曲面,在$\Sigma$上取一小块曲面$\Delta{S}$,把$\Delta{S}$投影到$xOy$面上得到一个**投影区域**,将这个投影区域的面积记为$(\Delta{\sigma})_{xy}$
- 假定$\Delta{S}$上各点处的**法向量**和**$z$轴**的**夹角$\gamma$的余弦$\cos\gamma$**有**相同的符号**,规定$\Delta{S}$在$xOy$上的**投影$(\Delta{S})_{xy}$**为
  - $(\Delta{S})_{xy}$=$(\Delta{\sigma})_{xy}$,$\cos\gamma>0$
  - $(\Delta{S})_{xy}$=$-(\Delta{\sigma})_{xy}$,$\cos{\gamma}<0$
  - $(\Delta{S})_{xy}=0,$$\cos\gamma=0$(即$(\Delta{\sigma})_{xy}=0$)

- 由上述规定可知,$\Delta{S}$在$xOy$面上的投影$(\Delta{S})_{xy}$实际上就是$\Delta{S}$在$xOy$面上的投影区域的面积$(\Delta{\sigma})_{xy}$附以一定的正负号
- 其他坐标面上的投影:类似地,可以定义$\Delta{S}$在$yOz$面和$zOx$面上的投影$(\Delta{S})_{yz}$和$\Delta{S}_{zx}$

### 平面区域投影

- 在讨论曲面面积的计算时,我们介绍了平面区域投影:$\sigma$=$A\cos\gamma$
  - $A$是被投影的平面区域$D$的面积
  - 而$\sigma$是$D$投影到坐标面上的区域$D_{0}$的面积
  - $\gamma$是两平面的夹角
- 在元素法的应用下,曲面区域投影可以转化为平面区域投影

## 对坐标的曲面积分

### 引言:流向曲面一侧的流量

#### 简单情形

- 设稳定流动(流速与时间$t$无关)的不可压缩流体(假定密度为1)的速度场由
  - $\bold{v}(x,y,z)$=$P(x,y,z)\bold{i}$+$Q(x,y,z)\bold{j}$+$R(x,y,z)\bold{k}$`(1)`
- 给出,$\Sigma$是速度场中的一片有向曲面;函数$P,Q,R$都在$\Sigma$上连续,求在**单位时间内**流向$\Sigma$指定侧的**流体的质量**,即流量$\Phi$

##### 分析

- 若流体流过平面上面积为$A$的一个闭区域,且流体在闭区域上各点处的流速为$\bold{v}$(常**向量**),又设$\bold{n}$为该平面的**单位法向量**,那么在单位时间内流过该闭区域的流体组成一个**底面积**为$A$,**斜高**位$\bold{v}$的**斜柱体**$V$
- 令$\theta=<\bold{v,n}>$`(2)`,令$P$=$A\bold{v\cdot{n}}$`(3)`
  - 若$\theta<\frac{\pi}{2}$.此时$V$的体积:$A|\bold{v}|\cos\theta$=$A\bold{v\cdot{n}}$=$P$
  - 若$\theta=\frac{\pi}{2}$,显然流体通过闭区域$A$流向$\bold{n}$所指的一侧的流量$\Phi$为0,$P=0$
    - 所以$\Phi$=$P$=0
  - 若$\theta>\frac{\pi}{2}$,$P<0$,这时我们仍然把$P$称为流体通过闭区域$A$流向$n$所指的一侧的流量
    - 这表示流体通过闭区域$A$流向$-\bold{n}$所指的一侧,且流向$-\bold{n}$所指一侧的流量为$-P$=$-A\bold{v}\cdot{\bold{n}}$(和流向$\bold{n}$所指的一侧的流量为$P$=$A\bold{v\cdot{n}}$是同样的意思)
    - 换句话说:令$\bold{m}=-\bold{n}$,则流量$-A\bold{v}\cdot{\bold{n}}$表示为$A\bold{v}\cdot{\bold{m}}$,因此,流向$\bold{m}$的一侧的流量为$A\bold{v}\cdot{\bold{m}}$
- 因此不论$\theta$取何值,流体通过闭区域$A$流向$\bold n$所指的一侧的流量$\Phi$均为$P$

#### 一般情形

- 现在考虑非平面闭区域的情形,而是一片曲面区域,且流速$\bold{v}$不是常向量的情形
- 此时无法直接利用上一情形计算,但是可以利用元素法积分的方式应用
- 把曲面$\Sigma$分成$n$小块$\Delta{S}_i$,($\Delta{S}_{i}$同时也表示第$i$个小区块的面积)
- 在$\Sigma$是光滑的和$\bold{v}$是连续的前提下,只要$\Delta{S_{i}}$的**直径很小**,我们就可以用$\Delta{S_{i}}$上任意一点$(\xi_{i}\eta_{i},\zeta_{i})$处的流速$\bold{v}_{i}$=$\bold{v}_{i}(\xi_{i},\eta_{i},\zeta_{i})$=$P(\xi_{i},\eta_{i},\zeta_{i})\bold{i}+Q(\xi_{i},\eta_{i},\zeta_{i})\bold{j}+R(\xi_{i},\eta_{i},\zeta_{i})\bold{k}$`(4)`代替$\Delta{S_{i}}$上其他个点处的流速,以该点$(\xi_{i},\eta_{i},\zeta_{i})$处曲面$\Sigma$的法向量$\bold{n}_{i}$=$\cos\alpha_{i}\bold{i}$+$\cos\beta_{i}\bold{j}$+$\cos\gamma_{i}\bold{k}$`(5)`代替$\Delta{S_{i}}$上其他各点处的单位法向量
- 从而得到通过$\Delta{S_{i}}$流向指定侧的流量近似为$\bold{v}_{i}\cdot{\bold{n}_{i}}\Delta{S}_{i}$,$(i=1,2,\cdots,n)$
- 于是,通过$\Sigma$流向指定侧的流量$\Phi\approx{\sum_{i=1}^{n}\bold{v}_{i}\cdot{\bold{n}_{i}}}\Delta{S_{i}}$`(6)`这是初步的近似
- 考虑到以下近似组`(7)`
  - $\cos\alpha_{i}\cdot{\Delta{S_{i}}} \approx{(\Delta{S_{i}})_{yz}}$;
  - $\cos\beta_{i}\cdot{\Delta{S_{i}}} \approx{(\Delta{S_{i}})_{zx}}$;
  - $\cos\gamma_{i}\cdot{\Delta{S_{i}}} \approx{(\Delta{S_{i}})_{xy}}$
- 将(4,5,7)代入式(6),得:$\Phi\approx$ $\sum_{i=1}^{n} [P(\xi_{i},\eta_{i},\zeta_{i}){(\Delta{S_{i}})_{yz}}$+$Q(\xi_{i},\eta_{i},\zeta_{i}){(\Delta{S_{i}})_{zx}}$+$R(\xi_{i},\eta_{i},\zeta_{i}){(\Delta{S_{i}})_{xy}}]$`(8)`

- 当各小块曲面的直径的最大值$\lambda\to{0}$取和式(8)的**极限**,得到流量$\Phi$的精确值

#### 小结

- 上述问题的数学模型还会再其他问题中遇到,可从其中抽象出**对坐标的曲面积分的概念**

### 对坐标的曲面积分定义👺

- 设$\Sigma$为光滑的有向曲面,函数$P(x,y,z)$在$\Sigma$上**有界**,把$\Sigma$任意分成$n$块小区面$\Delta{S}_{i}$($\Delta{S}_{i}$同时也表示第$i$个小区块的面积)
- $\Delta{S_{i}}$在$xOy$面上的**投影**为$(\Delta{S}_{i})_{xy}$,
- $(\xi_{i},\eta_{i},\zeta_{i})$是$\Delta{S}_{i}$上任意取定的一点,作乘积$R(\xi_{i},\eta_{i},\zeta_{i})(\Delta{S}_{i})_{xy}$,$(i=1,2,\cdots,n)$
- 作和式$\sum_{i=1}^{n}R(\xi_{i},\eta_{i},\zeta_{i})(\Delta{S}_{i})_{xy}$`(1)`
- 若当各小区曲面的直径的最大值$\lambda\to{0}$,和式(1)的极限总是存在,且与曲面$\Sigma$的分发以及点$(\xi_{i},\eta_{i},\zeta_{i})$的取法无关,那么称此极限为**函数$R(x,y,z)$在有向曲面$\Sigma$上对坐标$x,y$的曲面积分**,记为$\iint\limits_{\Sigma}R(x,y,z)\mathrm{d}x\mathrm{d}y$
- 即$\iint\limits_{\Sigma}R(x,y,z)\mathrm{d}x\mathrm{d}y$=$\lim\limits_{\lambda\to{0}} \sum_{i=1}^{n}R(\xi_{i},\eta_{i},\zeta_{i})(\Delta{S}_{i})_{xy}$`(2-1)`
- 其中$R(x,y,z)$称为**被积函数**,$\Sigma$称为**积分曲面**

### 其他定义

- 类似地可以定义函数$P(x,y,z)$在有向曲面$\Sigma$上对坐标$y,z$的曲面积分$\iint\limits_{\Sigma}P(x,y,z)\mathrm{d}y\mathrm{d}z$=$\lim\limits_{\lambda\to{0}} \sum_{i=1}^{n}P(\xi_{i},\eta_{i},\zeta_{i})(\Delta{S}_{i})_{yz}$`(2-2)`
- 以及函数$Q(x,y,z)$在有向曲面$\Sigma$上对坐标$z,x$的曲面积分$\iint\limits_{\Sigma}Q(x,y,z)\mathrm{d}z\mathrm{d}x$=$\lim\limits_{\lambda\to{0}} \sum_{i=1}^{n}Q(\xi_{i},\eta_{i},\zeta_{i})(\Delta{S}_{i})_{zx}$`(2-3)`
- 上述(2-1,2-2,2-3)也称为**第二类曲面积分**

### 第二类曲面积分的存在性

- 当$P,Q,R$函数在**有向光滑曲面$\Sigma$上连续**时,对坐标的曲面积分存在
- 讨论第二类曲面积分时,总假设$P,Q,R$在$\Sigma$上连续

### 并写和简写👺

- 和第二类曲线积分类似的简写方案
- $\iint\limits_{\Sigma}P(x,y,z)\mathrm{d}y\mathrm{d}z$+$\iint\limits_{\Sigma}Q(x,y,z)\mathrm{d}z\mathrm{d}x$+$\iint\limits_{\Sigma}R(x,y,z)\mathrm{d}x\mathrm{d}y$可以简写为$\iint\limits_{\Sigma}P(x,y,z)\mathrm{d}y\mathrm{d}z+Q(x,y,z)\mathrm{d}z\mathrm{d}x+R(x,y,z)\mathrm{d}x\mathrm{d}y$`(3)`
- 进一步简写为$\iint\limits_{\Sigma}P\mathrm{d}y\mathrm{d}z+Q\mathrm{d}z\mathrm{d}x+R\mathrm{d}x\mathrm{d}y$`(4)`
- 计算的时候,通常采用分项积分的方式,逐项计算被积表达式中的各项

### 流量用第二类曲面积分描述

- 前述流量问题用第二类曲面积分描述:$\Phi$=$\iint\limits_{\Sigma}P(x,y,z)\mathrm{d}y\mathrm{d}z+Q(x,y,z)\mathrm{d}z\mathrm{d}x+R(x,y,z)\mathrm{d}x\mathrm{d}y$

## 性质

- 对坐标的曲面积分具有和对坐标的曲线积分类似的性质
- 可加性:
  - 若$\Sigma$是分片光滑的有向曲面,则规定函数在$\Sigma$上对坐标的曲面积分等于函数在各片光滑曲面上对坐标的曲面积分之和
  - 若$\Sigma$=$\Sigma_{1}+\Sigma_{2}$,
    - 则$\iint\limits_{\Sigma}P\mathrm{d}y\mathrm{d}z+Q\mathrm{d}z\mathrm{d}x+R\mathrm{d}x\mathrm{d}y$=$\iint\limits_{\Sigma_1}P\mathrm{d}y\mathrm{d}z+Q\mathrm{d}z\mathrm{d}x+R\mathrm{d}x\mathrm{d}y$+$\iint\limits_{\Sigma_2}P\mathrm{d}y\mathrm{d}z+Q\mathrm{d}z\mathrm{d}x+R\mathrm{d}x\mathrm{d}y$`(5)`
    - 可以推广到$\Sigma$=$\Sigma_{1}+\cdots+\Sigma_{n}$的情形
- 方向性质
  - 设$\Sigma$是有向曲面,$\Sigma^{-}$表示与$\Sigma$取相反侧的有向曲面,则
    - $\iint\limits_{\Sigma^{-}}P\mathrm{d}y\mathrm{d}z$=$-\iint\limits_{\Sigma}P\mathrm{d}y\mathrm{d}z$`(6-1)`
    - 类似的有
      - $\iint\limits_{\Sigma^{-}}Q\mathrm{d}z\mathrm{d}x$=$-\iint\limits_{\Sigma}Q\mathrm{d}z\mathrm{d}x$`(6-2)`
      - $\iint\limits_{\Sigma^{-}}R\mathrm{d}x\mathrm{d}y$=$-\iint\limits_{\Sigma}R\mathrm{d}x\mathrm{d}y$`(6-3)`
  - 即当积分曲面改变为相反侧时,对坐的曲面积分要改变符号

## 对坐标的曲面积分的计算👺

- 设积分曲面$\Sigma$是由方程$z=z(x,y)$`(0)`所给出的曲面**上侧**
  - $\Sigma$在$xOy$面上的投影区域为$D_{xy}$函数$z=z(x,y)$在$D_{xy}$上具有**一阶连续偏导数**
  - 被积函数$R(x,y,z)$在$\Sigma$上**连续**
- 按对坐标的曲面积分定义,有$\iint\limits_{\Sigma}R(x,y,z)\mathrm{d}x\mathrm{d}y$=$\lim\limits_{\lambda\to{0}} \sum_{i=1}^{n} R(\xi_{i},\eta_{i},\zeta_{i})(\Delta{S}_{i})_{xy}$`(1)`
- 因为$\Sigma$取上侧,$\cos\gamma>0$,所以$(\Delta{S}_{i})_{xy}$=$(\Delta{\sigma}_{i})_{xy}$`(2)`
- 又因为$(\xi_{i},\eta_{i},\zeta_{i})$是$\Sigma$上一点,所以$\zeta_{i}$=$z(\xi_{i},\eta_i)$`(3)`,
- 由(2,3)得:$\sum_{i=1}^{n}  R(\xi_{i},\eta_{i},\zeta_{i})(\Delta{S}_{i})_{xy}$=$\sum_{i=1}^{n} R (\xi_{i},\eta_{i},z(\xi_{i},\eta_i))(\Delta{\sigma}_{i})_{xy}$`(4)`
- 令各小块曲面的直径的最大值$\lambda\to{0}$,取上式两端的极限,并分别由曲面积分的定义和二重积分的定义,得
  - $\iint\limits_{\Sigma}R(x,y,z)\mathrm{d}x\mathrm{d}y$=$\iint\limits_{D_{xy}}R(x,y,z(x,y))\mathrm{d}x\mathrm{d}y$`(5)`
- 这就是把对坐标的曲面积分化为二重积分的公式
- 公式(5)的曲面积分是取在曲面$\Sigma$**上侧**,
- 若曲面积分取在$\Sigma$**下侧**,此时$\cos{\gamma}<0$,则$(\Delta{S_{i}})_{xy}$=$-(\Delta{\sigma}_{i})_{xy}$,
- 从而 $\iint\limits_{\Sigma}R(x,y,z)\mathrm{d}x\mathrm{d}y$=$-\iint\limits_{D_{xy}}R(x,y,z(x,y))\mathrm{d}x\mathrm{d}y$`(5-1)`

### 公式的其他形式

- 若曲面$\Sigma$由$x=x(y,z)$给出,则

  - $\iint\limits_{\Sigma}P(x,y,z)\mathrm{d}y\mathrm{d}z$=$\pm\iint\limits_{D_{xy}}P(x(y,z),y,z)\mathrm{d}y\mathrm{d}z$`(6)`

  - 符号的确定
    - 若积分曲面$\Sigma$是由方程$x=x(y,z)$给出的曲面**前侧**,则$\cos\alpha>0$,取**正号**
    - 否则$\Sigma$取**后侧**,即$\cos\alpha<0$,应取**负号**

- 若曲面$\Sigma$由$y=y(z,x)$给出,则

  - $\iint\limits_{\Sigma}Q(x,y,z)\mathrm{d}z\mathrm{d}x$=$\pm\iint\limits_{D_{xy}}Q(x,y(z,x),z)\mathrm{d}z\mathrm{d}x$`(7)`
  - 符号确定:
    - 积分曲面$\Sigma$是由方程$y=y(z,x)$所给出的曲面**右侧**,即$\cos\beta>0$,应取正号,反之$\Sigma$取**左侧**,$\cos\beta<0$,应取负号

- 综上(5,6,7),我们讨论了在3中不同曲面方程和投影下的第二类曲面积分的公式

### 应用

- 例如,公式(5),公式表明,计算曲面积分$\iint\limits_{\Sigma}R(x,y,z)\mathrm{d}x\mathrm{d}y$时,分三步骤
  1. 确定符号:根据积分曲面的方向(侧),以及上数介绍的规则确定结果是否加符号
     - 若方向向`上/前/右`(这三个方向是沿着坐标轴$(z,x,y轴)$正方向的方向)则为正号(不加符号),否则加负号
  2. 其中的变量$z$换成$\Sigma$的函数$z(x,y)$,也就是被积表达式的变换
     - 如果是公式(6或7)(曲面方程分别为$x=x(y,z)$或$y=y(z,x)$,则分别将$x$替换为$x(y,z)$,$y$替换为$y(z,x)$)
  3. 然后在$\Sigma$的**投影区域**$D_{xy}$上计算二重积分即可
     - 这里建议积分区域最后处理,如果被积函数替换后发现可以提到积分号外,则只需要计算$D_{xy}$的面积,而不需要化为二次积分
- 分段积分不急于逐项独立计算
  - 有时积分曲面被划分为多个片,这些片的计算式可能有联系,合起来算可能比分开算更加简便

### 例

- 计算$I=\iint\limits_{\Sigma} x^2\mathrm{d}y\mathrm{d}z+y^2\mathrm{d}z\mathrm{d}x+z^2\mathrm{d}x\mathrm{d}y$;
  - 其中$\Sigma$是长方体$\Omega$的真个表面的**外侧**
  - $\Omega$=$\set{(x,y,z)|x\in[0,a],y\in[0,b],z\in[0,c]}$
- 解
  - 分析积分曲面,可以将有向曲面分为6个部分:$\Sigma_1,\cdots\Sigma_{6}$
    1. $\Sigma_{1}:z=c$,$(x\in[0,a],y\in[0,b])$的**上侧**
    2. $\Sigma_{2}:z=0$,$(x\in[0,a],y\in[0,b])$的**下侧**
    3. $\Sigma_{3}:x=a$,$(y\in[0,b],z\in[0,c])$的**前侧**
    4. $\Sigma_{4}:x=0$,$(y\in[0,b],z\in[0,b])$的**后侧**
    5. $\Sigma_{5}:y=b$,$(x\in[0,a],z\in[0,c])$的**右侧**
    6. $\Sigma_{6}:y=0$,$(x\in[0,a],z\in[0,c])$的**左侧**
  - 对于积分$I$,采用分项积分的方式
    - 第一项$I_1$:要投影到$x=0$面上,可知仅有$\Sigma_3,\Sigma_4$的投影非0
      - $I_1$=$\iint\limits_{\Sigma}x^2\mathrm{d}y\mathrm{d}z$=$\iint\limits_{\Sigma_3}x^2\mathrm{d}y\mathrm{d}z$+$\iint\limits_{\Sigma_4}x^2\mathrm{d}y\mathrm{d}z$
        - 利用公式(6-1)得$I_{1}$=$\iint\limits_{D_{yz}}a^2\mathrm{d}y\mathrm{d}z$-$\iint\limits_{D_{yz}}0^2\mathrm{d}y\mathrm{d}z$=$a^2bc$
      - 类似地,$I_2$=$\iint\limits_{\Sigma}y^2\mathrm{d}z\mathrm{d}x$=$b^2ac$;$I_3$=$\iint\limits_{\Sigma}z^2\mathrm{d}x\mathrm{d}y$=$c^2ab$
    - 所以$I=I_1+I_2+I_3$=$(a+b+c)abc$

### 例

- 计算曲面积分$I=\iint\limits_{\Sigma}xyz\mathrm{d}x\mathrm{d}y$,其中$\Sigma$是球面$x^2+y^2+z^2=1$**外侧**在$x,y\geqslant{0}$的部分
  - 显然,$\Sigma$是第一和第五卦象的部分是$\frac{1}{4}$的球面
  - 考虑曲面的方向(侧),检查该曲面可知,$\Sigma$在$z=0$以上的外侧对应于**上侧**;而$z=0$以下的外侧对应于**下侧**
    - 而下侧部分记为$\Sigma_1$方程为${z_1}$=$-\sqrt{1-x^2-y^2}$,此时$z_1\leqslant{0}$
    - 将上侧部分记为$\Sigma_2$,方程为${z_2}$=$\sqrt{1-x^2-y^2}$,此时$z_2\geqslant{0}$
  - 从而可以分区域积分:
    - $I$=$\iint\limits_{\Sigma_2}xyz\mathrm{d}x\mathrm{d}y$+$\iint\limits_{\Sigma_1}xyz\mathrm{d}x\mathrm{d}y$
      - =$\iint\limits_{D_{xy}} xy\sqrt{1-x^2-y^2}\mathrm{d}x\mathrm{d}y$-$\iint\limits_{D_{xy}}xy(-\sqrt{1-x^2-y^2})\mathrm{d}x\mathrm{d}y$
      - =$2\iint\limits_{D_{xy}} xy\sqrt{1-x^2-y^2}\mathrm{d}x\mathrm{d}y$
    - 容易求出$D_{xy}$=$\set{(x,y)|x^2+y^2\leqslant{1}}$,则上述积分适合用极坐标计算
      - $\theta\in[0,\frac{\pi}{2}]$;$r\in[0,1]$
      - 从而$I$=$2\iint\limits_{D_{xy}} r^2\sin\theta\cos\theta\sqrt{1-r^2}r\mathrm{d}r\mathrm{d}\theta$
        - =$\int_{0}^{\frac{\pi}{2}}\sin{2\theta}\mathrm{d}\theta \int_{0}^{1}r^3\sqrt{1-r^2}\mathrm{d}\rho$
          - 观察可知,两次积分可以独立计算,$\int_{0}^{\frac{\pi}{2}}\sin{2\theta}\mathrm{d}\theta$=1
          - 利用第2类换元法积分,可以求得$\int_{0}^{1}r^3\sqrt{1-r^2}\mathrm{d}\rho$=$\frac{2}{15}$
        - =$1\cdot{\frac{2}{15}}$=$\frac{2}{15}$
  - 综上$I$=$\frac{2}{15}$





