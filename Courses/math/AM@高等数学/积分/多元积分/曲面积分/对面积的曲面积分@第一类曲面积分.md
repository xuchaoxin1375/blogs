[toc]

## 对面积的曲面积分

- 和对弧长的曲线积分的定义相仿

- 对面积的曲面积分对应的一个问题模型式曲面质量

  - $m$=$\lim\limits_{\lambda\to{0}}\sum_{i=1}^{n}\mu(\xi_{i},\eta_{i})\Delta{s_{i}}$`(0)`曲线形构件质量,$\mu(x,y)$为曲线的**线密度**,$\Delta{s_{i}}$曲线弧元素
    - $m$=$\lim\limits_{\lambda\to{0}}\sum_{i=1}^{n}\mu(\xi_{i},\eta_{i},\zeta_{i})\Delta{s}_{i}$`(0-1)`,空间曲线也是类似的
  - $m$=$\lim\limits_{\lambda\to{0}}\sum_{i=1}^{n}\mu(\xi_{i},\eta_{i},\zeta_{i})\Delta{S}_{i}$`(0-2)`,曲面构件质量,$\mu(x,y,z)$为**面密度**,$\Delta{S_i}$为曲面面积元素
    - 空间曲面,可以兼容平面曲面

- 将此问题模型具体意义抽去,抽象出对面积的曲面积分的概念

  

### 定义

- 设去曲面$\Sigma$是光滑的,函数$f(x,y,z)$在$\Sigma$上**有界**,把$\Sigma$任意分成$n$**小块曲面**$\Delta{S_i}$,($\Delta{S}_{i}$同时也表示第$i$块小曲面的面积),设$(\xi_i,\eta_i,\zeta_i)$是$\Delta{S_{i}}$上任意取定的一点
  - 作乘积$f(\xi_i,\eta_i,\zeta_i)\Delta{S}_{i}$,$(i=1,2,\cdots,n)$`(1)`,并作和$\sum_{i=1}^{n}f(\xi_i,\eta_i,\zeta_i)\Delta{S}_{i}$`(1-1)`
- 若个小块曲面的直径最大值$\lambda\to{0}$时,式(1)的极限总是存在,且与曲面$\Sigma$的分法和点$(\xi_i,\eta_i,\zeta_i)$的取法无关,则称此极限为**函数$f(x,y,z)$在曲面$\Sigma$上对面积的曲面积分**或第一类曲面积分,记为$\iint\limits_{\Sigma}f(x,y,z)\mathrm{d}S$`(1-2)`
  - Note:曲面直径指曲面上任意两点间距离最大值
- 即:$\iint\limits_{\Sigma}f(x,y,z)\mathrm{d}S$=$\lim\limits_{\lambda\to{0}}\sum_{i=1}^{n}\mu(\xi_{i},\eta_{i},\zeta_{i})\Delta{S}_{i}$`(2)`
  - 其中$f(x,y,z)$为**被积函数**,$\Sigma$为**积分曲面**

#### 曲面积分存在性

- 当$f(x,y,z)$在光滑曲面$\Sigma$上连续时,对面积的曲面积分存在
- 讨论曲面积分时,总假设$f(x,y,z)$在$\Sigma$上连续

### 使用面积分描述问题

- 根据上述定义,面密度为连续函数$\mu(x,y,z)$的光滑曲面$\Sigma$的质量$m$可以表示为
  - $m=\iint\limits_{\Sigma}\mu(x,y,z)\mathrm{d}S$
  - 即$\mu(x,y,z)$在$\Sigma$上对面积的曲面积分

## 性质

- 可加性

  - 若$\Sigma$是**分片光滑**的,我们规定函数在$\Sigma$上对面积的曲面积分等于函数在光滑的各片曲面上对面积的曲面积分之和
    - Note:分片光滑指的曲面是指,由有限个光滑曲面组成的曲面

  - 例如:设$\Sigma$可以分为2片光滑曲面$\Sigma_1,\Sigma_2$,记为$\Sigma=\Sigma_1+\Sigma_2$,
    - $\iint\limits_{\Sigma_1+\Sigma_2}f(x,y,z)\mathrm{d}S$=$\iint\limits_{\Sigma_1}f(x,y,z)\mathrm{d}S$+$\iint\limits_{\Sigma_2}f(x,y,z)\mathrm{d}S$`(3)`

- 与第一类曲线积分类似,第一类曲面积分有类似的性质

## 计算

- 设:积分**曲面**$\Sigma$由方程$z=z(x,y)$`(4)`给出,$\Sigma$在$xOy$上的**投影**区域为$D_{xy}$
  - 函数$z$=$z(x,y)$在$D_{xy}$上具有**连续偏导数**,
  - 被积函数$f(x,y,z)$在$\Sigma$上**连续**
- 按对面积的曲面积分的定义:$\iint\limits_{\Sigma}f(x,y,z)\mathrm{d}S$=$\lim\limits_{\lambda\to{0}}\sum_{i=1}^{n}\mu(\xi_{i},\eta_{i},\zeta_{i})\Delta{S}_{i}$,即式(2)
  - $(\xi_{i},\eta_{i},\zeta_{i})$是$\Delta{S_{i}}$上任意一点`(4-1)`

- 设$\Sigma$上第$i$小块曲面为$\Delta{S_i}$`(5)`,($\Delta{S}_{i}$同时也表示第$i$块小曲面的面积),在$xOy$面上的投影区域为$(\Delta{\sigma}_{i})_{xy}$`(6)`,(它的面积也记为$(\Delta{\sigma_{i}})_{xy}$)
- 则式(2)中的$\Delta{S}_{i}$可以表示为二重积分(曲面面积公式):$\Delta{S}_{i}$=$\iint\limits_{(\Delta{\sigma}_{i})_{xy}} \sqrt{1+z_{x}^2 (x,y)+z_{y}^{2}(x,y)}\mathrm{d}x\mathrm{d}y$`(7)     `
- 利用二重积分的中值定理,式(7)写作$\Delta{S_{i}}$=$\sqrt{1+z_{x}^2 (\xi_{i}',\eta_{i}')+z_{y}^{2}(\xi_{i}',\eta_{i}')} (\Delta{\sigma}_{i})_{xy}$`(8)`
  - 其中$(\xi_{i}',\eta_{i}')$是小闭区域$(\Delta{\sigma}_{i})_{xy}$上的一点
  - 又由(4-1)可知,$(\xi_{i},\eta_{i},\zeta_{i})$为$\Sigma$上一点,所以$\zeta_{i}$=$z(\xi_{i},\eta_{i})$`(9)`,从而$f(\xi_{i},\eta_{i},\zeta_{i})$=$f(\xi_{i},\eta_{i},z(\xi_{i},\eta_{i})$`(9-1)`
  - 这里$(\xi_{i},\eta_{i},0)$也是小闭区域$(\Delta{\sigma}_{i})_{xy}$上的点`(10)`

- 从而,由(8,9-1)$\sum_{i=1}^{n}f(\xi_{i},\eta_{i},\zeta_{i})\Delta{S}_i$=$\sum_{i=1}^{n}f(\xi_{i},\eta_{i},z(\xi_{i},\eta_{i})) \sqrt{1+z_{x}^2 (\xi_{i}',\eta_{i}')+z_{y}^{2}(\xi_{i}',\eta_{i}')} (\Delta{\sigma}_{i})_{xy}$`(11)`
- 由于函数$f(x,y,z(x,y))$和$\sqrt{1+z_{x}^2 (x,y)+z_{y}^{2}(x,y)}$都在闭区域$D_{xy}$上连续,
- 将式(11)右端$\xi_i',\eta_{i}'$用$\xi_{i},\eta_{i}$**替换**,得$\sum_{i=1}^{n}f(\xi_{i},\eta_{i},z(\xi_{i},\eta_{i})) \sqrt{1+z_{x}^2 (\xi_{i},\eta_{i})+z_{y}^{2}(\xi_{i},\eta_{i})} (\Delta{\sigma}_{i})_{xy}$`(11-1)`
- 当$\lambda\to{0}$时,式(11)右端的极限和式(11-1)的极限相等,在假定条件下,这个极限存在,且等于二重积分:$\iint\limits_{D_{xy}} f(x,y,z(x,y))\sqrt{1+z_{x}^2 (x,y)+z_{y}^{2}(x,y)} \mathrm{d}x\mathrm{d}y$`(12)`
- 因此式(11)左端的极限,即$\lim\limits_{\lambda\to{0}} \sum_{i=1}^{n}f(\xi_{i},\eta_{i},\zeta_{i})\Delta{S}_i$=$\iint_{\Sigma} f(x,y,z)\mathrm{d}S$也存在,且
  - $\iint\limits_{\Sigma} f(x,y,z)\mathrm{d}S$=$\iint\limits_{D_{xy}} f(x,y,z(x,y))\sqrt{1+z_{x}^2 (x,y)+z_{y}^{2}(x,y)} \mathrm{d}x\mathrm{d}y$`(13)`

- 公式(13)就是把**对面积的曲面积**分化为**二重积分**的公式

### 闭曲面上的积分

- 当$\Sigma$是闭曲面时,对其曲面积分记为$\Large{\oiint\limits_{\Sigma}}$

### 公式总结和应用👺

- 计算$\iint\limits_{\Sigma} f(x,y,z)\mathrm{d}S$(其中$\Sigma$由方程$z=z(x,y)$给出)时,只需要将
  - 函数$f$的变量$z$用曲面方程替换,即换为$z(x,y)$;
  - $\mathrm{d}S$换为$G=\sqrt{1+z_{x}^{2}+z_{y}^{2}}\mathrm{d}x\mathrm{d}y$

- 再确定$\Sigma$在$xOy$面上的投影区域$D_{xy}$即可化为二重积分计算
- 若积分曲面$\Sigma$由方程$x=x(y,z)$或$y=y(z,x)$给出,可以类似地把对面积的积分化为二重积分计算

#### 例

- 计算$I=\iint\limits_{\Sigma} \frac{1}{z}\mathrm{d}S$,其中$\Sigma$是由:球面$x^2+y^2+z^2=a^2$被平面$z=h$,$(0<h<a)$截出的**顶部**曲面
- 解:
  - $\Sigma$的方程表示为$z$=$\sqrt{a^2-x^2-y^2}$
  - $\Sigma$在$xOy$面上的投影区域$D_{xy}$为圆形闭区域$\set{(x,y)|x^2+y^2\leqslant{a^2-h^2}}$,
    - $f(x,y,z(x,y))$=$\frac{1}{\sqrt{a^2-x^2-y^2}}$
    - $G=\sqrt{1+z_{x}^{2}+z_{y}^{2}}$=$\frac{a}{\sqrt{a^2-x^2-y^2}}$
  - 由曲面积分公式:$I$=$\iint\limits_{D_{xy}}f(x,y,z(x,y))G\mathrm{d}\sigma$=$\iint\limits_{D_{xy}}\frac{a}{a^2-x^2-y^2}\mathrm{d}\sigma$
    - 化为极坐标计算:$I$=$\int_{0}^{2\pi}\mathrm{d}\theta \int_{0}^{\sqrt{a^2-h^2}}\frac{a}{a^2-r^2}r\mathrm{d}r$=$2\pi{a}\frac{-1}{2}[\ln|a^2-r^2|]_{0}^{\sqrt{a^2-h^2}}$=$2\pi{a}\ln\frac{a}{h}$

### 例

- 计算$I={\oiint\limits_{\Sigma}} xyz\mathrm{d}S$,其中$\Sigma$由平面$x=0,y=0,z=0$以及$x+y+z=1$所围成的四面体的整个边界曲面
- 解
  - 该积分曲面$\Sigma$是分片光滑的闭曲面
  - 可将其分为4个部分,并将:平面$x=0,y=0,z=0$以及$x+y+z=1$这4个平面上的部分分别记为$\Sigma_{1}$,$\Sigma_2$,$\Sigma_3$,$\Sigma_{4}$
  - 令$I_i$=${\oiint\limits_{\Sigma_{i}}} xyz\mathrm{d}S$,$i=1,2,3,4$;于是$I$=$I_1+I_2+I_3+I_4$
  - 而在$\Sigma_1,\Sigma_2,\Sigma_3$上,被积函数$f(x,y,z)$=$xyz$均为0,从而$I_1=I_2=I_3=0$
  - 对于$\Sigma_{4}$,其方程$z=1-x-y$;对应的投影$D_{xy}$=$\set{(x,y)|x=0,y=0,x+y=1围成的闭区域}$
    - $f(x,y,z(x,y))$=$xy(1-x-y)$
    - $G$=$\sqrt{1+z_{x}^{2}+z_{y}^{2}}$=$\sqrt{1+1+1}$=$\sqrt{3}$
  - 由曲面积分公式:$I_{4}$=$\int_{0}^{1}\mathrm{d}x\int_{0}^{1-x}\sqrt{3}xy(1-x-y)\mathrm{d}y$=$\frac{\sqrt{3}}{6}\int_{0}^{1}(x-3x^2+3x^3-x^4)\mathrm{d}x$=$\frac{\sqrt{3}}{120}$



