[toc]

## 归纳法

- **归纳法**是通过对事物观察,实验,经过概括和总结而得出**普遍规律**的一种方法,是从**认识特殊的事物**扩大到**认识一般事物**的一种**研究方法**
- 归纳法分为两类:完全归纳法和不完全归纳法

## 完全归纳法

- 完全归纳法是研究了事物的所有(有限种)可能情况而得出的一般结论的方法
- 这种归纳法得出的结论是**可靠的**,可以用于数学证明之中,往往体现在有限种情况的**分类讨论**之中

### 例

- 例如求证$|\sin{x}|\leqslant{|x|},\forall{x}\in{\mathbb{R}}$时,我们可以将实数集$\mathbb{R}$分为以下几个子集合

- $$
  \mathbb{R}=[0,\frac{\pi}{2})\cup(-\frac{\pi}{2})\cup{(-\infin,-{\frac{\pi}{2}}]}\cup[\frac{\pi}{2},+\infin)
  \\或:
  \mathbb{R}=[0,\frac{\pi}{2})\cup(-\frac{\pi}{2})\cup\{x|\;|x|\geqslant{\frac{\pi}{2}}\}
  $$

- 以第二种分割方案为例,我们可以分别证明三个集合上都有$|\sin{x}|\leqslant{|x|}$,就证明了$|\sin{x}|\leqslant{|x|}$对于一切实数$x$成立

### 形式化描述

- 从证明上看,归纳法应用的时完全归纳推理,即要证明的命题分为有限种情况,若每一种情况都能证明命题正确,那么原命题正确

- 设$B_{i},i=1,2,\cdots,n$是构成$A$的全部元素,若:$B_i\Rightarrow{C},i=1,2,\cdots,n$,那么$A\Rightarrow{C}$(这里C表示被归纳的命题)

## 不完全归纳法

- 不完全归纳法是仅仅根据事物的一部分特例读出的一般性结论的推理方法
- 不完全归纳法的**结论**不一定是正确的,因此有待于证明
  - 例如费马(Fermat)根据不完全归纳法,由下列4个等式都为质数猜测$F_n=2^{2^{n}}+1$对任意正整数$n$成立
    - $F_1=2^{2^{1}}+1=5$
    - $F_2=2^{2^{2}}+1=17$
    - $F_3=2^{2^{3}}+1=257$
    - $F_4=2^{2^{4}}+1=65537$
  - 遗憾的是,$F_5$就已经不是质数了(验证$F_5=2^{2^{5}}+1=2^{32}+1$是否为质数不容易,因为这个数字特别大)
- 但是不完全归纳法来获得命题(结论)是人类研究科学,探索真理,发现客观规律的一种重要手段

### 例

- 牛顿提出万有引力学说,门捷列夫发现元素周期律的过程中都曾运用了不完全归纳法









