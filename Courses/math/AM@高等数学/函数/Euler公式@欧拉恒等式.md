[toc]

## Euler公式

- 欧拉公式（英语：Euler's formula，又称尤拉公式）是复分析领域的公式，它将**三角函数**与**复指数函数**关联起来，因其提出者莱昂哈德·欧拉而得名。

- 欧拉公式提出，对任意实数 $x$，都存在

  - $$
    e^{ix} = \cos x + i\sin x
    $$

  - 其中 $e$ 是自然对数的底数，$i$ 是虚数单位，而 $\cos$ 和 $\sin$ 则是余弦、正弦对应的三角函数，参数 $x$ 则以弧度为单位。

  - 这一复数指数函数有时还写作 $\mathrm{cis} x$ （英语：cosine plus i sine，余弦加i 乘以正弦）。

  - 由于该公式在 $x$ 为复数时**仍然成立**，所以也有人将这一更通用的版本称为**欧拉公式 **。

- 欧拉公式在数学、物理和工程领域应用广泛。


### Euler恒等式



- 当 ${\displaystyle x=\pi }$ 时，欧拉公式变**欧拉恒等式**

  - $$
    {\displaystyle {{{e}^{{i}\,{\pi }}}+{1}}=0}
    $$

- 另外

  - $$
    e^{ix} = \cos x + i\sin x\\
    e^{-ix} = \cos (-x) + i\sin (-x)=\cos{x} - i\sin (x)
    \\
    {e^{ix}+e^{-ix}}=2\cos{x}
    \\
    e^{ix}-e^{-ix}=2i\sin{x}
    $$

- 即:

  - $$
    \sin x={\frac  {e^{{ix}}-e^{{-ix}}}{2i}}
    \\
    \cos x={\frac  {e^{{ix}}+e^{{-ix}}}{2}}
    $$

### cis函数🎈

- **cis**:"cos plus i sin"即
  - $\operatorname{cis} \theta = \cos \theta+i\sin \theta$
  - $\operatorname{cis} \theta = e^{i\theta}$
- 在复分析领域，欧拉公式亦可以以函数的形式表示
- 并且一般定义域为$\theta \in \mathbb{R}\,$，值域为$\theta \in \mathbb{C}\,$（复平面上的所有单位向量）。
- 当一复数的模为1，其反函数就是辐角（arg函数）。
- 当$\theta$值为复数时，cis函数仍然是有效的





