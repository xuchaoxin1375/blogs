[toc]



## abstract

- 全导数
- 多元复合函数求导法则@基础情形
  - 根据复合情形,有不同的求导法则

## 一元函数和多元函数复合的情形

### 全导数公式

- 若函数$u=\phi(t)$,$v=\psi(t)$都在点$t$可导,函数$z=f(u,v)$在对应点$(u,v)$具有有连续偏导数,则复合函数$z=f(\phi(t),\psi(t))$在点$t$可导,且$\frac{\mathrm{d}{z}}{\mathrm{d}t}$=$\frac{\partial{z}}{\partial{u}}\frac{\mathrm{d}u}{\mathrm{d}t}$+$\frac{\partial{z}}{\partial{v}}\frac{\partial{v}}{\partial{t}}$`(0)`
  - 简写:$z'=z_{t}'=z_{u}'u_{t}'+z_{v}'v_{t}'$或$z_{t}=z_{u}u_t+z_{v}v_{t}$

- 将字母$t$换成$x$也是一样的结论:若函数$u=\phi(x),v=\psi(x)$都在点$x$处可导,函数$z=f(u,n)$在对应点$(u,v)$具有连续的偏导数,则复合函数$z=f(u,v)$在点$x$处可导且$\frac{\mathrm{d}z}{\mathrm{d}x}$=$\frac{\partial{z}}{\partial{u}}\frac{\mathrm{d}u}{\mathrm{d}x}$+$\frac{\partial{z}}{\partial{v}}\frac{\partial{v}}{\partial{x}}$ `(1)`
- 这里使用字母$t$可能会更好,字母$x,y$通常作为多元函数的自变量,式子$\frac{\mathrm{d}z}{\mathrm{d}x}$容易让人联想到偏导(容易发生歧义),而使用字母$t$就很好的避免了这个问题
- 该公式(0),(1)是同一个公式,称为**全导数**公式,下面给出两个证法,为了便于区别两种证法自变量字母分别用$t,x$表示

### 公式结构

- ```mermaid
  flowchart TB
  	z---u & v--- x
  ```

  

### 公式证明

#### 证法1

- 设$t$获得增量$\Delta{t}$,(即$t\to{t+\Delta{t}}$),这时$u=\phi(t)$,$v=\psi(t)$的对应增量为$\Delta{u}$,$\Delta{v}$,函数$z=f(u,v)$相应地获得增量$\Delta{x}$
- 由假设,函数$z=f(u,v)$在点$(u,v)$处由连续偏导数(这是$f(u,v)$在$(u,v)$处可微的充分条件),这时$\Delta{z}$=$\frac{\partial{z}}{\partial{u}}\Delta{u}$+$\frac{\partial{z}}{\partial{v}}\Delta{v}$+$\epsilon_1\Delta{u}+\epsilon_2{\Delta{v}}$`(2)`(当$\Delta{u}\to{0}$,$\Delta{v}\to{0}$时,$\epsilon_1,\epsilon_2\to{0}$)
- 将式(2)两端同时除以$\Delta{t}$,得$\frac{\Delta{z}}{\Delta{t}}$=$\frac{\partial{z}}{\partial{u}}\frac{\Delta{u}}{\Delta{t}}$+$\frac{\partial{z}}{\partial{v}}\frac{\Delta{v}}{\Delta{t}}$+$\epsilon_1\frac{\Delta{u}}{\Delta{t}}+\epsilon_2\frac{\Delta{v}}{\Delta{t}}$
  - 因为$\Delta{t}\to{0}$,时$\Delta{u}\to{0}$,$\Delta{v}\to{0}$;$\frac{\Delta{u}}{\Delta{t}}\to{\frac{\mathrm{d}u}{\mathrm{d}t}}$=$u_{t}'$,$\frac{\Delta{v}}{\Delta{t}}\to{\frac{\mathrm{d}v}{\mathrm{d}t}}$=$v_{t}'$ `(2-1)`
- 所以$\lim\limits_{\Delta{t}\to{0}}\frac{\Delta{z}}{\Delta{t}}$=$\frac{\partial{z}}{\partial{u}}\frac{\mathrm{d}u}{\mathrm{d}x}$+$\frac{\partial{z}}{\partial{v}}\frac{\partial{v}}{\partial{x}}$`(3)`,由导数定义可知,这就证明了公式(1)成立

#### 证法2

- 当$x\to{x+\Delta{x}}$则$u\to{u+\Delta{u}}$,$v\to{v+\Delta{v}}$

- 函数$f(x,y)$的全增量表示为$\Delta{z}=f(u+\Delta{u},v+\Delta{v})-f(u,v)$

- 由$z=f(u,v)$在点$x$处可微(可导即可微,而且连续),从而根据微分的定义:

  - $\Delta{z}=\frac{\partial{z}}{\partial{u}}\Delta{u}$+$\frac{\partial{z}}{\partial{v}}\Delta{v}+o(\rho)$ `(4)`

    - 其中$\rho=\sqrt{(\Delta{u})^2+(\Delta{v})^2}$

  - 因为函数$u=\phi(x),v=\psi(x)$都在点$x$处可导,当$\Delta{x}\to{0}$时由导数的极限定义$\frac{\Delta{u}}{\Delta{t}}\to{\frac{\mathrm{d}u}{\mathrm{d}t}}$=$u_{t}'$ `(4-1)`,$\frac{\Delta{v}}{\Delta{t}}\to{\frac{\mathrm{d}v}{\mathrm{d}t}}$=$v_{t}'$ `(4-2)`

  - 将式(4)两端同时除以$\Delta{x}$

    - $\frac{\Delta{z}}{\Delta{x}}$=$\frac{\partial{z}}{\partial{u}}\frac{\Delta{u}}{\Delta{x}}$+$\frac{\partial{z}}{\partial{v}}\frac{\Delta{v}}{\Delta{x}}$+$\frac{o(\rho)}{\Delta{x}}$`(5)`

  - $\lim\limits_{\Delta{x}\to{0}}|\frac{o(\rho)}{\Delta{x}}|$=$\lim\limits_{\Delta{x}\to{0}}|\frac{o(\rho)}{\rho}\frac{\rho}{\Delta{x}}|$

    - =$\lim\limits_{\Delta{x}\to{0}}
      \left[|\frac{o(\rho)}{\rho}| \sqrt{
      (\frac{\Delta{u}}{\Delta{x}})^2
      +(\frac{\Delta{v}}{\Delta{x}})^2
      }\right]$
    - =$\lim\limits_{\Delta{x}\to{0}}|\frac{o(\rho)}{\rho}|$ $\lim\limits_{\Delta{x}\to{0}}\sqrt{
      (\frac{\Delta{u}}{\Delta{x}})^2
      +(\frac{\Delta{v}}{\Delta{x}})^2
      }$=$0\cdot\sqrt{\frac{\mathrm{d}u}{\mathrm{d}x}+\frac{\mathrm{d}v}{\mathrm{d}x}}=0$`(6)`

  - 式(5)两边同时取极限

    - $$
      \lim_{\Delta{x}\to{0}}\frac{\Delta{z}}{\Delta{x}}
      =\lim_{\Delta{x}\to{0}}\frac{\partial{z}}{\partial{u}}\frac{\Delta{u}}{
      \Delta{x}}
      +\lim_{\Delta{x}\to{0}}\frac{\partial{z}}{\partial{v}}\frac{\Delta{v}}{\Delta{x}}
      +\lim_{\Delta{x}\to{0}}\frac{o(\rho)}{\Delta{x}}
      $$

    - 代入(4-1,4-2,6)得$\frac{\mathrm{d}z}{\mathrm{d}x}$=$\frac{\partial{z}}{\partial{u}}\frac{\mathrm{d}{u}}{
      \mathrm{d}{x}}$+$\frac{\partial{z}}{\partial{v}}\frac{\mathrm{d}{v}}{\mathrm{d}{x}}$,即式(1)得证

### 推广

- 可以把定理推广到复合函数**中间变量**多于2个的情形

- 例如3个中间变量$u=\phi(t)$,$v=\psi(t)$,$w=\omega(t)$,由函数$z=f(u,v,w)$复合得到$z=f(\phi(t),\psi(t),w(t))$的全导数公式为$z'$=$z_{u}'u_t'$+$z_{v}'v_{t}'$+$z_{w}'w_{t}'$

  

### 例

- 设$z=e^{u-2v},u=\sin{x},v=e^x$,求$z'$
  - $z'$=$=e^{u-2v}\cdot{\cos{x}}+e^{u-2v}(-2)e^{x}$=$e^{u-2v}(\cos{x-2e^x})$
  - 将$u=\sin{x}$,$v=e^{x}$;带入到上式:$z'$=$e^{\sin{x}-2e^{x}}(\cos{x}-2e^x)$



## 多元函数与多元函数的复合

- 如果$u=u(x),v=v(x)$都在点$(x,y)$有对$x,y$的偏导数(可偏导)

- 函数$z=f(u,v)$在对应点$(u,v)$具有**连续一阶偏导数**,则**复合函数**$z=f(u(x,y),v(x,y))$在点$(x,y)$有对$x,y$的偏导数

  - $$
    \frac{\partial{z}}{\partial{x}}=\frac{\partial{z}}{\partial{u}}\frac{\partial{u}}{\partial{x}}
    +\frac{\partial{z}}{\partial{v}}\frac{\partial{v}}{\partial{x}}
    \\
    \frac{\partial{z}}{\partial{y}}=\frac{\partial{z}}{\partial{u}}\frac{\partial{u}}{\partial{y}}
    +\frac{\partial{z}}{\partial{v}}\frac{\partial{v}}{\partial{y}}
    $$

  - 即,采用复合函数**因变量**和**中间变量**的偏导表达式来描述公式

- 上面的例子是二元函数复合二元函数

### 公式结构示@变量关系图

- ```mermaid
  flowchart TB
  	z---u & v--- x & y
  ```

- 上述关系图的最后一层是复合函数的两个自变量$x,y$,它们同时是中间变量对应函数$u,v$的自变量

- 不妨把第一层的唯一节点称为根节点,最后一层的节点称为叶子节点.叶子节点个数对应于复合函数包含的**自变量**个数

- 每个叶子对应于复合函数对该叶子中的自变量的一个导数公式,该公式由该叶子相关的边决定的,边数等于公式中被求和的项的数目,而每项是通过根节点到该叶子节点决定的(运用复合函数求导法则的一个乘法链)

### 多元和多元复合与多元与一元复合两类型的联系

- 多元函数复合多元函数的情形可以转换为多元函数复合一元函数的情形
- 例如,当$z$对$x$求偏导时,将$y$视为常量,因此**二元函数**$u=u(x,y)$,$v=v(x,y)$此时可以看作是关于$x$的**一元函数**,因此可以利用**全导数公式**计算$\frac{\partial{z}}{\partial{x}}$这一**复合偏导**
  - 但由于函数$f,\phi,\psi$都是二元函数,所以全微分公式中符号$\mathrm{d}$要替换为$\partial$来得到复合偏导公式
- 类似的,求$\frac{\partial{z}}{\partial{y}}$也是如此

### 推广

- 更一般的,多元函数复合多元函数

  - $z=f(g_1(x_1,\cdots,x_p),\cdots,g_n(x_1,\cdots,x_p))$

    - $n$表示f是$n$元函数

    - $p$表示函数$g_i$中,具有最多变量的函数的变量个数

      - 函数$g_k,k\in\{1,2,\cdots,p\}$可能很简单;(甚至直接等于某个变量,例如$g_k=x_t,t\in\set{1,2,\cdots,p}$)

        

    - ```mermaid
      flowchart TB
      	z---g1  & ... & gn--- x1 & .... & xp
      ```

      

      - 树状图中的某些边可能缺失(最后一层)

  - 对某个自变量$x_i$(偏)导数的(展开公式中)项数等于最后一层格子(叶子)中$x_i$的关联边的条数

    - 展开式(加式)中(构成)各项的因子的数目=因变量(函数z)到数值末端该变量($x_i$)之间的数树数目
      - 每条路径都可构成一个项
      - 每个项含有的因子数是路径上的分支(枝干)数

