[toc]

## abstract

- 多元函数极值和最值@多元函数极值存在定理@条件极值

## 多元函数极值存在定理

- 本定理给出极值存在充分条件
- 设函数$z=f(x,y)$在点$(x_0,y_0)$的某个邻域内,且有一阶和二阶连续偏导数,又$f_{x}(x_0,y_0)=0$,$f_{y}(x_0,y_0)=0$(驻点存在),令
  - $f_{xx}(x_0,y_0)=A$
  - $f_{xy}(x_0,y_0)=B$
  - $f_{yy}(x_0,y_{0})=C$
  - $\Delta$=$AC-B^2$
- 则$f(x,y)$在点$(x_0,y_0)$处是否取得极值的条件和结论为:
  - $\Delta{>0}$时具**有极值**,
    - $A<0$时有极大值,
    - $A>0$时有极小值
  - $\Delta<0$时**没有**极值
  - $\Delta=0$时需要另外讨论才能确定

### 极值分布👺

- 这里指无条件极值
- 分为两部分讨论:所有**驻点**和**不可导点**(一阶偏导不存在的点)处都有可能是极值

### 求解步骤

#### 驻点部分

- 建立并解方程组$f_{x}(x,y)=0$;$f_{y}(x,y)=0$;得到一切**实数解**,得一切**驻点**
  - 两个方程都是一元方程,可能有多个解,方程1解设为$x_1,\cdots,x_{m}$;方程2的解设为$y_1,\cdots,y_{n}$
  - 构造驻点坐标:$(x_i,y_{j})$,$i=1,\cdots,n;j=1,\cdots,m$,共有$n\times{m}$个驻点
- 对每个驻点$(x_i,y_{j})$,求处二阶偏导数$A,B,C$
- 确定$\Delta=AC-B^2$得符号,由定理2的结论判定$f(x_i,y_{j})$是否为极值(若为极值,进一步根据$A$得符号判断该极值是极大值还是极小值)

#### 偏导不存在的点

- 如果函数存在某些偏导数不存在的点,那么这些点也可能是极值点,需要逐个判断
- 如果函数处处可偏导,则不需要处理这部分

### 例

- 求函数$f(x,y)$=$x^2-y^3+3x^2+3y^2-9x$的极值
  - 构造并解方程组:$f_{x}(x,y)=3x^2+6x-9=0$;$f_{y}(x,y)$=$-3y^2+6y=0$
  - 分别解得$x_1=1$;$x_2=-3$;$y_1=0$;$y_2=2$
  - 构造驻点$(1,0)$,$(1,2)$;$(-3,0)$;$(-3,2)$
  - 计算函数的$A,B,C$:$A=f_{xx}(x,y)=6x+6$;$B=f_{xy}(x,y)=0$,$C=f_{yy}=-6y+6$;$\Delta=AC-B^2=36(x+1)(-y+1)$
  - 分别判断驻点的判别式
    - 在$(1,0)$处,$\Delta=72>0$,且$A=12>0$,所以函数在$(1,0)$处有极小值$f(1,0)=-5$
    - $(1,2)$处,$\Delta=-72<0$,所以$f(1,2)$不是极值
    - $(-3,0)$处,$\Delta=-72<0$,$f(-3,0)$不是极值
    - $(-3,2)$处,$\Delta=72<0$,$A<0$,所以函数在$(-3,2)$处有极大值$f(-3,2)=31$

## 多元函数最值

- 与一元函数相类似,我们可以利用函数的极值来求函数的最大值和最小值(候选值).
- 如果$f(x ,y)$在有界闭区域$D$上连续,那么$f( x ,y)$在D上必定能取得最大值和最小值.
- 这种使函数取得最大值或最小值的点既可能在D的**内部**(驻点和不可导点),也可能在$D$的**边界**上.
  - 区域$D$的边界可能分为几段,也可能是无限延申(非有限封闭区域),对于限延申的边界段(记为$L_{\infin}$的采用极限的方式计算:例如$L_{\infin}$上的候选值为$\lim\limits_{x\to{\infin},y\to{\infin}}f(x,y)$
- 我们假定,**函数在D上连续、在D内可微分且只有有限个驻点**,
  - 这时如果函数在D的内部取得最大值(最小值),那么这个最大值(最小值)也是函数的极大值(极小值).

### 一般方法和步骤

- 在上述假定下,求函数的最大值和最小值的**一般方法**是
  - 将函数$f(x,y)$在D内的所有驻点处的函数值及在D的边界上的最大值和最小值**相互比较**,其中最大的就是最大值,最小的就是最小值.
  - 但这种做法,由于要求出$f(x,y)$在D的**边界**上的最大值和最小值,所以**往往相当复杂**.
- 在通常遇到的**实际问题中**,如果根据问题的性质,知道函数$f(x ,y)$的最大值(最小值)**一定在D的内部取得**,而函数在D内**只有一个驻点**,那么可以肯定该驻点处的函数值就是函数$f(x,y)$在D上的最大值(最小值).
- 如果不能排除位于边界处可能存在最值,则需要判断边界处的最值

## 应用

### 例

- 制作体积为$2$的封闭长方体水箱,设长,宽分别为$x,y$,则高为$\frac{2}{xy}$,问$x,y$分别取多少时用料最省?
- 水箱的表面积为$A=2(xy+x\frac{2}{xy}+y\frac{2}{xy})$=$2(xy+\frac{2}{x}+\frac{2}{y})$;$(0<x,y)$
  - $A_{x}$=$2(y-\frac{2}{x^2})$;$A_{y}=2(x-\frac{2}{y^2})$=0
  - $x=\sqrt[3]{2}$;$y=\sqrt[3]{2}$
  - 函数有唯一驻点$(\sqrt[3]{2},\sqrt[3]{2})$,并且在函数$A$开区域$D$内部取得,则该点一定是极值点
  - 又由题意可知,函数$A$在区域$D$内一定由最小值,所以当$x=\sqrt[3]{2}$;$y=\sqrt[3]{2}$时,函数$A$取得最小值$A(\sqrt[3]{2},\sqrt[3]{2})$,此时高度为$\frac{2}{\sqrt[3]{2}\sqrt[3]{2}}$=$\sqrt[3]{2}$

### 例

- 当$x\geqslant{0}$,$y\geqslant{0}$,时$x^2+y^2\leqslant ke^{x+y}$**恒成立**,求$k$的取值范围

  - 变形为$(x^2+y^2)e^{-(x+y)}\leqslant{k}$`(1)`恒成立,令$f(x,y)$为式(1)左边,即$k\geqslant{\max{f(x,y)}}$
  - 这是一个求二元函数的最大值问题

- 方法1:

  - 设$f(x,y)$的定义域区域为$D$

  - 求$D$内的可能最值点(驻点)

    - $f_{x}$=$e^{-(x+y)}(2x-x^2-y^2)$,令$f_{x}=0$`(2)`
    - $f_{y}$=$e^{-(x+y)}(2y-x^2-y^2)$,令$f_{y}=0$`(3)`

    - 联立(2,3),解得$x=y=0$或$x=y=1$

    - $f(0,0)$=$0$;$f(1,1)$=$2e^{-2}$
    - $D$内不存在不可导点

  - 求边界的可能最值点

    - 当$x=0$时,$g(y)=f(0,y)$=$y^2e^{-y}$.
      - $g'(y)$=$2ye^{-y}+y^2e^{-y}(-1)$=$e^{-y}(2y-y^2)$=0,可求得$y\in[0,2]$区间内单调递增,而$[2,+\infin)$内单调递减,从而函数$g(y)$在$y=2$处取得最大值$4e^{-2}$
    - 当$y=0$时,$h(x)$=$f(x,0)$=$x^2e^{-x}$,类似的有$h(x)$最大值为$4e^{-2}$

  - 综上$f(x,y)$在边界$x=0$或$y=0$处取得最大值$4e^{-2}$

- 方法2:
  - 本问题还可以使用三角代换:令$x^2+y^2=r^2$,则可以令$x=r\cos{\alpha}$;$y=r\sin\alpha$,$(\alpha\in[0,\frac{\pi}{2}])$,$r>0$
  - 从而$f(x,y)$=$f(r\cos\alpha,r\sin\alpha)$=$r^2e^{-r(\cos\alpha+\sin\alpha)}$
    - 而$t=r(\cos\alpha+\sin\alpha)$=$\sqrt{2}r\sin(\alpha+\theta)$,$\tan\theta=1$,不妨取$\theta=\frac{\pi}{4}$,从而$t=\sqrt{2}{r}\sin(\alpha+\frac{\pi}{4})$,
    - 而$\alpha+\frac{\pi}{4}\in[\frac{\pi}{4},\frac{3\pi}{4}]$,$\sin(\alpha+\frac{\pi}{4})\in[\frac{\sqrt{2}}{2}.1]$所以$t\in[r,\sqrt{2}r]$
    - 而$e^{-t}$是关于$t$的减函数,从而$f\in[r^2e^{-\sqrt{2}r},r^2e^{-r}]$,$f\leqslant{r^{2}e^{-r}}$
  - 令$h(r)$=$r^2e^{-r}$.$h'(r)$=$e^{-r}(2r-r^2)$,其中$e^{-r}>0$.从而$h'(r)$取决于$2r-r^2$=$r(2-r)$,当$r\in[0,2]$时递增,而$r>2$时递减
  - 从而$h(r)$在$r=2$处取得最大值,即$h(2)$=$4e^{-r}$,也就是$f(x,y)$的最大值
  
- 综上,$k\geqslant{4e^{-2}}$









