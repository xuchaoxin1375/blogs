[toc]

## abstract

- 二元函数偏微分@全微分及其应用@多元函数估算问题
- 函数可导,可微,连续的关系

## 偏微分

- 二元函数对某个自变量的**偏导数**表示当另一个自变量固定时,因变量相对于该自变量的**变化率**
- 根据一元函数微分学中增量和微分的关系,有
  - $f(x+\Delta{x},y)-f(x,y)\approx{f_{x}(x,y)}\Delta{x}$
  - $f(x,y+\Delta{y})-f(x,y)\approx{f_{y}(x,y)}\Delta{y}$
- 两式左端分别叫做二元函数对$x$和对$y$的偏增量,而右端分别叫做二元函数对$x,y$的**偏微分**,可以分别记为:$\frac{\partial{z}}{\partial{x}}\mathrm{d}x$和$\frac{\partial{z}}{\partial{y}}\mathrm{d}x$,习惯上,常把$\Delta{x},\Delta{y}$分别用$\mathrm{d}x,\mathrm{d}y$表示

## 全微分👺

- 研究一元函数的近似时,我们引入了一元函数微分的概念
- 为了近似计算二元函数的全增量,我们引入全微分的概念
- 和一元函数的情形一样,二元函数中,我们希望用自变量增量$\Delta{x},\Delta{y}$的**线性函数**(即全微分)来近似代替函数的全增量$\Delta{z}$

### 二元函数点处可微@点处全微分

- 设函数$z=f(x,y)$在点$P_0(x_0,y_0)$的某个领域内有定义,如果导数在点$P_0$的全增量$\Delta{z}$=$f(x_0+\Delta{x},y_0+\Delta{y})-f(x_0,y_0)$`(0-1)`(或表示为$\Delta{z}$=$f(x,y)-f(x_0,y_0)$`(0-2)`,其中$x=x_0+\Delta{x}$;$y=y_0+\Delta{y}$)

  能够表示为$\Delta{z}$=$A\Delta{x}+B\Delta{y}+o(\rho)$`(0)`(带有无穷小项的全增量分解形式)

  - 其中$A,B$不依赖于$\Delta{x},\Delta{y}$而仅和$x_0,y_0$相关($A,B$允许等于0)
  - $\rho=\sqrt{(\Delta{x})^2+(\Delta{y})^2}$=$\sqrt{(x-x_0)^{2}+(y-x_0)^2}$(后一形式可称为点$P(x,y)$处的$\rho$值)
    - 特别的,当对于点$(0,0)$处的$\rho$值,,表示为$\rho_{(0,0)}$=$\sqrt{x^2+y^2}$

- 那么称函数$z=f(x,y)$在**点$(x_0,y_0)$可微分**

- 有两种表示形式

  - $f(x,y)-f(x_0,y_0)$=$A\Delta{x}+B\Delta{y}+o(\rho)$`(0-3)`
  - $f(x_0+\Delta{x},y_0+\Delta{y})-f(x_0,y_0)$=$A\Delta{x}+B\Delta{y}+o(\rho)$`(0-4)`

- 特别的,当$(x_0,y_0)$=$(0,0)$`(0-5)`,而$\Delta{x}=x-0=x$`(0-6)`;$\Delta{y}=y-0=y$`(0-7)`,代入(0-3)的$f(x,y)-f(0,0)$=$Ax+By+o(\rho)$

- 并且$A\Delta{x}+B\Delta{y}$称为函数$z=f(x,y)$在$P_0$的**全微分**,记为$\mathrm{d}z|_{(x_0,y_0)}$=$A\Delta{x}+B\Delta{y}$`(1)`(不带无穷小项的全微分分解形式)

### 自变量增量的微分表示

- 习惯上,我们将自变量的增量$\Delta{x},\Delta{y}$分别记为$\mathrm{d}x,\mathrm{d}y$,并分别称为**自变量$x,y$的微分**



### 二元函数可微

- 若函数$f(x,y)$在区域$D$内各点处可微分,则称$f(x,y)$在区域$D$内**可微分**,简称**可微**,记为$\mathrm{d}z$



## 可微@连续@可偏导之间的关系定理



### 定理1: 函数可微分必连续👺

- 若函数$z=f(x,y)$在点$P_0(x_0,y_0)$处可微分,则函数在点$P_0$处连续


#### 证明

- 因为函数$z=f(x,y)$在$(x_0,y_0)$处可微分,则:$\Delta{z}$=$A\Delta{x}+B\Delta{y}+o(\rho)$
- 从而$\lim\limits_{(x,y)\to{x_0,y_0}}\Delta{z}$=$\lim\limits_{(x,y)\to{x_0,y_0}}{[A\Delta{x}+B\Delta{y}+o(\rho)]}$=0
- 根据连续的等价定义,函数$z=f(x,y)$,在$(x_0,y_0)$处连续

- 推论:若函数$f(x,y)$在点$P_0$处不连续,则$f(x,y)$在$P_0$处不可微

### 定理2:可微偏导必存在(可微的必要条件)

- 若$z=f(x,y)$在点$P_0(x_0,y_0)$处可微,则$f(x,y)$在该点处的两个偏导数都存在且
  - $A=f_{x}(x_0,y_0)$;`(3-1)`
  - $B=f_{y}(x_0,y_0)$`(3-2)`



#### 证明

- 在式(0)中令$\Delta{y}=0$,则全增量转换为偏增量:
  - $\rho=\sqrt{(\Delta{x})^2+0^2}$=$|\Delta{x}|$
  - $\Delta_{x}z$=$f(x_0+\Delta{x},y_0)-f(x_0,y_0)$=$A\Delta{x}+o(|\Delta{x}|)$`(4-1)`
- 所以$f_{x}(x_0,y_0)$=$\lim\limits_{\Delta{x}\to{0}}\frac{\Delta_{x}z}{\Delta{x}}$=$\lim\limits_{\Delta{x}\to{0}}\frac{A\Delta{x}+o(|\Delta{x}|)}{\Delta{x}}$=$A$这就证明了(3-1);
- 同理,令$\Delta{x}=0$,则
  - $\rho=\sqrt{0^2+(\Delta{y})^2}$=$|\Delta{y}|$
  - $\Delta_{y}z$=$f(x_0,y_0+\Delta{y})-f(x_0,y_0)$=$B\Delta{y}+o(|\Delta{y}|)$`(4-2)`
- $f_{y}(x_0,y_0)$=$\lim\limits_{\Delta{y}\to{0}}\frac{\Delta_{y}z}{\Delta{y}}$=$B$,这就证明了(3-2)
- 综上,定理成立

- 为了方便后续讨论可微和可偏导之间的差异
  - 令等式(4)的右端$\theta=f_{x}(x_0,y_0)\mathrm{d}x$+$f_{y}(x_0,y_0)\mathrm{d}{y}$`(5-1)`,不妨称为候选微分公式
  - $\delta$=$\Delta_{z}-[f_{x}(x_0,y_0)\mathrm{d}x+f_{y}(x_0,y_0)\mathrm{d}{y}]$=$\Delta{z}-\theta$`(5-2)`
  - 判断式$\Delta{z}-\theta=o(\rho)$`(5-3)`是否成立,即$\lim\limits_{\rho\to{0}}\frac{\delta}{\rho}$=$0$`(5-4)`是否成立,是判断函数$z=f(x,y)$是否可微的方法
- 例如任给一个可偏导的二元函数$z=f(x,y)$时,尽管我们可以按照公式(5-1)写出一个表达式,
  - 式(5-1)的值作为微分的前提是微分存在(可微)
  - 计算式(5-1)与$\Delta{z}$之差$\delta$不一定是$\rho$的高阶无穷小,因此(4-1)也就不一定是函数的全微分
  - 若能证明$\delta=o(\rho)$,则说明$z=f(x,y)$可微



### 定理3:可微的充分条件

- 设函数$z=f(x,y)$在点$P_0(x_0,y_0)$的某个邻域内**可偏导**,且偏导数$f_x(x_0,y_0)$,$f_{y}(x_0,y_0)$都在点$P_0$处连续,则$f(x,y)$在点$(x_0,y_0)$处**可微**


#### 证明

- 可由一元化处理和Lagrange中值定理证明
- 由条件假定,函数的偏导数在点$P(x,y)$某个邻域内存在
- 设点$(x+\Delta{x,y+\Delta{y}})$为此邻域内的任意一点,考察函数的全增量$\Delta{z}$=$f(x+\Delta{x},y+\Delta{y})$-$f(x,y)$=$[f(x+\Delta{x},y+\Delta{y})-f(x,y+\Delta{y})]$-$[f(x,y+\Delta{y})-f(x,y)]$`(0)`
  - 第一个方括号由于$y+\Delta{y}$不变,可以堪称$x$的一元函数$f(x,y+\Delta{y})$的增量
    - 类比$f(x+\Delta{y},y)-f(x,y)$,由于$y$不变,可以看作是$x$的一元函数$f(x,y)$的增量

  - 第二个中括号也类似,看作是$y$的一元函数$f(x,y+\Delta{y})$的增量
  - 由Lagrange中值定理,$f(x+\Delta{x},y+\Delta{y})-f(x,y+\Delta{y})$=$f_{x}(x+\theta\Delta{x},y+\Delta{y})\Delta{x}$,$(\theta\in(0,1))$`(1)`
  - 又依假设,$f_{x}(x,y)$在点$(x,y)$处连续,所以式(1)可以写为$f(x+\Delta{x},y+\Delta{y})-f(x,y+\Delta{y})$=$f_{x}(x,y)\Delta{x}+\epsilon_1\Delta{x}$`(2)`,
    - 其中$\epsilon$是$\Delta{x},\Delta{y}$的函数,且$\epsilon_1\to{0}(\Delta{x}\to{0},\Delta{y\to{0}})$

  - Note:
    - 由连续,$\lim\limits_{\Delta{x}\to{0};\Delta{y}\to{0}}\Delta{f_{x}}$=0;$\lim\limits_{\Delta{x}\to{0};\Delta{y}\to{0}}{[f_{x}(x+\Delta{x},y+\Delta{y})-f_{x}(x,y)]}$=$\lim\limits_{\Delta{x}\to{0};\Delta{y}\to{0}}{[f_{x}(x+\Delta{x},y+\Delta{y})]}$=$f_{x}(x,y)$
    - 由极限的无穷小表示关系,$f_{x}(x+\Delta{x},y+\Delta{y})$=$f_{x}(x,y)+\epsilon_1$,其中$\epsilon_1\to{0}(\Delta{x}\to{0},\Delta{y\to{0}})$
    - 类似的$f_{x}(x+\theta{\Delta{x}},y+\Delta{y})$=$f_{x}(x,y)+\epsilon_1$
  - 同理,式(0)的第二个方括号可以表示为$f(x,y+\Delta{y})-f(x,y)$=$f_{y}(x,y)\Delta{y}+\epsilon_2{\Delta{y}}$`(3)`
    - 其中$\epsilon_2$为$\Delta{y}$的函数,且$\epsilon_2\to{0}(\Delta{y}\to{0})$

  - 由(3),(4),在偏导数连续的假定下,$\Delta{z}$可以表示为$\Delta{z}$=$f_{x}(x,y)\Delta{x}$+$f_{y}(x,y)\Delta{y}$+$\epsilon_1\Delta{x}+\epsilon_2\Delta{y}$`(4)`
  - 对(4),令$\delta=\epsilon_1\Delta{x}+\epsilon_2\Delta{y}$,$\rho=\sqrt{(\Delta{x})^2+(\Delta{y})^2}$,则$|\frac{\delta}{\rho}|\leqslant{|\epsilon_1|+|\epsilon_2|}$
    - Note:显然$\rho\geqslant{\sqrt{(\Delta{x})^2}}$=$|\Delta{x}|$;同理$\rho\geqslant{|\Delta{y}|}$;
    - 因此$\frac{|\Delta{x}|}{\rho}\leqslant{1}$;$\frac{|\Delta{y}|}{\rho}\leqslant{1}$
    - $|\frac{\delta}{\rho}|$=${|\frac{\epsilon_1\Delta{x}}{\rho}+\frac{\epsilon_2\Delta{y}}{\rho}|}$ $\leqslant$ ${|\frac{\epsilon_1\Delta{x}}{\rho}|+|\frac{\epsilon_2\Delta{y}}{\rho}|}$ $\leqslant$ ${|\epsilon_1|+|\epsilon_2|}$,

  - 定理得证

- 此定理表明,若$f(x,y)$对$x,y$的一阶偏导数都存在且连续,则$f(x,y)$在$P_0$可微

## 计算公式和应用👺



### 全微分的计算公式👺

- 点处全微分计算:式(1)改写为:$\mathrm{d}z|_{(x_0,y_0)}$=$A\Delta{x}+B\Delta{y}$=$f_{x}(x_0,y_0)\mathrm{d}x$+$f_{y}(x_0,y_0)\mathrm{d}{y}$`(4)`
- 函数$f(x,y)$的全微分为$\mathrm{d}f$=$f_{x}\mathrm{d}x+f_{y}\mathrm{d}y$

#### 例

- 设$f(x,y)$=$xe^{xy}$,则:$\mathrm{d}f(x,y)$=$\mathrm{d}(xe^{xy})$=$(e^{xy}+xe^{xy}y)\mathrm{d}x$+$(xe^{xy}\cdot{x})\mathrm{d}y$

### 全微分叠加原理👺

- 二元函数的**全微分**等于它的**两个偏微分之和**,这个规律称为二元函数的微分符合**叠加原理**
- 多余$n$元函数也适用
  - 例如三元函数$u=f(x,y,z)$的全微分:$\mathrm{d}u$=$f_{x}(x,y,z)\mathrm{d}x$+$f_{y}(x,y,z)\mathrm{d}y$+$f_{z}(x,y,z)\mathrm{d}z$



#### 相关参考

- [一元函数和二元函数的连续@可导@可微关系](http://t.csdnimg.cn/KphkR)



### 例

- 一般的,若已知函数可微,则计算全微分直接套用公式(4)计算即可
- 例:计算$z=\cos{\frac{x}{y}}$在$P_0(\pi,2)$处的全微分
  - 先计算各个偏导数:$z_{x}(P_0)$=$-\frac{1}{y}\sin{\frac{x}{y}}|_{P_0}$=$-\frac{1}{2}$;$z_{y}(P_0)=\frac{\pi}{4}$
  - $\mathrm{d}z$=$-\frac{1}{2}\mathrm{d}x+\frac{\pi}{4}\mathrm{d}y$=$-\frac{1}{4}(2\mathrm{d}x-\pi\mathrm{d}y)$

##  全微分的应用

- 和一元函数微分类似,全微分可用来作近似计算
- 设函数$z=f(x,y)$在$P_0(x_0,y_0)$处可微,则函数在该点处的全增量为$\Delta{z}=\mathrm{d}z+o(\rho)$`(1)`
  - 其中$\Delta{z}$=$f(x_0+\Delta{x_0},y_0+\Delta{y})-f(x_0,y_0)$;$\mathrm{d}z$=$f_{x}(x_0,y_0)\Delta{x}+f_{y}(x_0,y_0)\Delta{y}$;$\rho=\sqrt{(\Delta{x})^2+(\Delta{y})^2}$
  - 当$|\Delta{x}|,|\Delta{y}|$`(P0)`很小时,有
    - $\Delta{z}\approx{\mathrm{d}z}$`(2)`;
    - 或者写成$f(x_0+\Delta{x_0},y_0+\Delta{y})-f(x_0,y_0)\approx{\mathrm{d}z}$,即$f(x_0+\Delta{x_0},y_0+\Delta{y})$=$f(x_0,y_0)+\mathrm{d}z$`(3)`
- 总之,估算增量可以用公式(2),估算函数值,可以用公式(3);前提条件都是(P0)

### 例

- 锻造一个圆柱形无盖铁桶,其**内半径**为$r$,高度为$h$,桶的侧壁和底的厚度均为$d=0.01$单位长度,求该桶需要多少铁(体积)
- 利用体积差来求解铁的体积:
  - 通过绘制示意图可知,外体积的底面半径为$r+d$;高度为$h+d$
  - 圆柱体的体积为$V(r,h)=\pi r^2h$,则$\Delta{V}\approx\mathrm{d}V$=$V_r(r,h)\mathrm{d}r+V_{h}(r,h)\mathrm{d}h$=$2\pi{hr}\mathrm{d}r+\pi{r}^2\mathrm{d}h$,
  - 令$\mathrm{d}r$=$\mathrm{d}h$=$0.01$,当$r=0.25,h=0.5$时,$\Delta{V}$近似为$9.81$

- 对比:精确式:$\Delta{V}$=$\pi(r+d)^2(h+d)$-$\pi{r^2}h^2$

- ![在这里插入图片描述](https://img-blog.csdnimg.cn/b13bf37904914e55a7827558036edc58.png)

### 例

- 以较高的精度估算$z=(1.04)^{2.02}$
  - 令$f(x,y)$=$x^y$
  - 则$z=f(1.04,2.02)=f(1+0.04,2+0.02)$ $\approx$ $\mathrm{d}z|_{(1,2)}+f(1,2)$
    - $\mathrm{d}x=0.04$;$\mathrm{d}y=0.02$
    - $\mathrm{d}z|_{(1,2)}$=$f_{x}(1,2)\mathrm{d}x$+$f_{y}(1,2)\mathrm{d}y$=$2\times{0.04}+0$=$0.08$
    - $f(1,2)$=$1$
    - 所以$z\approx{1.08}$

