[toc]

## 有理系数多项式

- 有理数域上一元多项式的因式分解.
- 作为**因式分解定理**的一个特殊情形,我们有结论:
  - <u>每个次数大等于1的**有理系数多项式**都能**唯一地**分解成**不可约的有理系数多项式**的乘积.</u>
  - 有理数域版本中,从一般数域具体到了"**有理系数**"
  - 我们讨论多项式的时候,都假设多项式是在某个数域P内的,例如一般数域P中的结论在特殊数域(有理数域Q)中也是成立的
- 相关难题
  - 对于**任意**一个给定的多项式,要**具体地作出它的分解式**却是一个很复杂的问题
  - 即使要判别一个有理系数多项式**是否可约**也不是一个容易解决的问题
    - 这一点是**有理数域**与实数域,复数域不同的.
    - 在**复数域**上只有**一次多项式**才是不可约的
    - 而在实数域上不可约多项式只有**一次**的和**某些二次**的.
- 这里主要是指出有理系数多项式的两个重要的事实.
  - 有理系数多项式的因式分解的问题,可以归结为**整(数)系数**多项式的因式分解问题,并进而解决求**有理系数多项式的有理根**的问题.
  - 在有理系数多项式环中有**任意次数的不可约多项式**.

## 本原多项式

### 一般多项式到整系数多项式

- 设$f(x)=\sum_{i=1}^{n}a_{i}x^{i}$是一个有理数多项式,取适当的整数$c$乘以$f(x)$,总是可以使$G(x)=cf(x)$是一个**整系数多项式**(例如c取$a_i,i=1,2,\cdots,n$的最小公倍数)
- 如果$G(x)$的各系数有公因子$d$,就可以提取出来:$G(x)=dg(x)$,从而$f(x)=\frac{d}{c}g(x)$
  - 记$r=\frac{d}{c}$,$f(x)=rg(x)$
  - 其中$g(x)$是**整系数多项式**,且各项系数没有异于$\pm{1}$的公因子(各项系数公因子只有$\pm{1}$)
  - 例如:$f(x)=\frac{2}{3}x^4-2x^2-\frac{2}{5}x$=$\frac{2}{15}(5x^4-15x^2-3x)$
    - 其中c=15,d=2

### 本原多项式定义

- 如果一个非零的整系数多项式$g(x)=\sum_{i=0}^{n}b_ix^i$的系数$b_i,i=1,2,\cdots,n$的公因式只有$\pm{1}$,也就是说$b_{i},b_{j},i\neq{j}$是**互素**的

- 上一小节的讨论中可知,任意**非零的有理系数多项式**$f(x)$都可以表示称一个**有理数**$r$与一个**本原多项式**$g(x)$的**乘积**,即$f(x)=rg(x)$

  - 这种表示法除了相差一个正负号是唯一的
  - 若$f(x)=rg(x)=r_1g_1(x)$,其中$g(x),g_1(x)$都是本原多项式,则$r=\pm{r_1}$,$g(x)=\pm{g_1(x)}$

  - 由于$f(x)$和$g(x)$只差一个**常数倍**,所以$f(x)$的因式分解问题可以归结为本原多项式$g(x)$的因式分解问题

- 一个本原多项式能否分解为<u>两个次数较低的有理系数多项式的乘积</u>和它能否分解为两个次数较低的整系数多项式乘积的问题是一致的

- 该结论的证明需要一些准备知识

### 高斯引理

- 两个**本原多项式的乘积**还是**本原多项式**
- 证明:利用反证法证明
  - 设$f(x)=\sum_{i=0}^{n}a_ix^i$,$g(x)=\sum_{i=0}^{m}b_ix^i$是两个本原多项式
    - 则$h(x)=f(x)g(x)=\sum_{i=0}^{n+m}d_{i}x^{i}$
  - 令$D_a=\{0,1,2,\cdots,n\}$,$D_b=\{0,1,2,\cdots,m\}$,$D_s=\{0,1,2,\cdots,n+m\}$
  - 如果$h(x)$不是本原的,即$h(x)$的系数$d_{i},\forall{i}\in{D_s}$有非$\pm{1}$的公因子,则存在一个素数$p$,满足$p|d_i,\forall{i}\in{D_s}$
  - 由于$f(x)$是本原的,所以$p$不满足$p|a_i,\forall{i}\in{D_a}$,设$a_i$是第一个不能被$p$整除的系数($p\nmid{a_i}$),而$(p|a_{k},k=0,1,2,\cdots,i-1)$
  - 同样地,$g(x)$也是本原的,设$b_j$是第一个不能被$p$整除的系数,$p\nmid{b_{j}}$,$(p|b_k,k=0,1,2,\cdots,j)$
  - 而$h(x)$的$s=i+j$次项的系数$d_{s}=\sum_{r_1+r_2=s}a_{r_1}b_{r_2}$,$r_1\in{D_{n}},r_2\in{D_{m}}$,
    - 为了更直观,将等式右边展开$(a_{i}b_j+a_{i+1}b_{j-1}+\cdots)$+$(a_{i-1}b_{j+1}+a_{i-2}b_{j+2}+\cdots)$,
    - 分别记:$T(x)=a_{i}b_j$,$U(x)=(a_{i+1}b_{j-1}+a_{i+2}b_{j-2}+\cdots)$;$V(x)=(a_{i-1}b_{j+1}+a_{i-2}b_{j+2}+\cdots)$
    - 对于$T(x)$,$p\nmid{a_{i}},p\nmid{b_{j}}$,且$p$是一个质数,所以$p\nmid{a_{i}b_{j}}$
    - 对于$U(x)$,因为$b_{j-1},b_{j-2},\cdots$均可被p整除,所以$U(x)$也可以被$p$整除
    - 对于$V(x)$,因为$a_{i-1},a_{i-2},\cdots$均可被$p$整除,所以$V(x)$也可以被p整除,
    - 等式右边记为$RHS=T(x)+U(x)+V(x)$是否可以被$p$整除取决于$T(x)$,所以$p\nmid RHS$
    - 而由假设条件:$p|d_{s}$因此等式左边可以被p整除,这和等式右边不可以被p整除矛盾
  - 所以$h(x)$一定是本原多项式

### 整系数多项式分解定理

- 若**非零整系数**多项式$f(x)$能够分解为两个**次数较低**的**有理系数**多项式的乘积,则$f(x)$一定能分解为**两个**次数较低的**整系数**多形式的乘积
- 证明:
  - 设$f(x)$具有分解式$f(x)=g(x)h(x)$,其中$g(x),f(x)$都是有理系数多项式
  - $\partial(g(x)),\partial(h(x))<\partial(f(x))$
  - 令$f(x)=af_1(x)$,$g(x)=rg_1(x)$,$h(x)=sh_1(x)$,这里:
    - $f_1(x),g_1(x),h_1(x)$都是本原多项式
    - $a\in\mathbb{Z}$,$r,s\in{\mathbb{Q}}$
  - 则$af_1(x)=rs\cdot{g_1(x)h_1(x)}$
- 由高斯引理,$G(x)=g_1(x)h_1(x)$是本原多项式,从而$rs=\pm{a}$,从而$rs\in{\mathbb{Z}}$
- 所以有$f(x)=(rs\cdot g_1(x))h_1(x)$,其中$rs\cdot{g_1(x)},h_1(x)$都是整系数多形式,且次数都是低于$f(x)$的次数

#### 推论

- 设$f(x),g(x)$是整系数多项式,其中$g(x)$还是**本原**的,若$f(x)=g(x)h(x)$且$h(x)$是**有理系数多项式**,则$h(x)$一定还是**整系数的**有理多项式
- 证明:下面的证明中,前两种方式是类似地而且是有效的

##### 思路1:

- 由本节定理和推论条件可知$f(x)$可以分解为两个整系数多项式的乘积,设为$f(x)=g(x)bh_1(x)$,其中$h(x)=bh_1(x)$,$h_1(x)$是一个本原多项式
- 由高斯引理$G(x)=g(x)h_1(x)$是一个本原多项式,而$f(x)$是一个整系数多项式,可见$b\in{\mathbb{Z}}$
- 从而$h(x)=bh_1(x)$是一个整系数多项式

##### 思路2:

- 利用一般有理多项式可以表示为一个有理数乘以本原多项式的特点来推理
- 设$f(x)=af_1(x)$,$h(x)=bh_1(x)$,其中$f_1(x),h_1(x)$都是本原多项式
  - 而$f(x)$是整系数多项式,所以$a\in{\mathbb{Z}}$
  - 而$b\in{\mathbb{Q}}$
- 此时$f(x)=g(x)h(x)$改写为$af_1(x)=g(x)(bh_1(x))$=$bg(x)h_1(x)$
  - 由高斯引理,$G(x)=g(x)h_1(x)$是一个本原多项式和$|G(x)/f_1(x)|=1$
  - 可知$a=\pm{b}$(或说$|a|=|b|$),所以$b\in{\mathbb{Z}}$

- 因此$h(x)=bh_1(x)$是一个整系数多项式



##### 思路3:(废弃)

- 设$g(x)=\sum_{i=0}^{m}a_{i}x^{i}$,$b_i\in\mathbb{Z}$,$h(x)=\sum_{i=0}^{n}b_ix^i$
- 根据多项式乘法,设$f(x)=g(x)h(x)=\sum_{i=1}^{m+n}c_{i}x^{i}$,$m,n$分别式$g(x),h(x)$的次数
- 由于$g(x)$是本原的,所以$a_i\in{\mathbb{Z}}$,且$a_i,i=0,1,2,\cdots,m$的公因子只可能是$\pm{1}$
- 若$h(x)$的系数不都为整数,则$c_{i}=\sum_{r_1+r_2=i}a_{r_1}b_{r_2}\in\mathbb{Z}$

- 若$b_k$不是整数
  - 若$b_r,r\neq{k}$是整数,则$c_i$不是整数
  - 若$b_r,r\neq{k}$不全是整数,情况复杂一些,因为多个有理数之和的结果仍然有可能是整数,不能直接断定$c_i$不是整数

## 整系数多项式有理根定理与整根定理

- 设$f(x)=\sum_{i=0}^{n}a_ix^{i}$是一个**整系数多项式**,若$\frac{r}{s}$是$f(x)$的一个**有理根**($r,s$为互素整数),那么有整除关系:$r|a_0$,$s|a_{n}$
  - 若$r,s$不仅互素而且$|s|=1$,则有理根$\frac{r}{s}\in\mathbb{Z}$
- 定理表明,若$f(x)$的首相系数$a_n=1$,则由$s|1$,即$|s|=1$,因此,$f(x)$的**有理根**都是**整根**,并且是$a_0$的**因子**
  - 尽管如此,$g(x)=x^n+\sum_{i=0}^{n-1}a_{i}x^{i}$都有有理根,也就无法直接确定其是否有整根
  - 但是我们可以尝试判断$g(x)$的常数项$a_0$的因子,因为这类情况下,如果整根存在,一定是$a_0$的因子;特别是$a_0$为素数的时候,容易快速检验方程是否有整根

### 证明

- 因为$\frac{r}{s}$是$f(x)$的一个有理根,则$(x-\frac{r}{s})|f(x)$(余式定理),因此有$(sx-r)|f(x)$
- 因为$r,s$互素,$t(x)=sx-r$是一个一次本原多项式
- 根据上一节中的整系数分解推论,可设$f(x)=(sx-r)(\sum_{i=0}^{n-1}b_ix^{i})$,其中$b_i\in{\mathbb{Z}},i=0,1,\cdots,n-1$
- 比较$f(x)$的两种展开形式$\sum_{i=0}^{n}a_ix^{i}$=$(sx-r) \sum_{i=0}^{n-1}b_ix^{i}$,$a_n=sb_{n-1}$,$a_0=-rb_0$
- 因此,$s|a_n$,$r|a_0$

- 例:$f(x)$=$2x^4-x^3+2x-3=0$的有理根求解

  - 根据本节定理,

    - 求解$a_0=-3$在整数范围内的因子:$-3,-1,1,3$,简写为$\pm{1},\pm{3}$
    - 求解$a_n=2$在整数范围内的因子:$-2,-1,1,2$,简写为$\pm{1},\pm{2}$
    - 所有可能的有理根候选:$\pm{1}$,$\pm\frac{{1}}{2}$,$\pm{3}$,$\pm\frac{3}{2}$
    - 有两种方式检验这些根,通常推荐先检验整根
      - 逐个代入这8个根检验
      - 利用带余除法来检验
      - 混合两种方法,先用代入法检验整根,然后用带余除法检验非整根
    - 经检验,$x=-1,\pm{3}$不是根,$x=1$是根,$f(x)=(x-1)(2x^3+x^2+x+3)$

    - 对$f_1(x)=2x^3+x^2+x+3$继续检验剩余候选根
    - 只有$x=1$是根,即$f(x)=0$的有理根仅有$x=1$

- 例:$f(x)=x^3-5x-1$在有理数域上不可约

  - 证明,根据整系数多项式有理根定理,$a_0=1$,因子有$\pm{1}$,$a_n=1$,因子为$\pm{1}$
  - 因此方程的有理根只可能是$\pm{1}$
  - 经检验,$f(1)=-5\neq{0}$,$f(-1)=3\neq{0}$,所以方程没有有理根,从而$f(x)$在有理数域内不可约

## 爱森斯坦判别法

- 设$f(x)=\sum_{i=0}^{n}a_ix^{i}$是一个**整系数多项式**,若存在**素数**$p$使得下列三个条件成立(称为条件组E),则$f(x)$在有理数域内**不可约**
  - (1)$p\nmid{a_n}$
  - (2)$p|a_{i},i=0,1,\cdots,n-1$
  - (3)$p^2\nmid{a_0}$
- 证明:
  - 这里用反证法证明,假设$f(x)$在有理数域上可约,然后说明可约会导致矛盾,从而证明不可约
  - 若$f(x)$在有理数域上可约,则$f(x)$可以分解为两个次数较低的**整系数多项式**乘积:
  - $f(x)=\sum_{i=0}^{l}b_{i}x^{i}\sum_{j=0}^{m}c_jx^{j}$,其中$l,m<n$;$l+m=n$
    - 显然$a_n=b_lc_m$,$a_0=b_0c_0$
  - 由条件(2),$p|a_0$,即$p|b_0c_0$,所以$p|b_0$或$p|c_0$
  - 由条件(3),$p^2\nmid{a_0}$,即$p^2\nmid b_0c_0$,所以$p$不能同时整除$b_0,c_0$
    - 不妨设$p|b_0$且$p\nmid{c_0}$(4),
    - 由条件(1),$p\nmid{a_n}$有$p\nmid{b_l,c_m}$,即$b_i,i=1,2,\cdots,l$中至少存在一个不能被p整除
    - 假设$b_i,i=1,2,\cdots,l$中第一个不能被$p$整除的是$b_k$(5)
    - 比较$f(x)$的$x^k$的系数$a_k$,$a_k=\sum_{i=0}^{k}b_ic_{k-i}$
    - 由于$a_k,b_i(i=0,1,\cdots,k-1)$都能够被$p$整除,即$\sum_{i=0}^{k-1}b_ic_{k-i}$能够被$p$整除,所以$b_kc_0$也能被$p$整除
    - 而$p$是一个素数,所以$b_k,c_0$中至少有一个能够被$p$整除(6)
    - (4,5)和(6)产生矛盾,所以有理数域内$f(x)$在条件组E下不可约

### 构造任意次数的有理系数不可约多项式

- 判断$f(x)=x^n+2$在有理数域是否可约
- $a_n=1$,$a_0=2$
- 根据爱森斯坦判别法,不妨取素数p=2,则$p\nmid{a_n}$,$p^2\nmid{a_0}$且$p|a_i,i=0$,从而$f(x)$在有理数域不可约
- 这表明,在有理数域上,存在任意次数的不可约多项式

