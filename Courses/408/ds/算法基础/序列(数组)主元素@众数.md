

## abstract

寻找众数

## 普通算法

```c

/*
时间复杂度为O(n),空间复杂度为O(n)
 */
int get_main(int *a, int len)
{

    // iprint(len);
    // int s[2][len];
    int s[len];
    int h = len / 2;
    for (int i = 0; i < len; i++)
    {
        s[i] = 0;
    }

    for (int i = 0; i < len; i++)
    {
        s[a[i]]++;
    }
    for (int i = 0; i < len; i++)
    {
        // iprint(s[i]);
        printf("s[%i] = %i\n", i, s[i]);
        if (s[i] > h)
        {
            return i;
        }
    }
    return -1;
}
```

## 低空间复杂度算法

```c
int Majority(int A[], int n)
{
    int i, c, count = 1; // 用来保存候选众数元素，count 用来计数
    c = A[0]; // 设置 A[0] 为候选众数元素

    // 查找候选众数元素
    for (i = 1; i < n; i++)
    {
        if (A[i] == c)
            count++;
        else if (count > 0)
            count--;
        else
        {
            c = A[i]; // 更换候选众数元素，重新计数
            count = 1;
        }
    }

    // 统计候选众数元素的实际出现次数
    if (count > 0)
        for (i = 0; i < n; i++)
            if (A[i] == c)
                count++;

    // 确认候选众数元素
    if (count > n / 2)
        return c;
    else
        return -1; // 不存在众数元素
}
```

这段代码的主要逻辑如下：

1. 初始化 `c` 为数组的第一个元素 `A[0]`，并设置 `count` 为 1。
2. 遍历数组，如果当前元素等于 `c`，则增加 `count`；否则，如果 `count` 大于 0，则减少 `count`；如果 `count` 为 0，则更新 `c` 为当前元素，并将 `count` 重置为 1。
3. 最后，再次遍历数组，统计 `c` 的实际出现次数。
4. 如果 `c` 的出现次数超过数组长度的一半，则返回 `c`；否则，返回 -1 表示没有多数元素。

这段代码使用了Boyer-Moore投票算法的核心思想，有效地找到了数组中的多数元素。

## 运行调试

```c
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#define iprint(expr) printf(#expr " = %i\n", expr)  // 整型值调试宏:打印整数表达式


int main()
{
    extern int get_main(int *a, int len);
    extern int Majority(int A[], int n);
    int a[] = {1, 2, 3, 2, 2, 2, 5, 4, 2};
    int len = sizeof(a) / sizeof(int);
    // int res = get_main(a, len);
    int res = Majority(a, len);
    iprint(res);
}
```

