[toc]




## 逻辑代数的基本运算规则 🎈

### 代入规则

- 任何一个含有变量A的等式,如果将所有出现变量A的地方都代换成一个逻辑函数式F,则代换后的等式仍然成立。 

- 代入规则可以扩展所有基本公式或定律的应用范围 

### 反演规则

- 和概率论汇总的对偶向对应

- 对于任意逻辑函数表达式F,若将F中所有

  - 运算符

  - 常量

  - 变量

  - 作如下变换,得到的新函数式F,称为原函数F的**反函数**


$$
    \begin{array}{cccccc}
    \cdot & + & 0 & 1 & \text { 原变量 } & \text { 反变量 } \\
    \downarrow & \downarrow & \downarrow & \downarrow & \downarrow & \downarrow \\
    + & \cdot & 1 & 0 & \text { 反变量 } & \text { 原变量 }
    \end{array}
$$



- $'\cdot'\leftrightarrow{'+'}$
  - $'\cdot'$可能是隐含而不显式写出,需要换原出来并转换为'+'
- $Var\leftrightarrow{\overline{Var}}$
  - $\overline{ComplexVar}\leftrightarrow{\overline{ComplexVar}}$
  - 意思是说,单变量(简单变量才需要取**非号**' $\overline{\quad}$ ')
  - 符合变量(表达式的非号保留!)
- $0\leftrightarrow{1}$
- 对称地,反之也成立

- 运用反演规则时应注意两点： 

  - ① 不能破坏原式运算的优先顺序
    - 先算括号里和非号下的,然后按“先与后或”的原则 
    - 运用的时候(注意**加括号**)
  - ②🎈 <u>不属于单变量</u>上的**非号应保留不变**。

#### 例

- $$
  L=\overline{A}\ \overline{B}+CD+0
  \\
  \overline{L}=(A+B)\cdot({\overline{C}+\overline{D}})\cdot{1}
  \\
  若  F=\overline{A B+C} \cdot D+A C ,
  则  \overline{F}=[(\overline{\overline{A}+\overline{B}) 
  \cdot \overline{C}}
  +\overline{D}](\overline{A}+\overline{C}) ;
  \\
  若  F=A+\overline{B}+\overline{C+\overline{\overline{D}+E}} , 
  则  \overline{F}=\overline{A} \cdot B \cdot \overline{\overline{C} \cdot \overline{D \cdot \overline{E}}}  。
  $$



### 对偶规则

- 这里的对偶规则 和普通意义(命题逻辑/集合论)的对偶规则有些不同
- 数字逻辑对偶规则比**反演**的操作步骤更少一些

- 对于任意逻辑函数表达式F,若将F中所有<u>运算符,常量作</u>做如下变换,

  - 得到的新函数式$F^*$,称为原函数F的对偶式


$$
    \begin{array}{cccccc}
    \cdot & + & 0 & 1 &  \\
    \downarrow & \downarrow & \downarrow & \downarrow   \\
    + & \cdot & 1 & 0 &  
    \end{array}
$$



- 简单一句话:**与/或**符号对换(取代)
  - 包括隐藏的**与**号
  - 依然注意加括号(原则是转换前,就可以将括号划分出来)

- 运用对偶规则时应注意： 
  - ① 保持原式运算的优先次序； 
  - ② 原式中的<u>长短**非号**不变</u>(所有非号不变)； 
  - ③ 单变量的对偶式为自己。

#### 例

- $$
  若  \mathbf{F}=\mathbf{A} \overline{\mathbf{B}}+\mathbf{C} \overline{\mathbf{D}}  
  \\则  \mathbf{F}^{*}=(\mathbf{A}+\overline{\mathbf{B}})(\mathbf{C}+\overline{\mathbf{D}})
  $$

  

- ​	
  $$
  若  \mathbf{F}=\overline{\mathbf{A}+\overline{\mathbf{B}}+\overline{\mathbf{C}+\mathbf{D}+\overline{\mathbf{E}}}} \quad  
  \\则  \mathbf{F}^{*}=\mathbf{A} \overline{\mathbf{B} \mathbf{C D} \overline{\mathbf{E}}}
  $$

-  证明加对乘的分配律:
  $$
  已知  \mathbf{A}(\mathrm{B}+\mathbf{C})
  =\mathbf{A B}+\mathbf{A C} \quad \overrightarrow{\text { 对偶关系 }} \quad \mathbf{A}+\mathbf{B C}
  \\=(\mathbf{A}+\mathbf{B})(\mathrm{A}+\mathbf{C})
  $$

### 小结🎈

- 运用对偶律和反演律时,首先划分括号
- 在执行符号替换规则



## 对偶律(DeMorgan律)🎈

- 形式逻辑中此定律表达形式

- 在[命题逻辑](https://zh.wikipedia.org/wiki/命题逻辑)和[逻辑代数](https://zh.wikipedia.org/wiki/逻辑代数)中

  - **德摩根定律**（英语：De Morgan's laws,或称**笛摩根定理**,**对偶律**）
  - 是关于[命题](https://zh.wikipedia.org/wiki/命题)逻辑规律的一对法则[[1\]](https://zh.wikipedia.org/wiki/德摩根定律#cite_note-cihai-1)。
  - 19世纪英国数学家[奥古斯塔斯·德摩根](https://zh.wikipedia.org/wiki/奧古斯塔斯·德摩根)首先发现了在命题[逻辑](https://zh.wikipedia.org/wiki/逻辑)中存在着下面这些关系：

  

  - $$
    \begin{array}{l}
    \neg(p \wedge q) \equiv(\neg p) \vee(\neg q) \\
    \neg(p \vee q) \equiv(\neg p) \wedge(\neg q)
    \end{array}
    $$

    

    - 非  (p  且  q)  等价于 (非  p  ) 或 (非  q  )
    - 非  (p  或  q)  等价于 (非  p  ) 且 (非  q)

  - 德摩根定律在[数理逻辑](https://zh.wikipedia.org/wiki/数理逻辑)的定理推演中,在[计算机](https://zh.wikipedia.org/wiki/电子计算机)的逻辑设计中以及[数学](https://zh.wikipedia.org/wiki/数学)的[集合运算](https://zh.wikipedia.org/wiki/集合运算)中都起着重要的作用[[1\]](https://zh.wikipedia.org/wiki/德摩根定律#cite_note-cihai-1)。
  - 他的发现影响了[乔治·布尔](https://zh.wikipedia.org/wiki/乔治·布尔)从事的逻辑问题[代数](https://zh.wikipedia.org/wiki/代数)解法的[研究](https://zh.wikipedia.org/wiki/研究),这巩固了德摩根作为该规律的发现者的地位,[亚里士多德](https://zh.wikipedia.org/wiki/亚里士多德)亦曾注意到类似的现象,且这也为[古希腊](https://zh.wikipedia.org/wiki/古希腊)与[中世纪](https://zh.wikipedia.org/wiki/中世纪)的[逻辑学家](https://zh.wikipedia.org/wiki/逻辑学家)熟知 

- 在集合论/概率论 中：

  - $$
    \begin{array}{l}
    (A \cap B)^{C}=A^{C} \cup B^{C} \\
    (A \cup B)^{C}=A^{C} \cap B^{C}
    \end{array}
    $$

    - $F^C表示对逻辑表达式F取反,相当于F^C=\overline{F}$
    - 运用公式的时候,有三层变化:
      - 变量取反(A,B$\leftrightarrow{A^c,B^c}$)
      - 交/并号替换($\cup\leftrightarrow {\cap}$)
      - 表达式整体取反(区非)号($F\leftrightarrow{F^c}$)

### 利用对偶律证明某些代数公式(等式)

- 从对偶律的定义可以知道
  - 假设两个逻辑代数式
    - $F_1,F_2的对偶式分别为F_1^{*},F_2^{*}$
      - 如果$F_1^{*}=F_{2}^{*}$那么$F_1=F_2$(互为充要条件)
    - 并且$F_1^{*},F_{2}^{*}$形式比较间接,有利于推导/化简,
      - 那么通过验证$F_1^{*},F_{2}^{*}$是否相等
      - 来验证,$F_1=F_2$是否成立
    - Note:
      - 必须是原式的两个**对偶式之间**比较,不可以是<u>原式和对偶式</u>比较!(没有意义)

## 代数式公式

###  消去律 

$$
\mathbf{A B}+\mathbf{A} \overline{\mathbf{B}}=\mathbf{A}
$$



### 吸收律

#### 吸收律1

$$
\mathrm{A}+\mathrm{AB}=\mathrm{A}
\\
证明:
\\
\mathbf{A}+\mathbf{A B}=\mathbf{A}(1+\mathrm{B})=\mathbf{A} \cdot \mathbf{1}=\mathbf{A} \\
$$

#### 吸收律2

$$
\mathbf{A}+\overline{\mathbf{A}} \mathbf{B}=\mathrm{A}+\mathrm{B} 
\\
证明:
\\
\begin{array}{l}
\mathbf{A}+\overline{\mathbf{A}} \mathbf{B}=(\mathbf{A}+\overline{\mathbf{A}})(\mathbf{A}+\mathbf{B}) \quad \text { 根据对偶规则 } 
\\
=\mathbf{1} \cdot(A+B)=A+B \quad \longrightarrow A(A+B)=A B \\
\end{array}
$$



- $$
  记F_1=A+\overline{A}B
  \\F_2=A+B
  \\F_1^{*}=A(\overline{A}+B)=A\overline{A}+AB=AB
  \\F_2^{*}=AB
  \\可见F_1^*=F_2^*=AB
  \\\therefore{F_1=F_2},即\mathbf{A}+\overline{\mathbf{A}} \mathbf{B}=\mathrm{A}+\mathrm{B} 
  $$

  

- 事实上,从集合论的角度容易理解吸收律(几何Venn图)

  

###   冗余律

$$
\mathrm{AB}+\overline{\mathrm{A}} \mathbf{C}+\mathrm{BC}=\mathrm{AB}+\overline{\mathrm{A}}C
$$

- 证明

  - $$
    \begin{array}{l}
    \mathbf{A B}+\overline{\mathbf{A}} \mathbf{C}+\mathbf{B C} \\
    \begin{array}{l}
    =\mathbf{A B}+\overline{\mathbf{A}} \mathbf{C}+(\mathbf{A}+\overline{\mathbf{A}}) \mathbf{B C} \\
    =\mathbf{A B}+\overline{\mathbf{A}} \mathbf{C}+\overline{\mathbf{A B C}}+\overline{\mathbf{A}} \mathbf{B C}
    \end{array}
    \\
    \text { 根据对偶规则 } \quad(\mathrm{A}+\mathbf{B})(\overline{\mathrm{A}}+\mathbf{C})(\mathbf{B}+\mathbf{C}) \\
    =A B(1+C)+\bar{A} C(1+B) \\
    =(\mathbf{A}+\mathbf{B})(\overline{\mathrm{A}}+\mathrm{C}) \\
    =\mathrm{AB}+\mathrm{A} C \\
    \mathrm{AB}+\overline{\mathrm{A}} \mathrm{C}+\mathrm{BCD}=\mathrm{AB}+\overline{\mathrm{A}} \mathbf{C} \\
    \end{array}
    $$

    - $$
      F_1=AB+\overline{A}C+BC
      \\
      F_2=AB+\overline{A}C
      \\F_1^{*}=(A+B)(\overline{A}+C) (B+ C)=BC+AC+B\overline{A}
      \\F_2^{*}=(A+B)(\overline{A}+C)=AC+BC+B\overline{A}
      \\可见:F_1^{*}=F_2^{*}
      \\从而F_1=F_2
      \\原等式成立
      $$

      

### 交叉互换律

$$
\mathbf{A B}+\overline{\mathrm{A}} \mathbf{C}
=(\mathrm{A}+\mathrm{C})(\overline{\mathrm{A}}+\mathrm{B})
$$



## 集合论基础

- $$
  集合  A , B  ,若  \forall a \in A  ,有  a \in B \therefore A \subseteq B  。
  \\则称  A  是  B  的子集,亦称  A  包含于  B  ,或  B  包含  A  ,
  记作  A \subseteq B  或  B \supseteq A  ,
  \\
  否则称  A  不是  B  的子集
  记作  A \nsubseteq B  或  B \nsupseteq A  。 
  \\若  A \subseteq B  ,且  A \neq B  ,
  则称  A  是  B  的真子集,亦称  A  真包含于  B  ,或  B  真包含  A  ,
  \\记作  A \varsubsetneqq B  或  B \supsetneqq A  (有时也记作  A \subset B  或  B \supset A  )。
  $$

- 真包含关系

  - $$
    \\
    \varsubsetneqq  是集合间的一个严格偏序关系，因为它有如下性质:
    \\
    反自反性:  \forall  集合  S ， S \varsubsetneqq S  都不成立;
    \\
    非对称性:  A \varsubsetneqq B \Rightarrow B \varsubsetneqq A  不成立；反之亦然；
    \\
    传递性:  A \varsubsetneqq B  且  B \varsubsetneqq C \Rightarrow A \varsubsetneqq C ;
    $$

    

- 包含关系

  - $$
    \\
    \subseteq 是集合间的一个非严格偏序关系，因为它有如下性质：
    \\
    自反性:  \forall  集合  S, S \subseteq S ; （任何集合都是其本身的子集)
    \\
    反对称性:  A \subseteq B  且  B \subseteq A \Leftrightarrow A=B  ；
    （这是证明两集合相等的常用手段之一）
    \\
    传递性:  A \subseteq B  且  B \subseteq C \Rightarrow A \subseteq C ;
    $$

    

  

### deMorgan律证明

- 从集合论的角度

  - [集合 (数学) 维基百科,自由的百科全书 (wikipedia.org)](https://zh.wikipedia.org/wiki/集合_(数学))

  - 对$\forall A,B,总有:$

  - $$
    A\sub{(A\cup{B})}
    \\A\cup{A}=A\cap{A}=A
    $$

    

  - $$
    设P_1\sub{Q_1};Q_1=P_1\cup{M_1}
    \\P_2\sub{Q_2};Q_2=P_2\cup{M_2}
    \\Q_1Q_2=(P_1\cup{M_1})(P_2\cup{M_2})=P_1P_2\cup P_1M_2\cup M_1P_2\cup M_1M_2
    \\Q_1\cup{Q_2}=P_1\cup{M_1}\cup{P_2}\cup{M_2}
    \\显然:
    \\P_1P_2\sub{Q_1Q_2}
    \\
    (P_1\cup{P_2})\sub{(Q_1\cup{Q_2})}=P_1\cup{M_1}\cup{P_2}\cup{M_2}
    $$

    

    

  - $$
    设A\sub{B}
    \\记B=A\cup C;(AC={\varnothing})
    \\全集S=\Omega
    \\A^c=S-A
    \\B^c=S-B=S-(A\cup{C})
    \\容易知道B^c\sub{A^c}(当A\sub{B})
    \\从Venn图几何意义也可以直观理解
    \\A=\{x|x\in{A}\}
    \\B=\{x|x\in{A}或x\in{C}\}
    \\A^c=\{x|x\notin{A}\}
    \\B^c=\{x|x\notin{A}且x\notin{C}\}
    \\B^c的要求苛刻,元素空间比A^c窄
    $$
  
    
  
  - $$
    A{B}\sub{A},B\sub{A\cup{B}}
    \\
    (A\cup{B})^c\sub{A^c,B^c}\sub{(AB)^c}
    $$
  
    
  
    - $$
      (A\cup{B})^c\sub{A^cB^c},(A^c\cup{B^c})\sub{(AB)}^c\tag{T1}
      $$
  
      
  
  - 类似地有(对称地/不失一般性的,代入$A:A^c,B:B^c$)
  
    - $$
      (A^c\cup{B^c})^c\sub{AB},(A\cup{B})\sub{(A^cB^c)}^c
      $$
  
      
  
    - 再次利用取反规律:
      $$
      {A^cB^c}\sub{{(AB)^c},(A\cup{B})^c}\sub{(A^c\cup{B^c})}\tag{T2}
      $$
      
  
  - 比较$T_1,T_2$(得到两组利用对称性夹逼证明集合相等)
    $$
    (A\cup{B})^c\sub{A^cB^c}\sub{(A\cup{B})^c}
    \\
    (A^c\cup{B^c})\sub{(AB)^c}\sub{(A^c\cup{B^c})}
    \\即
    \\(A\cup{B})^c=A^cB^c
    \\或
    \\A^c\cup{B^c}=(AB)^c
    $$
    

