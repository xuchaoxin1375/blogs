## abstract

在 C++ 中，**引用（Reference）** 是一种通过别名访问变量的机制。它允许你创建一个变量的引用，这个引用就像是该变量的另一个名字。

引用在 C++ 中常用于函数参数传递、返回值以及对象操作，能够提高效率并简化代码。

### 引用的基本特点

1. **必须初始化**：引用在声明时必须被初始化。
2. **不可重新绑定**：引用一旦绑定到某个变量，就无法再更改引用的目标。
3. **与被引用的变量共享相同的内存地址**：引用并不会占用额外的内存空间，它是被引用变量的别名。

### 引用的语法

```cpp
int a = 10;
int &ref = a;  // ref 是 a 的引用
```

- `ref` 是变量 `a` 的引用，通过 `ref` 访问和修改数据等同于直接操作 `a`。

### 引用的类型

C++ 中的引用主要有两种类型：

1. **左值引用（Lvalue Reference）**：

   - 用于绑定到左值（可以被赋值的变量）。
   - 常用于函数参数传递，使函数可以修改调用者的变量。

   ```cpp
   void modify(int &x) {
       x = 20;
   }
   int main() {
       int a = 10;
       modify(a);  // 通过引用修改 a
       std::cout << a;  // 输出 20
   }
   ```

2. **右值引用（Rvalue Reference）**（C++11 引入）：

   - 用于绑定到右值（临时对象、常量、表达式的结果）。
   - 主要用于实现**移动语义**（Move Semantics）和**完美转发**（Perfect Forwarding），提高程序性能。

   ```cpp
   void print(int &&x) {  // x 是右值引用
       std::cout << x << std::endl;
   }
   int main() {
       print(5);  // 5 是右值
   }
   ```

### 引用与指针的比较

引用与指针有些相似，但有重要区别：

| 特性         | 引用（Reference）  | 指针（Pointer）                 |
| ------------ | ------------------ | ------------------------------- |
| **初始化**   | 必须初始化         | 可以延迟初始化                  |
| **重新绑定** | 不能重新绑定       | 可以重新指向其他地址            |
| **空引用**   | 不存在空引用       | 可以有空指针（nullptr）         |
| **访问符号** | 使用变量名直接访问 | 使用 `*` 操作符访问指针指向的值 |
| **内存占用** | 不占用额外内存     | 占用额外内存（存储地址）        |

### 引用的常见用法

1. **函数参数传递**：

   - 使用引用传参可以避免值传递带来的性能开销，同时允许函数修改原始变量。

   ```cpp
   void increment(int &x) {
       x++;
   }
   ```

2. **返回值优化**：

   - 函数返回引用可以避免复制操作，但要注意不要返回局部变量的引用。

   ```cpp
   int& getElement(int arr[], int index) {
       return arr[index];  // 返回数组元素的引用
   }
   ```

3. **常量引用（const 引用）**：

   - 防止修改引用的值，常用于接受 `const` 对象或临时对象。

   ```cpp
   void printValue(const int &x) {  // 不会修改 x
       std::cout << x;
   }
   ```

4. **用于操作对象**：

   - 引用可用于操作复杂数据类型，如类对象，避免不必要的对象拷贝。

   ```cpp
   class Example {
   public:
       void show() const { std::cout << "Example" << std::endl; }
   };
   
   void display(const Example &ex) {
       ex.show();  // 通过引用访问对象
   }
   ```

### 总结

- **引用是变量的别名**，提供了更自然的语法和高效的操作方式。
- **左值引用**用于绑定到左值，**右值引用**用于绑定到右值并支持移动语义。
- 在函数参数和返回值处理中使用引用，可以减少复制开销，提升程序性能。
- 使用引用时要注意初始化和作用域，避免引用悬挂问题。

## 示例

```c++

#include <iostream>
#include "4.5.3.1.h"
using namespace std;
int n = 10;
void Swap_inner(int a, int b)
{
	int tmp;
	// 以下三行将a,b值互换
	tmp = a;
	a = b;
	b = tmp;
	cout << "In Swap: a=" << a << " b=" << b << endl;
}
void Swap_ref(int &a, int &b)
{
	int tmp;
	tmp = a;
	a = b;
	b = tmp;
}
void Swap_ptr(int *a, int *b)
{
	int tmp;
	tmp = *a;
	*a = *b;
	*b = tmp;
}
int &GetValueRef()
{
	return n;
}

int GetValue()
{
	return n;
}

void test_ref_leftvalue()
{
    GetValueRef() = 1;
    int &r = GetValueRef();
    cout << r << endl;
}

void test_param_swap(int &a, int &b)
{
	cout << "Before swaping: a=" << a << " b=" << b << endl;
	// Swap_inner(a, b);
	// Swap_ref(a, b);
	Swap_ptr(&a, &b);
	cout << "After swaping: a=" << a << " b=" << b;
}

int main()
{
	int a = 4, b = 5;
	// test_param_swap(a, b);
	// GetValue()=1;
    test_ref_leftvalue();
    return 0;
}
```

